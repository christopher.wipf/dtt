CDS version of control room tools.

Building and packaging

To build out of tree, create a new directory somewhere.

$ cmake <path-to-source-dir>
$ make

Create a Tar Ball &&

Debian packages

Uses gbp (git build package).  There's a branch per debian release: 'debian/buster' for example.

git checkout debian/buster

to create cowbuilder may need to run:

sudo cowbuilder create --dist buster --basepath /var/cache/pbuilder/base-buster.cow --extrapackages 'wget apt-transport-https ca-certificates' 

sudo cowbuilder update --dist buster --basepath /var/cache/pbuilder/base-buster.cow --override-config --mirror http://ftp.debian.org/debian --othermirror 'deb [trusted=yes] https://apt.ligo-wa.caltech.edu/debian buster-unstable main'

to build run:

gbp buildpackage --git-debian-branch=debian/buster --git-upstream-branch=master --git-pbuilder --git-dist=buster



SL-7 packages

Sl7 package is handled in a separate repo.  Updating the SL-7 package requires only a tar ball from
this project.  Some update to the sl-7 specfile will also be needed.
