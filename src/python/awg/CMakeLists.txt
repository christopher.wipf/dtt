configure_file(awgbase.py.in awgbase.py @ONLY)

if(ENABLE_PYTHON2)
    INSTALL(FILES awg.py ${CMAKE_CURRENT_BINARY_DIR}/awgbase.py
            DESTINATION ${Python2_INSTALL_DIR})
endif(ENABLE_PYTHON2)
if(ENABLE_PYTHON3)
    INSTALL(FILES awg.py ${CMAKE_CURRENT_BINARY_DIR}/awgbase.py
            DESTINATION ${Python3_LIB_INSTALL_DIR})
endif(ENABLE_PYTHON3)