#!/usr/bin/python3
# some automated testing for awg.py

# specific to LLO big test stand with x2asc model running on x2asc0

import awg, time

chan = 'X2:ALS-C_TRY_A_LF_EXC'

def ExcTest(exc, name):
    print("***************")
    print("Starting %s" % name)
    exc.start(ramptime=1)
    time.sleep(5)
    print("Stopping %s" % name)
    exc.stop(ramptime=1)
    print("Finished %s\n" % name)
    time.sleep(2)

ExcTest(awg.Sine(chan, ampl=10, freq=2), "sine")
ExcTest(awg.Square(chan, ampl=10, freq=2), "square")
ExcTest(awg.Triangle(chan, ampl=10, freq=2), "triangle")
ExcTest(awg.Ramp(chan, ampl=10, freq=2),"ramp")
ExcTest(awg.Impulse(chan, ampl=10, freq=2), "impulse")
ExcTest(awg.Constant(chan, offset=10), "constant")
ExcTest(awg.GaussianNoise(chan, ampl=10, freq1=1, freq2=10), "gaussian noise")
ExcTest(awg.UniformNoise(chan, ampl=10, freq1=1, freq2=10), "uniform noise")
ExcTest(awg.SweptSine(chan, ampl1=10, freq1=1, ampl2=10, freq2=10, sweeptime=4), "swept sine")

exc_list = [awg.Sine(chan, freq=2, ampl=50), awg.UniformNoise(chan, ampl=20)]
noisy_sine = awg.Excitation.compose(exc_list)
ExcTest(noisy_sine, 'noisy sine')

filter = [5.311e-15,2,1,-1.9994832175,0.99948338,2,1,-1.99978540,0.99978598]
noisy_sine2 = awg.Excitation.compose(exc_list)
noisy_sine2.set_filter(filter)
ExcTest(noisy_sine2, 'filtered noisy sine')

from random import uniform
random_data = [uniform(0, 20) for n in range(8192)]
stream = awg.ArbitraryStream(chan, rate=2048)
print("**********")
print("Starting arbitrary stream")
stream.append(random_data)
time.sleep(4)
print("Ending arbitrary stream\n")


random_data2 = [uniform(0, 20) for n in range(65536)]
stream2 = awg.ArbitraryStream(chan, rate=2048)
print("**********")
print("Starting long arbitrary stream")
stream2.append(random_data2)
time.sleep(6)
print("Aborting arbitrary stream\n")
stream2.abort()

loop = awg.ArbitraryLoop(chan, random_data, rate=2048)
ExcTest(loop, "loop")

print("\n***********")
print("freezing swept sine")
swept = awg.SweptSine(chan, ampl1=10, freq1=1, ampl2=10, freq2=10, sweeptime=4)
swept.start(ramptime=1)
time.sleep(2)
swept.freeze()
time.sleep(5)
print("stopping")
swept.stop(ramptime=1)

print("Awg cleanup")
awg.awg_cleanup()
print("Cleanup finished\n")

emsg = awg.awgbase.SIStrErrorMsg(1)
print(emsg)
print(str(type(emsg)))