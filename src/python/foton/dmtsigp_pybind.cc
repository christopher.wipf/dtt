//
// Created by erik.vonreis on 3/1/23.
//


#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <sstream>
#include "FilterDesign.hh"
#include "Pipe.hh"
#include "iirutil.hh"

namespace py = pybind11;


template <class T>
void fill_list(py::list &list, std::vector<T> vec) {
  for(auto x : vec) {
    list.append(x);
  }
}

template <class T>
void fill_list(py::list &list, T *a, int n) {
  for(int i=0; i<n; ++i) {
    list.append(a[i]);
  }
}

PYBIND11_MODULE(pydmtsigp, m) {
  py::class_<FilterDesign>(m, "FilterDesign")
      .def(py::init<double, const char*>(),
           py::arg("fsample")=1.0, py::arg("name")="filter")
      .def(py::init<const char *, double, const char *>(),
          py::arg("spec"), py::arg("fsample")=1.0, py::arg("name")="filter")
      .def(py::init<const FilterDesign &>())
      .def("init", &FilterDesign::init)
      .def("isUnityGain", &FilterDesign::isUnityGain)
      .def("setName", &FilterDesign::setName)
      .def("getName", &FilterDesign::getName)
      .def("setFSample", &FilterDesign::setFSample)
      .def("getFSample", &FilterDesign::getFSample)
      .def("getFOut", &FilterDesign::getFOut)
      .def("getHeterodyne", &FilterDesign::getHeterodyne)
      .def("setPrewarp", &FilterDesign::setPrewarp)
      .def("getPrewarp", &FilterDesign::getPrewarp)
      .def("getFilterSpec", &FilterDesign::getFilterSpec)
      .def("get", static_cast<const Pipe&(FilterDesign::*)() const>(&FilterDesign::get),
           py::return_value_policy::reference_internal)
      .def("__call__", static_cast<const Pipe& (FilterDesign::*)() const>(&FilterDesign::operator()),
           py::return_value_policy::reference_internal,  py::is_operator())
      .def("copy", &FilterDesign::copy, py::return_value_policy::take_ownership)
      .def("release", &FilterDesign::release, py::return_value_policy::take_ownership)
      .def("reset", &FilterDesign::reset)
      .def("set", [](FilterDesign &fd, Pipe &p, double resampling, bool heterodyne){

            // clone the pipe because as of 2013
            // FilterDesign::set deletes the old pipe before cloning the new
            // but the new pipe could be the old pipe.
            const Pipe *p2 = p.clone();
            fd.set(*p2, resampling, heterodyne);
          },
           py::arg("filter"),
           py::arg("resampling")=1.0,
           py::arg("heterodyne")=false)
      .def("add", &FilterDesign::add,
           py::arg("filter"),
           py::arg("gain")=1.0,
           py::arg("heterodyne")=false)
      .def("filter", &FilterDesign::filter)
      .def("gain", &FilterDesign::gain, py::arg("g"), py::arg("format") = "scalar")
      .def("pole", static_cast<bool (FilterDesign::*)(double, double, const char *)>(&FilterDesign::pole),
           py::arg("f"), py::arg("gain"), py::arg("plane")="s")
      .def("pole", static_cast<bool (FilterDesign::*)(double, const char *)>(&FilterDesign::pole),
           py::arg("f"), py::arg("plane") = "s")
      .def("zero", static_cast<bool (FilterDesign::*)(double, double, const char *)>(&FilterDesign::zero),
           py::arg("f"), py::arg("gain"), py::arg("plane")="s")
      .def("zero", static_cast<bool (FilterDesign::*)(double, const char *)>(&FilterDesign::zero),
           py::arg("f"), py::arg("plane")="s")
      .def("pole2", static_cast<bool (FilterDesign::*)(double, double, double, const char *)>(&FilterDesign::pole2),
           py::arg("f"), py::arg("Q"), py::arg("gain"), py::arg("plane")="s")
      .def("pole2", static_cast<bool (FilterDesign::*)(double, double, const char *)>(&FilterDesign::pole2),
           py::arg("f"), py::arg("Q"), py::arg("plane")="s")
      .def("zero2", static_cast<bool (FilterDesign::*)(double, double, double, const char *)>(&FilterDesign::zero2),
           py::arg("f"), py::arg("Q"), py::arg("gain"), py::arg("plane")="s")
      .def("zero2", static_cast<bool (FilterDesign::*)(double, double, const char *)>(&FilterDesign::zero2),
           py::arg("f"), py::arg("Q"), py::arg("plane")="s")

      .def("zpk", [](FilterDesign &f,  std::vector<dComplex> &zeros, std::vector<dComplex> poles, double gain, const char * plane){
            return f.zpk(zeros.size(), zeros.data(), poles.size(), poles.data(), gain, plane);
          },
           py::arg("zeros"),  py::arg("poles"), py::arg("gain"),
           py::arg("plane") = "s")

      .def("rpoly", [](FilterDesign &f, std::vector<double> num, std::vector<double> den, double gain)
           { return f.rpoly(num.size(), num.data(), den.size(), den.data(), gain); })

      .def("biquad", &FilterDesign::biquad)

      .def("sos", [](FilterDesign &f, std::vector<double> ba, const char *plane){return f.sos(ba.size(), ba.data(), plane);},
          py::arg("sos"), py::arg("plane") = "s")

      .def("zroots", [](FilterDesign &f, std::vector<dComplex> zeros, std::vector<dComplex> poles, double gain){
                  return f.zroots(zeros.size(), zeros.data(), poles.size(), poles.data(), gain);} )

      .def("direct", [](FilterDesign &f, std::vector<double> b, std::vector<double> a)
           { return f.direct(b.size()-1, b.data(), a.size(), a.data()); })

      .def("ellip", &FilterDesign::ellip)
      .def("cheby1", &FilterDesign::cheby1)
      .def("cheby2", &FilterDesign::cheby2)
      .def("butter", &FilterDesign::butter)

      .def("notch", &FilterDesign::notch,
           py::arg("f0"), py::arg("Q"), py::arg("depth")=0.0)

      .def("resgain", &FilterDesign::resgain,
           py::arg("f0"), py::arg("Q"), py::arg("height")=30.0)

      .def("comb", &FilterDesign::comb,
           py::arg("f0"), py::arg("Q"), py::arg("amp") = 0.0, py::arg("N") = 0)

      .def("setFirType", &FilterDesign::setFirType)

      .def("remez", [](FilterDesign &f, int N,
                       std::vector<double> Bands, std::vector<double> Func,
                       std::vector<double> Weight) {
        int numbands = Func.size();

        if (numbands * 2 != Bands.size()) {
          throw std::length_error("argument 'Func' must be exactly half the length of 'Bands'");
        }

        double *weight = NULL;
        if (Weight.size() > 0) {
          if (Weight.size() != numbands) {
            throw std::length_error("argument 'Weight' must be the same length as 'Func', or else empty.");
          }

          weight = Weight.data();
        }

        return f.remez(N, numbands, Bands.data(), Func.data(), weight);
      })

//      .def("firls", [](FilterDesign &f, int N,
//                       std::vector<double> Bands, std::vector<double> Pass,
//                       std::vector<double> Weight) {
//
//        if (Bands.size() % 2 != 0) {
//          throw std::length_error("argument 'Bands' must have an even length");
//        }
//        int numbands = Bands.size()/2;
//
//        if (numbands *2 != Pass.size()) {
//          throw std::length_error(
//              "argument 'Pass' must be the same length of 'Bands'");
//        }
//
//
//
//        if (Weight.size() != numbands) {
//          throw std::length_error(
//              "argument 'Weight' must be exactly half the length of 'Bands', or else empty.");
//        }
//
//        return f.firls(N, numbands, Bands.data(), Pass.data(), Weight.data());
//       })


      .def("firw", &FilterDesign::firw,
           py::arg("N"), py::arg("type"), py::arg("window"),
           py::arg("Flow"), py::arg("Fhigh") = 0, py::arg("Ripple")=0,
           py::arg("dF")=0)

      .def("fircoefs", [](FilterDesign &f, std::vector<double> coeffs, bool zero_phase) {
        int N = coeffs.size();
        return f.fircoefs(N, coeffs.data(), zero_phase);
      })


      .def("difference", &FilterDesign::difference)

      .def("decimateBy2", &FilterDesign::decimateBy2,
           py::arg("N"), py::arg("FilterID")=1)

      .def("linefilter", &FilterDesign::linefilter,
           py::arg("f"), py::arg("T")=1, py::arg("fid")=1, py::arg("nT")=1)
      .def("limiter", &FilterDesign::limiter,
           py::arg("type"), py::arg("l1"),  py::arg("l1")=0,  py::arg("l3")=0)
      .def("multirate", &FilterDesign::multirate,
           py::arg("type"), py::arg("m1"), py::arg("m2")=1e-3, py::arg("atten")=80)
      .def("mixer", &FilterDesign::mixer, py::arg("fmix"), py::arg("phase")=0)
      .def("frontend", &FilterDesign::frontend)
      .def("setgain", &FilterDesign::setgain)
      .def("closeloop", &FilterDesign::closeloop, py::arg("k")=1)
      .def("Xfer", [](FilterDesign &fd, double f){
        fComplex tf;
        if(!fd.Xfer(tf, f)) {
          throw std::domain_error("FilterDesign::Xfer returned false");
        }
        return tf;
      })
      //.def("Xfer", static_cast< bool (FilterDesign::*)(FSeries&, double, double, double) const>(&FilterDesign::Xfer))
      .def("Xfer",  [](FilterDesign &fd, std::vector<float> f) {
        std::vector<fComplex> tf(f.size());
        if(!fd.Xfer(tf.data(), f.data(), f.size())) {
          throw std::domain_error("FilterDesign::Xfer returned false");
        }
        return tf;
      })

      .def("Xfer", [](FilterDesign &fd, double f1, double f2, int numpoints){

          std::vector<float> freqs(numpoints);
          std::vector<fComplex> tf(numpoints);
          if(!fd.Xfer(freqs.data(), tf.data(), f1, f2, numpoints)) {
            throw std::domain_error("FilterDesign::Xfer returned false");
          }

          py::list pyfreqs;
          fill_list(pyfreqs, freqs);
          py::list pytf;
          fill_list(pytf, tf);

          return py::make_tuple(pyfreqs, pytf);

      }, py::arg("f1"), py::arg("f2"), py::arg("numpoint")=101)

      //.def("Xfer", static_cast< bool (FilterDesign::*)(float*, fComplex*, const SweptSine&) const>(&FilterDesign::Xfer))
      .def("response",[](FilterDesign &fd,  const char *input_type, double duration_sec){
        std::vector<std::string> valid_types = {"step", "impulse", "ramp"};
        bool found = false;
        for (auto t: valid_types) {
          if(t == input_type) {
            found = true;
            break;
          }
        }
        if(!found) {
          std::string valid_out = "";
          bool first = true;
          for (auto t: valid_types) {
            if(first) {
              first = false;
            }
            else {
              valid_out += ", ";
            }
            valid_out += t;
          }
          throw std::domain_error(std::string("input type was not valid.  Must be one of ") + valid_out);
        }

        TSeries output_ts;
        int success = fd.response(output_ts, input_type, Interval(duration_sec));
        std::vector<double> output(output_ts.getNSample());
        output_ts.getData(output.size(), output.data());
        py::list pyoutput;
        fill_list(pyoutput, output);
        return pyoutput;
      }

      )
      //.def("response", static_cast<bool (FilterDesign::*)(TSeries&, const char*, const Interval&) const>(&FilterDesign::response))
      .def("response", [](FilterDesign &fd, std::vector<double> input){
        std::vector<double> output(input.size());
        Interval dt (1.0/fd.getFSample());
        TSeries input_ts(Time(), dt, input.size(), input.data());
        TSeries output_ts;
        bool success = fd.response(output_ts, input_ts);
        if(!success) {
          throw std::runtime_error("response calculation failed. See stderr output.");
        }
        output_ts.getData(output.size(), output.data());
        py::list pyoutput;
        fill_list(pyoutput, output);
        return pyoutput;
      });
//      .def("bode", static_cast< bool (FilterDesign::*)(double, double, int, const char *) const>(&FilterDesign::bode))
//      .def("bode", static_cast< bool (FilterDesign::*)(const float*, int) const>(&FilterDesign::bode))
//      .def("bode", static_cast< bool (FilterDesign::*)(const SweptSine&) const>(&FilterDesign::bode))


  /// Pipe return by FilterDesign::get and other functions
  /// not used directly, so we can treat it as a black box.
  py::class_<Pipe>(m,"Pipe");


  //py::class_<FSeries>(m, "FSeries");


  /// iirutil.hh functions
  m.def("bilinear",&bilinear,
        py::arg("fs"), py::arg("root"), py::arg("prewarp")=true );

  m.def("inverse_bilinear",&inverse_bilinear,
        py::arg("fs"), py::arg("root"), py::arg("unwarp")=true );

  m.def("sort_roots",[](std::vector<dComplex> roots, bool splane){
    bool success = sort_roots(roots.data(), roots.size(), splane);
    return success;
  }, py::arg("roots"), py::arg("splane") = true);

  m.def("s2z", [](double fs, std::vector<dComplex> zeros,
                  std::vector<dComplex> poles, double gain,
                  const char *plane, bool prewarp) {

        bool success = s2z(fs, zeros.size(), zeros.data(),
                           poles.size(), poles.data(), gain, plane, prewarp);

        py::list pyzeros, pypoles;
        fill_list(pyzeros, zeros);
        fill_list(pypoles, poles);

        return py::make_tuple(success, pyzeros, pypoles, gain);
  }, py::arg("fs"), py::arg("zeros"), py::arg("poles"),
      py::arg("gain"), py::arg("plane") = "s", py::arg("prewarp") = true);


  m.def("z2s",[](double fs, std::vector<dComplex> zeros,
                          std::vector<dComplex> poles, double gain,
                          const char *plane, bool unwarp) {

        bool success = z2s(fs, zeros.size(), zeros.data(),
                           poles.size(), poles.data(), gain, plane, unwarp);

        py::list pyzeros, pypoles;
        fill_list(pyzeros, zeros);
        fill_list(pypoles, poles);

        return py::make_tuple(success, pyzeros, pypoles, gain);
      }, py::arg("fs"), py::arg("zeros"), py::arg("poles"),
      py::arg("gain"), py::arg("plane") = "s", py::arg("unwarp") = true);

  m.def("z2z",[](std::vector<double> ba, const char *format) {

    int max_zeros_poles = ba.size() / 2;
    std::vector<dComplex> zeros(max_zeros_poles);
    std::vector<dComplex> poles(max_zeros_poles);
    int npoles, nzeros;
    double gain;

    bool success = z2z(ba.size(), ba.data(), nzeros, zeros.data(),
                       npoles, poles.data(), gain, format);

    py::list pyzeros;
    fill_list(pyzeros, zeros.data(), nzeros);
    py::list pypoles;
    fill_list(pypoles, poles.data(), npoles);
    return py::make_tuple(success, pyzeros, pypoles, gain);
  }, py::arg("ba"), py::arg("format")="s");

  m.def("z2z", [](std::vector<dComplex> zeros, std::vector<dComplex> poles,
                  double gain, const char *format) {

    int nba;
    int nsos = std::max(zeros.size(), poles.size()) * 2 + 4;
    std::vector<double> ba(nsos);

    bool success = z2z(zeros.size(), zeros.data(), poles.size(), poles.data(),
                       gain, nba, ba.data(), format);

    py::list pyba;
    fill_list(pyba, ba.data(), nba);
    return py::make_tuple(success, pyba);
  }, py::arg("zeros"), py::arg("poles"), py::arg("gain"), py::arg("format")="s");


  m.def("isiir", &isiir);
  m.def("iirsoscount", &iirsoscount);
  m.def("iirpolecount", &iirpolecount);
  m.def("iirzerocount", &iirzerocount);
  m.def("iirpolezerocount",[](Pipe &p){
    int npoles, nzeros;

    if(!iirpolezerocount(p, npoles, nzeros)) {
        throw std::domain_error("filter is not an IIR filter");
    }

    return py::make_tuple(npoles, nzeros);
  });

  m.def("iirorder",&iirorder);
  m.def("iircmp",&iircmp);
  //m.def("iir2iir",&iir2iir);

  m.def("iir2zpk",[](Pipe &p, const char *plane, bool unwarp){

    int nzeros, npoles;
    if(!iirpolezerocount(p, npoles, nzeros)) {
      throw std::domain_error("filter is not an IIR filter");
    }
    auto zeros = std::vector<dComplex>(nzeros);
    auto poles = std::vector<dComplex>(npoles);
    double gain=1;

    bool success = iir2zpk(p, nzeros, zeros.data(),
                           npoles, poles.data(), gain, plane, unwarp);

    py::list pyzeros;
    fill_list(pyzeros, zeros);
    py::list pypoles;
    fill_list(pypoles, poles);

    return py::make_tuple(success, pyzeros, pypoles, gain);
  }, py::arg("filter"), py::arg("plane") = "s", py::arg("unwarp") = true);

  m.def("iir2zpk_design",[](Pipe &p, const char *plane, bool unwarp){
        std::string design;
        bool success = iir2zpk(p, design, plane, unwarp);

        return py::make_tuple(success, design);
      }, py::arg("filter"), py::arg("zpk") = "s", py::arg("unwarp") = true);

  m.def("iir2poly", [](Pipe &p, bool unwarp) {

    int numsos = iirsoscount(p);
    int nnumer = numsos*2+1;
    int ndenom = numsos*2+1;
    auto numer = std::vector<double>(nnumer);
    auto denom = std::vector<double>(ndenom);
    double gain = 1;

    bool success = iir2poly(p, nnumer, numer.data(), ndenom,
                            denom.data(), gain, unwarp);

    py::list pynumer;
    fill_list(pynumer, numer.data(), nnumer);
    py::list pydenom;
    fill_list(pydenom, denom.data(), ndenom);

    return py::make_tuple(success, pynumer, pydenom, gain);
  }, py::arg("filter"), py::arg("unwarp")=true);


  m.def("iir2z_sos",[](Pipe &p, const char *format){
    int num_sos = iirsoscount(p);
    if(num_sos < 0) {
      throw std::domain_error("filter is not an IIR filter");
    }
    int nba = num_sos * 4 + 1;
    auto ba = std::vector<double>(nba);
    bool success = iir2z(p, nba, ba.data(), format);
    py::list pyba;
    fill_list(pyba, ba);

    return py::make_tuple(success, pyba);
  }, py::arg("filter"), py::arg("format") = "s");


  m.def("iir2z",[](Pipe &p){
        int nzeros, npoles;
        if(!iirpolezerocount(p, npoles, nzeros)) {
          throw std::domain_error("filter is not an IIR filter");
        }
        auto zeros = std::vector<dComplex>(nzeros);
        auto poles = std::vector<dComplex>(npoles);
        double gain=1;
        bool success = iir2z(p, nzeros, zeros.data(), npoles, poles.data(), gain);

        py::list pyzeros;
        fill_list(pyzeros, zeros);
        py::list pypoles;
        fill_list(pypoles, poles);
        return py::make_tuple(success, pyzeros, pypoles, gain);
      });

  m.def("iir2z_design", [](Pipe &p, const char *format){
        std::string design;
        bool success = iir2z(p, design, format);

        return py::make_tuple(success, design);
      }, py::arg("filter"), py::arg("format") = "r");

  m.def("iir2direct",[](Pipe &p) {

    int nb = iirzerocount(p);
    int na = iirpolecount(p);

    auto b = std::vector<double>(nb+1);
    auto a = std::vector<double>(na);
    bool success = iir2direct(p, nb, b.data(), na, a.data());

    py::list pyb;
    fill_list(pyb, b.data(), nb+1);
    py::list pya;
    fill_list(pya, a.data(), na);

    return py::make_tuple(success, pyb, pya);
  });


  py::class_<dComplex>(m, "dComplex")
      .def(py::init())
      .def(py::init<double, double>())
      .def(py::init<dComplex&>())
      .def("__iadd__", static_cast< dComplex& (dComplex::*)(const dComplex&)>(&dComplex::operator+=), py::is_operator())
      .def("__isub__", static_cast< dComplex& (dComplex::*)(const dComplex&)>(&dComplex::operator-=), py::is_operator())
      .def("__imul__", static_cast< dComplex& (dComplex::*)(const dComplex&)>(&dComplex::operator*=), py::is_operator())
      .def("__imul__", static_cast< dComplex& (dComplex::*)(double)>(&dComplex::operator*=), py::is_operator())
      .def("__itruediv__", static_cast< dComplex& (dComplex::*)(const dComplex&)>(&dComplex::operator/=), py::is_operator())
      .def("__itruediv__", static_cast< dComplex& (dComplex::*)(double)>(&dComplex::operator/=), py::is_operator())
      .def("__eq__", static_cast< bool (dComplex::*)(const dComplex &) const>(&dComplex::operator==), py::is_operator())
      .def("__ne__", static_cast< bool (dComplex::*)(const dComplex &) const>(&dComplex::operator!=), py::is_operator())
      .def("__invert__", static_cast< dComplex (dComplex::*)() const>(&dComplex::operator~), py::is_operator())
      .def("__neg__", static_cast< dComplex (dComplex::*)() const>(&dComplex::operator-), py::is_operator())
      .def("__abs__", [](dComplex &c){return c.Mag();})
      .def("setMArg", &dComplex::setMArg)
      .def("xcc", &dComplex::xcc)
      .def("MagSq", &dComplex::MagSq)
      .def("Mag", &dComplex::Mag)
      .def("Arg", &dComplex::Arg)
      .def("Real", &dComplex::Real)
      .def("Imag", &dComplex::Imag)
      .def("__add__", [](dComplex& a, dComplex& b){ return a + b;}, py::is_operator())
      .def("__sub__", [](dComplex& a, dComplex& b){ return a - b;}, py::is_operator())
      .def("__mul__", [](dComplex& a, dComplex& b){ return a * b;}, py::is_operator())
      .def("__mul__", [](dComplex& a, double b){ return a * b;}, py::is_operator())
      .def("__rmul__", [](dComplex& b, double a){ return a * b;}, py::is_operator())
      .def("__truediv__", [](dComplex& a, dComplex& b){ return a / b;}, py::is_operator())
      .def("__truediv__", [](dComplex& a, double b){ return a / b;}, py::is_operator())
      .def("__repr__", [](dComplex &c){
        std::stringstream ss;
        ss << "dComplex(" << c.Real() << ", " << c.Imag() << ")";
        py::str pys(ss.str());
        return pys;
      });


  py::class_<fComplex>(m, "fComplex")
      .def(py::init())
      .def(py::init<float, float>())
      .def(py::init<fComplex&>())
      .def("__iadd__", static_cast< fComplex& (fComplex::*)(const fComplex&)>(&fComplex::operator+=), py::is_operator())
      .def("__isub__", static_cast< fComplex& (fComplex::*)(const fComplex&)>(&fComplex::operator-=), py::is_operator())
      .def("__imul__", static_cast< fComplex& (fComplex::*)(const fComplex&)>(&fComplex::operator*=), py::is_operator())
      .def("__imul__", static_cast< fComplex& (fComplex::*)(double)>(&fComplex::operator*=), py::is_operator())
      .def("__itruediv__", static_cast< fComplex& (fComplex::*)(const fComplex&)>(&fComplex::operator/=), py::is_operator())
      .def("__itruediv__", static_cast< fComplex& (fComplex::*)(double)>(&fComplex::operator/=), py::is_operator())
      .def("__eq__", static_cast< bool (fComplex::*)(const fComplex &) const>(&fComplex::operator==), py::is_operator())
      .def("__ne__", static_cast< bool (fComplex::*)(const fComplex &) const>(&fComplex::operator!=), py::is_operator())
      .def("__invert__", static_cast< fComplex (fComplex::*)() const>(&fComplex::operator~), py::is_operator())
      .def("__neg__", static_cast< fComplex (fComplex::*)() const>(&fComplex::operator-), py::is_operator())
      .def("__abs__", [](fComplex &c){return c.Mag();})
      .def("setMArg", &fComplex::setMArg)
      .def("xcc", &fComplex::xcc)
      .def("MagSq", &fComplex::MagSq)
      .def("Mag", &fComplex::Mag)
      .def("Arg", &fComplex::Arg)
      .def("Real", &fComplex::Real)
      .def("Imag", &fComplex::Imag)
      .def("__add__", [](fComplex& a, fComplex& b){ return a + b;}, py::is_operator())
      .def("__sub__", [](fComplex& a, fComplex& b){ return a - b;}, py::is_operator())
      .def("__mul__", [](fComplex& a, fComplex& b){ return a * b;}, py::is_operator())
      .def("__mul__", [](fComplex& a, double b){ return a * b;}, py::is_operator())
      .def("__rmul__", [](fComplex& b, double a){ return a * b;}, py::is_operator())
      .def("__truediv__", [](fComplex& a, fComplex& b){ return a / b;}, py::is_operator())
      .def("__truediv__", [](fComplex& a, double b){ return a / b;}, py::is_operator())
      .def("__repr__", [](fComplex &c){
        std::stringstream ss;
        ss << "fComplex(" << c.Real() << ", " << c.Imag() << ")";
        py::str pys(ss.str());
        return pys;
      });

  py::enum_<Filter_Type>(m, "Filter_Type")
      .value("kLowPass", kLowPass)
      .value("kHighPass", kHighPass)
      .value("kBandPass", kBandPass)
      .value("kBandStop", kBandStop);
}