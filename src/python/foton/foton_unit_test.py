from pathlib import Path
import foton

import pydmtsigp as sigp
import pyfilterfile as filterfile
import unittest
import numpy as np
import io
import pydmtsigp as sigp

input_file_name = 'test_filter.txt'
output_file_name = 'test_out.txt'


class TestFotonMethods(unittest.TestCase):
    def setUp(self):
        self.file = foton.FilterFile(input_file_name)
        self.module = self.file['BOUNCE_ALL']
        self.section = self.module[0]
        self.design = foton.FilterDesign(self.section.get_filterdesign())
        self.filter = foton.Filter(self.design)

    # Module

    def test_module_instance(self):
        self.assertIsInstance(self.module, foton.Module)

    def test_module_fm_instance(self):
        self.assertIsInstance(self.module.fm, filterfile.FilterModule)

    def test_module_name(self):
        self.assertEqual(self.module.name, 'BOUNCE_ALL')

    def test_module_rate(self):
        self.assertEqual(self.module.rate, 16384.0)

    def test_module_length(self):
        self.assertEqual(len(self.module), 10)

    def test_module_list_instance(self):
        for v in self.module:
            self.assertIsInstance(v, foton.Section)

    # Section

    def test_section_instance(self):
        self.assertIsInstance(self.section, foton.Section)

    def test_section_sec_instance(self):
        self.assertIsInstance(self.section.sec, filterfile.FilterSection)

    def test_section_index(self):
        self.assertEqual(self.section.index, 0)

    def test_section_name(self):
        self.assertEqual(self.section.name, 'bp9-11')

    def test_section_design(self):
        self.assertEqual(self.section.design, 'cheby1("BandPass",4,1,9,11)')

    def test_section_filt(self):
        self.assertIsInstance(self.section.filt, foton.FilterDesign)

    def test_section_order(self):
        self.assertEqual(self.section.order, 8)

    def test_section_input_switch(self):
        self.assertEqual(self.section.input_switch, 'ZeroHistory')

    def test_section_output_switch(self):
        self.assertEqual(self.section.output_switch, 'Immediately')

    def test_section_ramp(self):
        self.assertEqual(self.section.ramp, 0.0)

    def test_section_tolerance(self):
        self.assertEqual(self.section.tolerance, 0.0)

    def test_section_timeout(self):
        self.assertEqual(self.section.timeout, 0.0)

    def test_section_header(self):
        lhs = self.section.header
        rhs = '\n# FILTERS FOR ONLINE ## Computer generated file: DO NOT EDIT###################################################################################### BOUNCE_ALL                                                               ######################################################################################                                                                          ###'
        self.assertEqual(lhs, rhs)

    def test_section_empty(self):
        self.assertFalse(self.section.is_empty)

    def test_section_check(self):
        self.assertTrue(self.section.check())

    def test_section_valid(self):
        self.assertTrue(self.section.valid())

    def test_section_refresh(self):
        self.assertTrue(self.section.refresh())

    def test_section_clear(self):
        self.section.clear()
        temp_file = "/tmp/test_filter.txt"
        self.file.write_file(temp_file)
        fftmp = foton.FilterFile(temp_file)
        sec2 = fftmp["BOUNCE_ALL"][0]
        self.assertTrue(sec2.is_empty)
        self.assertTrue(sec2.is_unity)

    # Design

    def test_design_instance(self):
        self.assertIsInstance(self.design, foton.FilterDesign)

    def test_design_string(self):
        self.assertEqual(self.design.string, 'cheby1("BandPass",4,1,9,11)')

    def test_design_design(self):
        self.assertEqual(self.design.design, 'cheby1("BandPass",4,1,9,11)')

    def test_design_rate(self):
        self.assertEqual(self.design.rate, 16384.0)

    def test_design_iir2zpk(self):
        lhs = foton.iir2zpk(self.design)
        rhs = 'zpk('
        self.assertEqual(lhs[:len(rhs)], rhs)
        f2 = foton.Filter(foton.FilterDesign(lhs, self.filter.design.rate))
        self.assertIsNone(np.testing.assert_allclose(f2.coef, self.filter.coef))


    def test_design_iir2z(self):
        lhs = foton.iir2z(self.design)
        rhs = [5.3113353394938464e-15, -2.0, 1.0, -1.9998915575865894, 0.999903508991431, -2.0, 1.0,
               -1.9997388162416827, 0.9997522294284502, 2.0, 1.0, -1.9997152854972924, 0.9997310857818233,
               2.0, 1.0, -1.9998647215491248, 0.9998824570556457]
        self.assertIsNone(np.testing.assert_allclose(lhs, rhs))

    def test_design_iir2poly(self):
        lhs = foton.iir2poly(self.design)
        rhs = ([1.0, -0.0, 0.0, -0.0, 0.0],
               [1.0, 11.97341032487548, 15863.048550492917, 141862.9758392324,
                93453381.49923556, 554452064.8544362, 242312899647.63275,
                714829857259.9366, 233334932716774.6], 6125.820627807043)
        for l, r in zip(lhs,rhs):
            self.assertIsNone(np.testing.assert_allclose(l, r))

    def test_design_iir2direct(self):
        lhs = foton.iir2direct(self.design)
        rhs = (
            [5.3113353394938464e-15, -2.1245341357975386e-14, 3.186801203696308e-14, -2.1245341357975386e-14,
             5.3113353394938464e-15],
            [7.99921038087469, -27.994531788585004, 55.98377269830372, -69.97324999465526, 55.97354543666938,
             -27.98430449470714, 7.994827231895801, -0.9992694697961847])
        for l, r in zip(lhs,rhs):
            self.assertIsNone(np.testing.assert_allclose(l, r))

    # Filter

    def test_filter_instance(self):
        self.assertIsInstance(self.filter, foton.Filter)

    def test_filter_apply(self):
        lhs = self.filter.apply([1, 2, 3, 4, 5, 6, 7, 8, 9, 8, 7, 6, 5, 4, 3, 2, 1])
        rhs = np.array([5.3113353394938464e-15, 5.3109159462973460e-14,
                        2.7083165835495728e-13, 9.5577660914395666e-13,
                        2.6599410852062305e-12, 6.2747841232037422e-12,
                        1.3115907390871857e-11, 2.5007648899668323e-11,
                        4.4367584811872717e-11, 7.4280311726705639e-11,
                        1.1852864488238310e-10, 1.8156101617272869e-10,
                        2.6841636582665616e-10, 3.8463843122555474e-10,
                        5.3619006831396447e-10, 7.2936761061890929e-10,
                        9.7071527088958473e-10])
        self.assertIsNone(np.testing.assert_allclose(lhs, rhs))

    def test_report_load_errors(self):
        """Test whether load errors are reported on save"""
        sec = self.module[4]
        design = 'butter("lowpass",4,0.0017)'
        fd = foton.FilterDesign(design, self.module.rate)
        sec.set_filterdesign(fd)
        tmpfile = "/tmp/butter.txt"

        # turn on correction.  Should generate coefficient warning
        # but not design mismatch error
        self.file.correct_on_load = True
        self.file.write_file(tmpfile)

        warnings = list(filter(lambda m: m.get_level() == filterfile.message_level_t.WARNING, self.file.ff.getFileMessages()))
        warnings = list(map(lambda m: m.get_fmt_str(), warnings))
        errors = list(filter(lambda m: m.get_level() == filterfile.message_level_t.ERROR, self.file.ff.getFileMessages()))
        errors = list(map(lambda m: m.get_fmt_str(), errors))
        print(f"warnings={warnings}")
        print(f"errors={errors}")
        self.assertNotEqual(len(warnings), 0)
        # should NOT be an error for mis-matched design and coeffs.
        # since the read check on load cannot change coefficients,
        # correct_on_load is forced to false during the read check
        # this check verifies the behavior
        self.assertEqual(len(errors), 0)
        
    def test_butterworth_correct(self):
        # for i in range(1, 200):
            #f = i * 0.0005
        f=0.00175
        design = f'butter("LowPass",4,{f:g})'
        fd = foton.FilterDesign(design, self.section.rate)
        self.module[4].set_filterdesign(fd)
        print(f"starting messages={[m.get_fmt_str() for m in self.file.ff.getFileMessages()]}")
        butter_txt = self.file.ff.write_string()

        warnings = list(filter(lambda m: m.get_level() == filterfile.message_level_t.WARNING, self.file.ff.getFileMessages()))
        warnings = list(map(lambda m: m.get_fmt_str(), warnings))
        errors = list(filter(lambda m: m.get_level() == filterfile.message_level_t.ERROR, self.file.ff.getFileMessages()))
        errors = list(map(lambda m: m.get_fmt_str(), errors))
        print(f"warnings={warnings}")
        print(f"errors={errors}")
        self.assertTrue(warnings)
        self.assertFalse(errors)

        ff = foton.FilterFile()
        print(f"correct_on_load={ff.ff.correct_on_load}")
        ff.ff.read_string(butter_txt)

        warnings = list(filter(lambda m: m.get_level() == filterfile.message_level_t.WARNING, ff.ff.getFileMessages()))
        warnings = list(map(lambda m: m.get_fmt_str(), warnings))
        errors = list(filter(lambda m: m.get_level() == filterfile.message_level_t.ERROR, ff.ff.getFileMessages()))
        errors = list(map(lambda m: m.get_fmt_str(), errors))
        print(f"read warnings={warnings}")
        print(f"read errors={errors}")
        self.assertTrue(warnings)
        self.assertFalse(errors)

        sec2 = ff['BOUNCE_ALL'][4]

        # check that still no error
        butter_txt2 = ff.ff.write_string()
        ff2 = foton.FilterFile()
        ff2.ff.read_string(butter_txt2)
        errors = [m.get_fmt_str() for m in ff2.ff.getFileMessages() if m.get_level() == filterfile.message_level_t.ERROR]
        print(f"read2 errors = {errors}")
        self.assertEqual(len(errors), 0)

        # fd3 = sigp.FilterDesign(design, self.section.rate)
        # fd4 = sigp.FilterDesign(sec2.get_filterdesign().design, self.section.rate)
        # _, sos3 = sigp.iir2z_sos(fd3.get())
        # _, sos4 = sigp.iir2z_sos(fd4.get())
        # print(f"SOS3: {sos3}")
        # print(f"SOS4: {sos4}")

        # should have same design string
        self.assertEqual(sec2.get_filterdesign().design, design)

    def test_butterworth_nocorrect(self):
        # for i in range(1, 200):
        #f = i * 0.0005
        f=0.00175
        design = f'butter("LowPass",4,{f:g})'
        fd = foton.FilterDesign(design, self.section.rate)
        self.module[4].set_filterdesign(fd)
        print(f"starting messages={[m.get_fmt_str() for m in self.file.ff.getFileMessages()]}")
        butter_txt = self.file.ff.write_string()

        warnings = list(filter(lambda m: m.get_level() == filterfile.message_level_t.WARNING, self.file.ff.getFileMessages()))
        warnings = list(map(lambda m: m.get_fmt_str(), warnings))
        errors = list(filter(lambda m: m.get_level() == filterfile.message_level_t.ERROR, self.file.ff.getFileMessages()))
        errors = list(map(lambda m: m.get_fmt_str(), errors))
        print(f"warnings c={warnings}")
        print(f"errors c={errors}")
        self.assertTrue(warnings)
        self.assertFalse(errors)

        ff = foton.FilterFile()
        ff.ff.correct_on_load = True
        print(f"correct_on_load c={ff.ff.correct_on_load}")
        ff.ff.read_string(butter_txt)

        warnings = list(filter(lambda m: m.get_level() == filterfile.message_level_t.WARNING, ff.ff.getFileMessages()))
        warnings = list(map(lambda m: m.get_fmt_str(), warnings))
        errors = list(filter(lambda m: m.get_level() == filterfile.message_level_t.ERROR, ff.ff.getFileMessages()))
        errors = list(map(lambda m: m.get_fmt_str(), errors))
        print(f"read warnings c={warnings}")
        print(f"read errors c={errors}")
        self.assertTrue(warnings)
        self.assertTrue(errors)

        sec2 = ff['BOUNCE_ALL'][4]

        # check that error was corrected
        butter_txt2 = ff.ff.write_string()
        ff2 = foton.FilterFile()
        ff2.ff.correct_on_load = True
        ff2.ff.read_string(butter_txt2)
        errors = [m.get_fmt_str() for m in ff2.ff.getFileMessages() if m.get_level() == filterfile.message_level_t.ERROR]
        print(f"read2 errors c= {errors}")
        self.assertEqual(len(errors), 0)

        # fd3 = sigp.FilterDesign(design, self.section.rate)
        # fd4 = sigp.FilterDesign(sec2.get_filterdesign().design, self.section.rate)
        # _, sos3 = sigp.iir2z_sos(fd3.get())
        # _, sos4 = sigp.iir2z_sos(fd4.get())
        # print(f"SOS3: {sos3}")
        # print(f"SOS4: {sos4}")

        # should have "corrected" design string
        self.assertNotEqual(sec2.get_filterdesign().design, design)

    # File

    def test_file_write(self):
        self.file.filename = output_file_name
        self.file.write()
        ff2 = foton.FilterFile(output_file_name)
        mod2 = ff2["BOUNCE_ALL"]
        lhs = foton.iir2direct(self.design)
        rhs = foton.iir2direct(mod2[0].get_filterdesign())
        for l, r in zip(lhs,rhs):
            self.assertIsNone(np.testing.assert_allclose(l, r))

    def test_file_no_create(self):
        path = Path("should_exist.txt")
        path.touch()
        ff = foton.FilterFile(path, create_if_not_exist=False)
        bad_path = Path("should_not_exist.txt")
        self.assertFalse(bad_path.exists())
        self.assertRaises(OSError, ff.write, str(bad_path))
        self.assertRaises(OSError, ff.write_file, str(bad_path))
        ff.write(str(path))
        ff.write_file(str(path))
        self.assertFalse(bad_path.exists())
        self.assertRaises(OSError, foton.FilterFile, str(bad_path), create_if_not_exist=False)

    def test_read_only(self):
        path = Path("should_exist.txt")
        path.touch()
        ff = foton.FilterFile(path, read_only=True)
        self.assertRaises(OSError, ff.write, str(path))
        self.assertRaises(OSError, ff.write_file, str(path))
        self.assertRaises(OSError, ff.write)
        bad_path = Path("should_not_exist.txt")
        self.assertFalse(bad_path.exists())
        self.assertRaises(OSError, foton.FilterFile, str(bad_path), read_only=True)

    def test_zroots_design(self):
        foton.FilterDesign(rate=16384).zroots([0.1], [0.2], 1)

if __name__ == '__main__':
    unittest.main()
