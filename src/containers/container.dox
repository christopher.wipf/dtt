/** \defgroup gds_container GDS Data Containers
  *
  *  The Container library include data classes to contain:
  *  
  *  - Data Objects
  *  - Filter IO Containers 
  *  - %Wavelet Data
  *  - Numeric Data Vectors
  *  - Calibration Data
  *  - Plot Sets
  *
  *  The container class source code is available from 
  *  <a href="https://redoubt.ligo-wa.caltech.edu/websvn/listing.php?repname=gds&path=/trunk/Containers/">SVN</a>
  *  \brief Container class libraries
  *  \author John Zweizig and Daniel Sigg
  *  \{
  */

/**  \defgroup gds_cntr_datobj Data Objects
  *
  *  The  group of data object classes includes several analysis data classes
  *  including:
  *  - TSeries: %Time series object.
  *  - containers::fSeries: Frequency series object and its specializations:
  *    - containers::ASD: Amplitude spectral density container
  *    - containers::CSD: Cross spectral density container
  *    - containers::DFT: Discrete Fourier transform container.
  *    - containers::PSD: Power spectral density container.
  *  - Histogram1 1-D histogram container.
  *  - Histogram2 2-D histogram
  *  
  *  Each container class includes all important metadata for the data object.
  *  \brief Analysis data objects: time series, frequency series \& histograms
  *  \author John Zweizig
  */

/**  \defgroup gds_cntr_dvect  Numeric Data Vectors
  *  The numeric data vector group includes templated data vectors with
  *  a common DVector API that allows manipulation of the data with
  *  numeric (\e e.g. +, -, *, /) and subset manipulation (\e e.g. extend, 
  *  extract, insert, replace) operations. The data vectors are based on 
  *  a templated copy-on-write storage vector (CWVec) that allows efficient
  *  transfer of arguments and return values in a threaded or unthreaded 
  *  environment. Classes in this group include
  *
  *  - DVector: Data vector API
  *  - DVecType: Templated type specific %DVector implementation.
  *  - CWVec:  Copy on write data container template.
  *
  *  \brief  Numeric data containers with copy on write data vectors
  *  \author John Zweizig
  */

/** \defgroup gds_cntr_filtio Filter IO Containers
  *
  *  \brief  I/O container for generalized time domain filters.
  *  \author John Zweizig
  */

/**  \name gds_cntr_wavelet Wavelet Data
  *
  *  Wavelet data containers include:
  *  - wavearray
  *  - WaveDWT
  *  - Wavelet
  *  - wseries
  *
  *  \brief  Wavelet data containers
  *  \author Sergei Klimenko
  */

/**\defgroup gds_cntr_calib Calibration Data
  *
  *  Calibration data containers are members of the calibration name space
  *  and include:
  *
  *  - Calibration
  *  - CalibrationCmp
  *  - Descriptor
  *  - SignalInfo
  *  - Table
  *  - Unit
  *  - UnitList
  *  - UnitScaling
  *
  *  \brief  Calibration data containers
  *  \author Daniel Sigg
  */

/**  \defgroup gds_cntr_plotset Plot Sets
  *
  *  \brief   Descriptors for graphics utilities
  *  \author Daniel sigg
  */

/** \}
*/
