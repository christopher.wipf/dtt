#include "PlotSet.hh"
#include "tconv.h"
#include "calutil.h"
#include <string>
#include <cstring>
#include <time.h>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <strings.h>


   using namespace std;
   using namespace calibration;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// PlotListLink                                                         //
//                                                                      //
// Link in PlotList						        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   bool PlotListLink::operator== (const char* s) const {
      return (strcasecmp (fName.c_str(), s) == 0); }

//______________________________________________________________________________
   bool PlotListLink::operator< (const char* s) const {
      return (strcasecmp (fName.c_str(), s) < 0); }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// PlotList                                                             //
//                                                                      //
// Stores the relation between grapgh type, A channel and B channel     //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   bool PlotMap::Add (PlotDescriptor* plot)
   {
      if (plot == 0) {
         return false;
      }
      const char* graph = plot->GetGraphType();
      const char* Achn = plot->GetAChannel();
      const char* Bchn = plot->GetBChannel();
   
      // find/add graph
      PlotListLink* g = FindChild (&fRoot, graph);
      if (g == 0) {
         g = AddChild (&fRoot, graph, 0);
         if (g == 0) {
            delete plot;
            return false;
         }
      }
      // find/add A channel
      PlotListLink* a = FindChild (g, Achn);
      if (a == 0) {
         a = AddChild (g, Achn, Bchn ? 0 : plot);
         if (a == 0) {
            delete plot;
            return false;
         }
         if (Bchn == 0) {
            return true;
         }   
      }
      else {
         // no B channel and already in list
         if (Bchn == 0) {
            a->SetPlot (plot);
            return true;
         }
      }
      // find/add B channel
      PlotListLink* b = FindChild (a, Bchn);
      if (b == 0) {
         b = AddChild (a, Bchn, plot);
         if (b == 0) {
            delete plot;
            return false;
         }
      }
      else {
         // already in list
         b->SetPlot (plot);
      }
      return true;
   }

//______________________________________________________________________________
   bool PlotMap::Remove (const PlotDescriptor* plot, bool deleteIt)
   {
      return Remove (plot->GetGraphType(), plot->GetAChannel(),
                    plot->GetBChannel(), deleteIt);
   }

//______________________________________________________________________________
   bool PlotMap::Remove (const char* graph, const char* Achn, 
                     const char* Bchn, bool deleteIt)
   {
      // find graph
      PlotListLink* g = FindChild (&fRoot, graph);
      if (g == 0) {
         return false;
      }
      // find A channel
      PlotListLink* a = FindChild (g, Achn);
      if (a == 0) {
         return false;
      }
      // find B channel if necessary
      if (Bchn != 0) {
         PlotListLink* b = FindChild (a, Bchn);
         if (b == 0) {
            return false;
         }
         // remove B channel
         PlotListLink* p = a->fChild;
         if (p == b) {
            a->fChild = b->fSibling;
         }
         else {
            while (p->Next() != b) {
               p = (PlotListLink*) p->Next();
            }
            p->fSibling = b->fSibling;
         }
         if (!deleteIt) b->fPlot = 0;
         delete b; // deletes plot descr. if deleteIt true!
      }
      else {
         if (deleteIt) {
            PlotDescriptor* pd = a->fPlot;
            a->fPlot = 0;
            delete pd; // this will call remove again with deleteIt false!!!
            return true;
         }
      }
      // check if to remove A channel
      if (a->fChild == 0) {
         PlotListLink* p = g->fChild;
         if (p == a) {
            g->fChild = a->fSibling;
         }
         else {
            while (p->Next() != a) {
               p = (PlotListLink*) p->Next();
            }
            p->fSibling = a->fSibling;
         }
         if (!deleteIt) a->fPlot = 0;
         delete a;
      }
      // check if to remove graph
      if (g->fChild == 0) {
         PlotListLink* p = fRoot.fChild;
         if (p == g) {
            fRoot.fChild = g->fSibling;
         }
         else {
            while (p->Next() != g) {
               p = (PlotListLink*) p->Next();
            }
            p->fSibling = g->fSibling;
         }
         delete g;
      }
      return true;
   }

//______________________________________________________________________________
   bool PlotMap::Empty () const
   {
      return (fRoot.fChild == 0);
   }

//______________________________________________________________________________
   void PlotMap::Clear ()
   {
      RemoveAllChilds (&fRoot);
   }

//______________________________________________________________________________
   PlotListLink* PlotMap::FindChild (const PlotListLink* first, 
                     const char* name) const
   {
      if ((name == 0) || (first == 0)) {
         return 0;
      }
      PlotListLink* p = first->fChild;
      return FindSibling (p, name);
   }

//______________________________________________________________________________
   PlotListLink* PlotMap::FindSibling (const PlotListLink* first, 
                     const char* name) const
   {
      if (name == 0) {
         return 0;
      }
      const PlotListLink* p = first;
      while (p) {
         if (*p == name) { 
            return (PlotListLink*) p;
         }
         p = p->Next();
      }
      return 0;
   }

//______________________________________________________________________________
   PlotListLink* PlotMap::AddChild (PlotListLink* first, 
                     const char* name, PlotDescriptor* plot)
   {
      if ((name == 0) || (first == 0)) {
         return 0;
      }
      // first child?
      if (first->fChild == 0) {
         first->fChild = new PlotListLink (name, plot);
         return first->fChild;
      }
      // before first child?
      else if (!(*first->fChild < name)) {
         PlotListLink* p = first->fChild;
         first->fChild = new PlotListLink (name, plot);
         if (first->fChild == 0) {
            first->fChild = p;
            return 0;
         }
         else {
            first->fChild->fSibling = p;
            return first->fChild;
         }
      }
      // add in chain after first
      else {
         PlotListLink* add = AddSibling (first->fChild, name, plot);
         return add;
      }
   }

//______________________________________________________________________________
   PlotListLink* PlotMap::AddSibling (PlotListLink* first, 
                     const char* name, PlotDescriptor* plot)
   {
      if ((name == 0) || (first == 0)) {
         return 0;
      }
      // look for place in list
      PlotListLink* p = first;
      while ((p->Next() != 0) && (*p->Next() < name)) {
         p = (PlotListLink*) p->Next();
      }
      // now add after p
      PlotListLink* next = (PlotListLink*) p->Next();
      p->fSibling = new PlotListLink (name, plot);
      if (p->fSibling == 0) {
         p->fSibling = next;
         return 0;
      }
      else {
         p->fSibling->fSibling = next;
         return p->fSibling;
      }
   }

//______________________________________________________________________________
   void PlotMap::RemoveAllChilds (PlotListLink* first)
   {
      if (first == 0) {
         return;
      }
   
      PlotListLink* p = first->fChild;
      while (p != 0) {
         RemoveAllChilds (p);
         PlotListLink* del = p;
         p = (PlotListLink*) p->Next();
         delete del;
      }
      first->fChild = 0;
   }

//______________________________________________________________________________
   int PlotMap::GetChildID (const PlotListLink* first, 
                     const char* name) const
   {
      if (first == 0) {
         return -1;
      }
      int id = 0;
      const PlotListLink* p = first->fChild;
      while (p != 0) {
         if (*p == name) {
            return id;
         }
         p = p->Next();
         id++;
      }
      return -1;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// PlotSet::iterator                                                    //
//                                                                      //
// Iterator for plot descriptors stored in a plot set                   //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   PlotSet::basic_iterator PlotSet::basic_iterator::operator++ (int) 
   {
      basic_iterator temp = *this;
      this->operator++();
      return temp;
   }

//______________________________________________________________________________
   PlotSet::basic_iterator& PlotSet::basic_iterator::operator++ () 
   {
      fP = 0;
      // make sure we get initialization right
      if (fG == 0) {
         return *this;
      }
      // look for B channel 
      while (fG != 0) {
      
         // make sure we have a valid A
         if (fA == 0) {
            fB = 0;
            fA = fG->Child();
            if ((fA != 0) && ((fP = (PlotDescriptor*)fA->GetPlot()) != 0)) {
               return *this;
            }
         }
         while (fA != 0) {
            // look for a valid B
            if (fB == 0) {
               fB = fA->Child();
               if ((fB != 0) && ((fP = (PlotDescriptor*)fB->GetPlot()) != 0)) {
                  return *this;
               }
            }
            while (fB != 0) {
               fB = fB->Next();
               if ((fB != 0) && ((fP = (PlotDescriptor*)fB->GetPlot()) != 0)) {
                  return *this;
               }
            }
            fA = fA->Next();
            fB = 0;
            if ((fA != 0) && ((fP = (PlotDescriptor*)fA->GetPlot()) != 0)) {
               return *this;
            }
         }
         fG = fG->Next();
         fA = 0;
      }
      return *this;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// PlotSet                                                              //
//                                                                      //
// Set of plot descriptors (used by TLGPad)                             //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   const PlotSet::PlotFilter PlotSet::kNoFilter;

//______________________________________________________________________________

   PlotSet::PlotSet () 
   {
   }

//______________________________________________________________________________
   PlotSet::~PlotSet ()
   {
      // close all registered main windows
      for (winlist::iterator i = fMainWindows.begin(); 
          i != fMainWindows.end(); ++i) {
         delete *i;
      }
   }

//______________________________________________________________________________
   void PlotSet::Merge (PlotSet& pl, const PlotFilter& filter) 
   {
      while (!pl.Empty()) {
         PlotDescriptor* pd = &*(pl.begin());
         if (pd == 0) {
            break;
         }
         pl.Remove (pd, false);
         string g = pd->GetGraphType() ? pd->GetGraphType() : "";
         string a = pd->GetAChannel() ? pd->GetAChannel() : "";
         string b = pd->GetBChannel() ? pd->GetBChannel() : "";
         if (filter (*pd, g, a, b)) {
            pd->SetGraphType (g.c_str());
            pd->SetAChannel (a.c_str());
            pd->SetBChannel (b.size() == 0 ? 0 : b.c_str());
            Add (pd);
         }
         else {
            delete pd;
         }
      }
   }

//______________________________________________________________________________
   void PlotSet::RegisterWindow (VirtualPlotWindow* win)
   {
      for (winlist::iterator i = fMainWindows.begin(); 
          i != fMainWindows.end(); ++i) {
         if (*i == win) {
            return;
         }
      }
      // add to list
      fMainWindows.push_back (win);
   }

//______________________________________________________________________________
   void PlotSet::UnregisterWindow (VirtualPlotWindow* win)
   {
      fMainWindows.remove (win);
   }

//______________________________________________________________________________
   void PlotSet::RegisterPad (VirtualPlotPad* pad)
   {
      for (padlist::iterator i = fPads.begin(); i != fPads.end(); ++i) {
         if (*i == pad) {
            return;
         }
      }
      // add to list
      fPads.push_back (pad);
   }

//______________________________________________________________________________
   void PlotSet::UnregisterPad (VirtualPlotPad* pad)
   {
      fPads.remove (pad);
   }

//______________________________________________________________________________
   const PlotDescriptor* PlotSet::Get (const char* graphtype, 
                     const char* Achn, const char* Bchn) const
   {
      const PlotListLink* link;
      if ((Bchn == 0) || (strlen (Bchn) == 0)) {
         link = fPlotList.Get (graphtype, Achn);
      }
      else {
         link = fPlotList.Get (graphtype, Achn, Bchn);
      }
   
      return (link != 0) ? link->GetPlot() : 0;
   }

//______________________________________________________________________________
   PlotDescriptor* PlotSet::Add (PlotDescriptor* plotd)
   {
      if (plotd == 0) {
         return 0;
      }
      if (fPlotList.Add (plotd)) {
         plotd->fOwner = this;
         // cout << "added to plot set " << plotd->GetGraphType() <<
            // " " << plotd->GetAChannel() << " " <<
            // (plotd->GetBChannel() == 0 ? "" : plotd->GetBChannel()) << endl;
         return plotd;
      }
      else {
         return 0;
      }
   }

//______________________________________________________________________________
   PlotDescriptor* PlotSet::Add (BasicDataDescriptor* desc,
                     const char* graphtype, 
                     const char* Achn, const char* Bchn,
                     const ParameterDescriptor* prmd,
                     const Descriptor* cald)
   {
      return Add (new PlotDescriptor (desc, graphtype, Achn, Bchn,
                                   prmd, cald)); 
   }

//______________________________________________________________________________
   PlotDescriptor* PlotSet::Add (const AttDataDescriptor& data, 
                     const char* graphtype, 
                     const char* Achn, const char* Bchn) 
   {
      PlotDescriptor* plotd = 
         (PlotDescriptor*) data.GetParentPlotDescriptor();
      if (plotd != 0) {
         if (plotd->fOwner == this) {
            return plotd;
         }
         else {
            return 0;
         }
      }
      else {
         return Add (new PlotDescriptor (data, graphtype, Achn, Bchn)); 
      }
   }

//______________________________________________________________________________
   void PlotSet::Remove (const PlotDescriptor* plotd, bool deleteIt)
   {
      fPlotList.Remove (plotd, deleteIt);
   }

//______________________________________________________________________________
   void PlotSet::Clear (bool all)
   {
      if (all) {
         fPlotList.Clear ();
      }
      else {
         PlotMap map;
         for (iterator i = begin(); i != end(); i++) {
            if (i->IsPersistent()) {
               if (i.fB) {
                  ((PlotListLink*)i.fB)->SetPlot (0, false);
               }
               else if (i.fA) {
                  ((PlotListLink*)i.fA)->SetPlot (0, false);
               }
               map.Add (&*i);
            }
         }
         fPlotList = map;
      }
   }

//______________________________________________________________________________
   bool PlotSet::Empty () const
   {
      return fPlotList.Empty();
   }

//______________________________________________________________________________
   void PlotSet::Update (const PlotDescriptor* plotd) const
   {
      // update all registered graphics pads
      for (padlist::const_iterator i = fPads.begin(); 
          i != fPads.end(); ++i) {
         if (*i) (*i)->UpdatePlot (plotd, true);
      }
   }

//______________________________________________________________________________
   void PlotSet::Update () const
   {
      // update all registered graphics pads
      for (padlist::const_iterator i = fPads.begin(); 
          i != fPads.end(); ++i) {
         if (*i) (*i)->Update (true);
      }
   }

//______________________________________________________________________________
   void PlotSet::ShowDisconnect(bool show)
   {
       for(iterator i=begin(); i != end(); i++)
       {
           i->SetShowDisconnect(show);
       }
   }

//______________________________________________________________________________
   const PlotSet::PlotFilter& PlotSet::NoFilter()
   {
      return kNoFilter;
   }

