/* version $Id: ParamDesc.hh 6236 2010-05-24 21:34:01Z alexander.ivanov $ */
#ifndef _LIGO_PARAMDESC_H
#define _LIGO_PARAMDESC_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: ParamDesc						*/
/*                                                         		*/
/* Module Description: Parameter descriptor				*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 29Oct99  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: PlotDesc.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include "DataDesc.hh"
#include <time.h>
#include <string>


/** Describes the parameters asscoaiated with a plot.
   
    @brief Parameter descriptor
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class ParameterDescriptor {
   protected:
      /// Flag for start time
      static const int kPrmStartTime;
      /// Flag for number of averages
      static const int kPrmAverages;
      /// Flag for third parameter
      static const int kPrmThird;
      /// Flag for user string
      static const int kPrmUser;
   
      /// Flag indicating which parameters are set
      unsigned int	fParamSet;
      /// Start time (GPS sec)
      unsigned int	fT0Sec;
      /// Start time (nsec)
      unsigned int	fT0NSec;
      /// Number of averages;
      int		fAverages;
      /// Third parameter
      std::string	fThird;
      /// User string
      char*		fUser;
   
   public:
      /// Default constructor
      ParameterDescriptor ();
      /// Copy constructor
      ParameterDescriptor (const ParameterDescriptor& prmd);
      /// Similar to copy constructor
      ParameterDescriptor (const ParameterDescriptor* prmd);
      /// Destructor  
      virtual ~ParameterDescriptor();
      /// Asignment operator
      ParameterDescriptor& operator= (const ParameterDescriptor& prmd);
   
      /// Set start time
      virtual void SetStartTime (unsigned int sec, unsigned int nsec = 0);
      /// Get start time (valid if return true)
      virtual bool GetStartTime (unsigned int& sec, unsigned int& nsec) const;
      /// Get start time (valid if return true)
      virtual bool GetStartTime (std::string& s, bool utc = true,
                        double timeshift = 0.0) const;
      /// Reset start time
      virtual void ResetStartTime () {
         fParamSet &= ~kPrmStartTime; }
      /// Set number of averages
      virtual void SetAverages (int averages);
      /// Get start time (valid if return true)
      virtual bool GetAverages (int& averages) const;
      /// Get start time (valid if return true)
      virtual bool GetAverages (std::string& s) const;
      /// Reset number of averages
      virtual void ResetAverages () {
         fParamSet &= ~kPrmAverages; }
      /// Set third parameter
      virtual void SetThird (const char* name);
      /// Get start time (valid if not 0)
      virtual const char* GetThird () const;
      /// Get start time (valid if not 0)
      virtual bool GetThird (std::string& s) const;
      /// Set user string
      virtual void SetUser (const char* xml);
      /// Get user string (valid if not 0)
      virtual const char* GetUser () const;
   };


#endif // _LIGO_PARAMDESC_H

