SET(UTIL_SRC
    gdserr.c
    gdsheartbeat.c
    gdsndshost.c
    gdsprm.c
    gdsreadtppar.c
    gdssock.c
    gdsstring.c
    gdsstringcc.cc
    gdstask.c
    rpcinc.c   
)

add_library(util_lib OBJECT
    ${UTIL_SRC}
)

target_include_directories(util_lib PRIVATE
    ${DTT_INCLUDES}
)

target_include_directories(util_lib PRIVATE SYSTEM BEFORE
        ${RPC_INCLUDE_DIR}
        )

set_target_properties(util_lib PROPERTIES
        PRIVATE_HEADER
            "gdserr.h;gdserrmsg.h;gdsheartbeat.h;gdsmain.h;gdsndshost.h;gdsprm.h;gdsreadtppar.h;gdssock.h;gdsstring.h;gdsstringcc.hh;gdstask.h;gdsutil.h;rpcinc.h;rpcdefs.h"
        )

INSTALL_HEADERS(util_lib)