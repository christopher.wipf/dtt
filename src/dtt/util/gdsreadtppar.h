/* Version $Id: gdsreadtppar.h 7344 2015-04-22 20:15:00Z james.batch@LIGO.ORG $ */
/* This file declares functions for reading a testpoint.par file into structure to
 * allow associating models, hostnames, and dcuid numbers.
 */

/* The model_info structure holds data for one entry from the testpoint.par file, which
 * should look something like:
 *
 * [H-node18]
 * hostname=h1asc0
 * system=h1iopasc0
 *
 */
struct model_info
{
   int  dcuid ;
   char hostname[64] ;
   char modelname[64] ;
} ;

/* For convenience */
typedef struct model_info modelInfo ;

/* This function reads the testpoint.par file, */
/* The node points to the first entry in an array of nodes, which the caller must define. 
 * The function returns the number of nodes read in nread, while max defines the maximum
 * number of entries the caller expects.
 * A return value of 0 indicates success, while nonzero indicates failure.
 */
int readTestpointPar(modelInfo *node, int *nread, int max) ;
