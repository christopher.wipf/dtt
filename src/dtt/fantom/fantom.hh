/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: fantom							*/
/*                                                         		*/
/* Module Description: Frame and NDS translation module			*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 4Oct00   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: framefast.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_FANTOM_H
#define _LIGO_FANTOM_H

#include <cstring>
#include <string>
#include <vector>
#include "smartio.hh"
#include "framemux.hh"

namespace fantom {


/** @name fantom
    This header defines the fantom main program.
   
    @memo Frame and NDS translation module
    @author Written October 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{


/** The main class.
    This class export the main program function for parsing the command
    line arguments for invoking the command line interpreter.s

    @memo fantom main class.
 ************************************************************************/
   class fantom {
   public:
      /// Default constructor
      fantom ();
      /// Creates fantom and parses command line arguments
      fantom (int argc, char* argv[]);
      /// Destructor
      ~fantom ();
   
      /// Initialiion
      bool init ();
   
      /// Return true on error
      bool operator! () const { 
         return fError; }
      /// Processes an input line
      bool operator() ();
      /// True if program has terminated
      bool finished () const { 
         return fFinished; }
      /// True if in interactive mode
      bool interactive() const {
         return fInteractive; }
      /// True if in verbose mode
      bool verbose() const {
         return fVerbose; }
   
      /// Process frames
      bool process (int sec = 0);
   
      /// read an input file
      bool read (const char* line);
      /// Parses an input line
      bool parse (const char* line);
      /// Printes a help message
      void help ();
      /// Prints the mostr recent error message
      void errorMessage ();
      /// Signal interrupt (Control-C)
      void interrupt (int signal);
   
      /// Main program: creates fantom and start the main event loop
      static int main (int argc, char* argv[]);
   
   protected:
      /// Error flag
      bool 		fError;
      /// Help flag
      bool              fHelp;
      /// Interactive mode
      bool 		fInteractive;
      /// Verbose mode
      bool 		fVerbose;
      /// Control-C active
      bool		fCtrlC;
      /// Initialization file
      std::string	fFilename;
      /// Initializing commands
      std::string	fCommands;
      /// Last input line
      std::string 	fLastline;
      /// Finished flag
      bool 		fFinished;
      /// Last error message
      std::string	fMsg;
   
      /// List of input devices
      smart_ilist	fInputs;
      /// List of output devices
      smart_olist	fOutputs;
      /// Frame multiplexer
      framemux		fMux;
   };


//@}

}


#endif // _LIGO_FANTOM_H


