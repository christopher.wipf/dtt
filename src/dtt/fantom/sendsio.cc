/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "PConfig.h"
#include "sendsio.hh"
#include "framefast/frametype.hh"
#include "framefast/framefast.hh"
#include <iostream>
#include <cstring>
#include <cstdlib>

#define _TIMEOUT 600	/* 10 min */

#include "NDS2Socket.hh"

namespace fantom {
   using namespace std;
   using namespace framefast;
   using namespace sends;

   static int my_debug = 0 ;
#define TEST_RATE 1

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// channel list chache						        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

//   typedef std::pair <std::string, int> ndsserver;
#if TEST_RATE
   // Replace the channel name with a name/rate pair, since names aren't
   // enough to define a channel for NDS2. 
   typedef std::pair<std::string, int> ChannelNameRate ;
   typedef std::map <ChannelNameRate, DAQDChannel> DAQDChannelList ;
#else
   typedef std::map <std::string, DAQDChannel> DAQDChannelList;
#endif
   typedef std::map <std::string, DAQDChannelList> ndschannelcache;
   static ndschannelcache gNdsChnCache; // global

   struct DAQTimeList {
      Time fStart;
      Time fStop;
      Time fStartTrend;
      Time fStopTrend;
      Time fStartMinuteTrend;
      Time fStopMinuteTrend;
   };


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Forwards							        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   static bool readChnDAQServer (const char* server, int port,
				 frametype ftyp, DAQDChannelList& chns,
				 unsigned long start, unsigned long end);
   static bool readTimeDAQServer (const char* server, int port,
				  DAQTimeList& time);

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// utility functions                                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   static string trim (const char* p)
   {
      while (isspace (*p)) p++;
      string s = p;
      while ((s.size() > 0) && isspace (s[s.size()-1])) {
         s.erase (s.size() - 1);
      }
      return s;
   }

   static string
   makeServerName(const char* server, int port, frametype utype,
			   unsigned long start, unsigned long stop)
   {
      std::ostringstream ss ;
      ss << server << ":" << int(port) ;
      switch (utype) {
	 default:
	 case FF: ss << "/frames" ;
	    break ;
	 case STF: ss << "/trend" ;
	    break ;
	 case MTF: ss << "/minute-trend" ;
	    break ;
      }
      ss << "?epoch_start=" << int(start) << "&epoch_end=" << int(stop) ;
      return ss.str() ;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// sends_support		                                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   const int sends_support::kNDSPORT = 31200;

//______________________________________________________________________________
   sends_support::sends_support (const char* servername, 
                     const char* conf) 
   : fPort (kNDSPORT), fType (FF), fNDS (0), fEpochStart(0), fEpochStop(0)
   {
      setServer (servername);
      setConf (conf);
   }

//______________________________________________________________________________
   sends_support::~sends_support()
   {
      close();
   }

//______________________________________________________________________________
   void sends_support::setServer (const char* servername)
   {
      string n = trim (servername ? servername : "");
      fAddr = "";
      fPort = kNDSPORT;
      fType = FF;

      // Look for the epoch start, stop times.
      unsigned long epoch_start = 0, epoch_stop = 0 ;
      string s(servername) ;
      string es("epoch_start=") ;
      string ee("epoch_end=") ;
      string ff("/frames") ;
      string stf("/trend") ;
      string mtf("/minute-trend") ;
      string ps(":") ;
      if (s.find(stf) != string::npos)
      {
	 fType = STF ;
      }
      else if (s.find(mtf) != string::npos)
      {
	 fType = MTF ;
      }
      else if (s.find(ff) != string::npos)
      {
	 // This is also default?
	 fType = FF ;
      }
      string::size_type es_pos = s.find(es) ;
      string::size_type ee_pos = s.find(ee) ;
      if (es_pos != string::npos && ee_pos != string::npos) 
      {
	 // Both start and stop were found, set the epoch.
	 fEpochStart = atoi(s.c_str() + es_pos + es.length()) ;
	 fEpochStop = atoi(s.c_str() + ee_pos + ee.length()) ;
      }
      // Look for the port number.
      string::size_type ps_pos = s.find(ps) ;
      if (ps_pos != string::npos)
      {
	 fPort = atoi(s.c_str() + ps_pos + ps.length()) ;
      }
      // The address should be the start of the string, until
      // the port number or epoch names are found.
      string::size_type name_end = s.find_first_of(":&?/") ;
      // Note that name_end = npos works for the end value.
      string addr = s.substr(0,name_end) ;
      fAddr = addr.c_str() ;
      // Debug stuff.
      if (my_debug) {
	 cerr << "sends_support::setServer(" << servername << ")" << endl ;
	 cerr << "  fAddr = " << fAddr << endl ;
	 cerr << "  fPort = " << fPort << endl ;
	 cerr << "  fEpochStart = " << fEpochStart << endl ;
	 cerr << "  fEpochStop = " << fEpochStop << endl ;
	 switch (fType) {
	    case STF: cerr << "  fType = STF" << endl ;
	      break ;
	    case MTF: cerr << "  fType = MTF" << endl ;
	      break ;
	    case FF: cerr << "  fType = FF" << endl ;
	      break ;
	    default: cerr << "  fType could not be determined!" << endl ;
	      break ;
	 }
      }
   }

//______________________________________________________________________________
   void sends_support::setConf (const char* conf)
   {
      // nothing
   }

//______________________________________________________________________________
   bool sends_support::selectChannels (const channelquerylist* chns)
   {
      // check if connection is open
      if (!fNDS) {
         if (!open()) {
            close();
            return false;
         }
      }
      // get channel list
      string ns = makeServerName(fAddr.c_str(), fPort, fType, fEpochStart, fEpochStop) ;
      ndschannelcache::iterator l = gNdsChnCache.find (ns);
      if (l == gNdsChnCache.end()) {
         return false;
      }
      // Add normal channels to nds
      if (fType == FF) {
         for (DAQDChannelList::const_iterator i = l->second.begin(); i != l->second.end(); ++i) {
#ifdef TEST_RATE
            if (!chns || chns->empty() || (chns->findMatch (i->second.mName) != 0)) 
#else
            if (!chns || chns->empty() || (chns->findMatch (i->first) != 0)) 
#endif
	    {
               //cerr << "add a channel " << i->second.mName << endl;
               fNDS->AddChannel (i->second);
            }
         }
      }
      // Add trend channels to nds
      else {
         for (DAQDChannelList::const_iterator i = l->second.begin();
             i != l->second.end(); ++i) {
            // generate all derived names
            string max = string (i->second.mName) + ".max";
            string mean = string (i->second.mName) + ".mean";
            string min = string (i->second.mName) + ".min";
            string n = string (i->second.mName) + ".n";
            string rms = string (i->second.mName) + ".rms";
            string stddev = string (i->second.mName) + ".stddev";
            DAQDChannel c = i->second;
            // add all if no extension is specified
            if (!chns || chns->empty() || 
               (chns->findMatch (i->second.mName) != 0)) {
               c.mName = max;
               fNDS->AddChannel (c);
               c.mName = mean;
               fNDS->AddChannel (c);
               c.mName = min;
               fNDS->AddChannel (c);
               c.mName = n;
               fNDS->AddChannel (c);
               // :KLUDGE: misuse group id for std. dev. calculation!!
               c.mName = rms;
               fNDS->AddChannel (c);
            }
            // check individual
            else if (chns->findMatch (max) != 0) {
               c.mName = max;
               fNDS->AddChannel (c);
            }
            else if (chns->findMatch (mean) != 0) {
               c.mName = mean;
               fNDS->AddChannel (c);
            }
            else if (chns->findMatch (min) != 0) {
               c.mName = min;
               fNDS->AddChannel (c);
            }
            else if (chns->findMatch (n) != 0) {
               c.mName = n;
               fNDS->AddChannel (c);
            }
            else if (chns->findMatch (rms) != 0) {
               c.mName = rms;
               fNDS->AddChannel (c);
            }
            // std. dev.!
            else if (chns->findMatch (stddev) != 0) {
	       //  mGroup is no longer used for Std Dev processing. The
	       //  calculation of stddev will have to be handled at a 
	       //  later point.
               if (fNDS->FindChannel(mean) == fNDS->chan_end()) {
		  c.mName = mean;
                  fNDS->AddChannel (c);
               }
               if (fNDS->FindChannel(n) == fNDS->chan_end()) {
		  c.mName = n;
                  fNDS->AddChannel (c);
               }
               if (fNDS->FindChannel(rms) == fNDS->chan_end()) {
		  c.mName = rms;
                  fNDS->AddChannel (c);
               }
            }
         }
      }
      return true;
   }

//______________________________________________________________________________
   framefast::basic_frame_storage* sends_support::readFrame ()
   {
      // check if connection is open
      if (!fNDS) {
         if (!open()) {
            close();
            cerr << "Unable to open connection" << endl;
            return 0;
         }
      }
      // check if request has been sent
      if (!fNDS->isOn()) {
         if (!request()) {
            close();
            cerr << "Unable to send request" << endl;
            return 0;
         }
      }
      // get data
      char* buf = 0;
      int buflen;
      if (!getdata (buf, buflen)) {
         close();
         cerr << "Unable to obtain data" << endl;
         return 0;
      }
      // copy data into a frame
      framewriter* fr = 0;
      DAQDRecHdr*	head = (DAQDRecHdr*) buf;
      char*		dptr = buf + sizeof (DAQDRecHdr);
      // generate frame
      fr = new (nothrow) framewriter (head->Secs);
      if (fr == 0) {
         delete [] buf;
         cerr << "Unable to create frame" << endl;
         return 0;
      }
      fr->setTime (Time (head->GPS, 0));
      // fill detector structure
      detector_t det;
      string chan0_name;
      DAQC_api::const_channel_iter iter = fNDS->chan_begin();
      if (iter != fNDS->chan_end()) chan0_name = iter->mName;
      if (!chan0_name.empty()) {
         // LHO 
         // :TODO: check values
         if (chan0_name[0] == 'H') {
            strcpy (det.fName, "LIGO_1");
            det.fLongitudeD = -119;
            det.fLongitudeM = 24;
            det.fLongitudeS = 27.5657005310059;
            det.fLatitudeD = 46;
            det.fLatitudeM = 27;
            det.fLatitudeS = 0.565699994564056;
            det.fElevation = 142.554000854492;
            det.fArmXAzimuth = 2.19910407066345;
            det.fArmYAzimuth = 3.76990103721619;
         }
         // LLO
         else if (chan0_name[0] == 'L') {
            strcpy (det.fName, "LIGO_2");
            det.fLongitudeD = -90;
            det.fLongitudeM = 46;
            det.fLongitudeS = 27.2653999328613;
            det.fLatitudeD = 30;
            det.fLatitudeM = 33;
            det.fLatitudeS = 46.4196014404297;
            det.fElevation = -6.57399988174438;
            det.fArmXAzimuth = 3.45080399513245;
            det.fArmYAzimuth = 4.49800109863281;
         }
         fr->setDetectorInfo (det);
      }
   
      // go through channels
      const real_8_t* keepMean = 0;
      const real_8_t* keepRms = 0;
      const int_4s_t* keepN = 0;
      for (DAQC_api::const_channel_iter iter = fNDS->chan_begin();
          iter != fNDS->chan_end(); ++iter) {
         // compute data pointers
         const char* values = dptr;
         int len = 0;
         int_2u_t dtype = (int_2u_t)-1;
         data_t val;
         strncpy (val.fADC.fName, iter->mName.c_str(), maxName-1);
         val.fADC.fName[maxName-1] = 0;
         string::size_type pos = string::npos;
         int mGroup = 0;
         bool forget = false;
         bool calc_stddev = false;
         // normal data channel
         if (fType == FF) {
            len = int(iter->mRate * head->Secs + 0.5);
	    int BPS = 0;
            switch (iter->mDatatype) {
	    case _16bit_integer: 
	       BPS = 2;
	       dtype = typeID_int_2s_t;
	       break;
	    case _32bit_integer: 
	       BPS = 4;
	       dtype = typeID_int_4s_t;
	       break;
	    case _64bit_integer: 
	       BPS = 8;
	       dtype = typeID_int_8s_t;
	       break;
	    case _32bit_float: 
	       BPS = 4;
	       dtype = typeID_real_4_t;
	       break;
	    case _64bit_double: 
	       BPS = 8;
	       dtype = typeID_real_8_t;
	       break;
	    case _32bit_complex: 
	       BPS = 8;
	       dtype = typeID_complex_8_t;
	       break;
	    default:
	       BPS = 0;
            }
            dptr += len * BPS;
            val.fADC.fNBits = BPS * 8;
         }
         // trend channels
         else {
            // :KLUDGE: misuse group id for std. dev. calculation!!
            // mGroup = 0: normal channel
            // mGroup = 1: forget channel
            // mGroup = 2: calculate std. dev. (rms only)
            // mGroup = 3: calculate std. dev. and forget (rms only)
            //mGroup = iter->mGroup;
            //forget = (mGroup & 0x01) != 0;
            len = head->Secs / (fType == MTF ? 60 : 1);
            if ((pos = iter->mName.rfind (".max")) != string::npos) {
               dptr += sizeof (int_4s_t) * len;
               dtype = typeID_int_4s_t;
               val.fADC.fNBits = 32;
            }
            else if ((pos = iter->mName.rfind (".mean")) != string::npos) {
               dptr += sizeof (real_8_t) * len;
               dtype = typeID_real_8_t;
               val.fADC.fNBits = 64;
               keepMean = (real_8_t*) values;
               keepN = 0; keepRms = 0;
            }
            else if ((pos = iter->mName.rfind (".min")) != string::npos) {
               dptr += sizeof (int_4s_t) * len;
               dtype = typeID_int_4s_t;
               val.fADC.fNBits = 32;
            }
            else if ((pos = iter->mName.rfind (".n")) != string::npos) {
               dptr += sizeof (int_4s_t) * len;
               dtype = typeID_int_4s_t;
               val.fADC.fNBits = 32;
               keepN = (int_4s_t*) values;
            }
            else if ((pos = iter->mName.rfind (".rms")) != string::npos) {
               dptr += sizeof (real_8_t) * len;
               dtype = typeID_real_8_t;
               val.fADC.fNBits = 64;
               calc_stddev = (mGroup & 0x02) != 0;
               keepRms = (real_8_t*) values;
            }
            // unrecognized trend channel
            else {
               break;
            }
         }
         // check if data is within limits of data buffer
         if ((char*)dptr > buf + sizeof (DAQDRecHdr) + buflen) {
            break;
         }
         // fill in units, slope and bias
         val.fADC.fBias = iter->mOffset;
         val.fADC.fSlope = iter->mGain * iter->mSlope;
         if (iter->mUnit.empty()) {
            strcpy (val.fADC.fUnit, "#");
         }
         else {
            strncpy (val.fADC.fUnit, iter->mUnit.c_str(), maxName - 1);
	    val.fADC.fName[maxName-1] = 0;
         }
         // fill in rest
         val.fADC.fSampleRate = (double)len / head->Secs;
         val.fADC.fDataValid = 0;
         val.fVect.fNDim = 1;
         val.fVect.fNx[0] = len;
         val.fVect.fDx[0] = 1. / val.fADC.fSampleRate;
         val.fVect.fStartX[0] = 0;
         strcpy (val.fVect.fUnitX[0], "s");
         strcpy (val.fVect.fUnitY, val.fADC.fUnit);
         // add data to frame
         val.allocate (dtype, len);
         val.fVect.fill (0, len, values, littleendian());
         if (!forget) {
            if (!fr->addData (val.fADC, val.fVect)) {
               ;
            }
         
         }
         // calculate std. dev.
         if (calc_stddev && keepMean && keepN && keepRms) {
            real_8_t* stddev = new real_8_t [len];
            if (stddev) {
               for (int k = 0; k < len; k++) {
                  // :TRICKY: work around alignment and byte-order
                  real_8_t xmean;
                  memcpy (&xmean, ((char*)keepMean) + k*sizeof (real_8_t), 
                         sizeof (real_8_t));
                  int_4s_t xn;
                  memcpy (&xn, ((char*)keepN) + k*sizeof (int_4s_t), 
                         sizeof (int_4s_t));
                  real_8_t xrms;
                  memcpy (&xrms, ((char*)keepRms) + k*sizeof (real_8_t), 
                         sizeof (real_8_t));
                  if (littleendian()) {
                     swap (&xmean);
                     swap (&xn);
                     swap (&xrms);
                  }
                  if (xn <= 1) {
                     stddev[k] = 0;
                  }
                  else {
                     stddev[k] = 
                        sqrt (((double)xn) / ((double) xn - 1.0) *
                             (xrms*xrms - xmean*xmean));
                  }
               }
               val.fVect.fill (0, len, (const char*)stddev, false);
               char* p = strstr (val.fADC.fName, ".rms");
               if (p) {
                  strcpy (p, ".stddev");
                  fr->addData (val.fADC, val.fVect);
               }
            }
            delete [] stddev;
         }
         else {
            keepMean = 0; keepN = 0; keepRms = 0;
         }
      }
      // delete buffer
      delete [] buf;
      buf = 0;
      // complete frame
      if (!fr->next()) {
         delete fr;
         cerr << "Unable to complete data" << endl;
         return 0;
      }
      // write frame to memory
      memory_out* mem = new (nothrow) memory_out ();
      if (!fr->write (mem)) {
         delete mem; 
         mem = 0;
      }
      delete fr;
   
      //cerr << "frame size = " << (mem && mem->data() ? mem->size() : -1) << endl;
      if (!mem || !mem->data()) {
         cerr << "Unable to write frame to memory buffer" << endl;
      }
      return mem;
   }

//______________________________________________________________________________
   bool sends_support::eof() const
   {
      return fNDS == 0;
   }

//______________________________________________________________________________
   bool sends_support::getChannels (const char* server, int port,
				    channellist& chns, frametype utype, 
				    unsigned long start, unsigned long stop)
   {
      if (my_debug) cerr << "sends_support::getChannels( " << server << ", " << port << ", ..., " << start <<", " << stop << ")" << endl;
      //ndsserver ns (server, port);
      string ns = makeServerName(server, port, utype, start, stop) ;
      DAQDChannelList ndschns;
      DAQDChannelList::const_iterator begin;
      DAQDChannelList::const_iterator end;

      //--------------------------------  get channel names (check cache first)
      ndschannelcache::iterator i = gNdsChnCache.find (ns);
      if (i != gNdsChnCache.end()) {
	 if (my_debug) cerr << "  getChannels() - gNdsChnCache.find() succeeded." << endl ;
         begin = i->second.begin();
         end = i->second.end();
      }

      // get names from NDS2 server
      else if (readChnDAQServer (server, port, utype, ndschns, start, stop)) {
         gNdsChnCache.insert (ndschannelcache::value_type (ns, ndschns));
         begin = ndschns.begin();
         end = ndschns.end();
         if (my_debug) cerr << "  getChannels() - loaded from server" << endl;
      }
      else {
         cerr << "load failed" << endl;
         return false;
      }
      // copy channels into return list
      chns.clear();
      for (DAQDChannelList::const_iterator i = begin; i != end; ++i) {
	 channelentry chn (i->second.mName.c_str(), i->second.mRate);
         if (chn.Active()) {
            // normal channels
            if (true || (utype == FF)) {
               chns.push_back (chn);
            }
            // trend channels
            else {
               float rate = (utype == STF) ? 1 : 1./60.;
               string cname = string (i->second.mName) + ".max";
               chn = channelentry (cname.c_str(), rate);
               chns.push_back (chn);
               cname = string (i->second.mName) + ".mean";
               chn = channelentry (cname.c_str(), rate);
               chns.push_back (chn);
               cname = string (i->second.mName) + ".min";
               chn = channelentry (cname.c_str(), rate);
               chns.push_back (chn);
               cname = string (i->second.mName) + ".n";
               chn = channelentry (cname.c_str(), rate);
               chns.push_back (chn);
               cname = string (i->second.mName) + ".rms";
               chn = channelentry (cname.c_str(), rate);
               chns.push_back (chn);
               cname = string (i->second.mName) + ".stddev";
               chn = channelentry (cname.c_str(), rate);
               chns.push_back (chn);
            }
         }
      }
      if (my_debug) cerr << "  getChannels() - sort channel list." << endl ;
      SortChannels (chns, true);
      if (my_debug) cerr << "  getChannels() return true." << endl ;
      return true;
   }

//______________________________________________________________________________
   bool sends_support::getChannels (channellist& chns) const
   {
      return getChannels (fAddr.c_str(), fPort, chns, fType);
   }

//______________________________________________________________________________
   bool sends_support::getTimes (const char* server, int port,
                     Time& start, Time& stop, frametype utype)
   {
      //cerr << "get times from " << server << " @ " << port << endl;
      DAQTimeList times;
      if (!readTimeDAQServer (server, port, times)) {
         return false;
      }
      switch (utype) {
         default:
         case FF: 
            {
               start = times.fStart;
               stop = times.fStop;
               break;
            }
         case STF: 
            {
               start = times.fStartTrend;
               stop = times.fStopTrend;
               break;
            }
         case MTF: 
            {
               start = times.fStartMinuteTrend;
               stop = times.fStopMinuteTrend;
               break;
            }
      }
      return true;
   }

//______________________________________________________________________________
   bool sends_support::getTimes (Time& start, Time& stop) const
   {
      return getTimes (fAddr.c_str(), fPort, start, stop, fType);
   }

//______________________________________________________________________________
   bool sends_support::open ()
   {
      if (fNDS) {
         close();
      }
      fNDS = new (nothrow) NDS2Socket(fAddr, fPort);
      return (fNDS != 0) && fNDS->isOpen();
   }

//______________________________________________________________________________
   void sends_support::close()
   {
      if (fNDS) {
         delete fNDS;
         fNDS = 0;
      }
   }

//______________________________________________________________________________
   bool sends_support::request ()
   {
      if (!fNDS) {
         return false;
      }
      if ((fT0 == Time (0, 0)) || (fDt <= Interval (0.))) {
         return false;
      }
   
      // determine GPS time
      Time::ulong_t start = fT0.getS();
      Time::ulong_t duration  = fDt.GetS();
      while (Time (start + duration, 0) < fT0 + fDt) {
         duration++;
      }
      int r = 0; // return

      r = fNDS->RequestData (start, duration, _TIMEOUT);
      return r == 0;
   }

//______________________________________________________________________________
   bool sends_support::getdata (char*& data, int& len)
   {
      if (!fNDS) {
         return false;
      }
      // wait for next data block
      int size = 0;
      do {
         if (data) {
            delete [] data; data = 0;
         }
         size = fNDS->GetData (&data, 6 * _TIMEOUT);
         // skip reconfig blocks
      } while ((size > 0) && 
              (((DAQDRecHdr*)data)->GPS == (int)0x0FFFFFFFF));
      // error
      if (size < 0) {
         cerr << "error during receiving" << endl;
         len = 0;
         delete [] data; data = 0;
         return false;
      }
      // eof
      else if (size == 0) {
         close();
         cerr << "eof" << endl;
         len = 0;
         delete [] data; data = 0;
         return false;
      }
      // received next data block
      else {
         len = size;
         return true;
      }
   }

   //////////////////////////////////////////////////////////////////////////
   //                                                                      //
   //                        readChnDAQServer                              //
   //                                                                      //
   //////////////////////////////////////////////////////////////////////////
   static bool 
   readChnDAQServer (const char* server, int port, frametype ftyp, 
		     DAQDChannelList& chns, unsigned long start,
		     unsigned long stop) 
   {
      if (my_debug) cerr << "sendsio.cc readChnDAQServer(server=" << server << ", port=" << port << "..., start=" << start << " stop=" << stop << ")" << endl ;
      NDS2Socket* daq = new NDS2Socket;
      if (!daq) {
	 cerr << "Unable to construct NDS2Socket object" << endl;
	 return false;
      }
      //daq->setDebug(1);

      int rc = daq->open(server, port);
      if (rc) {
	 cerr << "Unable to open nds2 client to server " << server 
	      << ":" << port << ", rc = " << rc << endl;
	 delete daq;
	 return false;
      }

      //--------------------------------  Decide on channel type from frame type
      sends::chantype ctyp = cUnknown;
      switch (ftyp) {
      case FF:
	 ctyp = cRaw;
	 break;
      case MTF:
	 ctyp = cMTrend;
	 break;
      case STF:
	 ctyp = cSTrend;
	 break;
      case RDS:
	 ctyp = cRDS ;
	 break ;
      default:
	 break;
      }

      //-------------------------------  Set the epoch if start and stop are specified.
      if (start != stop)
      {
	 if (my_debug) cerr << " readChnDaqServer() - calling SetEpoch(" << start << ", " << stop << ")" << endl ;
	 daq->SetEpoch(start, stop) ;
      }

      //--------------------------------  Get the channel list
      // If we send the epoch start time as the GPS time to Available, we get the channel list at the
      // time of the start of the epoch.  This won't be the complete list for the epoch, only the channels
      // that existed at the start.  Instead, pass a gps time of 0.  This forces the server to give us a
      // complete list of channels for the epoch.
      DAQC_api::chan_list c;
      if (my_debug) cerr << " readChnDaqServer() - calling Available(ctyp, " << "0" << ", c) " << endl ;
      // rc holds the number of channels available if >= 0, otherwise an error code.
      rc = daq->Available (ctyp, 0, c);
      // Get reduced data set channels from NDS2.
      if (ctyp == cRaw) {
	 int nRDS = daq->addAvailable (cRDS, 0, c);
	 if (nRDS > 0) rc += nRDS;
      }
      if (rc < 0) {
	 cerr << "  No nds2 channels available, rc = " << rc << endl;	 
      }
      else {
	 if (my_debug) cerr << "  nds2 channels available, rc = " << rc << endl ;
	 // copy the channels from the chan_list to the DAQDChannelList.
	 for (DAQC_api::const_channel_iter i=c.begin(); i != c.end(); ++i) {
#ifdef TEST_RATE
	    ChannelNameRate nr(i->mName, i->mRate) ;
	    chns.insert (DAQDChannelList::value_type (nr, *i)) ;
#else
	    ////////////////////////////////////////////////////////////////////////////////////////
	    /// This is the spot that channels with the same name but different rates disappear. ///
	    ///  The insert is a stl call that uses the channel name as the key, so duplicate    ///
	    ///  names can't exist in the list.                                                  ///
	    ////////////////////////////////////////////////////////////////////////////////////////
	    chns.insert (DAQDChannelList::value_type (i->mName, *i));
#endif
	 }
      }
      delete daq;
      bool retval = rc >= 0 ;
      if (my_debug) cerr << "sendsio.cc readChnDAQServer() returns " << (retval ? "true)" : "false)") << endl ;
      return rc >= 0;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// readTimeDAQServer						        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   static bool readTimeDAQServer (const char* server, int port,
				  DAQTimeList& time) 
   {
      unsigned long f1, f2, s1, s2, m1, m2;
      bool rc = true;
      NDS2Socket* daq = new NDS2Socket(server, port);
      if (daq->Times (cUnknown, f1, f2, _TIMEOUT) ||
	  daq->Times (cSTrend,  s1, s2, _TIMEOUT) ||
	  daq->Times (cMTrend,  m1, m2, _TIMEOUT)) {
         rc = false;
      }
      delete daq;
      if (rc) {
	time.fStart = Time (f1);
	time.fStop = time.fStart + Interval (f2);
	time.fStartTrend = Time (s1);
	time.fStopTrend = time.fStartTrend + Interval (s2);
	time.fStartMinuteTrend = Time (m1);
	time.fStopMinuteTrend = time.fStartMinuteTrend + Interval (m2);
      }
      return rc;
   }

}
