/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "PConfig.h"
#include "ndsio.hh"
#include "DAQSocket.hh"
#include "framefast/frametype.hh"
#include "framefast/framefast.hh"
#include <iostream>
#include <cstring>
#include <cstdlib>
#include <algorithm>

#include <rmorg.h>

#define _TIMEOUT 600	/* 10 min */

namespace fantom {
   using namespace std;
   using namespace framefast;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// channel list chache						        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   typedef std::pair <std::string, int> ndsserver;
   typedef std::map <std::string, DAQDChannel> DAQDChannelList;
   typedef std::map <ndsserver, DAQDChannelList> ndschannelcache;
   static ndschannelcache gNdsChnCache; // global

   struct DAQTimeList {
      Time fStart;
      Time fStop;
      Time fStartTrend;
      Time fStopTrend;
      Time fStartMinuteTrend;
      Time fStopMinuteTrend;
   };


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Forwards							        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   static bool readChnDAQServer (const char* server, int port,
                     DAQDChannelList& chns);
   static bool readTimeDAQServer (const char* server, int port,
                     DAQTimeList& time);

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// utility functions                                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   static string trim (const char* p)
   {
      while (isspace (*p)) p++;
      string s = p;
      while ((s.size() > 0) && isspace (s[s.size()-1])) {
         s.erase (s.size() - 1);
      }
      return s;
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// nds_support		                                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   const int nds_support::kNDSPORT = 8088;

//______________________________________________________________________________
   nds_support::nds_support (const char* servername, 
                     const char* conf) 
   : fPort (kNDSPORT), fType (FF), fNDS (0)
   {
      setServer (servername);
      setConf (conf);
   }

//______________________________________________________________________________
   nds_support::~nds_support()
   {
      close();
   }

//______________________________________________________________________________
   void nds_support::setServer (const char* servername)
   {
      string n = trim (servername ? servername : "");
      fAddr = "";
      fPort = kNDSPORT;
      fType = FF;
      string::size_type pos = n.find ('/');
      if (pos != string::npos) {
         if (strcmp (n.c_str() + pos, "/trend") == 0) {
            fType = STF;
         }
         else if (strcmp (n.c_str() + pos, "/minute-trend") == 0) {
            fType = MTF;
         }
         n.erase (pos);
      }
      if ((pos = n.find (':')) != string::npos) {
         fPort = atoi (n.c_str() + pos + 1);
         n.erase (pos);
      }
      fAddr = trim (n.c_str());
   }

//______________________________________________________________________________
   void nds_support::setConf (const char* conf)
   {
      // nothing
   }

//______________________________________________________________________________
   bool nds_support::selectChannels (const channelquerylist* chns)
   {
      // check if connection is open
      if (!fNDS) {
         if (!open()) {
            close();
            return false;
         }
      }
      // get channel list
      ndsserver ns (fAddr.c_str(), fPort);
      ndschannelcache::iterator l = gNdsChnCache.find (ns);
      if (l == gNdsChnCache.end()) {
         return false;
      }
      // Add normal channels to nds
      if (fType == FF) {
         for (DAQSocket::Channel_iter i = l->second.begin();
             i != l->second.end(); ++i) {
            if (!chns || chns->empty() || 
		(chns->findMatch (i->second.mName) != 0)) {
               //cerr << "add a channel " << i->second.mName << endl;
               fNDS->AddChannel (i->second);
            }
         }
      }
      // Add trend channels to nds
      else {
         for (DAQSocket::Channel_iter i = l->second.begin();
             i != l->second.end(); ++i) {
            // generate all derived names
            string max = string (i->second.mName) + ".max";
            string mean = string (i->second.mName) + ".mean";
            string min = string (i->second.mName) + ".min";
            string n = string (i->second.mName) + ".n";
            string rms = string (i->second.mName) + ".rms";
            string stddev = string (i->second.mName) + ".stddev";
            DAQDChannel c = i->second;
            // :KLUDGE: misuse group id for std. dev. calculation!!
            c.mGroup = 0; 
            // add all if no extension is specified
            if (!chns || chns->empty() || 
               (chns->findMatch (i->second.mName) != 0)) {
               strcpy (c.mName, max.c_str());
               fNDS->AddChannel (c);
               strcpy (c.mName, mean.c_str());
               fNDS->AddChannel (c);
               strcpy (c.mName, min.c_str());
               fNDS->AddChannel (c);
               strcpy (c.mName, n.c_str());
               fNDS->AddChannel (c);
               // :KLUDGE: misuse group id for std. dev. calculation!!
               c.mGroup = 2; 
               strcpy (c.mName, rms.c_str());
               fNDS->AddChannel (c);
            }
            // check individual
            else if (chns->findMatch (max) != 0) {
               strcpy (c.mName, max.c_str());
               fNDS->AddChannel (c);
            }
            else if (chns->findMatch (mean) != 0) {
               strcpy (c.mName, mean.c_str());
               fNDS->AddChannel (c);
            }
            else if (chns->findMatch (min) != 0) {
               strcpy (c.mName, min.c_str());
               fNDS->AddChannel (c);
            }
            else if (chns->findMatch (n) != 0) {
               strcpy (c.mName, n.c_str());
               fNDS->AddChannel (c);
            }
            else if (chns->findMatch (rms) != 0) {
               strcpy (c.mName, rms.c_str());
               fNDS->AddChannel (c);
            }
            // std. dev.!
            else if (chns->findMatch (stddev) != 0) {
               // :KLUDGE: misuse group id for std. dev. calculation!!
               // mGroup = 0: normal channel
               // mGroup = 1: forget channel
               // mGroup = 2: calculate std. dev. (rms only)
               // mGroup = 3: calculate std. dev. and forget (rms only)
               strcpy (c.mName, mean.c_str());
               DAQSocket::Channel_iter j = fNDS->mChannel.find (c.mName);
               if (j == fNDS->mChannel.end()) {
                  c.mGroup = 1; 
                  fNDS->AddChannel (c);
               }
               strcpy (c.mName, n.c_str());
               j = fNDS->mChannel.find (c.mName);
               if (j == fNDS->mChannel.end()) {
                  c.mGroup = 1; 
                  fNDS->AddChannel (c);
               }
               strcpy (c.mName, rms.c_str());
               j = fNDS->mChannel.find (c.mName);
               if (j == fNDS->mChannel.end()) {
                  c.mGroup = 3; 
                  fNDS->AddChannel (c);
               }
               else {
                  j->second.mGroup = 2;
               }
            }
         }
      }
      return true;
   }

//______________________________________________________________________________
   framefast::basic_frame_storage* nds_support::readFrame ()
   {
      // check if connection is open
      if (!fNDS) {
         if (!open()) {
            close();
            cerr << "Unable to open connection" << endl;
            return 0;
         }
      }
      // check if request has been sent
      if (!fNDS->isOn()) {
         if (!request()) {
            close();
            cerr << "Unable to send request" << endl;
            return 0;
         }
      }
      // get data
      char* buf = 0;
      int buflen;
      if (!getdata (buf, buflen)) {
         close();
         cerr << "Unable to obtain data" << endl;
         return 0;
      }
      // copy data into a frame
      framewriter* fr = 0;
      DAQDRecHdr*	head = (DAQDRecHdr*) buf;
      char*		dptr = buf + sizeof (DAQDRecHdr);
      // generate frame
      fr = new (nothrow) framewriter (head->Secs);
      if (fr == 0) {
         delete [] buf;
         cerr << "Unable to create frame" << endl;
         return 0;
      }
      fr->setTime (Time (head->GPS, 0));
      // fill detector structure
      detector_t det;
      string chan0_name;
      DAQSocket::Channel_iter iter = fNDS->mChannel.begin();
      if (iter != fNDS->mChannel.end()) chan0_name = iter->second.mName;
      if (!chan0_name.empty()) {
         // LHO 
         // :TODO: check values
         if (chan0_name[0] == 'H') {
            strcpy (det.fName, "LIGO_1");
            det.fLongitudeD = -119;
            det.fLongitudeM = 24;
            det.fLongitudeS = 27.5657005310059;
            det.fLatitudeD = 46;
            det.fLatitudeM = 27;
            det.fLatitudeS = 0.565699994564056;
            det.fElevation = 142.554000854492;
            det.fArmXAzimuth = 2.19910407066345;
            det.fArmYAzimuth = 3.76990103721619;
         }
         // LLO
         else if (chan0_name[0] == 'L') {
            strcpy (det.fName, "LIGO_2");
            det.fLongitudeD = -90;
            det.fLongitudeM = 46;
            det.fLongitudeS = 27.2653999328613;
            det.fLatitudeD = 30;
            det.fLatitudeM = 33;
            det.fLatitudeS = 46.4196014404297;
            det.fElevation = -6.57399988174438;
            det.fArmXAzimuth = 3.45080399513245;
            det.fArmYAzimuth = 4.49800109863281;
         }
         fr->setDetectorInfo (det);
      }
   
      // go through channels
      const real_8_t* keepMean = 0;
      const real_8_t* keepRms = 0;
      const int_4s_t* keepN = 0;
      for (DAQSocket::Channel_iter iter = fNDS->mChannel.begin();
          iter != fNDS->mChannel.end(); ++iter) {
         // compute data pointers
         const char* values = dptr;
         int len = 0;
         int_2u_t dtype = (int_2u_t)-1;
         data_t val;
         strncpy (val.fADC.fName, iter->first.c_str(), maxName-1);
         val.fADC.fName[maxName-1] = 0;
         string::size_type pos = string::npos;
         int mGroup = 0;
         bool forget = false;
         bool calc_stddev = false;
         // normal data channel
         if (fType == FF) {
            len = iter->second.mRate * head->Secs;
            dptr += len * iter->second.mBPS;
            val.fADC.fNBits = iter->second.mBPS * 8;
            switch (iter->second.mDatatype) {
               case 1: 
                  {
                     dtype = typeID_int_2s_t;
                     break;
                  }
               case 2: 
                  {
                     dtype = typeID_int_4s_t;
                     break;
                  }
               case 3: 
                  {
                     dtype = typeID_int_8s_t;
                     break;
                  }
               case 4: 
                  {
                     dtype = typeID_real_4_t;
                     break;
                  }
               case 5: 
                  {
                     dtype = typeID_real_8_t;
                     break;
                  }
               case 6: 
                  {
                     dtype = typeID_complex_8_t;
                     break;
                  }
            }
         }
         // trend channels
         else {
            // :KLUDGE: misuse group id for std. dev. calculation!!
            // mGroup = 0: normal channel
            // mGroup = 1: forget channel
            // mGroup = 2: calculate std. dev. (rms only)
            // mGroup = 3: calculate std. dev. and forget (rms only)
            mGroup = iter->second.mGroup;
            forget = (mGroup & 0x01) != 0;
            len = head->Secs / (fType == MTF ? 60 : 1);
            if ((pos = iter->first.rfind (".max")) != string::npos) {
               dptr += sizeof (int_4s_t) * len;
               dtype = typeID_int_4s_t;
               val.fADC.fNBits = 32;
            }
            else if ((pos = iter->first.rfind (".mean")) != string::npos) {
               dptr += sizeof (real_8_t) * len;
               dtype = typeID_real_8_t;
               val.fADC.fNBits = 64;
               keepMean = (real_8_t*) values;
               keepN = 0; keepRms = 0;
            }
            else if ((pos = iter->first.rfind (".min")) != string::npos) {
               dptr += sizeof (int_4s_t) * len;
               dtype = typeID_int_4s_t;
               val.fADC.fNBits = 32;
            }
            else if ((pos = iter->first.rfind (".n")) != string::npos) {
               dptr += sizeof (int_4s_t) * len;
               dtype = typeID_int_4s_t;
               val.fADC.fNBits = 32;
               keepN = (int_4s_t*) values;
            }
            else if ((pos = iter->first.rfind (".rms")) != string::npos) {
               dptr += sizeof (real_8_t) * len;
               dtype = typeID_real_8_t;
               val.fADC.fNBits = 64;
               calc_stddev = (mGroup & 0x02) != 0;
               keepRms = (real_8_t*) values;
            }
            // unrecognized trend channel
            else {
               break;
            }
         }
         // check if data is within limits of data buffer
         if ((char*)dptr > buf + sizeof (DAQDRecHdr) + buflen) {
            break;
         }
         // fill in units, slope and bias
         val.fADC.fBias = iter->second.mOffset;
         val.fADC.fSlope = iter->second.mGain * iter->second.mSlope;
         if (strlen (iter->second.mUnit) == 0) {
            strcpy (val.fADC.fUnit, "#");
         }
         else {
            strncpy (val.fADC.fUnit, iter->second.mUnit, maxName - 1);
         val.fADC.fName[maxName-1] = 0;
         }
         // fill in rest
         val.fADC.fSampleRate = (double)len / head->Secs;
         val.fADC.fDataValid = 0;
         val.fVect.fNDim = 1;
         val.fVect.fNx[0] = len;
         val.fVect.fDx[0] = 1. / val.fADC.fSampleRate;
         val.fVect.fStartX[0] = 0;
         strcpy (val.fVect.fUnitX[0], "s");
         strcpy (val.fVect.fUnitY, val.fADC.fUnit);
         // add data to frame
         val.allocate (dtype, len);
         val.fVect.fill (0, len, values, littleendian());
         if (!forget) {
            if (!fr->addData (val.fADC, val.fVect)) {
               ;
            }
         
         }
         // calculate std. dev.
         if (calc_stddev && keepMean && keepN && keepRms) {
            real_8_t* stddev = new real_8_t [len];
            if (stddev) {
               for (int k = 0; k < len; k++) {
                  // :TRICKY: work around alignment and byte-order
                  real_8_t xmean;
                  memcpy (&xmean, ((char*)keepMean) + k*sizeof (real_8_t), 
                         sizeof (real_8_t));
                  int_4s_t xn;
                  memcpy (&xn, ((char*)keepN) + k*sizeof (int_4s_t), 
                         sizeof (int_4s_t));
                  real_8_t xrms;
                  memcpy (&xrms, ((char*)keepRms) + k*sizeof (real_8_t), 
                         sizeof (real_8_t));
                  if (littleendian()) {
                     swap (&xmean);
                     swap (&xn);
                     swap (&xrms);
                  }
                  if (xn <= 1) {
                     stddev[k] = 0;
                  }
                  else {
                     stddev[k] = 
                        sqrt (((double)xn) / ((double) xn - 1.0) *
                             (xrms*xrms - xmean*xmean));
                  }
               }
               val.fVect.fill (0, len, (const char*)stddev, false);
               char* p = strstr (val.fADC.fName, ".rms");
               if (p) {
                  strcpy (p, ".stddev");
                  fr->addData (val.fADC, val.fVect);
               }
            }
            delete [] stddev;
         }
         else {
            keepMean = 0; keepN = 0; keepRms = 0;
         }
      }
      // delete buffer
      delete [] buf;
      buf = 0;
      // complete frame
      if (!fr->next()) {
         delete fr;
         cerr << "Unable to complete data" << endl;
         return 0;
      }
      // write frame to memory
      memory_out* mem = new (nothrow) memory_out ();
      if (!fr->write (mem)) {
         delete mem; 
         mem = 0;
      }
      delete fr;
   
      //cerr << "frame size = " << (mem && mem->data() ? mem->size() : -1) << endl;
      if (!mem || !mem->data()) {
         cerr << "Unable to write frame to memory buffer" << endl;
      }
      return mem;
   }

//______________________________________________________________________________
   bool nds_support::eof() const
   {
      return fNDS == 0;
   }

//______________________________________________________________________________
   bool nds_support::getChannels (const char* server, int port,
                     channellist& chns, frametype utype)
   {
      //cerr << "get channels from " << server << " @ " << port << endl;
      ndsserver ns (server, port);
      DAQDChannelList ndschns;
      DAQDChannelList::const_iterator begin;
      DAQDChannelList::const_iterator end;
      ndschannelcache::iterator i = gNdsChnCache.find (ns);
      // get channel names (check cache first)
      if (i != gNdsChnCache.end()) {
         begin = i->second.begin();
         end = i->second.end();
         //cerr << "load from cache" << endl;
      }
      // get names from NDS server
      else if (readChnDAQServer (server, port, ndschns)) {
         gNdsChnCache.insert (ndschannelcache::value_type (ns, ndschns));
         begin = ndschns.begin();
         end = ndschns.end();
         //cerr << "load from server" << endl;
      }
      else {
         cerr << "load failed" << endl;
         return false;
      }
      // copy channels into return list
      chns.clear();
      for (DAQDChannelList::const_iterator i = begin; i != end; ++i) {
         int chn_num = i->second.mNum;
         bool is_excitation = ((chn_num > 1) && (chn_num < TP_ID_LSC_TP_OFS)) || ((chn_num >= TP_ID_ASC_EX_OFS) && (chn_num < TP_ID_ASC_TP_OFS));
	       channelentry chn (i->second.mName, i->second.mRate, 0, is_excitation);
         if (chn.Active()) {
            // normal channels
            if (true || (utype == FF)) {
               chns.push_back (chn);
            }
            // trend channels
            else {
               float rate = (utype == STF) ? 1 : 1./60.;
               string cname = string (i->second.mName) + ".max";
               chn = channelentry (cname.c_str(), rate);
               chns.push_back (chn);
               cname = string (i->second.mName) + ".mean";
               chn = channelentry (cname.c_str(), rate);
               chns.push_back (chn);
               cname = string (i->second.mName) + ".min";
               chn = channelentry (cname.c_str(), rate);
               chns.push_back (chn);
               cname = string (i->second.mName) + ".n";
               chn = channelentry (cname.c_str(), rate);
               chns.push_back (chn);
               cname = string (i->second.mName) + ".rms";
               chn = channelentry (cname.c_str(), rate);
               chns.push_back (chn);
               cname = string (i->second.mName) + ".stddev";
               chn = channelentry (cname.c_str(), rate);
               chns.push_back (chn);
            }
         }
      }
      SortChannels (chns);
      return true;
   }

//______________________________________________________________________________
   bool nds_support::getChannels (channellist& chns) const
   {
      return getChannels (fAddr.c_str(), fPort, chns, fType);
   }

   bool is_not_excitation(fantom::channelentry &ce) {
     return !ce.IsExcitation();
   }

    bool nds_support::getExcitationChannels(channellist& chns, channellist& full_chns)
    {

      chns = full_chns;
      auto new_end = std::remove_if(chns.begin(), chns.end(), is_not_excitation);
      chns.resize(new_end - chns.begin());
      return true;
    }

//______________________________________________________________________________
   bool nds_support::getTimes (const char* server, int port,
                     Time& start, Time& stop, frametype utype)
   {
      //cerr << "get times from " << server << " @ " << port << endl;
      DAQTimeList times;
      if (!readTimeDAQServer (server, port, times)) {
         return false;
      }
      switch (utype) {
         default:
         case FF: 
            {
               start = times.fStart;
               stop = times.fStop;
               break;
            }
         case STF: 
            {
               start = times.fStartTrend;
               stop = times.fStopTrend;
               break;
            }
         case MTF: 
            {
               start = times.fStartMinuteTrend;
               stop = times.fStopMinuteTrend;
               break;
            }
      }
      return true;
   }

//______________________________________________________________________________
   bool nds_support::getTimes (Time& start, Time& stop) const
   {
      return getTimes (fAddr.c_str(), fPort, start, stop, fType);
   }

//______________________________________________________________________________
   bool nds_support::open ()
   {
      if (fNDS) {
         close();
      }
      fNDS = new (nothrow) DAQSocket (fAddr.c_str(), fPort);
      return (fNDS != 0) && fNDS->isOpen();
   }

//______________________________________________________________________________
   void nds_support::close()
   {
      if (fNDS) {
         delete fNDS;
         fNDS = 0;
      }
   }

//______________________________________________________________________________
   bool nds_support::request ()
   {
      if (!fNDS) {
         return false;
      }
      if ((fT0 == Time (0, 0)) || (fDt <= Interval (0.))) {
         return false;
      }
   
      // determine GPS time
      Time::ulong_t start = fT0.getS();
      Time::ulong_t duration  = fDt.GetS();
      while (Time (start + duration, 0) < fT0 + fDt) {
         duration++;
      }
      int r = 0; // return

      switch (fType) {
         case FF:
            {
               r = fNDS->RequestData (start, duration, _TIMEOUT);
               break;
            }
         case STF:
            {
               r = fNDS->RequestTrend (start, duration, false, _TIMEOUT);
               break;
            }
         case MTF:
            {
               r = fNDS->RequestTrend (start, duration, true, _TIMEOUT);
               break;
            }
         default:
            {
               return false; 
            }
      }
      return r == 0;
   }

//______________________________________________________________________________
   bool nds_support::getdata (char*& data, int& len)
   {
      if (!fNDS) {
         return false;
      }
      // wait for next data block
      int size = 0;
      do {
         if (data) {
            delete [] data; data = 0;
         }
         size = fNDS->GetData (&data, 6 * _TIMEOUT);
         // skip reconfig blocks
      } while ((size > 0) && 
              (((DAQDRecHdr*)data)->GPS == (int)0x0FFFFFFFF));
      // error
      if (size < 0) {
         cerr << "error during receiving" << endl;
         len = 0;
         delete [] data; data = 0;
         return false;
      }
      // eof
      else if (size == 0) {
         close();
         cerr << "eof" << endl;
         len = 0;
         delete [] data; data = 0;
         return false;
      }
      // received next data block
      else {
         len = size;
         return true;
      }
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// readChnDAQServer						        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   static bool readChnDAQServer (const char* server, int port,
                     DAQDChannelList& chns) 
   {
      DAQSocket daq (server, port);
      if (!daq.isOpen()) {
         return false;
      }
      std::vector<DAQDChannel> c;
      if (daq.Available (c) < 0) {
         return false;
      }
      for (std::vector<DAQDChannel>::const_iterator i = c.begin();
          i != c.end(); ++i) {
         chns.insert (DAQDChannelList::value_type (i->mName, *i));
      }
      return true;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// readTimeDAQServer						        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   static bool readTimeDAQServer (const char* server, int port,
                     DAQTimeList& time) 
   {
      unsigned long f1, f2, s1, s2, m1, m2;
      bool rc = true;
      DAQSocket daq (server, port);
      if (!daq.isOpen()) {
         rc = false;
      }
      else if (daq.Times (f1, f2, _TIMEOUT) ||
         daq.TimesTrend (s1, s2, false, _TIMEOUT) ||
         daq.TimesTrend (m1, m2, true, _TIMEOUT)) {
         rc = false;
      }
      if (rc) {
	time.fStart = Time (f1);
	time.fStop = time.fStart + Interval (f2);
	time.fStartTrend = Time (s1);
	time.fStopTrend = time.fStartTrend + Interval (s2);
	time.fStartMinuteTrend = Time (m1);
	time.fStopMinuteTrend = time.fStartMinuteTrend + Interval (m2);
      }
      return rc;
   }


}
