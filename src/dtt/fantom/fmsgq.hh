/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: fmsgq							*/
/*                                                         		*/
/* Module Description: Message queue					*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 4Oct00   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: fmsgq.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_FMSFQ_H
#define _LIGO_FMSFQ_H


#include "Time.hh"
#include "gmutex.hh"
#include <string>
#include <deque>


namespace fantom {


/** @name Message queue
    This header defines MT safe message queue.
   
    @memo Message queue
    @author Written February 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{

/** Message queue class. This class is MT safe.
    
    @memo Message queue.
 ************************************************************************/
   class fmsgqueue {
   public:
      /// Message class (string and time stamp)
      class fmsg {
      public:
         /// constructor
         explicit fmsg (const char* msg = 0, double p1 = 0.0, 
                       double p2 = 0.0, double p3 = 0.0, 
                       double p4 = 0.0);
         /// Get Message string
         const char* msg() const {
            return fMsg.c_str(); }
         /// Get message time
         Time time() const {
            return fTime; }
         /// Get message parameter
         double param(int i) const;
         /// Set message parameter
         void setparam (int i, double p);
      protected:
         /// Message
         std::string	fMsg;
         /// Optional parameters
         double		fParam[4];
         /// Time
         Time		fTime;
      };
   
      /// Constructor
      explicit fmsgqueue (int max = 100) : fMax (max) {
      }
      /// Copy constructor
      explicit fmsgqueue (const fmsgqueue& mq) {
         *this = mq; }
      /// Assignment operator
      fmsgqueue& operator= (const fmsgqueue& mq);
      /// Push a message
      bool push (const char* msg) {
         return push (fmsg (msg)); }
      /// Push a message
      bool push (const fmsg& msg);
      /// Pop a message
      bool pop (fmsg& msg);
      /// Front of queue
      fmsg front () const;
      /// Clear queue?
      void clear();
      /// Queue empty?
      bool empty() const;
      /// Size of queue?
      int size() const;
      /// Set max
      void setmax (int max);
      /// Get max
      int max() const;
   
   protected:
      /// Mutex
      mutable thread::mutex	fMux;
      /// Message queue
      std::deque<fmsg> 	fMsg;
      /// Maximum size of message queue (<0 = infinite)
      int		fMax;
   };

//@} 

}

#endif // _LIGO_FMSFQ_H
