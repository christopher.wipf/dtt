#include "framemux.hh"
#include "framefast/framefast.hh"
#include "fchannel.hh"
#include "tconv.h"
#include <iostream>
#include <algorithm>

namespace fantom {
   using namespace std;
   using namespace framefast;


   // Number of quick tests for each full test; 0 = no quick test
   const int kQuickTestRate = 10;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// utility functions                                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   static bool ispoweroftwo (double val)
   { 
      double twomant;
      int twoexp;
   
      twomant = frexp(val, &twoexp);
      if(twomant != 0.5)
         return false;
      else
         return true;
   }

//______________________________________________________________________________
   static double roundtopoweroftwo (double val) 
   {
      if (val == 0) {
         return 0;
      }
      double twomant;
      int twoexp;
      twomant = frexp(val, &twoexp);
      if (twomant == 0.5) {
         return val;
      }
      else {
         return ldexp (1.0, twoexp);
      }
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// outputqueue				                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   outputqueue::outputqueue (const channelquery& query, int len)
   : fLength (len) 
   {
      fRate = fabs (query.Rate());
      // rate must be at least 1 per frame!
      if ((fRate > 0) && (fRate < 1./(double)fLength)) {
         fRate = 1./(double)fLength;
      }
   }

//______________________________________________________________________________
   bool outputqueue::addData (const Time& t, 
                     const framefast::data_t& data,
                     const Interval& offset,
                     const Interval& duration)
   {
      // check if compatible data type: string & multi-dimensional arrays
      if ((data.fVect.fType == 8) || (data.fVect.fNDim != 1)) {
         return false;
      }
      // check if original data rate is compatible with frame rate
      double sample = fabs (data.fADC.fSampleRate);
      double mod = fmod (sample + 1E-8, 1./(double)fLength);
      if ((sample < 1E-7) || (mod > 1E-7)) {
         return false;
      }
      // make sure the new rate is a multiple of the frame rate
      // and can be derived from the sampling rate through
      // simple binning or straight interpolation
      double rate;
      if (fRate == 0) { // keep sample rate
         rate = sample;
      }
      // treat 2^N separate
      else if ((fRate >= 1) && ispoweroftwo (sample)) {
         rate = roundtopoweroftwo (fRate);
      }
      // all others
      else {
         rate = fRate;
         int n = (int) (sample * (double)fLength + 0.5);
         int m = (int) (rate * (double)fLength + 0.5);
         if (m == 0) m = 1;
         // interpolation
         if (m > n) {
            int r = (int) ((double)m/(double)n + 0.5);
            rate = sample * (double)r;
         }
         // decimation
         else {
            while ((m < n) && (n % m != 0)) {
               m++;
            }
            int r = (int) ((double)n/(double)m + 0.5);
            rate = sample / (double)r;
         }
      }
      // calculate transfer parameters
      Time time (t + offset);
      int i0 = offset * sample;
      int N = data.fVect.fNData;
      if (N > duration * sample) N = duration * sample;
      // Time time (t);
      // int N = data.fVect.fNData;
      double mul = rate / sample;
      int dN = (int) (fLength * rate + 0.5);
      if (dN <= 0) {
         return false;
      }
      int N1 = 0;	// # of samples at beginning (not fitting)
      int N2 = 0;	// # of samples at end (not fitting)
      if (mul < 1) {
         double dT = 1. / rate; 
         int div = (int) (1. / mul + 0.5);
         double tFoffs = t.getS() % fLength + t.getN() / 1E9;
         double toffs = fmod (tFoffs, dT);
         N1 = (int) (toffs / (dT / div) + 0.5);
         if (N1 > 0) N1 = div - N1;
         if (N1 > N) N1 = N;
         Time e = t + Interval (N / sample);
         tFoffs = e.getS() % fLength + e.getN() / 1E9;
         toffs = fmod (tFoffs, dT);
         N2 = (int) (toffs / (dT / div) + 0.5);
         if (N2 == div) N2 = 0;
         if (N2 > N - N1) N2 = N - N1;
      }
      int M = (int) ((N - N1 - N2) * mul + 0.5); // whole # of points in dest
      // cerr << "copy param: rate = " << rate << " mul = " << mul << 
         // " M = " << M << " dN = " << dN << endl;
      // cerr << "            len = " << fLength << " N1 = " << N1 << " N2 = " << N2 << endl;
      // cerr << "            wanted = " << fRate << " rate = " << rate << " sample = " << sample << endl;
   
      // transfer data frame-by-frame
      int i = i0;
      while (i - i0 < N) {
         // make sure we have a buffer
         Time checktime = time + Interval (1E-8);
         if (empty() || (checktime >= back().nexttime())) {
            // make new buffer
            Time t0 ((checktime.getS() / fLength) * fLength);
            outputbuffer buf (t0, Interval ((double)fLength));
            // fill in default parameters
            buf.clone (data);
            // make sure dimension parameters are set right
            buf.fVect.fNx[0] = dN;
            buf.fVect.fDx[0] = 1. / rate;
            // check for sample rate conversion
            int_2u_t type = data.fVect.fType;
            if (fabs (rate - sample) > 1E-8) {
               buf.fADC.fSampleRate = rate;
               if ((data.fVect.fType == 6) || (data.fVect.fType == 7)) {
                  type = 6;
               }
               else {
                  type = 3;
               }
            }
            // add to queue
            push (buf);
            // set data length and allocate memory
            // do this after push to avoid the big copy!
            if (!back().allocate (type, dN)) {
               return false;
            }
         }
         // determine time difference between first data point
         // in input and next data point in output
         Time t0 = back().fTime + 
            Interval ((double)back().fSoFar / rate);
         if (back().fPartialSoFar) {
            int div = (int) (1. / mul + 0.5);
            t0 += Interval (((double)back().fPartialSoFar/
                            (double)div) / rate);
         }
         Interval gap (time - t0);
         // ignore duplicate data
         if ((double)gap < -1E-8) {
            int dup = (int)((double)gap * sample + 0.5);
            time += dup / rate;
            i += dup;
            if (mul >= 1) {
               continue;
            }
            int div = (int) (1. / mul + 0.5);
            if (dup < N1) {
               N1 -= dup;
               continue;
            }
            dup -= N1;
            N1 = 0;
            M -= dup / div;
            dup -= div * (dup / div);
            if ((M < 0) || ((M == 0) && (dup >= N2))) {
               M = 0;
               N2 = 0;
               break;
            }
            if (dup == 0) {
               continue;
            }
            if (M == 0) {
               N1 = N2 - dup;
               N2 = 0;
            }
            else {
               N1 = div - dup;
               --M;
            }
            continue;
         }
         // bridge over gaps
         else if ((double)gap > 1E-8) {
            // warning message
            static Time oldt0 (0);
            static Interval oldgap (0, 0);
            if ((oldt0 == Time (0)) || 
               (oldt0 != t0) || (oldgap != gap)) {
               cerr << "Warning: gap (" << gap << " sec) from " << 
                  t0 << " to " << time << endl;
               oldt0 = t0;
               oldgap = gap;
            }
            // bridge
            Interval dT = time - back().fTime;
            int div =  (mul < 1) ? (int)(1. / mul + 0.5) : 1;
            int P = (int) ((double)dT * ((double)div * rate) + 0.5);
            int sofar = back().fSoFar;
            int partsofar = back().fPartialSoFar;
            // cerr << "sofar = " << sofar << " part sofar = " << partsofar <<
               // " P = " << P << " div = " << div << endl;
            back().fSoFar = P / div;
            back().fPartialSoFar = P % div;
            // finish previous partial fill
            if ((sofar < back().fSoFar) && partsofar) {
               back().fill (sofar, 1, back().fPartialData, 0, mul);
            }
            // init new partial fill if necessary
            if ((sofar < back().fSoFar) && back().fPartialSoFar) {
               back().fPartialData.allocate (back().fVect.fType, 
                                    int(div / mul + 0.5) );
            }
            // back().fSoFar += rate * (double)gap + 0.5;
            back().fMissing = true;
         }
      
         // add points from partial beginning
         if (N1 > 0) {
            int div = (int) (1. / mul + 0.5);
            int addpts= (back().fPartialSoFar + N1 > div) ? 
               div - back().fPartialSoFar : N1;
            // init new partial fill if necessary
            if (!back().fPartialSoFar) {
               back().fPartialData.allocate (back().fVect.fType, 
                                    int(div / mul + 0.5));
            }
            // add data pts to partial record
            // cerr << "fill part. points @ " << back().fPartialSoFar << " addpts = " << 
               // addpts << " from data ofs = " << i << endl;
            if (!back().fPartialData.fill (back().fPartialSoFar, addpts, 
                                 data, i, 1)) {
               back().fMissing = true;
            }
            back().fPartialSoFar += addpts;
            // check if partial record is full
            if (back().fPartialSoFar == div) {
               // cerr << "fill points @ " << back().fSoFar << " addpts = 1" << endl;
               if (!back().fill (back().fSoFar, 1, 
                                back().fPartialData, 0, mul)) {
                  back().fMissing = true;
               }
               back().fPartialData.deallocate();
               back().fSoFar++;
               back().fPartialSoFar = 0;
            }
            i += addpts;
            time += addpts / rate;
            N1 -= addpts;
            if ((back().fSoFar == dN) || (N1 > 0)) {
               continue;
            }
         }
      
         // add data points
         if (M > 0) {
            int addpts = (back().fSoFar + M > dN) ? dN - back().fSoFar : M;
            // cerr << "fill points @ " << back().fSoFar << " addpts = " << 
               // addpts << " from data ofs = " << i << endl;
            if (!back().fill (back().fSoFar, addpts, data, i, mul)) {
               back().fMissing = true;
            }
            back().fSoFar += addpts;
            i += (int)(addpts / mul + 0.5);
            time += addpts / rate;
            M -= addpts;
            if (back().fSoFar == dN) {
               continue;
            }
         }
      
         // add points from partial end
         if ((M == 0) && (N2 > 0)) {
            int div = (int) (1. / mul + 0.5);
            int addpts= (back().fPartialSoFar + N2 > div) ? 
               div - back().fPartialSoFar : N2;
            // init new partial fill if necessary
            if (!back().fPartialSoFar) {
               back().fPartialData.allocate (back().fVect.fType, 
                                    int(div / mul + 0.5));
            }
            // add data pts to partial record
            // cerr << "fill part. points @ " << back().fPartialSoFar << " addpts = " << 
               // addpts << " from data ofs = " << i << endl;
            if (!back().fPartialData.fill (back().fPartialSoFar, addpts, 
                                 data, i, 1)) {
               back().fMissing = true;
            }
            back().fPartialSoFar += addpts;
            // check if partial record is full
            if (back().fPartialSoFar == div) {
               // cerr << "fill points @ " << back().fSoFar << " addpts = 1" << endl;
               if (!back().fill (back().fSoFar, 1, 
                                back().fPartialData, 0, mul)) {
                  back().fMissing = true;
               }
               back().fPartialData.deallocate();
               back().fSoFar++;
               back().fPartialSoFar = 0;
            }
            i += addpts;
            time += addpts / rate;
            N2 -= addpts;
         }
      }
      return true;
   }

//______________________________________________________________________________
   bool outputqueue::writeData (const Time& time, 
                     framefast::framewriter* fr)
   {
      if ((fr == 0) || empty()) {
         return false;
      }
      if (time < front().fTime) {
         return false;
      }
      front().fADC.fDataValid = front().fMissing;
      //cerr << "add " << endl << front().fADC << endl << front().fVect << endl;
      bool ret = fr->addData (front().fADC, front().fVect);
      pop();
      return ret;
   }

//______________________________________________________________________________
   bool outputqueue::ready (Time& t) const
   {
      if (empty()) {
         return false;
      }
      else {
         t = front().fTime;
         return true;
      }
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// channelqueue				                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   bool channelqueue::ready (Time& t) const
   {
      bool ret = false;
      Time ttemp;
      for (const_iterator i = begin(); i != end(); ++i) {
         if (i->second.ready (ttemp)) {
            if (!ret || (ttemp < t)) {
               t = ttemp;
               ret = true;
            }
         }
      }
      return ret;
   }

//______________________________________________________________________________
   outputqueue* channelqueue::getChannel (const char* name, 
                     const channelquery* query)
   {
      string n (name);
      for (string::iterator i = n.begin(); i != n.end(); ++i) {
         *i = toupper (*i);
      }
      iterator i = find (n);
      if (i == end()) {
         if (query) {
            pair <iterator, bool> ins =
               insert (value_type (n, outputqueue (*query, frameLength())));
            return (ins.second) ? &ins.first->second : 0;
         }
         else {
            return 0;
         }
      }
      else {
         return &i->second;
      }
   }

//______________________________________________________________________________
   void channelqueue::channelPurge()
   {
      for (iterator i = begin(); i != end(); ) {
         if (i->second.empty()) {
            erase (i++);
         }
         else {
            ++i;
         }
      }
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// channelmux				                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   class channelmux {
   public:
      /// Constructs a channel transfer object
      channelmux (framefast::framereader& in, const char* name,
                 const Time& time, const Interval& offset,
                 const Interval& duration, framefast::int_8u_t pos, 
                 framefast::datatype_t dtype, channelqueue& out, 
                 const channelquery& query)
      : fTime (time), fOffset (offset), fDuration (duration),
      fPos (pos), fDatatype (dtype), fIn (&in) {
         fOut = out.getChannel (name, &query);
      }
      /// gets position
      framefast::int_8u_t pos() const {
         return fPos; }
      /// get type
      framefast::datatype_t type() const {
         return fDatatype; }
      /// read the data
      bool read() {
         data_t dat;
         if (fIn && fOut && fIn->getData (dat, fPos, fDatatype)) {
            // cerr << dat.fADC << endl;
            // cerr << dat.fVect << endl;
            return fOut->addData (fTime, dat, fOffset, fDuration);
         }
         else {
            return false;
         }
      }
      /// debug summary
      void debug() const {
         cerr << "time = " << fTime.getS() << "  pos = " << fPos << endl; }
   
   protected:
      /// Time of data
      Time			fTime;
      /// Time offset of start in data
      Interval			fOffset;
      /// Time duration of data
      Interval			fDuration;
      /// position of data structure
      framefast::int_8u_t	fPos;
      /// type of data structure
      framefast::datatype_t	fDatatype;
      /// Input frame
      framefast::framereader*	fIn;
      /// Output queue
      outputqueue*		fOut;
   };

//______________________________________________________________________________
   class cpyorder {
   public:
      bool operator() (const channelmux* c1, const channelmux* c2) const {
         return c1->pos() < c2->pos(); }
   };



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// framemux				                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   framemux::framemux (smart_ilist& in, smart_olist& out, 
                     const bool* ctrlC)
   : fDebug (0), fCtrlC (ctrlC), fClock (0), fIn (&in), fOut (&out)
   {
   }

//______________________________________________________________________________
   framemux::~framemux ()
   {
      flushOutput();
   }

//______________________________________________________________________________
   framefast::framereader* framemux::oldest()
   {
      framereader* old = 0;
      for (smart_ilist::iterator i = fIn->begin(); i != fIn->end(); i++) {
         if (i->second->eof()) {
            continue;
         }
         int n = i->second->getFrameNum();
         for (int j = 0; j < n; ++j) {
            framereader* fr = i->second->getFrameIn (j);
            if (fr == 0) {
               continue;
            }
            if ((old == 0) ||
               (fr->starttime() < old->starttime())) {
               old = fr;
            }
         }
      }
      return old;
   }

//______________________________________________________________________________
   double framemux::process ()
   {
      // check if clock beyond stop time
      if ((fStop > Time(0)) && (fClock >= fStop)) {
         return 0; 
      }
      // make sure we have set the clock
      if (!fClock) {
         // tainsec_t t0 = TAInow();
         // wait for frames to become available
         for (smart_ilist::iterator i = fIn->begin(); 
             i != fIn->end(); i++) {
            if (!i->second->wait (fCtrlC)) {
               return 0;
            }
         }
         // look for oldest frame
         framereader* old = oldest();
         if (old == 0) {
            return -1;
         }
         fClock = old->starttime();
         if (fDebug) cerr << "set clock to " << fClock << endl;
         // tainsec_t t1 = TAInow();
         // cerr << "Initial wait " << (t1-t0)/1E6 << " ms" << endl;
      }
   
      // sync inputs
      // tainsec_t t0 = TAInow();
      framereader* old = 0;
      do {
         // wait for frames to become available
         for (smart_ilist::iterator i = fIn->begin(); 
             i != fIn->end(); i++) {
            if (!i->second->wait (fClock, fCtrlC)) {
               return 0;
            }
         }
         // look for oldest frame
         old = oldest();
         // if nothing ready, assume everything is done
         if (old == 0) {
            return 0;
         }
         // done if beyond stop time
         else if ((fStop > Time(0)) && (old->starttime() >= fStop)) {
            if (old->starttime() > fClock) {
               fClock = old->starttime();
            }
            return 0;
         }
         // readjust clock, if gap in data
         else if (old->starttime() > fClock) {
            fClock = old->starttime();
         }
         // discard out-of-sync frames
         else if (old->starttime() < fClock) {
            for (smart_ilist::iterator i = fIn->begin(); 
                i != fIn->end(); i++) {
               if (i->second->eof()) {
                  continue;
               }
               int n = i->second->getFrameNum();
               for (int j = 0; j < n; ++j) {
                  framereader* fr = i->second->getFrameIn (j);
                  if (fr == 0) {
                     continue;
                  }
                  if (fr->nexttime() <= fClock) {
                     if (fDebug) cerr << "Frame " << fr->starttime() << 
                           "-" << fr->nexttime() << 
                           " out of sync with clock " << fClock << endl;
                     if (!i->second->next (j)) {
                        return 0;
                     }
                  }
               }
            }
         }
      } while (old->nexttime() <= fClock);
      // tainsec_t t1 = TAInow();
      // cerr << "wait for input " << (t1-t0)/1E6 << " ms" << endl;
   
      Time rstat0 = Now();
   
      // process frames of same age
      Interval duration = old->nexttime() - fClock;
      //cerr << "Duration of frame = " << (double)duration << endl;
      bool firstframe = true;
      //cerr << "process an input frame" << endl;
      for (smart_ilist::iterator i = fIn->begin(); 
          i != fIn->end(); i++) {
         // eof?
         if (i->second->eof()) {
            if (fDebug) cerr << "eof" << endl;
            continue;
         }
         // Loop through child frame streams
         int n = i->second->getFrameNum();
         for (int j = 0; j < n; ++j) {
            // any frame at all?
            framereader* fr = i->second->getFrameIn (j);
            if (fr == 0) {
               Time nexttime = i->second->nexttime (j);
               // check duration against next frame
               if ((fClock < nexttime) &&
                  (fClock + duration > nexttime)) {
                  duration =  nexttime - fClock;
               }
               if (fDebug) cerr << "no more input frames" << endl;
               continue;
            }
         
            // found one!
            if (fr->starttime() <= fClock) {
               if (fDebug) cerr << "frame found @ GPS " << fClock << endl;
               // set detector info & run number, if first
               if (firstframe) {
                  detector_t det;
                  if (fr->getDetectorInfo (det)) {
                     for (smart_olist::iterator i = fOut->begin(); 
                         i != fOut->end(); ++i) {
                        i->second->setDetectorInfo (det);
                     }
                  }
                  if (fDebug > 2) cerr << "detector info:" << endl << det << endl;
                  const toc_t* toc = fr->getTOC();
                  if (toc && (toc->fNFrame > 0) && toc->fFrames) {
                     for (smart_olist::iterator i = fOut->begin(); 
                         i != fOut->end(); ++i) {
                        i->second->setRunNum (toc->fFrames[0].fRun);
                     }   
                  }
                  if (fDebug > 2) cerr << "TOC:" << endl << *toc << endl;
                  firstframe = false;
               }
               Interval ofs = fClock - fr->starttime();
               Interval dur = fr->nexttime() - fClock;
               if ((fStop > Time(0)) && (fr->nexttime() > fStop)) {
                  dur = fStop > fClock ? fStop - fClock : Interval(0);
               }
               if (dur < duration) duration = dur;
               // do the data copy
               readData (i->first, i->second->getFrameIn (j), ofs, dur);
               if (!i->second->next (j)) {
                  return 0;
               }
            }
            // check duration, if newer frame is waiting
            else if (fClock + duration > fr->starttime()) {
               duration =  fr->starttime() - fClock;
            }
         }
      }
      // tainsec_t t2 = TAInow();
      // cerr << "process time " << (t2-t1)/1E6 << " ms" << endl;
   
      // set clock and flush output
      //cerr << "old clock = " << fClock << endl;
      fClock += duration;
      //cerr << "new clock = " << fClock << endl;
      flushOutput (fClock);
      // tainsec_t t3 = TAInow();
      // cerr << "flush Output " << (t3-t2)/1E6 << " ms" << endl;
   
      Time rstat1 = Now();
      static Interval accum = 0.0;
      static int n = 0;
      accum += (rstat1 - rstat0);
      n++;
      if (n % 10 == 0) {
         cerr << "Number of processed frames = " << n << endl;
         cerr << "Average time of processing = " << ((double)accum / (double)n) 
            << " sec " << endl;
      }
   
      return duration;
   }

//______________________________________________________________________________
   bool framemux::readData (int innum, framefast::framereader* fr,
                     const Interval& offset, const Interval& duration)
   {
      //dumpframe (cerr, fr->frame(), fr->length());
      // tainsec_t t0 = TAInow();
      // get TOC
      if (fr == 0) {
         fMsg = "Error: Unable to access frame";
         return false;
      }
      int nframe = fr->nframe();
      Time tStart = fr->starttime() + offset;
      const toc_t* toc = fr->getTOC();
      if (toc == 0) {
         fMsg = "Error: Frame does not have a table of contents";
         return false;
      }
      int NData = 0;
      for (int i = 0; i < 5; ++i) {
         NData += toc->fNData[i];
      }
      if ((fOut->size() <= 0) || (NData <= 0) || 
         (nframe <= 0)) {
         return true;
      }
      smart_input* in = fIn->Get (innum);
      if (!in) {
         fMsg = "Error: Unable to access input";
         return false;
      }
      const channelquerylist* iq = in->getChannelList();
      // tainsec_t t1 = TAInow();
      // cerr << "readData: init " << (t1-t0)/1E6 << " ms" << endl;
   
      // generate copy list
      int cpymax = NData * nframe * fOut->size();
      channelmux** cpylist = new (nothrow) channelmux* [cpymax];
      if (cpylist == 0) {
         fMsg = "Error: Not enough memory for copy list";
         return false;
      }
      int cpylen = 0;
      // loop over output frames
      bool error = false;
      for (smart_olist::iterator i = fOut->begin(); 
          i != fOut->end(); ++i) {
         // get channel output queues
         if (i->second->getUser() == 0) {
            framewriter* fw = i->second->getFrameOut();
            int flen = (fw == 0) ? 
               i->second->getFrameLength() : fw->frameLength();
            fQueues.push_back (channelqueue (flen));
            i->second->setUser (&fQueues.back());
         }
         channelqueue* q = (channelqueue*) i->second->getUser();
         // check cache
         outputcache* cache = UpdateCache (innum, i->first, 
                              i == fOut->begin(), toc, iq, 
                              i->second->getChannelList());
         if (!cache) {
            continue; // ?? can't be
         }
         // now loop through cache line
         for (cacheindex::const_iterator iter = cache->fIndex.begin(); 
             iter != cache->fIndex.end(); ++iter) {
            int c = iter->fOffset;
            int cat = iter->fTocCategory;
            int idx = iter->fTocIndex;
            // for each frame add a channelmux to the copy list 
            //cerr << "found " << toc->fData[cat][idx].fName << endl;
            const int_8u_t* pos = toc->fData[cat][idx].fPosition;
            for (int f = 0; f < nframe; f++, pos++) {
               Time t0  = fr->starttime(f);
               Interval dt  = fr->duration(f);
               if (t0 + dt <= tStart) {
                  continue;
               }
               Interval ofs = 0;
               if (t0 + Interval (1E-8) < tStart) {
                  ofs = tStart - t0;
               }
               Interval dur = dt - ofs;
               if (t0 + dt > tStart + duration + Interval (1E-8)) {
                  dur -= (t0 + dt) - (tStart + duration);
               }
               if (cpylen < cpymax) {
                  cpylist[cpylen] = new (nothrow) 
                     channelmux (*fr, toc->fData[cat][idx].fName, t0, 
                                ofs, dur, *pos, (framefast::datatype_t)cat,
                                *q, cache->fQueries[c]);
                  if (!cpylist[cpylen]) {
                     fMsg = "Error: Not enough memory for copy list";
                     error = true;
                  }
                  cpylen++;
               }
            }
         }
      }
      // tainsec_t t3 = TAInow();
      // cerr << "readData: copy list " << (t3-t1)/1E6 << " ms" << endl;
   
      // sort copy list by data position within the frame
      // force sequential read as much as possible
      if (!error) {
         sort (cpylist, cpylist + cpylen, cpyorder());
         if (fDebug > 1) {
            for (int i = 0; i < cpylen; i++) {
               if (cpylist[i]) cpylist[i]->debug();
            }
         }
      }
      // tainsec_t t4 = TAInow();
      // cerr << "readData: sort " << (t4-t0)/1E6 << " ms" << endl;
   
      // read the data
      if (fDebug) cerr << "copy data from input " << cpylen << endl;
      for (int c = 0; c < cpylen; c++) {
         if (cpylist[c]) {
            if (!cpylist[c]->read()) {
               // just ignore: probably just an unsupported data type
               // fMsg = "Error: Not enough memory for copy";
               // error = true;
               // break;
            }
         }
      }
      // tainsec_t t5 = TAInow();
      // cerr << "readData: read " << (t5-t4)/1E6 << " ms" << endl;
   
      // destroy the copy list
      if (fDebug) cerr << "copy data from input done" << endl;
      for (int c = 0; c < cpylen; c++) {
         delete cpylist[c];
      }
      delete [] cpylist;
   
      if (fDebug > 1) {
         cerr << "Number of output queues = " << fQueues.size() << endl;
         for (queuelist::iterator i = fQueues.begin(); 
             i != fQueues.end(); ++i) {
            cerr << "Number of channels in ouput queue = " << i->size() << endl;
            for (channelqueue::iterator j = i->begin();
                j != i->end(); ++j) {
               cerr << "  Number of data segments in channel queue = " << 
                  j->second.size() << endl;
            }
         }
      }
      // tainsec_t t6 = TAInow();
      // cerr << "readData: cleanup " << (t6-t5)/1E6 << " ms" << endl;
      // cerr << "readData: total   " << (t6-t0)/1E6 << " ms" << endl;
   
      return !error;
   }


//______________________________________________________________________________
   bool framemux::flushOutput (const Time& before)
   {
      // cerr << "framemux::flushOutput " << before << endl;
      // loop over output device
      for (smart_olist::iterator i = fOut->begin(); 
          i != fOut->end(); ++i) {
         // check channel queue
         channelqueue* q = (channelqueue*) i->second->getUser();
         if (q) {
            // check for full frame
            framewriter* fw = i->second->getFrameOut();
            if (fw && (fw->status() == framewriter::frameend)) {
               i->second->next (fCtrlC);
            }
         
            // check ready time
            Time tready;
            while (q->ready (tready) && 
                  ((before == Time (0)) ||
                  (tready + Interval (q->frameLength(), 0) <= before))) {
               // cerr << "Time of flush " << tready << "; before = " <<
                  // before << " fLen = " << q->frameLength() << endl;
               // setup the output frame
               fw = i->second->getFrameOut();
               if (fw == 0) {
                  //cerr << "create new frame" << endl;
                  fw = i->second->createFrame();
                  if (fw == 0) {
                     //cerr << "create failed" << endl;
                     break;
                  }
               }
               // write the frame including all ready buffers
               fw->setTime (tready);
               //cerr << "new frame at t = " << tready << endl;
               //cerr << "fw = " << (int) fw << endl;
               for (channelqueue::iterator j = q->begin();
                   j != q->end(); ++j) {
                  j->second.writeData (tready, fw);
               }
               //cerr << "transferred data to new frame" << endl;
               //timespec ttt = {2, 0};
               //nanosleep (&ttt, 0);
               //cerr << "fw = " << (int) fw << endl;
               // move to next frame & check if full frame
               fw->next();
               if (fw->status() == framewriter::frameend) {
                  i->second->next (fCtrlC);
               }
            }
         }
      
         // if before=0, also write unfinished frames
         if (before == Time (0)) {
            i->second->next (fCtrlC);
         }
      }   
      return true;
   }

//______________________________________________________________________________
   void framemux::InvalidateCache()
   {
      fCache.clear();
   }

//______________________________________________________________________________
   void framemux::InvalidateInputCache (int i)
   {
      fCache.erase (i);
      cerr << "Invalidate cache line " << i << endl;
   }

//______________________________________________________________________________
   framemux::outputcache* framemux::UpdateCache (
                     int i, int o, bool firsti,
                     const toc_t* toc, 
                     const channelquerylist* iq, 
                     const channelquerylist* oq)
   {
      // make sure TOC exists
      if (!toc) {
         return 0;
      } 
      cachelist::iterator iter = fCache.find (i);
      // check TOC against channel list (only if first time used)
      if (firsti && (iter != fCache.end())) {
         // first check size ot TOC against length of cached list
         int n = 0;
         for (int k = 0; k < 5; ++k) n += toc->fNData[k];
         bool eq = (int)iter->second.fValues.size() == n;
         // now go through TOC and check the channel names
         if (iter->second.fQuickTest--) {
            // check index only
            for (cacheindex::iterator i = iter->second.fIndex.begin(); 
                eq && (i != iter->second.fIndex.end()); ++i) {
               eq = strcmp (iter->second.fValues[i->fOffset].c_str(),
                           toc->fData[i->fTocCategory]
                           [i->fTocIndex].fName) == 0;
            }
         }
         else {
            // check full list
            n = 0;
            int m = 0;
            for (int k = 0; eq && (k < 5); ++k) {
               m = toc->fNData[k];
               for (int j = 0; eq && (j < m); ++j) {
                  // check for equal channel name but only if used
                  if (iter->second.fUsed[n+j]) {
                     eq = strcmp (iter->second.fValues[n+j].c_str(),
                                 toc->fData[k][j].fName) == 0;
                  }
               }
               n += toc->fNData[k];
            }
            iter->second.fQuickTest = kQuickTestRate;
         }
         // if TOC has changed, invalidate the input cache
         if (!eq) {
            InvalidateInputCache (i);
            iter = fCache.end();
         }
      }
      // make new input cache, if necessary
      if (iter == fCache.end()) {
         cerr << "UpdateCache: new input cache " << i << endl;
         // mark input cache lines 
         iter = fCache.insert 
            (cachelist::value_type (i, inputcache())).first;
         iter->second.fQuickTest = kQuickTestRate;
      
         // build channel and selected list
         const char* name;
         int n;
         for (int k = 0; k < 5; ++k) {
            n = toc->fNData[k];
            for (int j = 0; j < n; ++j) {
               name = toc->fData[k][j].fName;
               // add name
               iter->second.fValues.push_back (name);
               // add select
               iter->second.fSelect.push_back
                  ((iq == 0) || iq->findMatch (name));
            }
         }
         // initialize used channel list
         n = iter->second.fValues.size();
         iter->second.fUsed.assign (n, false);
      } 
      // check if we need to make an output cache
      cacheline::iterator jter = iter->second.fHits.find (o);
      if (jter == iter->second.fHits.end()) {
         cerr << "UpdateCache: new output cache " << o << endl;
         jter = iter->second.fHits.insert (cacheline::value_type 
                              (i, outputcache())).first;
         // initialize output channel hits with selected input channels
         int n = iter->second.fValues.size();
         jter->second.fHits = iter->second.fSelect;
         jter->second.fQueries.assign (n, channelquery("*", 0));
         // mark output cache line
         const channelquery* chn = 0;
         for (int j = 0; j < n; ++j) {
            // do we need to check the output channel list?
            if (jter->second.fHits[j] && (oq != 0)) {
               chn = oq->findMatch (iter->second.fValues[j].c_str());
               if (chn) {
                  jter->second.fQueries[j] = *chn; // hit
               }
               else {
                  jter->second.fHits[j] = false;   // miss
               }
            }
            // if used inf output, make sure we update the used list
            if (jter->second.fHits[j]) {
               iter->second.fUsed[j] = true;
            }
         }
         // make index
         n = 0;
         int m = 0;
         iter->second.fIndex.clear();
         jter->second.fIndex.clear();
         cacheindexentry entry;
         for (int k = 0; k < 5; ++k) {
            m = toc->fNData[k];
            for (int j = 0; j < m; ++j) {
               entry.fOffset = n + j;
               entry.fName = iter->second.fValues[n+j].c_str();
               entry.fTocCategory = k;
               entry.fTocIndex = j;
               // add to input if used
               if (iter->second.fUsed[n+j]) {
                  iter->second.fIndex.push_back (entry);
                  cerr << "Index = " << entry.fName << endl;
               }
               // add to output if hit
               if (jter->second.fHits[n+j]) {
                  jter->second.fIndex.push_back (entry);
               }   
            }
            n += toc->fNData[k];
         }
      }
      return &jter->second;
   }


}
