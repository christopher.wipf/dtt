#include <time.h>
#include <stdio.h>
#include "robotctrl.hh"
#include "pipe_exec.hh"
#include <cstdlib>

namespace fantom {
   using namespace std;


   // 30 minutes timeout total
   static const int _ROBOT_TIMEOUT = 1800;
   // 10 minutes for changing tapes
   static const int _ROBOT_CHANGE_TIMEOUT = 600;
   // 5 sec wait before changing tapes
   static const int _ROBOT_INITIAL_WAIT = 5;
   // 30 sec wait after tape has changed
   static const int _ROBOT_FINAL_WAIT = 30;
   // 2 minutes wait between reties
   static const int _ROBOT_RETRY_WAIT = 120;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// dfmtape							        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   robot_ctrl::robot_ctrl (const char* mtdev, const char* conf) 
   : fDone (false), fSlotFirst (1), fSlotLast (5), 
   fSlotCur (-1), fTapesTotal (5), fTapesIndex (0), fDevNum(0)
   {
      setConf (mtdev, conf);
   }

//______________________________________________________________________________
   robot_ctrl::~robot_ctrl () 
   {
   }

//______________________________________________________________________________
   bool robot_ctrl::setConf (const char* mtdev, const char* conf)
   {
      // initialize
      fMtPath = mtdev ? mtdev : "";
      fConf = conf ? conf : "";
      fDone = false;
      fSlotFirst = 1;
      fSlotLast = 5;
      fSlotCur = -1;
      fTapesTotal = 5;
      fTapesIndex = 0;
      fDevNum = 0;
      fScript = "";
      if (!conf) {
         return true;
      }
      // parse
      string::size_type pos = fConf.find_first_of ("@#");
      if (pos == string::npos) {
         fScript = fConf;
      }
      else {
         fScript = fConf.substr (0, pos);
      }
      int expo = 1;
      while (!fScript.empty() && (expo < 100000) &&
            isdigit (fScript[fScript.size()-1])) {
         fDevNum += expo * (fScript[fScript.size()-1] - '0');
         expo *= 10;
         fScript.erase (fScript.size()-1);
      }
      if (!fScript.empty()) {
         fScript += ".robot";
      }
      // no parameters: quit
      if (pos == string::npos) {
         fDone = fScript.empty();
         return !fDone;
      }
      // parse slot numbers
      if (fConf[pos] == '@') {
         // first slot
         string s = fConf.substr (pos + 1);
         fSlotFirst = atoi (s.c_str());
         pos = s.find_first_of (":");
         if (pos == string::npos) {
            fSlotLast = fSlotFirst;
         }
         // last slot
         else {
            s = s.substr (pos + 1);
            fSlotLast = atoi (s.c_str());
            // slot index
            pos = s.find_first_of (":");
            if (pos != string::npos) {
               fTapesIndex = atoi (s.c_str() + pos + 1) - fSlotFirst;
            }
         }
      }
      // parse number of tapes
      pos = fConf.find_last_of ("#");
      if (pos != string::npos) {
         fTapesTotal = atoi (fConf.c_str() + pos + 1);
      }
      else {
         fTapesTotal = fSlotLast - fSlotFirst + 1;
      }
      // check arguments
      if (fScript.empty() || fMtPath.empty() || (fSlotFirst < 0) ||
         (fSlotLast < fSlotFirst) ||
         (fTapesIndex < 0) || (fTapesTotal <= 0)) {
         fDone = true;
      }
      cerr << "script = " << fScript << endl;
      cerr << "fSlotFirst = " << fSlotFirst << endl;
      cerr << "fSlotLast = " << fSlotLast << endl;
      cerr << "fTapesIndex = " << fTapesIndex << endl;
      cerr << "fTapesTotal = " << fTapesTotal << endl;
      cerr << "fDevNum = " << fDevNum << endl;
      cerr << "fMtPath = " << fMtPath << endl;
      return !fDone;
   }

//______________________________________________________________________________
   bool robot_ctrl::next()
   {
      if (fDone) {
         return false;
      }
      // behind last ?
      if (fTapesIndex > fTapesTotal) {
         fDone = true;
         return false;
      }
      // if not first unload old ?
      int range = fSlotLast - fSlotFirst + 1;
      char uload[32] = "";
      if (fSlotCur >= 0) {
         sprintf (uload, " -u %i", fSlotCur);
      }
      // increase index by one
      fSlotCur = fSlotFirst + fTapesIndex % range;
      fTapesIndex++;
      // load if not last
      char load[32] = "";
      if (fTapesIndex <= fTapesTotal) {
         sprintf (load, " -l %i", fSlotCur);
      }
      // manual loading/unloading
      bool manual = (fScript == "MAN.robot");
      time_t stop = time (0) + (manual ? 0 : _ROBOT_TIMEOUT);
      timespec wait = {_ROBOT_INITIAL_WAIT, 0};
      do {
            // wait a little while to let the system settle down
         nanosleep (&wait, 0);
         wait.tv_sec = _ROBOT_RETRY_WAIT;
            // start script
         char cmd[1024];
         sprintf (cmd, "%s -d %i -f %s%s%s", fScript.c_str(), 
                 fDevNum, fMtPath.c_str(), uload, load);
         cerr << "Cmd = " << cmd << endl;
         pipe_exec pe (cmd);
         if (!pe) {
            cerr << "Could not start script" << endl;
            return false;
         }
            // wait for robot
         int ret = 0;
         if ((pe.wait ((manual ? -1 : _ROBOT_CHANGE_TIMEOUT), &ret) > 0) && 
            (ret == 0)) {
            fDone = (fTapesIndex > fTapesTotal);
            cerr << "return value is " << ret << endl;
            wait.tv_sec = _ROBOT_FINAL_WAIT;
            nanosleep (&wait, 0);
            return !fDone;
         }
      } while (time (0) < stop);
   
      return false;
   }


}
