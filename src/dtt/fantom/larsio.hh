/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: larsio							*/
/*                                                         		*/
/* Module Description: LARS support for smartio				*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 3Mar01   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: larsio.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_LARSIO_H
#define _LIGO_LARSIO_H

#include "iosupport.hh"
#include "fchannel.hh"
#include "fname.hh"
#include "refcount.hh"
#include <string>
#include <iosfwd>

//namespace std {
   //class pipe_exec;
//}

namespace fantom {

   class namerecord;


/** @name LARS support
    This header defines support methods for reading from the LIGO
    archive server (LARS).
   
    @memo LARS support
    @author Written March 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{


/** LARS IO class. This is a support class for the smart LARS input
    class.
    
    @memo LARS IO support class.
 ************************************************************************/
   class lars_support : public iosupport {
   public:
      /// Data interval list
      typedef std::map <Time, Interval> dataseglist;
      /// Data segment list iterator
      typedef dataseglist::iterator dsegiter;
      /// UDN list
      typedef std::vector <std::string> udnlist;
      /// UDN list iterator
      typedef udnlist::iterator udniter;
   
      /// Create LARS support
      explicit lars_support (const char* udn = 0, const char* conf = 0);
      /// Destructor
      virtual ~lars_support();
   
      /// Set UDN
      void setUDN (const char* udn);
      /// Get UDN
      const char* getUDN() const {
         return fUDN.c_str(); }
      /// Set configuration
      void setConf (const char* conf);
   
      /// Get frame type
      frametype getType() const {
         return fType; }
   
      /// Set channel selection
      bool selectChannels (const channelquerylist* chns);
      /// Read next frame into buffer (not used!)
      virtual framefast::basic_frame_storage* readFrame () {
         return 0; }
      /// Get number of frame streams
      virtual int getFrameStreamNum();
      /// Get URL of next frame in input stream N
      virtual std::string getFrameUrl (int N);
      /** Get a name record associated with stream N (owned by caller!)
          You can use the getNext method of the namerecord
          to obtain the list of URL returned by this frame stream.
          Calling this function with refcount = true will setup up 
          a reference counter to this object, ie., when the last
          namrecord gets deleted so does this object.
        */
      virtual namerecord* getNameRecord (int N, bool refcount = true);
   
      /// Get frame writer (output not supported!)
      virtual framefast::basic_frameout* getWriter (
                        const char* fname) {
         return 0; }
      /// End of file
      virtual bool eof() const;
   
      /** Checks and sets username/password; 
          uses last/default login if NULL arguments are supplied
        */
      static bool login (const char* udn, const char* uname, 
                        const char* pword);
      /// login
      bool login (const char* uname, const char* pword);
      /// Get frame type, list of channels and time intervals
      static bool getInfo (const char* udn, frametype& utype,
                        channellist& chns, dataseglist& dsegs);
      /// Get frame type, list of channels and time intervals
      bool getInfo (frametype& utype, 
                   channellist& chns, dataseglist& dsegs);
   
      /// Get list of UDNs
      static bool getUDNList (udnlist& udns);
   
   protected:
      /// Frame type (FF, MTF or STF)
      frametype		fType;
      /// UDN
      std::string	fUDN;
      /// Channel list
      channelquerylist	fChannels;
   
      /// Open connection
      bool open ();
      /// Close connection
      void close();
      /// request data
      bool request ();
      // set username/password
      bool setlogin (const char* uname, const char* pword);
      /// Get list of UDNs
      bool getUDNs (udnlist& udns);
      /// Get UDN channel info
      bool getUDNchns (channellist& chns);
      /// Get UDN time info
      bool getUDNtimes (dataseglist& dsegs);
   
   private:
      /// Reference counter type
      typedef static_ref_counter<lars_support> refcounter_type;
   
      /// LARS connection open
      bool		fOpen;
      /// Number of input streams
      int		fStreamNum;
      /// LARS connection
      std::iostream*	fLars;
      /// LARS connection socket
      int		fSock;
      /// Refernce counter for 
      refcounter_type	fRefCount;
   };


//@}

}

#endif // _LIGO_LARSIO_H
