#!/bin/bash

# file menu
xdotool mousemove $1 $2
xdotool mousedown 1
xdotool mouseup 1

# save as`
sleep 1
xdotool mousemove_relative 30 111
xdotool mousedown 1
xdotool mouseup 1

# try to hit file menu again fast
xdotool mousemove $1 $2
xdotool mousedown 1
xdotool mouseup 1

## click on graphics tab
#xdotool mousemove_relative 991 827
#xdotool mousedown 1
#xdotool mouseup 1