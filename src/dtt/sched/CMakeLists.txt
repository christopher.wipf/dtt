CREATE_RPC_TARGET(gdsrsched)

SET(SCHED_SRC
    gdssched_client.c
    gdssched_server.c
    gdssched_util.c
    gdssched.c
    gdsxdr_util.c
)

add_library(sched_lib OBJECT
    ${SCHED_SRC}
)

target_include_directories(sched_lib PRIVATE
    ${DTT_INCLUDES}    
    ${RPC_OUTPUT_DIR}
)

target_include_directories(sched_lib
        PRIVATE
        SYSTEM
        BEFORE
        ${RPC_INCLUDE_DIR}
        )

add_dependencies(sched_lib
    gdsrsched_rpc
)
