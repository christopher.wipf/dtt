
/* #define PORTMAP */

#ifndef _DEFAULT_SOURCE
#define _DEFAULT_SOURCE 1
#endif

/* Header File List: */

#include <sys/types.h>
#include <pthread.h>

#include "gdsutil.h"
#include "gdsheartbeat.h"
#include "gdssched_util.h"


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Constants: _NETID		  net protocol used for rpc		*/
/*            _DEFAULT_XDR_SIZE   max. length of an XDR'd task arg	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
#define _NETID			"tcp"
#define _DEFAULT_XDR_SIZE	100000	/* 100kByte */


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: threadSpawn					*/
/*                                                         		*/
/* Procedure Description: spawns a new task				*/
/*                                                         		*/
/* Procedure Arguments: attr - 	thread attr. detached/process (UNIX);	*/
/*				all task attr. (VxWorks)		*/
/* 			priority - thread/task priority			*/
/* 			taskIF - pointer to TID (return value)		*/
/* 			task - thread/task function			*/
/* 			arg - argument passed to the task		*/
/*                                                         		*/
/* Procedure Returns: 0 if succesful, -1 if failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   int _threadSpawn (int attr, int priority, schedTID_t* taskID, 
                    _schedtask_t task, _schedarg_t arg)
   {
   #ifdef OS_VXWORKS
      /* VxWorks task */
      *taskID = taskSpawn ("trpcSched", priority, attr, 10000, (FUNCPTR) task, 
                              (int) arg, 0, 0, 0, 0, 0, 0, 0, 0, 0);
      if (*taskID == ERROR) {
         return -1;
      }
   #else
   
      /* POSIX task */
      {
         pthread_attr_t		tattr;
         struct sched_param	schedprm;
         int			status;
      
      	/* set thread parameters: joinable & system scope */
         if (pthread_attr_init (&tattr) != 0) {
            return -1;
         }
         pthread_attr_setdetachstate (&tattr, 
                              attr & PTHREAD_CREATE_DETACHED);
         pthread_attr_setscope (&tattr, attr & PTHREAD_SCOPE_SYSTEM);
      	 /* set priority */
         pthread_attr_getschedparam (&tattr, &schedprm);
         schedprm.sched_priority = priority;
	 pthread_attr_setstacksize(&tattr, 1048576);
         pthread_attr_setschedparam (&tattr, &schedprm);
      
         /* create thread */
         status = pthread_create (taskID, &tattr, task, (void*) arg);
         pthread_attr_destroy (&tattr);
         if (status != 0) {
            return -1;
         }
      }
   #endif
      return 0;
   }

