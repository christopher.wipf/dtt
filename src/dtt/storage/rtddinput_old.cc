/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: rttdinput						*/
/*                                                         		*/
/* Module Description: reads in channel data through the RTDD interface	*/
/* implements decimation and zoom functions, partitions the data and	*/
/* stores it in a storage object					*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

//#define DEBUG

// Header File List:
#include <signal.h>
#include <pthread.h>
#include <math.h>
#include <errno.h>
#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include "gdsprm.h"
#include "rtddinput.h"
#if defined (_RTDD_PROVIDER) && (_RTDD_PROVIDER  != 2)
#include "gdsrtdd.h"
#endif
#include "gdsconst.h"
#include "decimate.h"
#include "gdssigproc.h"
#include "gdstask.h"
#include "map.h"
#if defined (_CONFIG_DYNAMIC)
#include "confinfo.h" 
#endif


namespace diag {
   using namespace std;


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Constants: PRM_FILE		  parameter file name			*/
/*            PRM_SECTION	  section heading is channel name!	*/
/*            PRM_SERVERNAME	  entry for server name			*/
/*            PRM_SERVERPORT	  entry for server port			*/
/*            DAQD_SERVER	  default server name for channel info	*/
/*            DAQD_PORT		  default server port for channel info	*/
/*            __ONESEC		  one second (in nsec)			*/
/*            _PREPROC_STARTUP	  startup factor for preprocessing	*/
/*            _PREPROC_CONTINUE	  continuation factor for preprocessing	*/
/*            _MIN_NDS_DELAY	  minimum delay allowed for NDS (sec)	*/
/*            _MAX_NDS_DELAY	  maximum delay allowed for NDS (sec)	*/
/*            _NDS_DELAY	  NDS delay for slow data (sec)		*/
/*            taskNdsName	  nds task priority			*/
/*            taskNdsPriority	  nds task name				*/
/*            taskNdsUpdateName	  nds update task priority		*/
/*            taskNdsUpdatePriority nds update task name		*/
/*            taskCleanupName	  nds clenaup task priority		*/
/*            taskCleanupPriority nds cleanup task name			*/
/*            decimationflag	  flags used for decimation filter	*/
/*            daqBufLen		  length of receiving socket buffer	*/
/*            								*/
/*----------------------------------------------------------------------*/
#define _CHNLIST_SIZE		200
#if !defined (_CONFIG_DYNAMIC)
#define PRM_FILE		gdsPathFile ("/param", "nds.par")
#define PRM_SECTION		gdsSectionSite ("nds")
#define PRM_SERVERNAME		"hostname"
#define PRM_SERVERPORT		"port"
#define DAQD_SERVER		"fb0"
#define DAQD_PORT		8088
#endif
#define __ONESEC		1E9
#define _PREPROC_STARTUP	10
#define _PREPROC_CONTINUE	2
#define _MIN_NDS_DELAY		0.0
#define _MAX_NDS_DELAY		1.0
#define _NDS_DELAY		1

   const char	taskNdsName[] = "tNDS";
   const int	taskNdsPriority = 0;
   const char	taskNdsUpdateName[] = "tNDSupdate";
   const int	taskNdsUpdatePriority = 4;
   const char	taskCleanupName[] = "tNDScleanup";
   const int	taskCleanupPriority = 20;
   const int	decimationflag = 1;
   const int 	daqBufLen = 1024*1024;
   const long	taskNdsGetDataTimeout = 10;

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Class Name: rtddCallback						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   rtddCallback::rtddCallback (const string& Chnname) 
   : chnname (Chnname), idnum (-1) 
   {
   }

   rtddCallback::~rtddCallback () 
   {
      unsubscribe ();
   }


   rtddCallback::rtddCallback (const rtddCallback& rtddcb) 
   {
      *this = rtddcb;
   }


   rtddCallback& rtddCallback::operator= (const rtddCallback& rtddcb) 
   {
      if (this != &rtddcb) {
         semlock	lockit (mux);
         idnum = -1;
         chnname = rtddcb.chnname;
      }
      return *this;
   }


   bool rtddCallback::subscribe (tainsec_t start, 
                     tainsec_t* active) 
   {
      semlock		lockit (mux);	// lock mutex
   
      // check if already subscribed
      if (idnum >= 0) {
         return true;
      }
      // subscribe
   #if !defined (_RTDD_PROVIDER) || (_RTDD_PROVIDER  == 2)
      idnum = 1;
   #else
      idnum = gdsSubscribeData (chnname.c_str(), 0, 
                           (gdsDataCallback_t) callbackC, this);
   #endif
      // set active time
      if (active != 0) {
         *active = max (start, TAInow());
      }
      return (idnum >= 0);
   }


   bool rtddCallback::unsubscribe () 
   {
      semlock		lockit (mux);	// lock mutex
   
      // unsubscribe
      if (idnum >= 0) {
      #if defined (_RTDD_PROVIDER) && (_RTDD_PROVIDER  != 2)
         gdsUnsubscribeData (idnum);
      #endif
         idnum = -1;
      }
      return true;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Class Name: rtddChannel::partition					*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   rtddChannel::partition::partition (
                     string Name, tainsec_t Start, tainsec_t Duration, 
                     double Dt, tainsec_t tp) 
   : name (Name), start (Start), duration (Duration), 
   precursor (tp), dt (Dt), decimate1 (1), decimate2 (1),
   zoomstart (0), zoomfreq (0), removeDelay (true), 
   decdelay (0.0), delaytaps (0), timedelay (0.0), done (false)
   {
      length = (int) ((double) duration / _ONESEC / dt + 0.5);
   }


   rtddChannel::partition::partition (const partition& p) 
   {
      *this = p;
   }


   void rtddChannel::partition::setup (double Dt, 
                     int Decimate1, int Decimate2, 
                     tainsec_t Zoomstart, double Zoomfreq, bool rmvDelay)
   {
      dt = Dt;
      decimate1 = Decimate1;
      decimate2 = Decimate2;
      zoomstart = Zoomstart;
      zoomfreq = Zoomfreq;
      removeDelay = rmvDelay;
      length = (int) ((double) duration / _ONESEC / dt + 0.5);
   }


   void rtddChannel::partition::fill (
                     const float data[], int len, int bufnum) 
   {
   #ifdef DEBUG
      cerr << "size = " << buf[bufnum].size() << " len = " << len << endl;
   #endif
      if ((bufnum < 0) || (bufnum >= 2)) {
         return;
      }
      int ncopy = min (len, length - (int) buf[bufnum].size());
      buf[bufnum].insert (buf[bufnum].end(), data, data + ncopy);
   #ifdef DEBUG
      cerr << "size = " << buf[bufnum].size() << " length = " << length << endl;
   #endif
   }


   void rtddChannel::partition::copy (
                     float data[], int max, bool cmplx) 
   {
      int 		len;          
      if (cmplx) {
         len = min (buf[0].size(), buf[1].size());
      } 
      else {
         len = buf[0].size();
      }
   
      for (int i = 0; i < len; i++) {
         if (i >= max) {
            return;
         }
         if (!cmplx) {
            data[i] = buf[0][i];
         }
         else {
            data[2*i] = buf[0][i];
            data[2*i+1] = buf[1][i];
         }
      }
   }


   int rtddChannel::partition::index (
                     tainsec_t Start, int Length) const
   {
      tainsec_t 	time;	// time of first sample needed
      time = start + (tainsec_t) (buf[0].size() * dt * _ONESEC);
   #ifdef DEBUG
      cerr << time << ":" << Start << "|" << Start - time << endl;
   #endif
      if (time < Start - (tainsec_t) (_ONESEC * dt / 2.0)) {
         // failure: gap in data
         cerr << "gap in data dt = " << (Start - time) / 1E9 << endl;
         return -2;
      }
      else if (time >= Start + Length * dt * _ONESEC) {
         // not yet
         return -1;
      }
      else {
         return (int) ((double) (time - Start) / 
                      (double) _ONESEC / dt + 0.5);
      }
   }


   int rtddChannel::partition::range (
                     tainsec_t Start, int Length) const       
   {  
      int		ndx;
   
      if ((ndx = index (Start, Length)) < 0) {
         return 0;
      }
      else {
      #ifdef DEBUG
         cerr << "Length = " << Length << " length = " << length <<
            " buf[0].size = " << buf[0].size() << endl;
      #endif
         return min (Length - ndx, length - (int) buf[0].size());
      }
   }


   inline void rtddChannel::partition::nomore ()
   {
      done = true;
   }


   inline bool rtddChannel::partition::isFull () const 
   {                 
      return ((int)buf[0].size() >= length);
   }


   inline bool rtddChannel::partition::isDone () const      
   {                 
      return isFull() || done;
   }


   inline bool rtddChannel::partition::operator< 
   (const partition& p) const 
   {           
      return (start < p.start);
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Class Name: rtddChannel::preprocessing				*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   rtddChannel::preprocessing::preprocessing (int dataRate, 
                     int Decimate1, int Decimate2, 
                     tainsec_t Zoomstart, double Zoomfreq,
                     bool rmvDelay)
   : datarate (dataRate), decimate1 (Decimate1), decimate2 (Decimate2),
   zoomstart (Zoomstart), zoomfreq (Zoomfreq), removeDelay (rmvDelay),
   useActiveTime (false), start (0), stop (0), bufTime (0), bufSize (0),
   t0Grid (0), buf (0), hpinit (false), hpval (0), 
   tmpdelay1 (0), tmpdec1 (0), tmpdec2I (0), tmpdec2Q (0)
   {
      float		tmp[1];
   
      // set time constants
      dt[0] = 1.0 / (double) datarate;
      dt[1] = dt[0] * decimate1;
      dt[2] = dt[1] * decimate2;
   
      // set decimation filter delay constants
      int dec = decimate1 * decimate2;
      decdelay = firphase (decimationflag, dec) / TWO_PI * dt[0];
      if (removeDelay) {
         int sDelay = (int)(firphase (decimationflag, dec) / TWO_PI + 0.5);
         int tDelay = dec * (int)((sDelay + dec - 1) / dec);
         delaytaps = tDelay - sDelay;
         delayshift = 
            (tainsec_t) ((double) tDelay * dt[0] * __ONESEC + 0.5);
         delay1 = (tainsec_t) 
            ((firphase (decimationflag, decimate1) / TWO_PI +
             (double) delaytaps) * dt[0] * __ONESEC + 0.5);
      #ifdef DEBUG
         cerr << "dec=" << dec << " sD=" << sDelay << " tD=" << tDelay << 
            " dtaps=" << delaytaps << " dshift=" << delayshift <<
            " decd=" << decdelay << " d1=" << delay1 << endl;
      #endif
      }
      else {
         delaytaps = 0;
         delayshift = 0;
         delay1 = 0;
      }
   
      // new memory for buffer
      if (dec >= 0) {
         buf = new (nothrow) float [dec];
      }
   
      // new temp memory for delay / decimation filter
      timedelay (tmp, tmp, 0, delaytaps, 0, &tmpdelay1);
      decimate (decimationflag, tmp, tmp, 0, decimate1, 0, &tmpdec1);
      decimate (decimationflag, tmp, tmp, 0, decimate2, 0, &tmpdec2I);
      decimate (decimationflag, tmp, tmp, 0, decimate2, 0, &tmpdec2Q);
   }


   rtddChannel::preprocessing::preprocessing (const preprocessing& p) 
   : buf (0), hpinit (false), hpval (0), tmpdelay1 (0), tmpdec1 (0), 
   tmpdec2I (0), tmpdec2Q (0)
   {
      *this = p;
   }


   rtddChannel::preprocessing& rtddChannel::preprocessing::operator= (
                     const preprocessing& p)
   {
      if (this != &p) {
         float		tmp[1];
      
         // release buffer memory
         if (buf != 0) {
            delete [] buf;
         }
         // release old temp memory for decimation filter
         if (tmpdelay1 != 0) {
            timedelay (tmp, tmp, 0, delaytaps, tmpdelay1, 0);
            tmpdelay1 = 0;
         }
         if (tmpdec1 != 0) {
            decimate (decimationflag, tmp, tmp, 0, decimate1, tmpdec1, 0);
            tmpdec1 = 0;
         }
         if (tmpdec2I != 0) {
            decimate (decimationflag, tmp, tmp, 0, decimate2, tmpdec2I, 0);
            tmpdec2I = 0;
         }
         if (tmpdec2Q != 0) {
            decimate (decimationflag, tmp, tmp, 0, decimate2, tmpdec2Q, 0);
            tmpdec2Q = 0;
         }
      
         // copy parameters
         datarate = p.datarate;
         decimate1 = p.decimate1;
         decimate2 = p.decimate2;
         zoomstart = p.zoomstart;
         zoomfreq = p.zoomfreq;
         memcpy (dt, p.dt, sizeof (dt));
         removeDelay = p.removeDelay;
         decdelay = p.decdelay;
         delaytaps = p.delaytaps;
         delayshift = p.delayshift;
         delay1 = p.delay1;
         useActiveTime = p.useActiveTime;
         start = p.start;
         stop = p.stop;
         bufTime = p.bufTime;
         bufSize = p.bufSize;
         t0Grid = p.t0Grid;
      
         // new memory for buffer
         if (decimate1 * decimate2 >= 0) {
            buf = new (nothrow) float [decimate1 * decimate2];
         }
      
         // new temp memory for decimation filter
         hpinit = false; hpval = 0;
         timedelay (tmp, tmp, 0, delaytaps, 0, &tmpdelay1);
         decimate (decimationflag, tmp, tmp, 0, decimate1, 0, &tmpdec1);
         decimate (decimationflag, tmp, tmp, 0, decimate2, 0, &tmpdec2I);
         decimate (decimationflag, tmp, tmp, 0, decimate2, 0, &tmpdec2Q);
      }
      return *this;
   }


   rtddChannel::preprocessing::~preprocessing ()
   {
      // release buffer memory
      if (buf != 0) {
         delete [] buf;
      }
      // release temp memory for decimation filter
      float		tmp[1];
      if (tmpdelay1 != 0) {
         timedelay (tmp, tmp, 0, delaytaps, tmpdelay1, 0);
         tmpdelay1 = 0;
      }
      if (tmpdec1 != 0) {
         decimate (decimationflag, tmp, tmp, 0, decimate1, tmpdec1, 0);
         tmpdec1 = 0;
      }
      if (tmpdec2I != 0) {
         decimate (decimationflag, tmp, tmp, 0, decimate2, tmpdec2I, 0);
         tmpdec2I = 0;
      }
      if (tmpdec2Q != 0) {
         decimate (decimationflag, tmp, tmp, 0, decimate2, tmpdec2Q, 0);
         tmpdec2Q = 0;
      }
   }


   bool rtddChannel::preprocessing::operator== (
                     const preprocessing& pre) const
   {
      return (decimate1 == pre.decimate1) && 
         (decimate2 == pre.decimate2) &&
         (zoomstart == pre.zoomstart) &&
         (fabs (zoomfreq - pre.zoomfreq) < 1E-6) &&
         (removeDelay == pre.removeDelay);
   }


   void rtddChannel::preprocessing::setActiveTime (
                     tainsec_t Start, tainsec_t Stop,
                     bool obey, bool newValues)
   {
      if (newValues) {
         start = Start;
         stop = Stop;
      }
      else {
         if (start == 0) {
            start = Start;
         }
         else if (Start == -1) {
            start = -1;
         }
         else if (start != -1) {
            start = min (start, Start);
         }
         if (stop == 0) {
            stop = Stop;
         }
         else if (Stop == -1) {
            stop = -1;
         }
         else if (stop != -1) {
            stop = max (stop, Stop);
         }
      }
      useActiveTime = obey;
   }


   bool rtddChannel::preprocessing::operator() (
                     taisec_t time, int epoch,
                     float data[], int ndata, int err,
                     partitionlist& partitions, 
                     mutex& mux, bool& update)
   {
      semlock		lockit (mux);
      int		dec1 = decimate1;
      int		dec2 = decimate2;
      int		dec = dec1 * dec2;
   
      // not enough data points for decimation
      if (ndata < dec) {
         // cerr << "not enough data points (" << ndata << "|" <<
            // bufSize << "|" << dec << ")" << endl;
         // check buffer allocation & size
         if ((buf == 0) || (ndata + bufSize > dec)) {
            bufSize = 0;
            return false;
         }
         // caluclate buffer epoch
         tainsec_t dStart = (tainsec_t) time * _ONESEC + epoch * _EPOCH;
         int	   bufEpoch;
         if (dt[2] >= 1.0 - 1E-12) {
            tainsec_t 	step = (tainsec_t) (dt[2] + 0.5);
            tainsec_t	tsec = 
               step * ((dStart - t0Grid) / _ONESEC / step);
            double rest = (dStart - t0Grid - tsec * _ONESEC) / __ONESEC;
            bufEpoch = (int) (rest / (ndata * dt[0]) + 0.5);
         }
         else {
            tainsec_t	tnsec  = (dStart - t0Grid) % _ONESEC;
            bufEpoch = (int) (((double) tnsec / __ONESEC) / 
                             (ndata * dt[0]) + 0.5);
            bufEpoch %= (int) (dt[2] / (ndata * dt[0]) + 0.5);
         }
         // set buffer start time first time around
         if (bufEpoch == 0) {
            bufTime = dStart;
         }
         // check for error, check start time
         if (err ||  (bufEpoch != bufSize / ndata)) {
            cerr << "buffer ERROR size = " << bufSize << " bufEpoch = " <<
               bufEpoch << endl;
            // ignore if buffer not yet filled
            if ((bufSize == 0) && !err) {
               return true;
            }
            // otherwise call process with error flag set
            else {
               bufSize = 0;
               bool ret = 
                  process (bufTime / _ONESEC, 
                          (bufTime % _ONESEC + _EPOCH / 10) / _EPOCH, 
                          buf, dec, true, partitions, mux, update);
               // if first epoch try to resync; otherwise quit
               if ((bufEpoch != 0) || !ret) {
                  return ret;
               }
            }
         }
      
         // copy data into buffer
         memcpy (buf + bufSize, data, ndata * sizeof (float));
         bufSize += ndata;
      
         // process if buffer is full; else return
         if (bufSize < dec) {
            return true;
         }
         else {
            bufSize = 0;
            return process (bufTime / _ONESEC, 
                           (bufTime % _ONESEC + _EPOCH / 10) / _EPOCH, 
                           buf, dec, false, partitions, mux, update);
         }
      }
      
      // enough data points: data just passed through
      else {
         return process (time, epoch, data, ndata, err, 
                        partitions, mux, update);
      }
   }


   bool rtddChannel::preprocessing::process (
                     taisec_t time, int epoch,
                     float data[], int ndata, int err,
                     partitionlist& partitions, 
                     mutex& mux, bool& update)
   {
      //cerr << "time = " << time << " epoch = " << epoch << endl;
   
      // setup parameters
      tainsec_t	dStart = (tainsec_t) time * _ONESEC + epoch * _EPOCH;
      tainsec_t duration = (tainsec_t) (ndata * dt[0] * __ONESEC);
   
      // check active time
      tainsec_t tpre = (tainsec_t) (decdelay * __ONESEC);
      if (useActiveTime && (start > 0) && 
         (dStart + duration < start - _PREPROC_STARTUP * tpre)) {
         return true;
      }
      if (useActiveTime && (stop > 0) &&
         (dStart > stop + _PREPROC_CONTINUE * tpre)) {
         return true;
      }
   
      mux.lock();
      int		dec1 = decimate1;
      int		dec2 = decimate2;
      tainsec_t		zoom0 = zoomstart;
      double		zoomf = zoomfreq;
      double 		zoomdt = dt[1];
      mux.unlock();
      /* setup temp storage */
      int		size = (zoomf == 0) ? ndata : 2 * ndata;
      float* 		yy1 = new (nothrow) float [size];
      float*		yy2 = new (nothrow) float [size];
      float*		y1 = data;
      float*		y2 = yy1;
      int		len = ndata;
      int		len2;
      // check allocation
      if ((yy1 == 0) || (yy2 == 0)) {
         delete [] yy1;
         delete [] yy2;
         return false;
      }
   #ifdef DEBUG
      cerr << "dec1 = " << dec1 << " dec2 = " << dec2 << 
         " zoom = " << zoomf << " tzoom = " << zoom0 << 
         " dtzoom = " << zoomdt << endl;
   #endif
   
      /* Primitive high pass filter for zoom analysis 
         This 'filter' will take the average from the first received time series
         and subtract this value from all subsequently received ones */
      if (zoomf != 0) {
      	 // first time around: init the hp value
         if (!hpinit) {
            double val = 0;
            for (int i = 0; i < len; i++) {
               val += y1[i];
            }
            hpinit = true;
            hpval = -val / len;
         #ifdef DEBUG
            cerr << "init hp filter to " << hpval << endl;
         #endif
         }
      	 // subtract the hp value from each point
         for (int i = 0; i < len; i++) {
            y2[i] = y1[i] + hpval;
         }
         y1 = y2;
         y2 = (y1 == yy1) ? yy2 : yy1;
      }
      /* delay stage */
      if (removeDelay) {
         timedelay (y1, y2, len, delaytaps, tmpdelay1, &tmpdelay1);
         y1 = y2;
         y2 = (y1 == yy1) ? yy2 : yy1;
      }
      /* first decimation stage */
      if (dec1 > 1) {
         decimate (decimationflag, y1, y2, len, dec1, 
                  tmpdec1, &tmpdec1);
         len = ndata / dec1;
         y1 = y2;
         y2 = (y1 == yy1) ? yy2 : yy1;
      }
      /* zoom stage */
      if (zoomf != 0) {
         double tzoom = (removeDelay) ? 
            (double) (dStart - delay1 - zoom0) / __ONESEC:
            (double) (dStart - zoom0) / __ONESEC;
      #ifdef DEBUG
         cerr << "tzoom = " << tzoom << " len = " << len << endl;
      #endif
         sMixdown (0, y1, NULL, y2, y2+len, len, tzoom, zoomdt, zoomf);
         y1 = y2;
         y2 = (y1 == yy1) ? yy2 : yy1;
      }
      /* second decimation stage */
      if (dec2 > 1) {
         decimate (decimationflag, y1, y2, len, dec2, 
                  tmpdec2I, &tmpdec2I);
         len2 = len / dec2;
         if (zoomf != 0) {
            decimate (decimationflag, y1+len, y2+len2, len, dec2, 
                     tmpdec2Q, &tmpdec2Q);
         }
         len = len2;
         y1 = y2;
         y2 = (y1 == yy1) ? yy2 : yy1;
      }
      /* remove delay */
      if (removeDelay) {
         dStart -= delayshift;
      }
      /* partition data */
      mux.lock();
      for (rtddChannel::partitionlist::iterator iter = partitions.begin(); 
          iter != partitions.end(); iter++) {
      #ifdef DEBUG
         cerr << "inspect partition " << iter->name << endl;
         cerr << "dec1 " << iter->decimate1 << "|" << decimate1 << endl;
         cerr << "dec2 " << iter->decimate2 << "|" << decimate2 << endl;
         cerr << "zoom " << iter->zoomstart << "|" << zoomstart << endl;
         cerr << "zoomf " << iter->zoomfreq << "|" << zoomfreq << endl;
         cerr << "is done = " << (iter->isDone() ? "true" : "false") << endl;
         cerr << "part size = " << iter->buf[0].size() << "$" << iter->length << endl;
      #endif
         // check partition preprocessing needs
         if ((iter->decimate1 != decimate1) || 
            (iter->decimate2 != decimate2) ||
            (iter->zoomstart != zoomstart) ||
            (fabs (iter->zoomfreq - zoomfreq) >= 1E-6) ||
            (iter->removeDelay != removeDelay) ||
            iter->isDone()) {
            continue;
         }
         // cerr << "found partition " << time << "." << epoch << 
            // " dat[0] = " << y1[0] << endl;
         // now copy data into partition if necessary
         int 	ndx = iter->index (dStart, len);
      #ifdef DEBUG
         cerr << "index = " << ndx << endl;
      #endif
         if (ndx == -1) {
            break;
         }	 
         if (err | (ndx == -2)) {
            iter->nomore();
            update = true;
            continue;
         }
         len2 = iter->range (dStart, len);
      #ifdef DEBUG
         cerr << "range = " << len2 << endl;
      #endif
         iter->fill (y1 + ndx, len2);
         if (zoomf != 0) {
            iter->fill (y1 + len + ndx, len2, 1);
         }
         if (iter->isDone()) {
            iter->decdelay = decdelay;
            iter->delaytaps = delaytaps;
            iter->timedelay = (decdelay + delaytaps * dt[0]) - 
               (double) delayshift / __ONESEC;
            if (iter->timedelay < 1E-9) {
               iter->timedelay = 0.0;
            }
            update = true;
         }
      }
      mux.unlock();
      delete [] yy1;
      delete [] yy2;
      return true;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Class Name: rtddChannel						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   rtddChannel::rtddChannel (string Chnname, gdsStorage& dat,
                     int dataRate, int dataType)
   : rtddCallback (Chnname), datarate (dataRate), datatype (dataType), 
   storage (&dat), timestamp (0), inUse (1), isTP (false),
   asyncUpdate (false)
   {
      switch (datatype) {
         case DAQ_DATATYPE_16BIT_INT:
            databps = sizeof (short);
            break;
         case DAQ_DATATYPE_32BIT_INT:
            databps = sizeof (int);
            break;
         case DAQ_DATATYPE_64BIT_INT:
            databps = sizeof (long long);
            break;
         case DAQ_DATATYPE_FLOAT:
            databps = sizeof (float);
            break;
         case DAQ_DATATYPE_DOUBLE:
            databps = sizeof (double);
            break;
         default:
            databps = 0;
            break;
      }
   }


   rtddChannel::rtddChannel (const rtddChannel& chn)
   : rtddCallback (chn.chnname)
   {
      *this = chn;
   }


   rtddChannel::~rtddChannel ()
   {
      updatelock.writelock();
   }


   rtddChannel& rtddChannel::operator= (const rtddChannel& chn)
   {
      if (this != &chn) {
         chn.updatelock.writelock();
         updatelock.writelock();
         this->rtddCallback::operator= (chn);
         datarate = chn.datarate;
         datatype = chn.datatype;
         databps = chn.databps;
         storage = chn.storage;
         partitions = chn.partitions;
         preprocessors = chn.preprocessors;
         timestamp = chn.timestamp;
         inUse = chn.inUse;
         isTP = chn.isTP;
         asyncUpdate = false;
         updatelock.unlock();
         chn.updatelock.unlock();
      }
      return *this;
   }


   bool rtddChannel::addPreprocessing (int Decimate1, int Decimate2, 
                     tainsec_t Zoomstart, double Zoomfreq, 
                     bool rmvDelay, bool useActiveTime,
                     tainsec_t Start, tainsec_t Stop)
   {
      preprocessing	pre (datarate, Decimate1, Decimate2, 
                         Zoomstart, Zoomfreq, rmvDelay);
   
      updatelock.writelock();
      semlock		lockit (mux);
      for (preprocessinglist::iterator iter = preprocessors.begin();
          iter != preprocessors.end(); iter++) {
         if (*iter == pre) {
            iter->setActiveTime (Start, Stop, useActiveTime);
            updatelock.unlock();
            return true;
         }
      }
      // not found, add it
      pre.setActiveTime (Start, Stop, useActiveTime);
      preprocessors.push_back (pre);
      updatelock.unlock();
      return true;
   }


   bool rtddChannel::addPartitions (const partitionlist& newPartitions,
                     bool useActiveTime) 
   {
      // check if preprocessing exists
      for (partitionlist::const_iterator iter = newPartitions.begin();
          iter != newPartitions.end(); iter++) {
         addPreprocessing (iter->decimate1, iter->decimate2,
                          iter->zoomstart, iter->zoomfreq, 
                          iter->removeDelay, useActiveTime, 
                          iter->start, iter->start + iter->duration);
      }
   
      // add partitions to list
      updatelock.writelock();
      semlock		lockit (mux);
      copy (newPartitions.begin(), newPartitions.end(), 
           back_inserter (partitions));
      sort (partitions.begin(), partitions.end());
      updatelock.unlock();
      return true;
   }


   void rtddChannel::reset ()
   {
      updatelock.writelock();
      semlock		lockit (mux);
      partitions.clear();
      preprocessors.clear();
      updatelock.unlock();
   }


   void rtddChannel::skip (tainsec_t stop)
   {
      bool		update = false;
      semlock		lockit (mux);
   
      for (partitionlist::iterator iter = partitions.begin();
          iter != partitions.end(); iter++) {
         if (iter->start + iter->duration <= stop) {
            iter->nomore();
            update = true;
         }
      }
      if (update) {
         updateStorage (true);
      }
   }


   tainsec_t rtddChannel::maxDelay () const
   {
      tainsec_t		delay = 0;
      semlock		lockit (mux);
   
      for (preprocessinglist::const_iterator iter = preprocessors.begin();
          iter != preprocessors.end(); iter++) {
         if (iter->removeDelay) {
            delay = max (delay, iter->delayshift);
         }
      }
      return delay;
   }


   int rtddChannel::callback (taisec_t time, int epoch,
                     float data[], int ndata, int err) 
   {
      bool	update = false;
   
   #ifdef DEBUG
      cerr << "callback for " << chnname << " (pre # = " << 
         preprocessors.size() << ")" << endl;
   #endif
      // cycle through preprocessing objects
      mux.lock();
      for (preprocessinglist::iterator iter = preprocessors.begin();
          iter != preprocessors.end(); iter++) {
         if (!(*iter)(time, epoch, data, ndata, err, 
                     partitions, mux, update)) {
            cerr << "PREPROCESSING ERROR " << chnname << endl;
            // error
         }
      }
      mux.unlock();
   
      if (update) {
         updateStorage (true);
      }
      return 0;
   }


   void rtddChannel::updateStorage (bool async)
   {
      if (async) {
         mux.lock();
         // do nothing if update task is already running
         if (asyncUpdate) {
            mux.unlock();
            return;
         }
         // else set flag and create task
         asyncUpdate = true;
         mux.unlock();
         taskID_t	taskID;
         int		attr;
      #ifdef OS_VXWORKS
         attr = VX_FP_TASK;
      #else
         attr = PTHREAD_CREATE_DETACHED | PTHREAD_SCOPE_PROCESS;
      #endif
         updatelock.readlock();
         if (taskCreate (attr, taskNdsUpdatePriority, &taskID, 
                        taskNdsUpdateName, (taskfunc_t) updateStorageTask, 
                        (taskarg_t) this) < 0) {
            updatelock.unlock();
            mux.lock();
            asyncUpdate = false;
            mux.unlock();
         }
      }
      else {
      #ifdef DEBUG
         cerr << "update " << chnname << endl;
      #endif
      
         while (1) {
            // find oldest parititon which is done
            mux.lock();
            partitionlist::iterator iter = partitions.end();
            for (partitionlist::iterator i = partitions.begin();
                i != partitions.end(); i++) {
               // found one which is done
               if (i->isDone()) {
                  // first one found
                  if (iter == partitions.end()) {
                     iter = i;
                  }
                  // else: test if older stop time
                  else if (i->start + i->duration < 
                          iter->start + iter->duration) {
                     iter = i;
                  }
               }
            }
            // if none found, exit
            if (iter == partitions.end()) {
               asyncUpdate = false;
               mux.unlock();
            #ifdef DEBUG
               cerr << "update done" << endl;
            #endif
               return;
            }
            // get partition parameters
            string		name = iter->name;
            tainsec_t		start = iter->start;
            tainsec_t		stop = start + iter->duration;
            double		precursor = 
               (double) iter->precursor / __ONESEC;
            double		dt = iter->dt;
            int			len = iter->length;
            int			decimate1 = iter->decimate1;
            int			decimate2 = iter->decimate2;
            tainsec_t		zoomstart = iter->zoomstart;
            double		zoomfreq = iter->zoomfreq;
            bool		cmplx = (iter->zoomfreq != 0);
            double 		decdelay = iter->decdelay;
            int 		delaytaps = iter->delaytaps;
            double		timedelay = iter->timedelay;
            bool		error = !iter->isFull();
            mux.unlock();
         
            // set time stamp
            if (error) {
               cerr << "UPDATE ERROR 3 " << name << endl;
            }
            /* setup channel if necessary */
            if (storage->findData (name) == 0) {
               gdsDataObject* 	dobj = 
                  storage->newChannel (name, start, dt, cmplx);
               if (dobj != 0) {
                  /* add parameters describing the preprocessing */
                  gdsParameter		prm;
                  prm = gdsParameter ("tp", precursor, "s");
                  storage->addParameter (name, prm);
                  prm = gdsParameter ("tf0", zoomstart, "ns");
                  storage->addParameter (name, prm);
                  prm = gdsParameter ("f0", zoomfreq, "Hz");
                  storage->addParameter (name, prm);
                  prm = gdsParameter ("N", len);
                  storage->addParameter (name, prm);
                  prm = gdsParameter ("AverageType", (int) 0);      
                  storage->addParameter (name, prm);
                  prm = gdsParameter ("Averages", (int) 1);
                  storage->addParameter (name, prm);
                  prm = gdsParameter ("Decimation", decimate1*decimate2);
                  storage->addParameter (name, prm);
                  prm = gdsParameter ("Decimation1", decimate1);
                  storage->addParameter (name, prm);
                  prm = gdsParameter ("DecimationType", decimationflag);
                  storage->addParameter (name, prm);
                  char		dectype[256];
                  decimationFilterName (decimationflag, dectype, 
                                       sizeof (dectype));
                  prm = gdsParameter ("DecimationFilter", dectype);
                  storage->addParameter (name, prm);
                  prm = gdsParameter ("DecimationDelay", decdelay, "s");
                  storage->addParameter (name, prm);
                  prm = gdsParameter ("DelayTaps", delaytaps);
                  storage->addParameter (name, prm);
                  prm = gdsParameter ("TimeDelay", timedelay, "s");
                  storage->addParameter (name, prm);
                  prm = gdsParameter ("Channel", chnname);
                  storage->addParameter (name, prm);
               }
            }
            float*	dat = storage->allocateChannelMem (name, len);
         
            // find partition again
            mux.lock();
            iter = partitions.end();
            for (partitionlist::iterator i = partitions.begin();
                i != partitions.end(); i++) {
               if ((i->name == name) && (i->start == start) &&
                  (i->length == len)) {
                  iter = i;
                  break;
               }
            }
            // error if not found
            if ((iter == partitions.end()) || !iter->isDone()) {
               asyncUpdate = false;
               mux.unlock();
               if (dat != 0) {
                  storage->notifyChannelMem (name, error);
               }
               cerr << "UPDATE ERROR " << name << endl;
               return;
            }
         
            // copy data
            if ((dat != 0) && (!error)) {
               iter->copy (dat, len, cmplx);
            }
            // remove partition
            partitions.erase (iter);
         
            // notify storage object of new data
            if (dat != 0) {
               mux.unlock();
               storage->notifyChannelMem (name, error);
               mux.lock();
            }
            // set time stamp
            if (error) {
               cerr << "UPDATE ERROR 2 " << name << endl;
            }
            timestamp = stop;
            mux.unlock();
         }
      }
   }


   void rtddChannel::updateStorageTask (rtddChannel* chn) 
   {
      chn->updateStorage (false);
      chn->updatelock.unlock();
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Class Name: rtddManager						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   int rtddManager::ndstask (rtddManager& RTDDMgr) 
   {
      DAQSocket*	nds = &RTDDMgr.nds;
      char*		buf = 0;// data buffer
      int		len;	// length of read buffer
      int		seqNum = -1;
      int		err;
      int		reconf;
      const timespec tick = {0, 1000000}; // 1ms
   
      // wait for data
      pthread_setcancelstate (PTHREAD_CANCEL_DISABLE, 0);
      while (1) {
         // get the mutex
         while (!RTDDMgr.ndsmux.trylock()) {
            pthread_setcancelstate (PTHREAD_CANCEL_ENABLE, 0);
            nanosleep (&tick, 0);
            pthread_testcancel();
            pthread_setcancelstate (PTHREAD_CANCEL_DISABLE, 0);
         }
         // check if data is ready
         // tainsec_t	t1 = TAInow();
         err = nds->WaitforData (true);
         if (err < 0) {
            cerr << "NDS socket ERROR" << endl;
            RTDDMgr.ndsTID = 0;
            RTDDMgr.nds.StopWriter ();
            RTDDMgr.nds.RmChannel ("all");
            RTDDMgr.nds.close();
            RTDDMgr.ndsmux.unlock();
            return -1;
         }
         else if (err == 0) {
            RTDDMgr.ndsmux.unlock();
            pthread_setcancelstate (PTHREAD_CANCEL_ENABLE, 0);
            nanosleep (&tick, 0);
            pthread_testcancel();
            pthread_setcancelstate (PTHREAD_CANCEL_DISABLE, 0);
            continue;
         }
         // get data
         err = 0;
         cerr << "get data from nds" << endl;
         len = nds->GetData (&buf, taskNdsGetDataTimeout);
         cerr << "got data from nds" << endl;
         if (len == 0) {
            cerr << "Data block with length 0 encountered " << 
               "****************************" << endl;
         }
         // reconfig block?
         reconf = 0;
         if ((len > 0) && 
            (((DAQDRecHdr*) buf)->GPS == (int)0x0FFFFFFFF)) {
            reconf = 1;
         }
         // check sequence number
         else if (len > 0) {
            err = (seqNum >= 0) && 
               (((DAQDRecHdr*) buf)->SeqNum != seqNum + 1) ? 1 : 0;
            seqNum = ((DAQDRecHdr*) buf)->SeqNum; 
            cerr << "seq # = " << seqNum << endl;
         }
         if (err || (len < 0)) {
            cerr << "DATA RECEIVING ERROR " << len << " errno " << errno << endl;
            // exit (1);
         }
      
         // process reconfigure information
         if (reconf) {
            // just skip for now
         }
         // process received data
         else if (len > 0) {
            if (!RTDDMgr.ndsdata (buf, err)) {
               len = -1;
            }
         }
         // end of data transmission encountered
         else if ((len <= 0) && !RTDDMgr.fastUpdate) {
            cerr << "TRAILER TIME = " << ((DAQDRecHdr*) buf)->GPS << endl;
         }
         delete [] buf;
         buf = 0;
         // quit if end of transfer is reached or on fatal error
         if ((len < 0) || ((len == 0) && !RTDDMgr.fastUpdate)) {
            if ((len <= 0) && !RTDDMgr.fastUpdate) {
               RTDDMgr.ndsCheckEnd();
            }
            RTDDMgr.ndsTID = 0;
            RTDDMgr.nds.StopWriter ();
            RTDDMgr.nds.RmChannel ("all");
            RTDDMgr.nds.close();
            RTDDMgr.ndsmux.unlock();
            return -1;
         }
         RTDDMgr.ndsmux.unlock();
         // tainsec_t	t2 = TAInow();
         // cerr << "TIME ndstask = " << (double)(t2-t1)/1E9 << endl;
         pthread_setcancelstate (PTHREAD_CANCEL_ENABLE, 0);
         pthread_testcancel();
         pthread_setcancelstate (PTHREAD_CANCEL_DISABLE, 0);
      }
   
      return 0;
   }


   int rtddManager::cleanuptask (rtddManager& RTDDMgr) 
   {
      const struct timespec delay = {1, 0};
   #ifndef OS_VXWORKS
      int 		oldtype;
      pthread_setcanceltype (PTHREAD_CANCEL_ASYNCHRONOUS, &oldtype);
   #endif
   
      while (true) {
         nanosleep (&delay, 0);
         // cerr << "rtdd cleanup start" << endl;
         RTDDMgr.cleanup ();
         // cerr << "rtdd cleanup stop" << endl;
      }
      return 0;
   }


   rtddManager::rtddManager (gdsStorage* dat, 
                     testpointMgr* TPMgr, double Lazytime) 
   : storage (dat), tpMgr (TPMgr), lazytime (Lazytime), cleartime (0), 
   RTmode (false), fastUpdate (false), nexttimestamp (0), stoptime (0),
   ndsTID (0), daqPort (0), cleanTID (0)
   {
      semlock		lockit (mux);	// lock mutex */
      strcpy (daqServer, "");
   
      // start cleanup task
      if (lazytime > 0) {
         int		attr;	// task create attribute
      #ifdef OS_VXWORKS
         attr = VX_FP_TASK;
      #else
         attr = PTHREAD_CREATE_DETACHED | PTHREAD_SCOPE_PROCESS;
      #endif
         taskCreate (attr, taskCleanupPriority, &cleanTID, 
                    taskCleanupName, (taskfunc_t) cleanuptask, 
                    (taskarg_t) this);
      }
   }


   rtddManager::~rtddManager () 
   {
      // cancel cleanup task
      mux.lock();
      taskCancel (&cleanTID);
      mux.unlock();
      // delete all test points
      del();
   }


   bool rtddManager::connect (const char* server, int port)
   {
      int		status;
   
      // get NDS parameters
      if (server == 0) {
         // dynamic configuration
      #if defined (_CONFIG_DYNAMIC)
         const char* const* cinfo;	// configuration info
         confinfo_t	crec;		// conf. info record
         for (cinfo = getConfInfo (0, 0); *cinfo != NULL; cinfo++) {
            if ((parseConfInfo (*cinfo, &crec) == 0) &&
               (gds_strcasecmp (crec.interface, 
                                CONFIG_SERVICE_NDS) == 0) &&
               (crec.ifo == -1) && (crec.progver == -1)) {
               strcpy (daqServer, crec.host);
               daqPort = crec.port_prognum;
            }
         }
      #else
         // from parameter file
         strcpy (daqServer, DAQD_SERVER);
         loadStringParam (PRM_FILE, PRM_SECTION, PRM_SERVERNAME, daqServer);
         daqPort = DAQD_PORT;
         loadIntParam (PRM_FILE, PRM_SECTION, PRM_SERVERPORT, &daqPort);
      #endif
         if (daqPort <= 0) {
            daqPort = DAQD_PORT;
         }
      }
      else {
         // user specified
         strncpy (daqServer, server, sizeof (daqServer) - 1);
         daqPort = (port <= 0) ? DAQD_PORT : port;
      }
   
      // connect to NDS
      status = nds.open (daqServer, daqPort, daqBufLen);
      cerr << "NDS version = " << nds.Version() << endl;
      return (status == 0);
   }


   bool rtddManager::init (gdsStorage* dat)
   {
      semlock		lockit (mux);
   
      if (storage == 0) {
         remove (false);
      }
      storage = dat;
      return true;
   }


   bool rtddManager::init (testpointMgr* TPMgr)
   {
      semlock		lockit (mux);
   
      if (TPMgr == 0) {
         remove (false);
      }
      tpMgr = TPMgr;
      return true;
   }


   bool rtddManager::areSet () const
   {
      semlock		lockit (mux);	// lock mutex */
   
      for (channellist::const_iterator iter = channels.begin();
          iter != channels.end(); iter++) {
         // if not set return false
         if (!iter->isSet()) {
            return false;
         }
      }
      // all set
      return true;
   }


   bool rtddManager::areUsed () const
   {
      semlock		lockit (mux);	// lock mutex */
   
      for (channellist::const_iterator iter = channels.begin();
          iter != channels.end(); iter++) {
         // if not used return false
         if (iter->inUse <= 0) {
            return false;
         }
      }
      // all set
      return true;
   }


   bool rtddManager::busy () const
   {
      semlock		lockit (mux);	// lock mutex */
      return (ndsTID != 0);
   }

   bool rtddManager::set (tainsec_t start, tainsec_t* active)
   {
      semlock		lockit (mux);	// lock mutex */
      tainsec_t		chnactive;	// time when channel active
   
      // check if lazy clears have to be committed
      if ((cleartime > 0) && !areUsed()) {
         mux.unlock();
         if (!ndsStop ()) {
            return false;
         }
         mux.lock();
         channellist::iterator iter = channels.begin();
         while (iter != channels.end()) {
            // if not used delete
            if (iter->inUse <= 0) {
               nds.RmChannel (iter->chnname.c_str());
               iter = channels.erase (iter);
            }
            else {
               iter++;
            }
         }
      }
   
      // set minimum active time
      if (active != 0) {
         *active = start;
      }
      cleartime = 0;
      // check if already set
   
      if (!areSet()) {
         // make sure nds is stopped
         mux.unlock();
         if (!ndsStop ()) {
            return false;
         }
         mux.lock();
         nds.RmChannel ("all");
         // setup channels
         for (channellist::iterator iter = channels.begin();
             iter != channels.end(); iter++) {
            // add channel to list
            nds.AddChannel (iter->chnname.c_str(), 
                           DAQSocket::rate_bps_pair 
                           (iter->datarate, iter->databps));
            if (iter->isSet()) {
               continue;
            }
            // activate channel
            if (!iter->subscribe (start, &chnactive)) {
               // error
               for (channellist::reverse_iterator iter2 (iter);
                   iter2 != channels.rend(); iter2++) {
                  iter2->unsubscribe();
               }
               nds.RmChannel ("all");
               return false;
            }
            if (active != 0) {
               *active = max (chnactive, *active);
            }
         }
      }
   
      // all set: start nds
      if (!ndsStart ()) {
         for (channellist::iterator iter = channels.begin();
             iter != channels.end(); iter++) {
            iter->unsubscribe();
         }     
         return false;
      }
      // round active time to next second after adding max. filter delays
      if (active != 0) {
         tainsec_t 	now = TAInow();
         now = _ONESEC * ((now + _ONESEC - 1) / _ONESEC);
         *active = max (now, *active);
      }
   
      return true;
   }


   bool rtddManager::set (taisec_t start, taisec_t duration)
   {
      semlock		lockit (mux);	// lock mutex */
      cerr << "TIME STAMP BEFORE START = " << timeStamp() << endl;
   
      // make sure nds is stopped
      mux.unlock();
      if (!ndsStop ()) {
         return false;
      }
      mux.lock();
      nds.RmChannel ("all");
      cleartime = 0;
   
      // setup channels
      cerr << "setup channels for NDS" << endl;
      for (channellist::iterator iter = channels.begin();
          iter != channels.end(); iter++) {
         // add channel to list
         nds.AddChannel (iter->chnname.c_str(), 
                        DAQSocket::rate_bps_pair 
                        (iter->datarate, iter->databps));
         if (iter->isSet()) {
            continue;
         }
         // activate channel
         if (!iter->subscribe (start, 0)) {
            // error
            for (channellist::reverse_iterator iter2 (iter);
                iter2 != channels.rend(); iter2++) {
               iter2->unsubscribe();
            }
            nds.RmChannel ("all");
            return false;
         }
      }
   
      // all set: start nds
      cerr << "start NDS @ " << start << ":" << duration << endl;
      if (!ndsStart (start, duration)) {
         for (channellist::iterator iter = channels.begin();
             iter != channels.end(); iter++) {
            iter->unsubscribe();
         }     
         return false;
      }
   
      cerr << "start NDS @ " << start << ":" << duration << " done" << endl;
      return true;
   }


   bool rtddManager::clear (bool lazy)
   {
      // mark only if lazy clear
      if (lazy) {
         semlock		lockit (mux);
         cleartime = (double) TAInow() / (double) _ONESEC;
      }
      else {
         // stop nds
         //cerr << "stop nds" << endl;
         ndsStop ();
         semlock		lockit (mux);
         del();
      //    //cerr << "stop nds 2" << endl;
         // semlock		lockit (mux);
      // 
      //    // loop through channel list and unsubscribe
         // for (channellist::iterator iter = channels.begin();
             // iter != channels.end(); iter++) {
            // iter->reset();
            // iter->unsubscribe();
         // }
         cleartime = 0;
      }
      return true;
   }


   bool rtddManager::add (const string& name, int* inUseCount)
   {
      semlock		lockit (mux);
   
      string		chnname (channelName (name));
      gdsChnInfo_t	info;
   
      // test if valid channel
      if (!channelInfo (name, info)) {
         return false;
      }
      rtddChannel	chn (chnname, *storage, 
                      info.dataRate, info.dataType);
   
      return add (chn, inUseCount);
   }


   bool rtddManager::add (const rtddChannel& chn, int* inUseCount)
   {
      semlock		lockit (mux);
   
      // test if already in list
      channellist::iterator iter = find (chn.chnname);
      if ((iter != channels.end()) && (*iter == chn)) {
         // increase in use count
         iter->inUse++;
         if (inUseCount != 0) {
            *inUseCount = iter->inUse;
         }
         // set test point if required
         if ((iter->inUse == 1) && (iter->isTP) && (tpMgr != 0)) {
            tpMgr->add (iter->chnname);
         }
      }
      else {
         // add to channel list
         iter = channels.insert (iter, chn);
         iter->inUse = 1;
         if (inUseCount != 0) {
            *inUseCount = 1;
         }
         // add to test point list
         iter->isTP = (tpMgr == 0) ? 
            false : tpMgr->add (chn.chnname);
      }
   
      return true;
   }


   bool rtddManager::add (const string& name, 
                     const rtddChannel::partitionlist& partitions,
                     bool useActiveTime)
   {
      semlock		lockit (mux);
      string		chnname (channelName (name));
   
      // test if already in list
      channellist::iterator iter = find (chnname);
      // add channel if not in list
      if ((iter == channels.end()) || (*iter != chnname)) {
         int		temp;
         if (!add (chnname, &temp)) {
            return false;
         }
         iter = find (chnname); 
         if ((iter == channels.end()) || (*iter != chnname)) {
            return false;
         }
      }
   
      // add partitions to channel object
      iter->addPartitions (partitions, useActiveTime);
   
      return true;
   }


   bool rtddManager::add (const string& name, 
                     int Decimate1, int Decimate2, 
                     tainsec_t Zoomstart, double Zoomfreq, 
                     bool rmvDelay)
   {
      semlock		lockit (mux);
      string		chnname (channelName (name));
   
      // test if already in list
      channellist::iterator iter = find (chnname);
      // add channel if not in list
      if ((iter == channels.end()) || (*iter != chnname)) {
         if (!add (chnname, 0)) {
            return false;
         }
         iter = find (chnname); 
         if ((iter == channels.end()) || (*iter != chnname)) {
            return false;
         }
      }
   
      // add partitions to channel object
      iter->addPreprocessing (Decimate1, Decimate2, Zoomstart, Zoomfreq,
                           rmvDelay);
   
      return true;
   }


   bool rtddManager::del (const string& chnname)
   {
      semlock		lockit (mux);	// lock mutex
   
      // test if in list
      channellist::iterator iter = find (chnname);
      if ((iter == channels.end()) || (*iter != chnname)) {
         // not found
         return false;
      }
   
      // check inUse
      if (--iter->inUse <= 0) {
         // remove test point if in use count reaches zero
         if ((iter->isTP) && (tpMgr != 0)) {
            tpMgr->del (iter->chnname);
         }
         // remove from list if not lazy clear
         if (cleartime == 0) {
            channels.erase (iter);
         }
      }
   
      return true;
   }


   bool rtddManager::del ()
   {
      // clear all channels
      // clear (false);
      semlock		lockit (mux);	// lock mutex
      // loop through channel list and delete test points
      for (channellist::iterator iter = channels.begin();
          iter != channels.end(); iter++) {
         if ((iter->isTP) && (tpMgr != 0)) {
            tpMgr->del (iter->chnname);
         }
      }
      channels.clear();
      return true;
   }


   bool rtddManager::reset (const string& chnname)
   {
      semlock		lockit (mux);	// lock mutex
   
      // test if in list
      channellist::iterator iter = find (chnname);
      if ((iter == channels.end()) || (*iter != chnname)) {
         // not found
         return false;
      }
   
      // reset
      iter->reset();
      return true;
   }


   bool rtddManager::reset ()
   {
      semlock		lockit (mux);	// lock mutex
      // loop through channel list and reset
      for (channellist::iterator iter = channels.begin();
          iter != channels.end(); iter++) {
         iter->reset();
      }
      return true;
   }


   tainsec_t rtddManager::timeStamp () const
   {
      tainsec_t		timestamp = -1;
      semlock		lockit (mux);	// lock mutex
      for (channellist::const_iterator iter = channels.begin();
          iter != channels.end(); iter++) {
         if (timestamp == -1) {
            timestamp = iter->timeStamp();
         }
         else {
            timestamp = min (timestamp, iter->timeStamp());
         }
      }
      return timestamp;
   }


   tainsec_t rtddManager::maxDelay () const
   {
      tainsec_t		delay = 0;
      semlock		lockit (mux);	// lock mutex
   
      for (channellist::const_iterator iter = channels.begin();
          iter != channels.end(); iter++) {
         delay = max (delay, iter->maxDelay());
      }
      return delay;
   }


   rtddManager::channellist::iterator rtddManager::find (
                     const string& name) const
   {
      semlock		lockit (mux);	// lock mutex */
      rtddChannel	chn (channelName (name), *storage, 0, 0);
   
      return lower_bound ((channellist::iterator) channels.begin(), 
                         (channellist::iterator) channels.end(), chn);
   }


template <class T>
   inline void convertRTDDData (float x[], T y[], int len)
   {
      for (int i = 0; i < len; i++) {
         x[i] = static_cast<float>(y[i]);
      }
   }


   bool rtddManager::ndsdata (const char* buf, int err)
   {
      const int size = 16*1024;
      //cerr << "data received" << endl;
   
      if (buf == 0) {
         return true;
      }
      semlock		lockit (mux);	// lock mutex 
      DAQDRecHdr*	head = (DAQDRecHdr*) buf;
      const char*	dptr =		// data pointer
         buf + sizeof (DAQDRecHdr);
      taisec_t		time = 		// time (sec) of data
         (taisec_t) head->GPS;
      int		epoch =		// epoch of data
         (head->NSec + _EPOCH / 10) / _EPOCH;
      tainsec_t		duration = 	// time duration (sec) of data
         fastUpdate ? _EPOCH : (taisec_t) head->Secs * _ONESEC;
      tainsec_t		timestamp = 	// time stamp
         (tainsec_t)time * _ONESEC + (tainsec_t)epoch * _EPOCH;
      float*		fptr;		// data pointer to float array
      float		fdat[size];	// data buffer
      int		ndata;		// number of data points
      int		datasize =	// size of data
         head->Blen - (sizeof (DAQDRecHdr) - sizeof (int));
      int		idata = 0;	// data index
   
      //cerr << "time GPS = " << 	time << " nsec = " << epoch << endl;
      if (!fastUpdate) { // NDS bug ???
         epoch = 0;
      }
      cerr << "time GPS = " << 	time << " nsec = " << epoch << 
         " duration sec = " << (double)duration / __ONESEC << endl;
   
      // check if we lost data
      if ((nexttimestamp != 0) && 
         (timestamp > nexttimestamp + 1000)) {
         cerr << "NDS RECEIVING ERROR: # of epochs lost = " <<
            (timestamp - (nexttimestamp - 1000)) / _EPOCH << endl;
      }
   
      // check NDS time
   #ifdef GDS_ONLINE
      if (RTmode) {
         double delay = (double) (TAInow() - timestamp) / __ONESEC;
         double maxdelay = _MAX_NDS_DELAY + duration / __ONESEC;
         if ((delay < _MIN_NDS_DELAY) || (delay > maxdelay)) {
            cerr << "TIMEOUT ERROR: NDS delay = " << delay << endl;
            //return false;
         }
      }
   #endif
   //    // STRESS
      // static int firsttime = 1;
      // if (lasttimestamp == 0) {
         // firsttime = 1;
      // }
      // if ((head->SeqNum == 102) && firsttime) {
         // firsttime = 0;
         // lasttimestamp = timestamp;
         // lasttime = TAInow();
         // return true;
      // }
   
      // go through channel list
      for (DAQSocket::Channel_iter iter = nds.mChannel.begin(); 
          iter != nds.mChannel.end(); iter++) {
         // calculate # of data points
         ndata = (int) ((double)iter->second.first * 
                       ((double)duration / __ONESEC) + 0.5);
         //ndata = fastUpdate ? iter->second.first / NUMBER_OF_EPOCHS :
            //iter->second.first;
         cerr << "rate = " << iter->second.first << 
            " length: ndata = " << ndata << " data size = " <<
            datasize << " bps = " << iter->second.second << 
            " idata = " << idata << endl;
         // check buffer length
         if (idata + ((ndata == 0) ? 1 : ndata) * iter->second.second > 
            datasize) {
            return false;
         }
      
         // find daq channel and invoke callback
         channellist::iterator chn = find (iter->first);
         if ((chn == channels.end()) || (*chn != iter->first)) {
            // not found; go to next data record
            idata += ((ndata == 0) ? 1 : ndata) * iter->second.second;
            dptr += ((ndata == 0) ? 1 : ndata) * iter->second.second;
            continue;
         }
         // check if data has to be converted into floats
         if (chn->datatype == DAQ_DATATYPE_FLOAT) {
            fptr = (float*) dptr;
         }
         else {
            // allocate buffer
            if (ndata <= size) {
               fptr = fdat;
            }
            else {
               fptr = new (nothrow) float [ndata];
               if (fptr == 0) {
                  idata += ((ndata == 0) ? 1 : ndata) * iter->second.second;
                  dptr += ((ndata == 0) ? 1 : ndata) * iter->second.second;
                  continue;
               }
            }
            switch (chn->datatype) {
               case DAQ_DATATYPE_16BIT_INT: 
                  {
                     convertRTDDData (fptr, (short*) dptr, ndata);
                     break;
                  }
               case DAQ_DATATYPE_32BIT_INT:
                  {
                     convertRTDDData (fptr, (int*) dptr, ndata);
                     break;
                  }
               case DAQ_DATATYPE_64BIT_INT:
                  {
                     convertRTDDData (fptr, (long long*) dptr, ndata);
                     break;
                  }
               case DAQ_DATATYPE_DOUBLE:
                  {
                     convertRTDDData (fptr, (int*) dptr, ndata);
                     break;
                  }
               default:
                  {
                     memset (fptr, 0, ndata * sizeof (float));
                     break;
                  }
            }
         }
      
         // invoke callback
         chn->callback (time, epoch, fptr, ndata, err);
      
         // free data buffer if necessary
         if ((ndata > size) && (chn->datatype != DAQ_DATATYPE_FLOAT)) {
            delete [] fptr;
         }
      
         // advance to next channel
         idata += ((ndata == 0) ? 1 : ndata) * iter->second.second;
         dptr += ((ndata == 0) ? 1 : ndata) * iter->second.second;
      }
   
   #ifndef DEBUG 
      cerr << "nds callback done " << 
         (double) ((time*_ONESEC+epoch*_EPOCH) % (1000 * _ONESEC)) / 1E9 <<
         " at " << (double) (TAInow() % (1000 * _ONESEC)) / 1E9 << endl;
      cerr << "time stamp = " << timeStamp() << endl;
   #endif
   
      // set time of last successful NDS data transfer
      nexttimestamp = timestamp + duration;
      lasttime = TAInow();
   
      return true;
   }


   bool rtddManager::ndsStart ()
   {
      // check if already running 
      if (ndsTID != 0) {
         return true;
      }
      // check if any channels are selected
      if (nds.mChannel.empty()) {
         return true;
      }
   
      // start net writer
      cerr << "nds start" << endl;
      RTmode = true;
      fastUpdate = true;
      for (DAQSocket::Channel_iter iter = nds.mChannel.begin(); 
          iter != nds.mChannel.end(); iter++) {
         // check data rate
         if (iter->second.first < NUMBER_OF_EPOCHS) {
            fastUpdate = false;
            break;
         }
      }
   
      // set last time
      nexttimestamp = 0;
      stoptime = 0; // no end
      lasttime = TAInow();
   
      // establish connection
   #if !defined (_RTDD_PROVIDER) || (_RTDD_PROVIDER  == 2)
      if (!nds.isOpen() && (nds.open (daqServer, daqPort) != 0)) {
         nds.RmChannel ("all");    
         return false;
      }
      if (nds.RequestOnlineData (fastUpdate, taskNdsGetDataTimeout) != 0) {     
         nds.RmChannel ("all");    
         return false;
      }
   
      // create nds task
      int		attr;	// task create attribute
   #ifdef OS_VXWORKS
      attr = VX_FP_TASK;
   #else
      attr = PTHREAD_CREATE_DETACHED;
   #endif
      if (taskCreate (attr, taskNdsPriority, &ndsTID, 
                     taskNdsName, (taskfunc_t) ndstask, 
                     (taskarg_t) this) != 0) {
         nds.StopWriter();     
         nds.RmChannel ("all");    
         return false;
      }
   #endif
      cerr << "nds started" << endl;
   
      return true;
   }


   bool rtddManager::ndsCheckEnd ()
   {
      cerr << "check end stop = " << stoptime << endl;
      if (stoptime == 0) {
         return true;
      }
      cerr << "check end expected = " << nexttimestamp << endl;
      if (fabs ((double)(stoptime - nexttimestamp)/__ONESEC) < 1E-6) {
         return true;
      }
      else {
         // notify channels on error
         cerr << "check end: skip" << endl;
         for (channellist::iterator iter = channels.begin();
             iter != channels.end(); iter++) {
            iter->skip (stoptime);
         }
         return false;
      }
   }


   bool rtddManager::ndsStart (taisec_t start, taisec_t duration)
   {
      // check if already running 
      if (ndsTID != 0) {
         return true;
      }
      // check if any channels are selected
      if (nds.mChannel.empty()) {
         return true;
      }
   
      // wait for data to become available
      while (TAInow() < (start + duration + _NDS_DELAY) * _ONESEC) {
         timespec wait = {0, 250000000};
         nanosleep (&wait, 0);
      }
   
      // set last time
      nexttimestamp = start * _ONESEC;
      stoptime = (start + duration) * _ONESEC;
      lasttime = TAInow();
   
      // start net writer
      RTmode = false;
      fastUpdate = false;
      cerr << "nds start old data" << endl;
   #if !defined (_RTDD_PROVIDER) || (_RTDD_PROVIDER  == 2)
      if (!nds.isOpen() && (nds.open (daqServer, daqPort) != 0)) {
         nds.RmChannel ("all");
         cerr << "nds error during open" << endl;
         return false;
      }
      if (nds.RequestData (start, duration, taskNdsGetDataTimeout) != 0) {     
         nds.RmChannel ("all");    
         cerr << "nds error during data request" << endl;
         return false;
      }
   
      // create nds task
      int		attr;	// task create attribute
   #ifdef OS_VXWORKS
      attr = VX_FP_TASK;
   #else
      attr = PTHREAD_CREATE_DETACHED;
   #endif
      if (taskCreate (attr, taskNdsPriority, &ndsTID, 
                     taskNdsName, (taskfunc_t) ndstask, 
                     (taskarg_t) this) != 0) {
         nds.StopWriter();     
         nds.RmChannel ("all");    
         cerr << "nds error during task spawn" << endl;
         return false;
      }
   #endif
      cerr << "nds started" << endl;
   
      return true;
   }


   bool rtddManager::ndsStop ()
   {
   #if !defined (_RTDD_PROVIDER) || (_RTDD_PROVIDER  == 2)
      cerr << "kill nds task: get mutex" << endl;
      // get the mutex
      int n = 30;
      const timespec tick = {0, 100000000}; // 100ms
      while ((n >= 0) && !ndsmux.trylock()) {
         nanosleep (&tick, 0);
         n--;
      	 // send a signal to unblock select in daqsocket
         if (n % 10 == 2) {
            pthread_kill (ndsTID, SIGCONT);
         }
      }
      if (n < 0) {
         return false;
      }
      //ndsmux.lock();
      if (ndsTID != 0) {
         cerr << "kill nds task" << endl;
         taskCancel (&ndsTID);
         ndsTID = 0;
         cerr << "killed nds task" << endl;
         nds.StopWriter ();
         nds.RmChannel ("all");
         nds.close();
         cerr << "killed nds" << endl;
      }
      ndsmux.unlock();
   #endif
      return true;
   }


   void rtddManager::cleanup ()
   {
      semlock		lockit (mux);	// lock mutex */
   
      // check time
      if ((lazytime > 0) && (cleartime > 0) &&
         ((double) TAInow() / _ONESEC > cleartime + lazytime)) {
         cleartime = 0;
         // stop nds
      #if !defined (_RTDD_PROVIDER) || (_RTDD_PROVIDER  == 2)
         mux.unlock();
         ndsStop ();
         mux.lock();
      #endif
         // cleanup
         cerr << "CLEAN RTDD" << endl;
         del();
      }
   }


}
