CREATE_RPC_TARGET(rchannel)
CREATE_RPC_TARGET(rlaunch)
CREATE_RPC_TARGET(rleap)

SET(CONF_SRC
    channel_server.cc
    confinfo.c
    confserver.c
    launch_client.cc
    launch_server.cc
    leap_server.cc
        )

add_library(conf_lib OBJECT
    ${CONF_SRC}
)

target_include_directories(conf_lib PRIVATE    
    ${DTT_INCLUDES}
    ${RPC_OUTPUT_DIR}
    )

target_include_directories(conf_lib PUBLIC SYSTEM BEFORE ${RPC_INCLUDE_DIR})

add_dependencies(conf_lib
    rchannel_rpc
    rlaunch_rpc
    rleap_rpc)   

