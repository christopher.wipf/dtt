/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: cmdline							*/
/*                                                         		*/
/* Module Description: command line interface	 			*/
/*									*/
/*                                                         		*/
/* Module Arguments: none				   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 14Jan99  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: cmdline.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8336  (509) 372-2178  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Ultra-Enterprise, Solaris 5.6			*/
/*	Compiler Used: sun workshop C 4.2				*/
/*	Runtime environment: sparc/solaris				*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			TBD			*/
/*			  POSIX			TBD			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*								 	*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_CMDLINE_H
#define _GDS_CMDLINE_H


/* Header File List: */
#include <string>
#include "cmdapi.hh"

namespace diag {
   using std::string;

/** @name Command Line Interface
    This module implements the command line interface to the
    diagnostics kernel. The following commands are supported 
    by the command line interface rather than the diagnostics
    kernel:
    
    open 'server': opens a communication channel to a 
    diagnostics kernel. If the server argument is omitted a 
    diagnostics kernel is loaded locally.

    close: closes the connection.

    exit: exits the command line interface

    read 'script': reads an ASCII file and interprets each line
    which doesn't start with a \# as a command.

    help: shows a help screen

    The following command line options are suported:

    -s 'name': starts the command line and opens a connection
    to the specified server.

    -l:  starts the command line and opens a connection
    to a local diagnostics kernel.

    -f 'filename':  starts the command line and reads
    the script into the command line interpreter.

    @memo Command line interface for diagnostics tasks
    @author Written January 1999 by Daniel Sigg
    @version 0.1
************************************************************************/

/*@{*/

/** Command line interpreter. Reads commands from the specified input
    file, executes them and writes the results to the given output
    file. If either the input or the output file is NULL the standard
    input or the standard output is taken as default.

    This object should be used the following way:
    \begin{verbatim}
    commandline		cmdline (argc, argv);
    if (!cmdline) {
       // error
       return;
    }
    while (cmdline)()) { 
       // line by line 
    }
    \end{verbatim}

    @memo Class to manage the command line interpreter
    @author DS, January 99
************************************************************************/
   class commandline : public basic_commandline {
   public:
   
      /** Constructs command line object object.
          @memo Constructor.
          @param argc number of arguments
          @param list of arguments
      	  @param silent no echo to terminal if true
          @return void
       ******************************************************************/
      commandline (int argc, char* argv[], bool Silent = false);
   
      /** Calls the command line interpreter.
          @memo Call operator.
          @return true if not yet finished
       ******************************************************************/
      virtual bool operator () ();
   
   protected:
      /// command prompt
      string		prompt;
      /// last input line
      string		lastline;
   
      /** Prints a line to the output of the command line.
          @param s message string
          @return true if successful
       ******************************************************************/
      virtual void printline (const std::string& s);
   };

/*@}*/
}

#endif /*_GDS_CMDLINE_H */
