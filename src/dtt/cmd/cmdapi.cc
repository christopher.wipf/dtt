/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: cmdapi							*/
/*                                                         		*/
/* Module Description: implements the basic command line interface	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Includes: 								*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
#include <time.h>
#include <string>
#include <fstream>
#include "cmdapi.hh"
#include "gdsstringcc.hh"
#include "gdsmsg.h"
#include "diagnames.h"
#include <iostream> // For cerr.
#include <cstdlib>

namespace diag {
   using namespace std;
   using namespace thread;

   static const int my_debug = 0 ;

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Constants: 								*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   const string 	_argLocal ("-l");
   const string 	_argServer ("-s");
   const string 	_argScript ("-f");
   const string 	_argCMD ("-c");
   const string		help_text 
   ("Diagnostics system commands:\n"
   "  help: print this screen\n"
   "  open 'server': open a connection to a diagnostics kernel\n"
   "  close: close the connection\n"
   "  read 'filename': read commands from a file\n"
   "  exit/quit: exit from the command line\n");


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Class Name: commandline::indexentry					*/
/*                                                         		*/
/* Class Description: index entry					*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   basic_commandline::indexentry::indexentry (const string& text)
   {
      string::size_type pos;
      string meas;
      if ((pos = text.find ("[")) == string::npos) {
         step = 0;
         meas = text;
      }
      else {
         step = atoi (text.c_str() + pos + 1);
         meas = text.substr (0, pos);
      }
      pos = 0;
      mtype = -1;
      while (icAll[pos] != 0) {
         if (compareTestNames (meas, icAll[pos]) == 0) {
            mtype = pos;
            break;
         }
         pos++;
      }
   }


   string basic_commandline::indexentry::getMeasType() const 
   {
      if (mtype >= 0) {
         return string (icAll[mtype]);
      }
      else {
         return "";
      }
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Class Name: commandline::masterindex					*/
/*                                                         		*/
/* Class Description: master index 					*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   basic_commandline::masterindex::masterindex (const char* mentry)
   : vector<indexentry> ()
   {
      if (mentry != 0) {
         istringstream is (mentry);
         string line;
         // skip first line
         getline (is, line);
      
         // loop over entries
         string::size_type pos;
         while (is) {
            getline (is, line);
            if ((pos = line.find (stIndexEntry)) == string::npos) {
               continue;
            }
            line.erase (0, pos + strlen (stIndexEntry));
            if ((pos = line.find ("[")) == string::npos) {
               continue;
            }
            int n = atoi (line.c_str() + pos + 1);
            if ((pos = line.find ("=")) == string::npos) {
               continue;
            }
            if ((int)size() < n + 1) {
               resize (n + 1);
            }
            line.erase (0, pos + 1);
            // remove blanks, etc.
            while ((pos = line.find_first_of (" \t;")) != string::npos) {
               line.erase (pos, 1);
            }
            (*this)[n] = indexentry (line);
         };
      }
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Method Name: basic_commandline					*/
/*                                                         		*/
/* Method Description: command line interpreter	constructor		*/
/*                                                         		*/
/* Method Arguments: cmd line arguments, input stream, output stream	*/
/*                                                         		*/
/* Method Returns: void							*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   basic_commandline::basic_commandline (bool Silent) 
   : finished (false), silent (Silent), fastmessages (false),
   state (unconnected), id (-1), 
   capabilities (0), nMessages (new (nothrow) ostringstream())
   {
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Method Name: basic_commandline					*/
/*                                                         		*/
/* Method Description: command line interpreter	constructor		*/
/*                                                         		*/
/* Method Arguments: cmd line arguments, input stream, output stream	*/
/*                                                         		*/
/* Method Returns: void							*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   basic_commandline::basic_commandline (int argc, char* argv[], 
   bool Silent) 
   : finished (false), silent (Silent), fastmessages (false), 
   state (unconnected), id (-1), 
   capabilities (0), nMessages (new (nothrow) ostringstream())
   {
      setup (argc, argv);
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Method Name: setup							*/
/*                                                         		*/
/* Method Description: command line interpreter	constructor		*/
/*                                                         		*/
/* Method Arguments: cmd line arguments, input stream, output stream	*/
/*                                                         		*/
/* Method Returns: void							*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   bool basic_commandline::setup (int argc, char* argv[]) 
   
   {
      msgAPIstate	st (unconnected); // cmd line arguments
      string		script (""); 	  // script name
   
      // check command line arguments
      int i = 1;
      string conf;
      while (i < argc) {
         // local server argument
         if (_argLocal == argv[i]) {
            st = local;
         }
         // remote server argument
         else if ((_argServer == argv[i]) && (i + 1 < argc)) {
            st = remote;
            i++;
            servername = argv[i];
         }
         // script argument
         else if ((_argScript == argv[i]) && (i + 1 < argc)) {
            i++;
            script = argv[i];
         }
         // ignore comamnd line argument
         else if (_argCMD == argv[i]) {
         }
         // else assume kernel configuration arguments
         else {
            if (conf.empty() && (argv[i][0] != '-')) {
               conf += " -";
            }
            else {
               conf += " ";
            }
            conf += argv[i];
         }
         i++;
      }
   
      // interpret command line arguments
      switch (st) {
         case local:
            if (!parse (string ("open" + conf))) 
               return false;
            break;
         case remote:
            if (!parse (string ("open " + servername + conf))) 
               return false;
            break;
         default:
            break;
      }
      if (script != "") {
         // read in file and call interpreter
         if (isXML (script)) {
            if (!parse (string ("restore -all " + script))) 
               return false;
         }
         else {
            if (!parse (string ("read " + script))) 
               return false;
         }
      }
      return true;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Method Name: ~commandline						*/
/*                                                         		*/
/* Method Description: command line interpreter	destructor		*/
/*                                                         		*/
/* Method Arguments: void						*/
/*                                                         		*/
/* Method Returns: void							*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   basic_commandline::~basic_commandline ()
   {
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Method Name: HasCapability						*/
/*                                                         		*/
/* Method Description: tests for supported capabilities			*/
/*                                                         		*/
/* Method Arguments: capability						*/
/*                                                         		*/
/* Method Returns: true if supported  					*/
/*                                                         		*/
/*----------------------------------------------------------------------*/  
   bool basic_commandline::HasCapability (int cap) const
   {
      if (!isConnected()) {
         return false;
      }
      switch (cap) {
         case -1:
            {
               return (((capabilities & CMD_TEST) != 0) &&
                  ((capabilities & CMD_TESTPOINT) != 0) &&
                  ((capabilities & CMD_AWG) != 0));
            }
         case 0:
            {
               return ((capabilities & CMD_TEST) != 0);
            }
         case 1:
            {
               return ((capabilities & CMD_TESTPOINT) != 0);
            }
         case 2:
            {
               return ((capabilities & CMD_AWG) != 0);
            }
         default: 
            {
               return false;
            }
      }
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Method Name: fileName						*/
/*                                                         		*/
/* Method Description: extracts the filename from a save/restore cmd	*/
/*                                                         		*/
/* Method Arguments: save/restore parameters				*/
/*                                                         		*/
/* Method Returns: filename or empty string				*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   static string fileName (const string& arg)
   {
      string buf (arg);
   
      while (!buf.empty() && isspace (buf[0])) buf.erase (0, 1);
      while (!buf.empty() && isspace (buf[buf.size()-1])) {
         buf.erase (buf.size()-1, 1);
      }
      if (!buf.empty() && (buf[0] == '-')) {
         while (!buf.empty() && !isspace (buf[0])) buf.erase (0, 1);
         while (!buf.empty() && isspace (buf[0])) buf.erase (0, 1);
      }
      return buf;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Method Name: fileFlags						*/
/*                                                         		*/
/* Method Description: extracts the flags from a save/restore cmd	*/
/*                                                         		*/
/* Method Arguments: save/restore parameters				*/
/*                                                         		*/
/* Method Returns: flags						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   static string fileFlags (const string& arg)
   {
      istringstream	is (arg.c_str());
      string		buf;
      string		ret = "";
   
      while (is >> buf) {
         if ((buf.size() > 0) && (buf[0] == '-')) {
            ret += " " + buf;
         }
      }
      return ret;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Function: notificatioCallback				*/
/*                                                         		*/
/* Function Description: callback function for notification messages	*/
/*                                                         		*/
/* Function Arguments: id, msg, prm, pLen, res, rLen			*/
/*                                                         		*/
/* Fcuntion Returns: 0 if success					*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   inline int notificatioCallback (int id, const char* msg, 
   const char* prm, int pLen, char** res, int* rLen) 
   {
      return basic_commandline::cbfunc (id, msg, prm, pLen, res, rLen);
   }

extern "C" 
   int notificatioCallbackC (int id, const char* msg, 
   const char* prm, int pLen, char** res, int* rLen) 
   {
      return notificatioCallback (id, msg, prm, pLen, res, rLen);
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Method Name: parse							*/
/*                                                         		*/
/* Method Description: command line interpreter	parser			*/
/*                                                         		*/
/* Method Arguments: command line					*/
/*                                                         		*/
/* Method Returns: true if successful					*/
/*                                                         		*/
/*----------------------------------------------------------------------*/     
   bool basic_commandline::parse (const string& line)
   {
      /* do not process empty strings, but look for notifications */
      if (line.empty()) {
         echoNotification ();
         return true;
      }
   
      if (my_debug)
	 cerr << "basic_commandline::parse line = " << line << endl ;
      /* separate first word */
      int		pos = line.find_first_of (' ');
      string	 	msg = line.substr (0, pos);
      string		prm;
      if (pos > 0) {
         prm.assign (line.c_str(), pos + 1, line.size() - pos);
      	 /* remove blanks */
         while (!prm.empty() && (prm[0] == ' ')) {
            prm.erase (0, 1);
         }
         while (!prm.empty() && (prm[prm.size() - 1] == ' ')) {
            prm.erase (prm.size() - 1, 1);
         }
      }
   
      stringcase	cmd (msg.c_str());
   
      /* finished? */
      if ((cmd == "exit") || (cmd == "quit")) {
         if (state != unconnected) {
            parse (string ("close"));
         }
         finished = true;
         return true;
      }
   
      /* go through command list */
      if (cmd == "help") {
         echo (help_text);
         char*		reply;
         int		rLen;
         if ((state != unconnected) &&
         (gdsMsgSend (id, "help", 0, 0, &reply, &rLen) >= 0) &&
         (reply != 0)) {
            echo (reply);
            free (reply);
         }
      }
      /* open command */
      else if (cmd == "open") {
         if (state != unconnected) {
            echo ((string) "error: diagnostics kernel already connected");
         }
         else {
            capabilities = -1;
            /* obtain server name and configuration string */
            string server;
            string conf;
            string::size_type pos = prm.find (' ');
            if (pos == string::npos) {
               pos = prm.find ('\t');
            }
            if (!prm.empty() && (prm[0] == '-')) {
               server = "";
               conf = prm;
            }
            else if (pos == string::npos) {
               server = prm;
               conf = "";
            }
            else {
               server = prm.substr (0, pos);
               conf = prm.substr (pos);
               while (!conf.empty() && (conf[0] == ' ')) {
                  conf.erase (0, 1);
               }
            }
            const char* cfg = conf.empty() ? 0 : conf.c_str();
            /* open connection to local kernel */
            if (server.empty()) {
               id = gdsMsgOpen (0, capabilities, cfg, &capabilities);
               if (id < 0) {
                  echo (string ("error: unable to connect to local"
                     " diagnostics kernel"));
               }
               else {
                  state = local;
               }
            }
            /* open connection to remote kernel */
            else {
               id = gdsMsgOpen (server.c_str(), capabilities, cfg, 
                  &capabilities);
               if (id < 0) {
                  echo ("error: unable to connect to remote diagnostics"
                     " kernel (host: " + server + ")");
               }
               else {
                  state = remote;
               }
            }
            if (state != unconnected) {
               if (gdsMsgInstallHandler (id, notificatioCallbackC) < 0) {
                  echo ("error: unable to install callback handler");
                  cblock.lock();
                  gdsMsgClose (id);
                  cblock.unlock();
                  state = unconnected;
               }
               else {
                  /* make entry in look up table */
                  cblookup [id] = this;
               }
            }
            if (state == unconnected) {
               capabilities = 0;
            }
            else {
               echo (string ("supported capabilities: ") + 
                  ((capabilities & CMD_TEST) ? "testing  " : "") +
                  ((capabilities & CMD_TESTPOINT) ? "testpoints  " : "") +
                  ((capabilities & CMD_AWG) ? "awg  " : ""));
            }
         }
      }
      /* close command */
      else if (cmd == "close") {
         if (state == unconnected) {
            echo (string ("error: not connected to a diagnostics kernel"));
         }
         else {
            echoNotification ();
            cblock.lock();
            cblookup.erase (id);
            cblock.unlock();
            gdsMsgClose (id);
            state = unconnected;
         }
      }
      /* read command */
      else if (cmd == "read") {
         if (!read (prm)) {
            echo ("error: file not found: " + prm);
         }
      }
      /* unknown */
      else if (state == unconnected) {
         echo ("error: unrecognized command: " + line);
      }
      
      /* save command */
      else if (cmd == "save") {
         char*		reply = 0;
         int		rLen = 0;
         int		ret = 0;
         string		filename = fileName (prm);
      
         if (filename.empty() || 
         ((filename.size() == 1) && (filename[0] == ':'))) {
            echo ("error: illegal filename");
            return true;
         }
         // optimize access time when kernel is local, 
         // i.e. always use remote save
         if ((state == local) && (filename[0] != ':')) {
            filename = string (":") + filename;
         }
      
         if ((filename.size() > 1) && (filename[0] == ':')) {
            /* remote save */
            filename.erase (0, 1);
            ret = gdsMsgSend 
               (id, string (msg + fileFlags (prm) + " " + filename).c_str(), 
               "", 0, &reply, &rLen);
            echo (ret, reply);
            free (reply);
         }
         else {
            /* local save */
            ofstream	out (filename.c_str());
            if (!out) {
               echo ("error: illegal filename");
               return true;
            }
            ret = gdsMsgSend 
               (id, string (msg + fileFlags (prm)).c_str(), 
               "", 0, &reply, &rLen);
            if ((ret < 0) || (rLen <= 0) || (reply == 0) ||
            (gds_strncasecmp (reply, "error", 5) == 0)) {
               echo (ret, reply);
               free (reply);
               return true;
            }
            out.write (reply, rLen);
            out.close();
            free (reply);
            if (!out) {
               echo ("error: unable to write file");
            }
            else {
               echo (filename + " saved");
            }
         }
      }
      
      /* restore command */
      else if (cmd == "restore") {
         char*		reply = 0;
         int		rLen = 0;
         int		ret = 0;
         string		filename = fileName (prm);
      
         if (filename.empty() || 
         ((filename.size() == 1) && (filename[0] == ':'))) {
            echo ("error: illegal filename");
            return true;
         }
         // optimize access time when kernel is local, 
         // i.e. always use remote restore
         if ((state == local) && (filename[0] != ':')) {
            filename = string (":") + filename;
         }
      
         if ((filename.size() > 1) && (filename[0] == ':')) {
            // remote restore
            filename.erase (0, 1);
            ret = gdsMsgSend 
               (id, string (msg + fileFlags (prm) + " " + filename).c_str(), 
               "", 0, &reply, &rLen);
            echo (ret, reply);
            free (reply);
         }
         else {
            // local restore
            ifstream	inp (filename.c_str());
            if (!inp) {
               echo ("error: illegal filename");
               return true;
            }
            inp.seekg (0, ios::end);
            int	fsize = inp.tellg();
            inp.seekg (0, ios::beg);
            char* p = new (nothrow) char[fsize + 10];
            if ((fsize <= 0) || (p == 0)) {
               delete [] p;
               echo ("error: file too long");
               return true;
            }
            if (!inp.read (p, fsize)) {
               delete [] p;
               inp.close();
               echo ("error: unable to read file");
               return true;
            }
            inp.close();
            ret = gdsMsgSend 
               (id, string (msg + fileFlags (prm)).c_str(), 
               p, fsize, &reply, &rLen);
            delete [] p;
            if ((ret < 0) || (rLen <= 0) || (reply == 0) ||
            (gds_strncasecmp (reply, "error", 5) == 0)) {
               echo (ret, reply);
               free (reply);
               return true;
            }
            free (reply);
            echo (filename + " restored");
         }
      }
      /* messages on/off command */
      else if (cmd == "messages") {
         if (prm == "on") {
            fastmessages = true;
         }
         else if (prm == "off") {
            fastmessages = false;
         }
         else {
            echo ("error: illegal argument");
         }  
      }
      
      /* send message to diagnostics kernel */
      else {
         char*		reply = 0;
         int		rLen = 0;
         int		ret = 0;
      
         ret = gdsMsgSend (id, string (msg + ' ' + prm).c_str(), "", 0, 
            &reply, &rLen);
         echo (ret, reply);
         free (reply);
      }
   
      return true;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Method Name: read							*/
/*                                                         		*/
/* Method Description: read a file					*/
/*                                                         		*/
/* Method Arguments: filename						*/
/*                                                         		*/
/* Method Returns: true if successful					*/
/*                                                         		*/
/*----------------------------------------------------------------------*/     
   bool basic_commandline::read (const string& filename)
   {
      ifstream		inp (filename.c_str());
      string		line;
   
      /* check if file exists */
      if (!inp) {
         return false;
      }
   
     /* read in file and call interpreter */
      getline (inp, line, '\n');
      while (inp) {
      	 /* remove blanks */
         while (!line.empty() && 
         ((line[0] == ' ') || (line[0] == '\t'))) {
            line.erase (0, 1);
         }
         if ((!line.empty()) && (line[0] != '#')) {
            // echo (line);
            parse (line);
         }
         getline (inp, line, '\n');
      }
      return true;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Method Name: echo							*/
/*                                                         		*/
/* Method Description: command line interpreter	echo			*/
/*                                                         		*/
/* Method Arguments: echo line						*/
/*                                                         		*/
/* Method Returns: true if successful					*/
/*                                                         		*/
/*----------------------------------------------------------------------*/     
   bool basic_commandline::echo (const string& line, bool notification)
   {
      string last = line;
      // Get rid of trailing '\n' character, since the printline
      // method adds a trailing '\n'.
      if (!last.empty()  && (last[last.size() - 1] == '\n')) {
         last.erase (last.size() - 1, 1);
      }
      if (!last.empty()) {
	 // Save a limited number of echo strings.  Not sure why...
         lastecho.push_back (last);
         while (lastecho.size() > 5) {
            lastecho.pop_front();
         }
	 // printline is implemented in the commandline class as a
	 // simple printf to stdout with a trailing '\n'
         if (!silent) {
            printline (last);
         }
      }
      if (notification) {
         echoNotification ();
      }
      return true;
   }


   bool basic_commandline::echo (int error, char* reply)
   {
      if (error < 0) {
         echo ("error: unable to send command to diagnostics kernel");
      }
      else if (error == 0) {
         if ((reply != 0) && (strlen (reply) > 0)) {
            echo (string (reply));
         }
         else {
            echoNotification ();
         }
      }
      else {
         echo ("error: unknown");
      }
      return true;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Method Name: echoNotification					*/
/*                                                         		*/
/* Method Description: notification echo				*/
/*                                                         		*/
/* Method Arguments: 							*/
/*                                                         		*/
/* Method Returns: true if successful					*/
/*                                                         		*/
/*----------------------------------------------------------------------*/     
   bool basic_commandline::echoNotification ()
   {
      cblock.lock();
      if ((nMessages.get() != 0) && (nMessages->tellp() > 0)) {
         // :COMPILER: the string returned by ostringstream.str()
         // is buggy; it has an incorrect size
         string		nfy (nMessages->str().c_str());
         nMessages.reset (new (nothrow) ostringstream(""));
         cblock.unlock();
      
         if (!nfy.empty()  && (nfy[nfy.size() - 1] == '\n')) {
            nfy.erase (nfy.size() - 1, 1);
         }
         if (!nfy.empty()) {
            lastnotification.push_back (nfy);
            while (lastnotification.size() > 5) {
               lastnotification.pop_front();
            }
            if (!silent) {
               printline (nfy);
            }
         }
         //echo (nfy, false);
         //printf ("nfy (%i) = %s\n", nfy.size(), nfy.c_str());
      }
      else {
         cblock.unlock();
      }
      return true;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Method Name: getVar / putVar 					*/
/*                                                         		*/
/* Method Description: read/write a parameter				*/
/*                                                         		*/
/* Method Arguments: variable name, value				*/
/*                                                         		*/
/* Method Returns: true if successful					*/
/*                                                         		*/
/*----------------------------------------------------------------------*/  
   bool basic_commandline::getVar (const string& var, double& val, int n)
   {
      string	s;
      if (!getVar (var, s)) {
         return false;
      }
      const char* p = s.c_str();
      for (int i = 0; i < n; i++) {
         (&val)[i] = strtod (p, (char**) &p);
      }
      return true;
   }

   bool basic_commandline::getVar (const string& var, float& val, int n)
   {
      string	s;
      if (!getVar (var, s)) {
         return false;
      }
      const char* p = s.c_str();
      for (int i = 0; i < n; i++) {
         (&val)[i] = strtod (p, (char**) &p);
      }
      return true;
   }


   bool basic_commandline::getVar (const string& var, float** val, int& n)
   {
      *val = 0;
      string	s;
      if (!getVar (var, s)) {
         return false;
      }
      const char* next = s.c_str();
      const char* p;
      n = -1;
      do {
         n++;
         p = next;
         strtod (p, (char**) &next);
      } while (p != next);
      *val = new float [n];
      if (*val == 0) {
         return false;
      }
      p = s.c_str();
      for (int i = 0; i < n; i++) {
         (*val)[i] = strtod (p, (char**) &p);
      }
      return true;
   }


   bool basic_commandline::getVar (const string& var, int& val, int n)
   {
      string	s;
      if (!getVar (var, s)) {
         return false;
      }
      const char* p = s.c_str();
      for (int i = 0; i < n; i++) {
         (&val)[i] = strtol (p, (char**) &p, 10);
      }
      return true;
   }

   bool basic_commandline::getVar (const string& var, bool& val, int n)
   {
      string	s;
      if (!getVar (var, s)) {
         return false;
      }
      char		buf[1024];	/* buffer */
      char*		wrk;		/* work space */
      strncpy (buf, s.c_str(), sizeof (buf));
      buf[sizeof(buf)-1] = 0;
      char* p =strtok_r (buf, " \t\n", &wrk);
      for (int i = 0; i < n; i++) {
         if (p != 0) {
            (&val)[i] = (tolower (p[0]) == 't');
            p = strtok_r (NULL, " \t\n", &wrk);
         }
         else {
            (&val)[i] = false;
         }
      }
      return true;
   }

   bool basic_commandline::getVar (const string& var, string& val)
   {
      char*		reply = 0;
      int		rLen = 0;
      int		ret = 0;
   
      ret = gdsMsgSend (id, string ("get " + var).c_str(), "", 0, 
         &reply, &rLen);
      if ((ret < 0) || (reply == 0) ||
      (strncmp (reply, "error", 5) == 0)) {
         val = "";
         free (reply);
         return false;
      }
      else {
         // remove everything before equal sign
         char* p = strchr (reply, '=');
         if (p == 0) {
            val = "";
            free (reply);
            return false;
         }
         // remove leading blanks
         p++;
         while (*p == ' ') p++;
         // remove trailing blanks & newlines
         for (int i = strlen (p) - 1; i >= 0; i--) {
            if ((p[i] == ' ') || (p[i] == '\n')) {
               p[i] = 0;
            }
            else {
               break;
            }
         }
         val = p;
         free (reply);
         return true;
      }
   }

   bool basic_commandline::getTime (const string& var, unsigned long& sec, 
   unsigned long& nsec)
   {
      sec = 0;
      nsec = 0;
   
      string s;
      if (!getVar (var, s) || s.empty()) {
         return false;
      }
      string::size_type pos = s.find_last_of ('.');
      if (pos == string::npos) {
         pos = (s.size() > 9) ? s.size() - 9 : 0;
      }
      else {
         pos++;
      }
      if (pos > 0) {
         nsec = strtoul (s.c_str() + pos, 0, 10);
         s.erase (pos);
         sec = strtoul (s.c_str(), 0, 10);
      }
      else {
         sec = strtoul (s.c_str(), 0, 10);
      }
      return true;
   }

   bool basic_commandline::putVar (const string& var, double val)
   {
      char	buf[100];
      sprintf (buf, "%0.15g", val);
      return putVar (var, string (buf));
   }

   bool basic_commandline::putVar (const string& var, const double* val, int n)
   {
      char*	buf = new (nothrow) char [n * 32];
      if (buf == 0) {
         return false;
      }
      char* p = buf;
      for (int i = 0; i < n; i++) {
         sprintf (p, "%0.15g ", val[i]);
         p += strlen(p);
      }
      bool ret = putVar (var, string (buf));
      delete [] buf;
      return ret;
   }

   bool basic_commandline::putVar (const string& var, const float* val, int n)
   {
      char*	buf = new (nothrow) char [n * 32];
      if (buf == 0) {
         return false;
      }
      char* p = buf;
      for (int i = 0; i < n; i++) {
         sprintf (p, "%0.15g ", val[i]);
         p += strlen(p);
      }
      bool ret = putVar (var, string (buf));
      delete [] buf;
      return ret;
   }


   bool basic_commandline::putVar (const string& var, int val)
   {
      char	buf[100];
      sprintf (buf, "%i", val);
      return putVar (var, string (buf));
   }

   bool basic_commandline::putVar (const string& var, const int* val, int n)
   {
      char*	buf = new (nothrow) char [n * 32];
      if (buf == 0) {
         return false;
      }
      char* p = buf;
      for (int i = 0; i < n; i++) {
         sprintf (p, "%i ", val[i]);
         p += strlen(p);
      }
      bool ret = putVar (var, string (buf));
      delete [] buf;
      return ret;
   }

   bool basic_commandline::putVar (const string& var, bool val)
   {
      char	buf[100];
      sprintf (buf, "%s", val ? "true" : "false");
      return putVar (var, string (buf));
   }

   bool basic_commandline::putVar (const string& var, const bool* val, int n)
   {
      char*	buf = new (nothrow) char [n * 32];
      if (buf == 0) {
         return false;
      }
      char* p = buf;
      for (int i = 0; i < n; i++) {
         sprintf (p, "%s ", val[i] ? "true" : "false");
         p += strlen(p);
      }
      bool ret = putVar (var, string (buf));
      delete [] buf;
      return ret;
   }

   bool basic_commandline::putVar (const string& var, const string& val)
   {
      char*		reply = 0;
      int		rLen = 0;
      int		ret = 0;
   
      if (my_debug)
      {
	 // JCB - debug
	 string my_str("set " + var + " = " + val) ;
	 cerr << "putVar(): " << my_str << endl ;
      }

      ret = gdsMsgSend (id, string ("set " + var + " = " + val).c_str(), 
         "", 0, &reply, &rLen);
      if ((ret == 0) && ((reply == 0) || 
      (strncmp (reply, "error", 5) != 0))) {
         free (reply);
         return true;
      }
      else {
         free (reply);
         return false;
      }
   }

   bool basic_commandline::putTime (const string& var, unsigned long sec, 
   unsigned long nsec)
   {
      char buf[100];
      sprintf (buf, "%lu%09lu", sec, nsec);
      return putVar (var, string (buf));
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Method Name: getData							*/
/*                                                         		*/
/* Method Description: reads the data from the diagnostics kernel	*/
/*                                                         		*/
/* Method Arguments: name, data array, length, offset			*/
/*                                                        		*/
/* Method Returns: true if successful					*/
/*                                                         		*/
/*----------------------------------------------------------------------*/   
   bool basic_commandline::getData (const string& name, float*& x, int len, 
   int dtype, int ofs)
   {
      float*	reply = 0;
      int	rLen = 0;
   
      string msg = string ("get ") + name;
      int ret = gdsMsgData (id, msg.c_str(), dtype, 
         len, ofs, &reply, &rLen);
      if ((ret < 0)  || (reply == 0) || (rLen == 0)) {
         free (reply);
         x = 0;
         return false;
      }
      else {
         x = reply;
         return true;
      }
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Method Name: putData							*/
/*                                                         		*/
/* Method Description: Writes data to the diagnostics kernel		*/
/*                                                         		*/
/* Method Arguments: graph type, A channel, B channel, data array, 	*/
/*                   length, data type					*/
/*                                                        		*/
/* Method Returns: true if successful					*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   bool basic_commandline::putData (string& name, const float* x, 
   int len, int dtype, int ofs)
   {
      float*	send = (float*) x;
      int	sLen = (dtype % 10 == 1 ? 2 : 1) * len;
   
      string msg = string ("put ") + name;
      int ret = gdsMsgData (id, msg.c_str(), dtype, 
         len, ofs, &send, &sLen);
      if (ret < 0) {
         return false;
      }
      else {
         if (name.empty()) {
            char buf[256];
            sprintf (buf, "Result[%i]", ret);
            name = buf;
         }
         return true;
      }
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Method Name: sendMessage						*/
/*                                                         		*/
/* Method Description: send a message to the diagnostics kernel		*/
/*                                                         		*/
/* Method Arguments: msg, prm, pLen, res, rLen				*/
/*                                                         		*/
/* Method Returns: true if successful					*/
/*                                                         		*/
/*----------------------------------------------------------------------*/   
   bool basic_commandline::sendMessage (const string& msg, 
   const char* prm, int pLen,
   char*& reply, int& rLen)
   {
      if (my_debug)
	 cerr << "basic_commandline::sendMessage msg = " << msg << endl ;
      reply = 0;
      int ret = gdsMsgSend (id, msg.c_str(), prm, pLen, &reply, &rLen);
      if ((ret < 0) || ((reply != 0) && 
      (strncmp (reply, "error", 5) == 0))) {
         return false;
      }
      else {
         return true;
      }
   }


   bool basic_commandline::sendMessage (const string& msg, 
   char*& reply, int& rLen)
   {
      return sendMessage (msg, "", 0, reply, rLen);
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Method Name: readMasterIndex						*/
/*                                                         		*/
/* Method Description: reads the master index from the diag. kernel	*/
/*                                                         		*/
/* Method Arguments: master index					*/
/*                                                         		*/
/* Method Returns: true if index exists					*/
/*                                                         		*/
/*----------------------------------------------------------------------*/   
   bool basic_commandline::readMasterIndex (masterindex& i)
   {
      string mentry;
      string index = string (stIndex) + "." + stIndexEntry + "[0]";
      if (!getVar (index, mentry)) {
         i.clear();
         return false;
      }
      else {
         i = masterindex (mentry.c_str());
         for (masterindex::iterator iter = i.begin(); 
         iter != i.end(); iter++) {
         }
         return true;
      }
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Method Name: isXML							*/
/*                                                         		*/
/* Method Description: tests if a file is LIGO light weight format	*/
/*                                                         		*/
/* Method Arguments: filename, exists					*/
/*                                                         		*/
/* Method Returns: true if XML file					*/
/*                                                         		*/
/*----------------------------------------------------------------------*/   
   bool basic_commandline::isXML (const string& filename, bool* exists)
   {
      bool isXML = false;
      ifstream		inp (filename.c_str());
      if (inp) {
         if (exists != 0) *exists = true;
         // get the first non-empty line
         string	line;
         while (inp) {
            getline (inp, line, '\n');
            while (!line.empty() && (line[0] == ' ')) {
               line.erase (0, 1);
            }
            if (!line.empty()) {
               break;
            }
         }
         // check if it has an xml header
         isXML =
            line.find ("<?xml version=\"1.0\"?>") != string::npos;
      }
      else {
         if (exists != 0) *exists = false;
      }
      inp.close();
      return isXML;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Method Name: callback						*/
/*                                                         		*/
/* Method Description: callback function				*/
/*                                                         		*/
/* Method Arguments: msg, prm, pLen, res, rLen				*/
/*                                                         		*/
/* Method Returns: true if successful					*/
/*                                                         		*/
/*----------------------------------------------------------------------*/     
   bool basic_commandline::notify (const string& msg, 
   const char* prm, int pLen, char** res, int* rLen)
   {
      // store notification message
      if ((nMessages.get() != 0) && (msg.size() > 0)) {
         if (fastmessages) {
            printline (string ("\n") + msg);
         }
         else {
            *nMessages << msg;
            if (msg.rfind ('\n') != msg.size()) {
               *nMessages << endl;
            }
         }
      }  
      // return nothing
      *res = 0;
      *rLen = 0;
      return true;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Static parameter: cblookup						*/
/*                   cblock                             		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

   basic_commandline::callbacklookup	basic_commandline::cblookup;
   thread::mutex			basic_commandline::cblock;


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Method Name: cbfunc							*/
/*                                                         		*/
/* Method Description: callback function				*/
/*                                                         		*/
/* Method Arguments: filename						*/
/*                                                         		*/
/* Method Returns: true if successful					*/
/*                                                         		*/
/*----------------------------------------------------------------------*/     
   int basic_commandline::cbfunc (int id, const char* msg, 
   const char* prm, int pLen, char** res, int* rLen)
   {
      semlock		lockit (cblock);
   
      /* look for callback function */
      callbacklookup::const_iterator iter = cblookup.find (id);
      if (iter == cblookup.end()) {
         return -10;
      }
      /* make callback */
      if (iter->second->notify (string (msg), prm, pLen, res, rLen)) {
         return 0;
      }
      else {
         return -11;
      }
   }


}
