/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: gdsmsgcmd						*/
/*                                                         		*/
/* Module Description: implements functions for handling cmd msg's	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _DYNAMIC_LOAD
#ifdef OS_VXWORKS
#define _DYNAMIC_LOAD		0
#else
#define _DYNAMIC_LOAD		1
#endif
#endif


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Includes: 								*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <ctype.h>

#ifdef OS_VXWORKS
#include <vxWorks.h>
#include <semLib.h>
#include <taskLib.h>
#include <sockLib.h>
#include <inetLib.h>
#include <hostLib.h>
#include <ioLib.h>
#include <sysLib.h>
#include <timers.h>

#else
#include <pthread.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#endif
#if _DYNAMIC_LOAD
#include "gdscmd_d.h"
#else
#include "gdscmd.hh"
#endif
#include "gdsmsg.h"
#include "rgdsmsg.h"
#include "gdssock.h"
#include "gdstask.h"
#include "gdserr.h" /* JCB */


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Constants: _NETID		  net protocol used for rpc		*/
/*            _MAX_CHANNEL	  max # of open channels		*/
/*            _PROGVER		  rpc prog. ver.			*/
/*            _PROGVER_CB	  rpc prog. ver. of callback		*/
/*            _PRIORITY_CB	  priority of callback task		*/
/*            _KEEPALIVE_PRIORITY priority of keep alive task		*/
/*            _KEEPALIVE_NAME	  name of keep alive task		*/
/*            _TIMEOUT		  connection timeout (5 sec)   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
#define _NETID			"tcp"
#define DIAG_WKA		5354
#define _MAX_CHANNEL		10
#define _PROGVER		1
#define _PROGVER_CB		1
#ifdef OS_VXWORKS
#define _PRIORITY_CB		50
#define _KEEPALIVE_PRIORITY	95
#else
#define _PRIORITY_CB		15
#define _KEEPALIVE_PRIORITY	20
#endif
#define _KEEPALIVE_NAME		"tMsgAlive"
#define _TIMEOUT		5000000

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Types: msgChannel		channel information struct		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   struct msgChannel {
      mutexID_t		mux;	/* mutex to protect struct data */
      int		valid;	/* is valid channel? */
      int		local;	/* is local receiver? */
      int		flag;	/* flag */
      char		server[256];/* server name */
      gdsMsgCallback	cb_func;/* callback function */
      u_long		outProgNum; /* output channel rpc prog. number */
      u_long 		outProgVer; /* output channel rpc prog. version */
      u_long		inpProgNum; /* callback channel rpc prog. number */
      u_long		inpProgVer; /* callback channel rpc prog. version */
      CLIENT*		client;	/* client handle */
      SVCXPRT*		transport;  /* rpc transport handle */
      taskID_t		tid;	/* task ID of callback service */
   };
   typedef struct msgChannel msgChannel;

#ifndef __CYGWIN__
#ifndef _IN_ADDR_T
#define _IN_ADDR_T
   typedef unsigned int    in_addr_t;
#endif
#endif

   static int 		my_debug = 0 ;

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Globals: msgChn		channel information array		*/
/*          init		initialization flag			*/
/*          aliveTID		keep alive TID				*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   static msgChannel	msgChn[_MAX_CHANNEL];
   static int		init = 0;
   static taskID_t	aliveTID = 0;


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Forward declarations: 						*/
/*	rgdsmsgcb_1		dispatch handler for callback		*/
/*	cmdNotificationMsg		callback notification routine	*/
/*      								*/
/*----------------------------------------------------------------------*/
   extern void rgdsmsgcb_1 (struct svc_req *rqstp, 
                     SVCXPRT *transp);
#if 0
   extern enum clnt_stat gdsmsgopen_1 (u_long , int , int , open_ret*, 
                     CLIENT *);
#endif
   extern enum clnt_stat gdsmsgsend_1 (message_r , reply_r *, CLIENT *);
   extern enum clnt_stat gdsmsgclose_1 (int *, CLIENT *);

   static int cmdNotificationMsg (const char* msg, const char* prm, 
                     int pLen, char** res, int* rLen);


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: keepAlive					*/
/*                                                         		*/
/* Procedure Description: sends keep alive to gds command message server*/
/*                                                         		*/
/* Procedure Arguments: void						*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   static void keepAlive (void)
   {
      int 		id;	/* channel idnex */
      int		reply;	/* server reply */
      struct timespec   wait = {30, 0}; /* keep alive rate */
   
      while (1) {
         /* wait 30 seconds */
         nanosleep (&wait, NULL);
      
         /* loop over all channels */
         for (id = 0; id < _MAX_CHANNEL; id++) { 
         
            /* check validity and whether remote server */
            MUTEX_GET (msgChn[id].mux);
            if (!msgChn[id].valid || msgChn[id].local) {
               MUTEX_RELEASE (msgChn[id].mux);
               continue;
            }
         
            /* send keep alive message to server */
            if ((gdsmsgkeepalive_1 (&reply, msgChn[id].client) != 
               RPC_SUCCESS) || (reply < 0)) {
               /* stop callback service */
               rpcStopCallbackService 
                  (msgChn[id].inpProgNum, msgChn[id].inpProgVer, 
                  msgChn[id].transport, msgChn[id].tid);
               clnt_destroy (msgChn[id].client);
               msgChn[id].valid = 0;
            }
            MUTEX_RELEASE (msgChn[id].mux);
         }
      }
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: startupCmd					*/
/*                                                         		*/
/* Procedure Description: connects to gds command message server	*/
/*                                                         		*/
/* Procedure Arguments: server, callback prognum, id, flag, 		*/
/*			prgnum (return) capability (return)		*/
/*                                                         		*/
/* Procedure Returns: ID if successful, <0 when failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   static int startupCmd (const char* server, unsigned long cbprognum, 
                     int id, int flag, const char* conf, unsigned long* prognum, 
                     int* capability) 
   {
      int			sock;		/* socket */
      struct sockaddr_in	name;		/* socket name */
      char			buf[256];	/* input buffer */
      int			nRead;		/* received bytes */
      int			nB;		/* temp */
      char*			p;		/* buffer pointer */
      int			status;		/* status of server */
      //struct timeval	timeout = 	/* timeout */
      //{_TIMEOUT / 1000000, _TIMEOUT % 1000000};
      wait_time timeout = _TIMEOUT / 1000000.0;
   
      /* create socket */
      sock = socket (PF_INET, SOCK_STREAM, 0);
      if (sock == -1) {
         return -1;
      }
   
      /* bind socket */
      name.sin_family = AF_INET;
      name.sin_port = 0;
      name.sin_addr.s_addr = htonl (INADDR_ANY);
      if (bind (sock, (struct sockaddr*) &name, sizeof (name)) < 0) {
         close (sock);
         return -2;
      }
   
      /* fill destination address */
      name.sin_family = AF_INET;
      name.sin_port = htons (DIAG_WKA);
      if (nslookup (server, &name.sin_addr) < 0) {
         close (sock);
         return -3;
      }
   
      /* connect to server */
      if (connectWithTimeout (sock, (struct sockaddr*) &name, 
         sizeof (name), timeout) < 0) {
         close (sock);
         return -4;
      }
   
      /* send startup commamnd */
      *((unsigned long*) buf) = htonl (cbprognum);
      *((int*) (buf + 4)) = (int) htonl (id);
      *((int*) (buf + 8)) = (int) htonl (flag);
      *((int*) (buf + 12)) = (conf == NULL) ? 0 : (int) htonl (strlen (conf));
   
      send (sock, buf, 16, 0);
      if (conf != NULL) {
         send (sock, conf, strlen (conf), 0);
      }
   
      /* wait for status */
      nRead = 0;
      p = buf;
      do {
         nB = recv (sock, p, 4 - nRead, 0);
         if (nB <= 0) {
            return -1;
         }
         p += nB;
         nRead += nB;
      } while (nRead < 4);
      buf[4] = 0;
      status = (int) ntohl (*((unsigned long*) buf));
      if (status < 0) {
         close (sock);
         return -6;
      }
   
      /* get prognum and capabilities */
      nRead = 0;
      p = buf;
      do {
         nB = recv (sock, p, 8 - nRead, 0);
         if (nB <= 0) {
            return -1;
         }
         p += nB;
         nRead += nB;
      } while (nRead < 8);
      buf[8] = 0;
      if (prognum != NULL) {
         *prognum = ntohl (*((unsigned long*) buf));
      }
      if (capability != NULL) {
         *capability = (int) ntohl (*((unsigned long*) (buf + 4)));
      }
   
      close (sock);
      return 0;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: gdsMsgOpen					*/
/*                                                         		*/
/* Procedure Description: connects to gds command message server	*/
/*                                                         		*/
/* Procedure Arguments: server name, flag, capability (return)		*/
/*                                                         		*/
/* Procedure Returns: ID if successful, <0 when failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   int gdsMsgOpen (const char* server, int flag, const char* conf,
                  int* capability)
   {
      int		i;	/* channel index */
      int		j;
      int		status; /* supported services */
   
      /* rpc init for VxWorks */
   #ifdef OS_VXWORKS
      rpcTaskInit ();
   #endif
   
      /* initialize */
      if (!init) {
         memset (msgChn, 0, _MAX_CHANNEL * sizeof (msgChannel));
         for (i = 0; i < _MAX_CHANNEL; i++) {
            if (MUTEX_CREATE (msgChn[i].mux) != 0) {
               return -1;
            }
         }
         init = 1;
      }
   
      /* test for empty slot */
      for (i = 0; i < _MAX_CHANNEL; i++) {
         MUTEX_GET (msgChn[i].mux);
         if (!msgChn[i].valid) {
            break;
         }
         MUTEX_RELEASE (msgChn[i].mux);
      }
      if (i >= _MAX_CHANNEL) {
         return -2;
      }
   
      /* open connection to server */
      if ((server != NULL) && (strlen (server) > 0)) {
         struct timeval 	timeout = {3, 0};
      
         msgChn[i].local = 0;
         strncpy (msgChn[i].server, server, sizeof (msgChn[i].server));
         msgChn[i].server[sizeof (msgChn[i].server)-1] = 0;
         /* start rpc callback service */
         msgChn[i].inpProgVer = _PROGVER_CB;
         if (rpcStartCallbackService (&msgChn[i].inpProgNum,
            msgChn[i].inpProgVer, &msgChn[i].transport, &msgChn[i].tid, 
            _PRIORITY_CB, rgdsmsgcb_1) < 0) {
            clnt_destroy (msgChn[i].client);
            MUTEX_RELEASE (msgChn[i].mux);
            return -3;
         }
      
         /* start diagnostics kernel through socket connection */
         if (startupCmd (server, msgChn[i].inpProgNum, i, 
            flag, conf, &msgChn[i].outProgNum, &status) < 0) {
            rpcStopCallbackService 
               (msgChn[i].inpProgNum, msgChn[i].inpProgVer, 
               msgChn[i].transport, msgChn[i].tid);
            MUTEX_RELEASE (msgChn[i].mux);
            return -4;
         }
      
      	 /* make rpc client handle */
         if (!rpcProbe ((char*) server, msgChn[i].outProgNum,
            RPC_PROGVER_GDSMSG, _NETID, &timeout, &msgChn[i].client)) {
            rpcStopCallbackService 
               (msgChn[i].inpProgNum, msgChn[i].inpProgVer, 
               msgChn[i].transport, msgChn[i].tid);
            MUTEX_RELEASE (msgChn[i].mux);
            return -5;
         }
      
         /* start keep alive task if necessary */
         if (aliveTID == 0) {
            int		attr;	/* task creation attribute */
         #ifdef OS_VXWORKS
            attr = 0;
         #else
            attr = PTHREAD_CREATE_DETACHED | PTHREAD_SCOPE_SYSTEM;
         #endif
            if (taskCreate (attr, _KEEPALIVE_PRIORITY, &aliveTID, 
               _KEEPALIVE_NAME, (taskfunc_t) keepAlive, 0) < 0) {
               rpcStopCallbackService 
                  (msgChn[i].inpProgNum, msgChn[i].inpProgVer, 
                  msgChn[i].transport, msgChn[i].tid);
               clnt_destroy (msgChn[i].client);
               MUTEX_RELEASE (msgChn[i].mux);
               return -6;
            }
         }
      }
      /* open connection to local diagnostics kernel */
      else {
         /* only one connection to a local server allowed */
         for (j = 0; j < _MAX_CHANNEL; j++) {
            if (msgChn[j].valid && msgChn[j].local) {
               MUTEX_RELEASE (msgChn[i].mux);
               return -6;
            }
         } 
         msgChn[i].local = 1;
      
      #if _DYNAMIC_LOAD
         status = _gdsCmdInit (flag, conf);
         if ((status < 0) ||
            (_gdsCmdNotifyHandler (cmdNotificationMsg) < 0)) {
            MUTEX_RELEASE (msgChn[i].mux);
            return -7;
         }
      #else
         status = gdsCmdInit (flag, conf);
         if ((status < 0) ||
            (gdsCmdNotifyHandler (cmdNotificationMsg) < 0)) {
            MUTEX_RELEASE (msgChn[i].mux);
            return -8;
         }
      #endif
      }
   
      /* set valid bit and return */
      msgChn[i].valid = 1;
      msgChn[i].cb_func = NULL;
      if (capability != NULL) {
         *capability = status;
      }
      MUTEX_RELEASE (msgChn[i].mux);
      return i;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: gdsMsgClose					*/
/*                                                         		*/
/* Procedure Description: Disconnects from gds command message server	*/
/*                                                         		*/
/* Procedure Arguments: service id					*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 when failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   int gdsMsgClose (int id)
   {
      int		retval;		/* return value */
   
      /* check id */
      if (!init || (id < 0) || (id >= _MAX_CHANNEL) || 
         !msgChn[id].valid) {
         return -1;
      }
   
      /* get mutex */
      MUTEX_GET (msgChn[id].mux);
   
      /* close connection to server */
      if (!msgChn[id].local) {
         /* call remote server */
         if (gdsmsgclose_1 (&retval, msgChn[id].client) != RPC_SUCCESS) {
            retval = -98;
         }
      	 /* stop callback service */
         rpcStopCallbackService 
            (msgChn[id].inpProgNum, msgChn[id].inpProgVer, 
            msgChn[id].transport, msgChn[id].tid);
         clnt_destroy (msgChn[id].client);
      }
      /* close connection to local diagnostics kernel */
      else {
      #if _DYNAMIC_LOAD
         retval = _gdsCmdFini ();
      #else
         retval = gdsCmdFini ();
      #endif
      }
   
      /* reset valid bit */
      msgChn[id].valid = 0;
      MUTEX_RELEASE (msgChn[id].mux);
   
      /* check if keep alive task has to be canceled */
      if (aliveTID != 0) {
         int 		notlast = 0;	/* false if last */
         taskID_t	tempTID = 0;	/* tmp. TID of keep alive */
         int		i;		/* channel index */
      
         for (i = 0; i < _MAX_CHANNEL; i++) {
            MUTEX_GET (msgChn[i].mux);
            notlast = notlast || msgChn[i].valid;
         }
         if (!notlast) {
            tempTID = aliveTID;
            aliveTID = 0;
         }
         for (i = 0; i < _MAX_CHANNEL; i++) {
            MUTEX_RELEASE (msgChn[i].mux);
         }
         taskCancel (&tempTID);
      }
      return retval;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: gdsMsgSend					*/
/*                                                         		*/
/* Procedure Description: Send a message to the diagnostics kernel	*/
/*                                                         		*/
/* Procedure Arguments: service id, message, parameter, reply pointer	*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 when failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   int gdsMsgSend (int id, const char* msg, const char* prm, int pLen,
                  char** res, int* rLen)
   {
      int		retval;		/* return value */
      message_r		message;	/* message for remote server */
      reply_r		reply;		/* reply from remote server */
   
      /* check id */
      if (!init || (id < 0) || (id >= _MAX_CHANNEL) || 
         !msgChn[id].valid) {
         return -1;
      }
   
      if (my_debug) fprintf(stderr, "gdsMsgSend() msg = %s\n", msg) ; // JCB
      /* get mutex */
      MUTEX_GET (msgChn[id].mux);
   
      /* send message to server */
      if (!msgChn[id].local) {
         /* call remote server */
         message.name = (char*) msg;
         message.prm.prm_val = (char*) prm;
         message.prm.prm_len = pLen;
         reply.res.res_val = NULL;
         if (gdsmsgsend_1 (message, &reply, msgChn[id].client) != 
            RPC_SUCCESS) {
            retval = -98;
         }
         else {
            retval = reply.status;
            *res = reply.res.res_val;
            *rLen = reply.res.res_len;
         }
      }
      /* send message to local diagnostics kernel */
      else {
      #if _DYNAMIC_LOAD
         retval = _gdsCmd (msg, prm, pLen, res, rLen);
      #else
         retval = gdsCmd (msg, prm, pLen, res, rLen);
      #endif
      }
   
      /* return */
      MUTEX_RELEASE (msgChn[id].mux);
      return retval;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: gdsMsgData					*/
/*                                                         		*/
/* Procedure Description: Transfers data to/from the diagnostics kernel	*/
/*                                                         		*/
/* Procedure Arguments: service id, message, data type, length, offset	*/
/*                      data array, data array length      		*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 when failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   int gdsMsgData (int id, const char* msg, int datatype, int len,
                  int ofs, float** data, int* datalength)
   {
      int		retval;		/* return value */
      bmessage_r	message;	/* message for remote server */
      breply_r		reply;		/* reply from remote server */
      char		buf[256];	/* temporary text buffer */
      char		name[256];	/* object name */
      char*		p; 		/* text pointer */
      int		i;		/* temp. index */
      float		z;		/* temp. data point */
   
      /* check id */
      if (!init || (id < 0) || (id >= _MAX_CHANNEL) || 
         !msgChn[id].valid) {
         return -1;
      }
   
      /* check arguments */
      if ((datatype < 0) || (datatype % 10 > 4) || (len < 0) ||
         (ofs < 0) || (data == 0) || (datalength == 0)) {
         return -2;
      }
   
      if (my_debug) fprintf(stderr, "gdsMsgData() msg = %s\n", msg) ; // JCB
      /* determine transfer direction */
      strncpy (buf, msg, 255);
      buf[255] = 0;
      p = buf;
      while ((*p == ' ') || (*p == '\t')) {
         p++;
      }
      for (i = 0; (i < 3) && (i < strlen (p)); i++) {
         p[i] = tolower (p[i]);
      }
      if (strncmp (p, "put", 3) == 0) {
         message.toKernel = 1;
         p += 3;
      }
      else if (strncmp (p, "get", 3) == 0) {
         message.toKernel = 0;
         p += 3;
      }
      else {
         return -3;
      }
      /* determine object name */
      while ((*p == ' ') || (*p == '\t')) {
         p++;
      }
      if ((strlen (p) == 0) && !message.toKernel) {
         return -4;
      }
      strcpy (name, p);
   
      /* get mutex */
      MUTEX_GET (msgChn[id].mux);
   
      /* send message to server */
      if (!msgChn[id].local) {
         /* call remote server */
         message.name = name;
         message.dtype = datatype;
         message.len = len;
         message.ofs = ofs;
         if (message.toKernel) {
            message.prm.prm_val = *data;
            message.prm.prm_len = *datalength;
         }
         else {
            message.prm.prm_val = &z;
            message.prm.prm_len = 0;
         }
         reply.res.res_val = NULL;
         if (gdsmsgdata_1 (message, &reply, msgChn[id].client) != 
            RPC_SUCCESS) {
            retval = -98;
         }
         else {
            retval = reply.status;
            if (message.toKernel) {
               free (reply.res.res_val);
            }
            else {
               *data = reply.res.res_val;
               *datalength = reply.res.res_len;
            }
         }
      }
      /* send message to local diagnostics kernel */
      else {
      #if _DYNAMIC_LOAD
         retval = _gdsCmdData (name, message.toKernel, datatype, 
                              len, ofs, data, datalength);
      #else
         retval = gdsCmdData (name, message.toKernel, datatype, 
                              len, ofs, data, datalength);
      #endif
      }
   
      /* return */
      MUTEX_RELEASE (msgChn[id].mux);
      return retval;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: gdsMsgInstallHandler			*/
/*                                                         		*/
/* Procedure Description: Send a message to the diagnostics kernel	*/
/*                                                         		*/
/* Procedure Arguments: service id, message, parameter, reply pointer	*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 when failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   int gdsMsgInstallHandler (int id, gdsMsgCallback callback)
   {
      /* check id */
      if (!init || (id < 0) || (id >= _MAX_CHANNEL) || 
         !msgChn[id].valid || (msgChn[id].cb_func != NULL)) {
         return -1;
      }
   
      /* get mutex */
      MUTEX_GET (msgChn[id].mux);
   
      msgChn[id].cb_func = callback;
   
      /* return */
      MUTEX_RELEASE (msgChn[id].mux);
      return 0;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* internal Procedure Name: cmdNotificationMsg				*/
/*                                                         		*/
/* Procedure Description: Sends a notfication to the callback		*/
/*                                                         		*/
/* Procedure Arguments: message, parameter, reply pointer		*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 when failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   static int cmdNotificationMsg (const char* msg, const char* prm, 
                     int pLen, char** res, int* rLen)
   {
      int		i;
   
      for (i = 0; i < _MAX_CHANNEL; i++) {
         if (msgChn[i].valid && msgChn[i].local) {
            break;
         }
      }
   
      /* error if no local message channel */
      if (i >= _MAX_CHANNEL) {
         *res = NULL;
         rLen = 0;
         return -1;
      }
   
      /* just acknowledge if no callback */
      if (msgChn[i].cb_func == NULL) {
         *res = NULL;
         rLen = 0;
         return 0;
      }
   
      /* else invoke callback routine */
      return (*msgChn[i].cb_func) (i, msg, prm, pLen, res, rLen);
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Remote Procedure Name: gdsmsgnotify_1_svc				*/
/*                                                         		*/
/* Procedure Description: receives a message 				*/
/*                                                         		*/
/* Procedure Arguments: service id, message, reply, transport handle	*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 if error			*/
/*                    reply	                        		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   bool_t gdsmsgnotify_1_svc (int id, message_r msg, reply_r* reply, 
                     struct svc_req* transport)
   {
      /* check id */
      if ((id < 0) || (id >= _MAX_CHANNEL) || 
         !msgChn[id].valid || msgChn[id].local) {
         reply->status = -99;
         return FALSE;
      }
   
      /* just acknowledge if no callback */
      if (msgChn[id].cb_func == NULL) {
         reply->status = 0;
         reply->res.res_len = 0;
         reply->res.res_val = malloc (1);
	 if (reply->res.res_val == NULL) /* JCB */
	 {
	    gdsDebug("gdsmsgnotify_1_svc malloc(1) failed.") ;
	    return FALSE ;
	 }
      }
      /* invoke callback routine */
      else {
         reply->status = (*msgChn[id].cb_func) 
                         (id, msg.name, msg.prm.prm_val, msg.prm.prm_len, 
                         &reply->res.res_val, (int*) &reply->res.res_len);
         if (reply->res.res_val == NULL) {
            reply->res.res_len = 0;
            reply->res.res_val = malloc (1);
	    if (reply->res.res_val == NULL) /* JCB */
	    {
	       gdsDebug("gdsmsgnotify_1_svc malloc(1) (2) failed.") ;
	       return FALSE ;
	    }
         }
      }
   
      return TRUE;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Remote Procedure Name: rgdsmsgcb_1_freeresult			*/
/*                                                         		*/
/* Procedure Description: frees memory of rpc call			*/
/*                                                         		*/
/* Procedure Arguments: rpc transport info, xdr routine for result,	*/
/*			pointer to result				*/
/*                                                         		*/
/* Procedure Returns: TRUE if successful, FALSE if failed		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   int rgdsmsgcb_1_freeresult (SVCXPRT* transp, 
                     xdrproc_t xdr_result, caddr_t result)
   {
      (void) xdr_free (xdr_result, result);
      return TRUE;
   }

