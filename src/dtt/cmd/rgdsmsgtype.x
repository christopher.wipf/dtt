
/* GDS test point rpc interface */

/* fix include problems with VxWorks */
#ifdef RPC_HDR
%#define		_RPC_HDR
#endif
#ifdef RPC_XDR
%#define		_RPC_XDR
#endif
#ifdef RPC_SVC		
%#define		_RPC_SVC
#endif
#ifdef RPC_CLNT		
%#define		_RPC_CLNT
#endif
%#include "rpcinc.h"


/* message structure */
struct message_r {
      string		name<>;		/* message name */
      opaque		prm<>;		/* message parameter */
};

/* reply structure */
struct reply_r {
      int		status;		/* status */
      opaque		res<>;		/* message answer */
};


/* data type */
enum datatype_r {
      datatype_r_asis = 0,
      datatype_r_cmlx = 1,
      datatype_r_real = 2,
      datatype_r_imag = 3
};

/* binary data message structure */
struct bmessage_r {
      bool		toKernel;	/* Transfer direction */
      string		name<>;		/* data object name */
      enum datatype_r	dtype;		/* data type */
      int		len;		/* data length */
      int		ofs;		/* data offset */
      float		prm<>;		/* message parameter */
};

/* data reply structure */
struct breply_r {
      int		status;		/* status */
      float		res<>;		/* binary data */
};
