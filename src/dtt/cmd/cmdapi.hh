/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: cmdapi							*/
/*                                                         		*/
/* Module Description: API for command line interface			*/
/*									*/
/*                                                         		*/
/* Module Arguments: none				   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 14Jan99  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: cmdapi.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8336  (509) 372-2178  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Ultra-Enterprise, Solaris 5.6			*/
/*	Compiler Used: sun workshop C 4.2				*/
/*	Runtime environment: sparc/solaris				*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			TBD			*/
/*			  POSIX			TBD			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*								 	*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_CMDAPI_H
#define _GDS_CMDAPI_H


/* Header File List: */
#include <string>
#include <map>
#include <vector>
#include <deque>
#include <memory>
#include "gmutex.hh"

namespace diag {

/** @name Basic Command Line API
    This module implements the basic API for the command line
    to communicate with the diagnostics kernel. 
    The following commands are supported 
    by the command line interface rather than the diagnostics
    kernel:
    
    open 'server': opens a communication channel to a 
    diagnostics kernel. If the server argument is omitted a 
    diagnostics kernel is loaded locally.

    close: closes the connection.

    exit: exits the command line interface

    read 'script': reads an ASCII file and interprets each line
    which doesn't start with a \# as a command.

    help: shows a help screen

    The following command line options are suported:

    -s 'name': starts the command line and opens a connection
    to the specified server.

    -l:  starts the command line and opens a connection
    to a local diagnostics kernel.

    -f 'filename':  starts the command line and reads
    the script into the command line interpreter.



    @memo Command line API to diagnostics kernel
    @author Written January 1999 by Daniel Sigg
    @version 0.1
************************************************************************/

/*@{*/

/** Basic command line interpreter. This class defines methods to
    communicate with the diagnsotics kernel.

    @memo Class to manage the command line interpreter
    @author DS, January 99
************************************************************************/
   class basic_commandline {
      /// Notification callback is a friend
      friend int notificatioCallback (int id, const char* msg, 
                        const char* prm, int pLen, char** res, int* rLen);
   public:
   
      /// index entry
      class indexentry {
      protected:
         /// measurement type
         int		mtype;
         /// measurement step
         int		step;
      
      public:
         /// creates an index entry from its master index entry
         explicit indexentry (const std::string& text);
         /// creates a default index entry
         indexentry () : step (-1) {
         }
      
         /// get measurement step
         int getStep () const {
            return step; }
         /// get measurement type
         std::string getMeasType () const;
         /// get graph type
         int getGraphType () const {
            return mtype - 1; }
      };
   
      /// master index
      class masterindex : public std::vector<indexentry> {
      public:
         /// Create a master index from the master entry
         explicit masterindex (const char* mentry = 0);
      };
   
      /** Constructs a basic command line object object.
          @memo Constructor.
      	  @param Silent no echo to terminal if true
          @return void
       ******************************************************************/
      basic_commandline (bool Silent = false);
   
      /** Constructs command line object object.
          @memo Constructor.
          @param argc number of arguments
          @param argv list of arguments
      	  @param Silent no echo to terminal if true
          @return void
       ******************************************************************/
      basic_commandline (int argc, char* argv[], bool Silent = false);
   
      /** Destructs the basic command line object.
          @memo Destructor.
          @return void
       ******************************************************************/
      virtual ~basic_commandline ();
   
      /** Sets up the basic command line interface.
          @memo Setup.
          @param argc number of arguments
          @param argv list of arguments
          @return void
       ******************************************************************/
      virtual bool setup (int argc, char* argv[]);
   
      /** Returns true if command line interpreter is finished.
          @memo Not operator.
          @return true if finished
       ******************************************************************/
      bool operator ! () const {
         return finished;
      }
   
      /** Returns true if connection to diagnostics kernel could be
          established.
          @memo Is connect method.
          @return true if conneteced
       ******************************************************************/
      virtual bool isConnected () const {
         return state != unconnected;
      }
   
      /** Returns true if the specified capability is available
          through the connected diagnostics kernel. The argument
          can be one of the following:
          @memo Has capability method. 0 - diagnostics tests,
          1 - test point manager, 2 - arbitrary waveform generator,
          or -1 for all of the above.
	  @param cap Capability to be tested.
          @return true if capability is supported
       ******************************************************************/
      virtual bool HasCapability (int cap) const;
   
      /** Parses a command line.
          @param line command line
          @memo Parse method.
          @return true if successful
       ******************************************************************/
      bool parse (const std::string& line);
   
      /** Reads a script file.
          @param filename name of script file
          @memo Read script method.
          @return true if successful
       ******************************************************************/
      bool read (const std::string& filename);
   
      /** Echos a string.
          @param line echo line
          @param notification if true adds notification messages
          @memo Echo method.
          @return true if successful
       ******************************************************************/
      virtual bool echo (const std::string& line, bool notification = true);
   
      /** Echos an error messages.
          @param error Error code.
	  @param reply message string
          @memo Echo method.
          @return true if successful
       ******************************************************************/
      virtual bool echo (int error, char* reply);
   
      /** Echos notification messages.
          @memo Echo notification method.
          @return true if successful
       ******************************************************************/
      virtual  bool echoNotification ();
   
      /** Get previous echo messages.
          @memo Get echo messages method.
          @return list of echo messages (most recent at end)
       ******************************************************************/
      virtual const std::deque<std::string>& getEcho() const {
         return lastecho; }
      /** Get previous notification messages.
          @memo Get notification messages method.
          @return list of notification messages (most recent at end)
       ******************************************************************/
      virtual const std::deque<std::string>& getNotification() const {
         return lastnotification; }
      /** Clear previous echo messages.
          @memo Clear echo messages method.
          @return void
       ******************************************************************/
      virtual void clearEcho () {
         lastecho.clear(); }
      /** Clear previous notification messages.
          @memo Clear notification messages method.
          @return void
       ******************************************************************/
      virtual void clearNotification () {
         lastnotification.clear(); }
   
      /** Reads a variable (parameter) from the diagnostics kernel.
          @memo Get var method.
          @param var Parameter name
          @param val Value (return)
          @param n Number of values to read
          @return true if successful
       ******************************************************************/
      bool getVar (const std::string& var, double& val, int n = 1);
      /** Reads a variable (parameter) from the diagnostics kernel.
          @memo Get var method.
          @param var Parameter name
          @param val Value (return)
          @param n Number of values to read
          @return true if successful
       ******************************************************************/
      bool getVar (const std::string& var, float& val, int n = 1);
      /** Reads a variable (parameter) from the diagnostics kernel.
          The caller is responsible to free the return array using
          delete [];
          @memo Get var method.
          @param var Parameter name
          @param val Value (return)
          @param n Number of values read (return)
          @return true if successful
       ******************************************************************/
      bool getVar (const std::string& var, float** val, int& n);
      /** Reads a variable (parameter) from the diagnostics kernel.
          @memo Get var method.
          @param var Parameter name
          @param val Value (return)
          @param n Number of values to read
          @return true if successful
       ******************************************************************/
      bool getVar (const std::string& var, int& val, int n = 1);
      /** Reads a variable (parameter) from the diagnostics kernel.
          @memo Get var method.
          @param var Parameter name
          @param val Value (return)
          @param n Number of values to read
          @return true if successful
       ******************************************************************/
      bool getVar (const std::string& var, bool& val, int n = 1);
      /** Reads a variable (parameter) from the diagnostics kernel.
          @memo Get var method.
          @param var Parameter name
          @param val Value (return)
          @return true if successful
       ******************************************************************/
      bool getVar (const std::string& var, std::string& val);
      /** Reads a time parameter from the diagnostics kernel.
          @memo Get time method.
          @param var Parameter name
          @param sec GPS seconds (return)
          @param nsec GPS nanoseconds (return)
          @return true if successful
       ******************************************************************/
      bool getTime (const std::string& var, unsigned long& sec, 
                   unsigned long& nsec);
      /** Write a variable (parameter) to the diagnostics kernel.
          @memo Put var method.
          @param var Parameter name
          @param val Value
          @return true if successful
       ******************************************************************/
      bool putVar (const std::string& var, double val);
      /** Write a variable (parameter) to the diagnostics kernel.
          @memo Put var method.
          @param var Parameter name
          @param val Value
          @param n number of points
          @return true if successful
       ******************************************************************/
      bool putVar (const std::string& var, const double* val, int n);
      /** Write a variable (parameter) to the diagnostics kernel.
          @memo Put var method.
          @param var Parameter name
          @param val Value
          @param n number of points
          @return true if successful
       ******************************************************************/
      bool putVar (const std::string& var, const float* val, int n);
      /** Write a variable (parameter) to the diagnostics kernel.
          @memo Put var method.
          @param var Parameter name
          @param val Value
          @return true if successful
       ******************************************************************/
      bool putVar (const std::string& var, int val);
      /** Write a variable (parameter) to the diagnostics kernel.
          @memo Put var method.
          @param var Parameter name
          @param val Value
          @param n number of points
          @return true if successful
       ******************************************************************/
      bool putVar (const std::string& var, const int* val, int n);
      /** Write a variable (parameter) to the diagnostics kernel.
          @memo Put var method.
          @param var Parameter name
          @param val Value
          @return true if successful
       ******************************************************************/
      bool putVar (const std::string& var, bool val);
      /** Write a variable (parameter) to the diagnostics kernel.
          @memo Put var method.
          @param var Parameter name
          @param val Value
          @param n number of points
          @return true if successful
       ******************************************************************/
      bool putVar (const std::string& var, const bool* val, int n);
      /** Write a variable (parameter) to the diagnostics kernel.
          @memo Put var method.
          @param var Parameter name
          @param val Value
          @return true if successful
       ******************************************************************/
      bool putVar (const std::string& var, const std::string& val);
      /** Write a time parameter to the diagnostics kernel.
          @memo Put time method.
          @param var Parameter name
          @param sec GPS seconds (return)
          @param nsec GPS nanoseconds (return)
          @return true if successful
       ******************************************************************/
      bool putTime (const std::string& var, unsigned long sec, 
                   unsigned long nsec = 0);
   
      /** Reads data from the diagnostics kernel. The supported
          data types are: 0 - real/asis, 1 - complex, 2 - real part, 
   	  and 3 - imaginary part. When successful, this method will 
          allocate a new data array. The caller is reponsible to 
          free the array (use free).
         
          @memo Get data method.
          @param name Data object name
          @param x Data array (used to store return result)
          @param len Length of data array
          @param dtype data type
          @param ofs Offset into data object
          @return true if successful
       ******************************************************************/
      bool getData (const std::string& name, float*& x, int len, 
                   int dtype = 0, int ofs = 0);
   
      /** Writes data to the diagnostics kernel. The supplied
          name must describes a data object name of the form 
          "Result[#]" or "Reference[#]"; or it can be empty, in which
          case a new "Result[#]" is chosen. The data type is  
   	  either: 1 - complex, 2 - real. When a new data object is 
          requested the following has to be added: 10 for time series, 
          20 - for spectrum, 30 - for transfer function, 40 - for 
          list of coefficient. Additionally, the data length and 
          the offset into the data object have to be specified in number 
          of points (i.e. 1 complex number point = 2 floating point 
          numbers). If an empty name is supplied the new name is 
          returned.
         
          @memo Put data method.
          @param name Name of the data object
          @param x Data array
          @param len Length of data array
          @param dtype data type
          @param ofs Offset into data object
          @return true if successful
       ******************************************************************/
      bool putData (std::string& name, const float* x, int len, 
                   int dtype = 0, int ofs = 0);
   
      /** Send a message to the diagnostics kernel. The reply string has
          to be freed by the caller (using free).
          @memo Send message method.
          @param msg Message header
          @param prm Parameter data
          @param pLen Length of parameter data
          @param reply Reply data
          @param rLen Length of reply data
          @return true if successful
       ******************************************************************/
      bool sendMessage (const std::string& msg, const char* prm, int pLen,
                       char*& reply, int& rLen);

     /** Send a message to the diagnostics kernel. The reply string has
          to be freed by the caller (using free). An empty parameter
          data array is supplied automatically.
          @memo Send message method.
          @param msg Message header
          @param reply Reply data
          @param rLen Length of reply data
          @return true if successful
       ******************************************************************/
      bool sendMessage (const std::string& msg, char*& reply, int& rLen);
   
      /** Read the master index from the diagnostics kernel. Returns
          true if index exists.
          @memo Read master index method.
          @param i master index object (return)
          @return true if index exists
       ******************************************************************/
      bool readMasterIndex (masterindex& i);
   
      /** Test if a file is LIGO light weight format.
          @memo Is XML method.
          @param filename Name of file to test
          @param exists Set true if file exists
          @return true if LIGO light weight
       ******************************************************************/
      static bool isXML (const std::string& filename, bool* exists = 0);
   
   protected:
      /// type representing a connection state
      enum msgAPIstate {
      /// not connected
      unconnected = 0,
      /// connected to a remote server
      remote = 1,
      /// connected to a local dynamic library
      local = 2};
     //typedef enum msgAPIstate msgAPIstate;
   
      /// true if finished
      bool		finished;
      /// true if silent
      bool		silent;
      /// display notification messages immediately?
      bool		fastmessages;
      /// command prompt
      std::string	prompt;
      /// state of connection
      msgAPIstate	state;
      /// name of server
      std::string 	servername;
      /// connection identifier
      int		id;
      /// capability flags
      int		capabilities;
      /// last echo message lines
      std::deque<std::string> lastecho;
      /// last notification message lines
      std::deque<std::string> lastnotification;
      /// notification message list
      std::unique_ptr<std::ostringstream>	nMessages;
   
      /** Prints a line to the output of the command line.
          @param s message string
          @return true if successful
       ******************************************************************/
      virtual void printline (const std::string& s) {
      }
   
      /** Callback for notification messages.
          @param msg message string
          @param prm message parameter
          @param pLen parameter length
          @param res message result (return)
          @param rLen result length (return)
          @memo Notification callback method.
          @return true if successful
       ******************************************************************/
      virtual bool notify (const std::string& msg, 
                        const char* prm, int pLen, char** res, int* rLen);
   
   private:

      typedef std::map <int, basic_commandline*> callbacklookup;
      static char _buffer[1024*1024];
      static callbacklookup cblookup;
      static thread::mutex	    cblock;
      static int cbfunc (int id, const char* msg, 
                        const char* prm, int pLen, char** res, int* rLen);
   };

/*@}*/
}

#endif /*_GDS_CMDAPI_H */
