/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: gdsMsg							*/
/*                                                         		*/
/* Module Description: API for sneding/receiving gds command messages	*/
/*									*/
/*                                                         		*/
/* Module Arguments: none				   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 11Jan99  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: gdsMsg.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8336  (509) 372-2178  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Ultra-Enterprise, Solaris 5.6			*/
/*	Compiler Used: sun workshop C 4.2				*/
/*	Runtime environment: sparc/solaris				*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			TBD			*/
/*			  POSIX			TBD			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*								 	*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_MSG_H
#define _GDS_MSG_H

#ifdef __cplusplus
extern "C" {
#endif


/* Header File List: */

/**
   @name Message API
   This API implements four functions to open and close a channel to a 
   diagnostics kernel, and to send and recieve messages to and from it,
   respectively. This API works over the network using remote procedure 
   calls.

   @memo API for diagnostics messages
   @author Written January 1999 by Daniel Sigg
   @version 0.1
************************************************************************/

/*@{*/

#ifndef DYNAMIC_LOAD
#error  DYNAMIC_LOAD must be defined as 0
#endif

#ifndef _DYNAMIC_LOAD
#error _DYNAMIC_LOAD must be defined as 0
#endif

#if DYNAMIC_LOAD
#error  DYNAMIC_LOAD must be defined as 0
#endif

#if _DYNAMIC_LOAD
#error _DYNAMIC_LOAD must be defined as 0
#endif

#if 0

/** Compiler flag for specifying to dynamically load the diagnostics
    kernel when running on a local machine. A value of 1 will enable
    dynamic loading and requires the shared library libgds.so.1. A 
    value of 0 disables dynamic loading and links the diagnostics 
    kernel statically. If undefined, the default is 1 for Solaris and
    0 for VxWorks.

    @author DS, June 98
    @see Test point API
************************************************************************/
#define _DYNAMIC_LOAD		1
#endif

#ifndef __CMD_FLAGS
#define __CMD_FLAGS

/** Flag representing the capability for performing diagnostics tests.

    @author DS, January 99
************************************************************************/
#define CMD_TEST		0x01


/** Flag representing the capability for controling test points.

    @author DS, January 99
************************************************************************/
#define CMD_TESTPOINT		0x02


/** Flag representing the capability for controling the arbitrary 
    waveform generator.

    @author DS, January 99
************************************************************************/
#define CMD_AWG			0x04
#endif


/** Message callback function. This function prototype is used by the
    message handler for the callback routine.

    @param communication channel id
    @param msg name of message
    @param prm parameter string
    @param pLen length of parameter string
    @param res pointer to reply string
    @param rLen returned length of reply string
    @return 0 if successful, <0 otherwise
    @author DS, January 99
************************************************************************/
   typedef int (*gdsMsgCallback) (int id, const char* msg, 
                     const char* prm, int pLen, char** res, int* rLen);


/** Opens a communication channel. If the server name is a valid
    network name of a remote machine running a diagnostics kernel,
    a communication channel to this remote machine is established.
    If the server name is NULL or of length zero, the diagnostics
    kernel is dynamically loaded onto the local machine.

    The flag argument specifies the required services (capabilities). 
    Currently supported are CMD_TEST (for performing diagnostics 
    tests), CMD_TESTPOINT (for controling testpoints) and 
    CMD_AWG (for controling the arbitarry waveform
    generator. The returned value is the bit encoded flag of the 
    capabilities of th ecommand line interface. If all requested
    capabilities are supported by the system, this return value will
    be indentical to the supplied flag value. 

    @param server name of a machine running the diagnostics kernel
    @param flag specifies required services
    @param conf configuration string
    @param capability return argument specifying supported services
    @return id of communicationb channel if successful, <0 otherwise
    @author DS, January 99
************************************************************************/
   int gdsMsgOpen (const char* server, int flag, const char* conf,
                  int* capability);

/** Closed the communication channel. 

    @param communication channel id
    @return 0 if successful, <0 otherwise
    @author DS, January 99
************************************************************************/
   int gdsMsgClose (int id);

/** Send a command message. The command message is sent through a
    previously opened communication channel. The return argument
    indicates the returned data type of reply.
    \begin{verbatim}
    0	character string
    1	binary
    \end{verbatim}

    @param communication channel id
    @param msg name of message
    @param prm parameter string
    @param pLen length of parameter string
    @param res pointer to reply string
    @param rLen returned length of reply string
    @return >=0 if successful, <0 otherwise
    @author DS, January 99
************************************************************************/
   int gdsMsgSend (int id, const char* msg, const char* prm, int pLen,
                  char** res, int* rLen);

/** Transfer data. The data are sent through a previously opened 
    communication channel. The data format is floats. The data type
    is either: 0 - asis, 1 - complex, 2 - real part, and 
    3 - imaginary part. Additionally, the data length and the offset
    into the data object have to be specified in number of points
    (i.e. 1 complex number point = 2 floating point numbers).
    Supported command messages are:
    \begin{verbatim}
    get	name	- retrieves dats from specified object 
    put name	- stores dats in specified object
    \end{verbatim}
    Depending whether data is retrieved or stored the arguments 'data' 
    and 'data length' are either return or input arguments.
    When retrieving a data array the caller is reponsible to free the 
    returned data array.

    @param communication channel id
    @param msg name of message
    @param datatype Type of data
    @param len Number of data points
    @param ofs Offset into data object (in number of data points)
    @param data pointer to data array
    @param datalen Number of float values in data array
    @return >=0 if successful, <0 otherwise
    @author DS, January 99
************************************************************************/
   int gdsMsgData (int id, const char* msg, int datatype, int len,
                  int ofs, float** data, int* datalen);

/** Installs a message handler for receiving. Notification messages from
    the diagnostics kernel are passed to a callback routine if a message
    handler has been installed.

    @param communication channel id
    @param callback callback routine
    @return 0 if successful, <0 otherwise
    @author DS, January 99
************************************************************************/
   int gdsMsgInstallHandler (int id, gdsMsgCallback callback);

/*@}*/


#ifdef __cplusplus
}
#endif

#endif /*_GDS_GDS_H */
