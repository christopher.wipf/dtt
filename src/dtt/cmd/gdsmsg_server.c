/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: gdsmsg_server						*/
/*                                                         		*/
/* Module Description: implements server functions for handling the 	*/
/* command messages for the diagnostics kernel				*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _DEFAULT_SOURCE
#define _DEFAULT_SOURCE 1
#endif

/*#ifndef DEBUG
#define DEBUG
#define RPC_SVC_FG
#endif*/


#ifndef _DYNAMIC_LOAD
#ifdef OS_VXWORKS
#define _DYNAMIC_LOAD		0
#else
#define _DYNAMIC_LOAD		1
#endif
#endif


/* #define PORTMAP */

/* Header File List: */
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <errno.h>

#ifdef OS_VXWORKS
#include <vxWorks.h>
#include <semLib.h>
#include <taskLib.h>
#include <taskVarLib.h>
#include <sockLib.h>
#include <inetLib.h>
#include <hostLib.h>
#include <ioLib.h>
#include <selectLib.h>
#include <signal.h>
#include <sysLib.h>
#include <timers.h>
#include <pipeDrv.h>

#else
#include <sys/types.h>
#include <sys/resource.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <signal.h>
#include <syslog.h>
#include <pthread.h>
#endif

#include "gdsutil.h"
#if _DYNAMIC_LOAD
#include "gdscmd_d.h"
#else
#include "gdscmd.hh"
#endif
#include "rgdsmsg.h"
#include "rgdsmsgcb.h"
#include "gdsmsg_server.h"


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Constants: 	_NETID		net protocol used for rpc		*/
/*              _CMDINIT_LEN	init cmd length in bytes		*/
/*              _PROGVER	rpc program version			*/
/* 	 	_PROGVER_CB	rpc program version of callback		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
#define _NETID			"tcp"
#define _CMDINIT_LEN		16
#define	_PROGVER 		1
#define _PROGVER_CB		1

#ifdef RPC_SVC_FG
#define _SVC_FG		1
#else
#define _SFC_FG		0
#endif
#if !defined (OS_VXWORKS) && !defined (PORTMAP)
#define _SVC_MODE	RPC_SVC_MT_AUTO
#else
#define _SVC_MODE	0
#endif


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Globals: servermux		protects globals			*/
/*          initServer		if 0 the server is not yet initialized	*/
/*          rpcpmstart		port monitor flag			*/
/*          shutdownflag	shutdown flag				*/
/*          prognum_srv		rpc program # of msg server		*/
/*          progver_srv		rpc program version of msg server	*/
/*          prognum_cb		rpc program # of msg callback		*/
/*          progver_cb		rpc program version of msg callback	*/
/*          id_cb		callback id				*/
/*          addr_cb		address of callback machine    		*/
/*          pipe_cb		pipe for callback function    		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   static mutexID_t		servermux;
   static int			initServer = 0;
   static int			shutdownflag = 0;
   static u_long		prognum_srv;
   static u_long		progver_srv;
   static u_long 		prognum_cb;
   static u_long		progver_cb;
   static int			id_cb;
   static struct in_addr	addr_cb;
   static int			pipe_cb[2];


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Forward declarations: 						*/
/*      rgdsmsgwka_1		rpc dispatch function			*/
/*      rgdsmsg_1		rpc dispatch function			*/
/*      notifyClient		implements notification callback	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   extern void rgdsmsg_1 (struct svc_req* rqstp, 
                     SVCXPRT* transp);


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Remote Procedure Name: gdsmsgsend_1_svc				*/
/*                                                         		*/
/* Procedure Description: sends a message 				*/
/*                                                         		*/
/* Procedure Arguments: message, reply, transport handle   		*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 if error			*/
/*                    reply	                        		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   bool_t gdsmsgsend_1_svc (message_r msg, reply_r* answer, 
                     struct svc_req* transport)
   {
      gdsDebug ("send message");
      printf ("message server: send\n");
   #if _DYNAMIC_LOAD
      answer->status = 
         _gdsCmd (msg.name, msg.prm.prm_val, msg.prm.prm_len, 
                 &answer->res.res_val, (int*) &answer->res.res_len);
   #else
      answer->status = 
         gdsCmd (msg.name, msg.prm.prm_val, msg.prm.prm_len, 
                 &answer->res.res_val, (int*) &answer->res.res_len);
   #endif
      if (answer->res.res_val == NULL) {
         answer->res.res_val = malloc (1);
	 if (answer->res.res_val == NULL) /* JCB */
	 {
	    gdsDebug("gdsmsgsend_1_svc malloc(1) failed\n") ;
	    return FALSE ;
	 }
         answer->res.res_len = 0;
      }
      printf ("message server: send done\n");
      return TRUE;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Remote Procedure Name: gdsmsgdata_1_svc				*/
/*                                                         		*/
/* Procedure Description: trensfers data 				*/
/*                                                         		*/
/* Procedure Arguments: message, reply, transport handle   		*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 if error			*/
/*                    reply	                        		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   bool_t gdsmsgdata_1_svc (bmessage_r msg, breply_r* answer, 
                     struct svc_req* transport)
   {
      float*		data;		/* data array */
      int		datalength;	/* array length */
   
      gdsDebug ("transfer data");
      printf ("message server: transfer\n");
      if (msg.toKernel) {
         data = msg.prm.prm_val;
         datalength = msg.prm.prm_len;
      }
      else {
         data = NULL;
         datalength = 0;
      }
   #if _DYNAMIC_LOAD
      answer->status = 
         _gdsCmdData (msg.name, msg.toKernel, msg.dtype, msg.len,
                     msg.ofs, &data, &datalength);
   #else
      answer->status = 
         gdsCmdData (msg.name, msg.toKernel, msg.dtype, msg.len,
                    msg.ofs, &data, &datalength);
   #endif
      if (msg.toKernel || (data == NULL)) {
         answer->res.res_val = malloc (1);
	 if (answer->res.res_val == NULL) /* JCB */
	 {
	    gdsDebug("gdsmsgdata_1_svc malloc(1) failed\n") ;
	    return FALSE ;
	 }
         answer->res.res_len = 0;
      }
      else {
         answer->res.res_val = data;
         answer->res.res_len = datalength;
      }
      printf ("message server: transfer done\n");
      return TRUE;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Remote Procedure Name: gdsmsgclose_1_svc				*/
/*                                                         		*/
/* Procedure Description: closes a message communication channel	*/
/*                                                         		*/
/* Procedure Arguments: return status, service handle   		*/
/*                                                         		*/
/* Procedure Returns: prognum of rpc interface, if successful, 		*/
/*		      0 if error        				*/
/*                                                         		*/
/*----------------------------------------------------------------------*/      
   bool_t gdsmsgclose_1_svc (int* ret, struct svc_req* transport)
   {
      gdsDebug ("close message server");
      printf ("message server: close\n");
   
      /* get server mux */
      MUTEX_GET (servermux);
   #if _DYNAMIC_LOAD
      *ret = _gdsCmdFini ();
   #else
      *ret = gdsCmdFini ();
   #endif
      initServer = 0;
   
      /* release server mutex */
      MUTEX_RELEASE (servermux);
      rpcSetServerBusy (0);
      printf ("message server: close done\n");
      return TRUE;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Remote Procedure Name: gdsmsgkeepalive_1_svc				*/
/*                                                         		*/
/* Procedure Description: keep alive function				*/
/* This function is used as follows: Clients call this function		*/
/* once every 15s. If the function is not called for more than 60s, 	*/
/* the communication channels is closed automatically. A negative 	*/
/* return value	indicates an error and the client should shut down. 	*/
/*                                                         		*/
/* Procedure Arguments: return status, service handle   		*/
/*                                                         		*/
/* Procedure Returns: prognum of rpc interface, if successful, 		*/
/*		      0 if error        				*/
/*                                                         		*/
/*----------------------------------------------------------------------*/      
   bool_t gdsmsgkeepalive_1_svc (int* ret, struct svc_req* transport)
   {
      gdsDebug ("keep alive message server");
      printf ("message server: keep alive\n");
   
      /* set server busy */
      rpcSetServerBusy (1);
   
      *ret = 0;
      return TRUE;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: getArgument/putArgument			*/
/*                                                         		*/
/* Procedure Description: get/put a message argument from/to a stream	*/
/*                                                         		*/
/* Procedure Arguments: stream descriptor, buffer, length		*/
/*                                                         		*/
/* Procedure Returns: read/written length if successful, <0 if failed	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   static int getArgument (int fd, char** buf) 
   {
      int 	size = 0;
      *buf = NULL;
   
      if (read (fd, (char*) &size, 4) < 0) {
         return -1;
      }
      size = ntohl (size);
      if (size < 0) {
         return -2;
      }
      *buf = malloc (size);
      if (*buf == NULL) {
	 gdsDebug("getArgument malloc(size) failed.") ; /* JCB */
         return -3;
      }
      if (read (fd, *buf, size) < 0) {
         free (*buf);
         return -4;
      }
      return size;
   }


   static int putArgument (int fd, const char* buf, int size) 
   {
      int 	sz;
   
      sz = htonl (size);
      if ((size < 0) || (write (fd, (char*) &sz, 4) < 0)) {
         return -1;
      }
      if ((size == 0) || (buf == NULL)) {
         return 0;
      }
      if (write (fd, (char*) buf, size) < 0) {
         return -2;
      }
      return size;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: rpcNotifyClient				*/
/*                                                         		*/
/* Procedure Description: send a message to the client			*/
/*                                                         		*/
/* Procedure Arguments: msg, parameter, reply				*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 if failed			*/
/*                    reply			             		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   static int rpcNotifyClient (const char* msg, const char* prm, 
                     int pLen, char** res, int* rLen)
   {
      int		retval;		/* result of rpc call */
      char		cbname[20]; 	/* callback host name */
      CLIENT*		clnt;		/* client handle */
      message_r 	message;	/* message */
      reply_r 		reply;		/* reply */
   
      printf ("message server: notification\n");
   
      /* rpc init for VxWorks */
   #ifdef OS_VXWORKS
      rpcTaskInit ();
   #endif
   
      /* get server mux */
      MUTEX_GET (servermux);
   
      /* get client name */
   #ifdef OS_VXWORKS
      inet_ntoa_b (addr_cb, cbname);
   #else
      inet_ntop(AF_INET, &addr_cb, cbname, sizeof(cbname));
   #endif
      /* create client handle */
      clnt = clnt_create (cbname, prognum_cb, progver_cb, _NETID);
      if (clnt == NULL) {
         MUTEX_RELEASE (servermux);
         return -1;
      }
   
      /* make the callback */
      message.name = (char*) msg;
      message.prm.prm_len = pLen;
      message.prm.prm_val = (char*) prm;
      reply.res.res_val = NULL;
      retval = gdsmsgnotify_1 (id_cb, message, &reply, clnt);
   
      /* destroy client handle */
      clnt_destroy (clnt);
   
      if ((retval != RPC_SUCCESS) || (reply.status < 0)) {
         MUTEX_RELEASE (servermux);
         return -2;
      }
   
      /* copy data */
      *res = reply.res.res_val;
      *rLen = reply.res.res_len;
   
      /* release server mutex */
      MUTEX_RELEASE (servermux);
      return 0;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: socketNotifyClient				*/
/*                                                         		*/
/* Procedure Description: send a message to the client			*/
/*                                                         		*/
/* Procedure Arguments: msg, parameter, reply				*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 if failed			*/
/*                    reply			             		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   static int socketNotifyClient (const char* msg, const char* prm, 
                     int pLen, char** res, int* rLen)
   {
      int		status;
   
      *res = NULL;
      *rLen = 0;
   
      /* send message to pipe */
      if ((putArgument (pipe_cb[1], msg, strlen (msg) + 1) < 0) ||
         (putArgument (pipe_cb[1], prm, pLen) < 0)) {
         return -1;
      }
   
      /* get answer from pipe */
      status = getArgument (pipe_cb[1], res);
      if (status < 0) {
         return -1;
      }
      else {
         *rLen = status;
         return 0;
      }
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: rgdsmsg_1_freeresult			*/
/*                                                         		*/
/* Procedure Description: frees memory of rpc call			*/
/*                                                         		*/
/* Procedure Arguments: rpc transport info, xdr routine for result,	*/
/*			pointer to result				*/
/*                                                         		*/
/* Procedure Returns: TRUE if successful, FALSE if failed		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   int rgdsmsg_1_freeresult (SVCXPRT* transp, 
                     xdrproc_t xdr_result, caddr_t result)
   {
      (void) xdr_free (xdr_result, result);
      return TRUE;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: rpcServerStart				*/
/*                                                         		*/
/* Procedure Description: start rpc server task for gds msg		*/
/*                                                         		*/
/* Procedure Arguments: socket						*/
/*                                                         		*/
/* Procedure Returns: does not return if successful, <0 otherwise	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   static int socketServerStart (int sock, short port, int flag,
                     const char* conf)
   {
      struct sockaddr_in peer;		/* peer address */
      struct sockaddr_in name;		/* socket name */
      socklen_t		addrlen;	/* peer address length */
      char		buf[16];	/* answer buffer */
      int		capability;	/* server capabilities */
      int		cbsock;		/* callback socket */
      int		busy;		/* busy flag */
   
      /* get server mux */
      MUTEX_GET (servermux);
   
      /* get client address */
      addrlen = sizeof (struct sockaddr_in);
      if (getpeername (sock, (struct sockaddr*) &peer, &addrlen) < 0) {
         MUTEX_RELEASE (servermux);
         return -1;
      }
      printf ("message server: socket open 1\n");
   
      /* open callback socket if necessary */
      if (port == 0) {
         cbsock = sock;
      }
      else {
         /* create callback socket */
         cbsock = socket (PF_INET, SOCK_STREAM, 0);
         if (cbsock == -1) {
            return -2;
         }
         /* bind callback socket */
         name.sin_family = AF_INET;
         name.sin_port = 0;
         name.sin_addr.s_addr = htonl (INADDR_ANY);
         if (bind (cbsock, (struct sockaddr*) &name, sizeof (name)) < 0) {
            close (cbsock);
            return -3;
         }
         /* connect callback socket */
         name.sin_family = AF_INET;
         name.sin_port = htons (port);
         name.sin_addr.s_addr = peer.sin_addr.s_addr;
         if (connect (cbsock, (struct sockaddr*) &name, sizeof (name)) < 0) {
            close (cbsock);
            return -4;
         }
      }
   
      /* create pipe for notification messages */
   #ifndef OS_VXWORKS
      if (pipe (pipe_cb) < 0) {
         close (cbsock);
         return -5;
      }
   #endif
   
      /* initializes diagnostics kernel */
   #if _DYNAMIC_LOAD
      capability = _gdsCmdInit (flag, conf);
      printf ("message server: open = %i\n", capability);
      if ((capability < 0) ||
         (_gdsCmdNotifyHandler (socketNotifyClient) < 0)) {
         MUTEX_RELEASE (servermux);
         return -6;
      }
   #else
      capability = gdsCmdInit (flag, conf);
      if ((capability < 0) ||
         (gdsCmdNotifyHandler (socketNotifyClient) < 0)) {
         MUTEX_RELEASE (servermux);
         return -6;
      }
   #endif
      printf ("message server: open 3\n");
   
      /* set startup results */
      addr_cb = peer.sin_addr;
      id_cb = port;
      shutdownflag = 0;
      initServer = 1;
      printf ("message server: open 4\n");
   
      /* set keep alive and release server mutex */
      MUTEX_RELEASE (servermux);
      busy = 1;
   
      /* send answer through socket */
      *((int*) buf) = (int) htonl (0); /* status = ok */
      *((int*) (buf + 4)) = (int) htonl (capability); /* server capab. */
      send (sock, buf, 8, 0);
      printf ("message server: open 5\n");
   
      /* wait for messages */
      while (1) {
      
         fd_set		port_set;	/* port set for select */
         struct timeval timeout = {60, 0}; /* timeout for select */
         int		nset;		/* set by select */
         int		status;		/* command status */
         char*		header;		/* header buffer */
         int		headerlen;	/* header size */
         char*		prm;		/* parameter buffer */
         int		prmlen;		/* parameter size */
         char*		res;		/* answer buffer */
         int		reslen;		/* answer size */
      
         /* use select to wait for socket or pipe message */
         FD_ZERO (&port_set);
         FD_SET (sock, &port_set);
         FD_SET (pipe_cb[0], &port_set);
         nset = select (FD_SETSIZE, &port_set, NULL, NULL, &timeout);
         /* quit on error */
         if (nset < 0) {
            return 0;
         }
         /* check busy flag on timeout */
         else if (nset == 0) {
            if (busy == 0) {
               return 0;
            }
            busy = 0;
         }
         
         /* receive message */
         else {
            busy = 1;
         
            /* message from client */
            if (FD_ISSET (sock, &port_set)) {
               /* get header */
               headerlen = getArgument (sock, &header);
               if (headerlen < 0) {
                  free (header);
                  return 0;
               }
               /* get parameter */
               prmlen = getArgument (sock, &prm);
               if (prmlen < 0) {
                  free (header);
                  free (prm);
                  return 0;
               }
               /* no answer if NULL message */
               if ((headerlen == 0) && (prmlen == 0)) {
                  free (header);
                  free (prm);
                  continue;
               }
            
               /* call command interface */
            #if _DYNAMIC_LOAD
               status = _gdsCmd (header, prm, prmlen, &res, &reslen);
            #else
               status = gdsCmd (header, prm, prmlen, &res, &reslen);
            #endif
               free (header);
               free (prm);
               /* set status */
               status = (status < 0) ? htonl (-1) : 0;
            
               /* send answer back */
               if ((putArgument (sock, (char*) &status, 4) < 0) ||
                  (putArgument (sock, res, reslen) < 0)) {
                  free (res);
                  return 0;
               }
               free (res);
            }
         
            /* notification message */
            if (FD_ISSET (pipe_cb[0], &port_set)) {
               /* get header */
               headerlen = getArgument (pipe_cb[0], &header);
               if (headerlen < 0) {
                  free (header);
                  return 0;
               }
               /* get parameter */
               prmlen = getArgument (pipe_cb[0], &prm);
               if (prmlen < 0) {
                  free (header);
                  free (prm);
                  return 0;
               }
               /* set status */
               status = htonl (-2);
               /* send notification message */
               if ((putArgument (cbsock, (char*) &status, 4) < 0) ||
                  (putArgument (cbsock, header, headerlen) < 0) ||
                  (putArgument (cbsock, prm, prmlen) < 0)) {
                  free (header);
                  free (prm);
                  return 0;
               }
               free (header);
               free (prm);
               /* send a NULL answer back */
               if (putArgument (pipe_cb[0], NULL, 0) < 0) {
                  return 0;
               }
            }
         }
      }
   
      return 0;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: rpcServerStart				*/
/*                                                         		*/
/* Procedure Description: start rpc server task for gds msg		*/
/*                                                         		*/
/* Procedure Arguments: socket						*/
/*                                                         		*/
/* Procedure Returns: does not return if successful, <0 otherwise	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   static int rpcServerStart (int sock, unsigned long callback_prognum, 
                     int id, int flag, const char* conf)
   {
      SVCXPRT*		transport;	/* service transport */
      struct sockaddr_in peer;		/* peer address */
      socklen_t		addrlen;	/* peer address length */
      unsigned long	prognum;	/* rpc program # */
      char		buf[16];	/* answer buffer */
      int		capability;	/* server capabilities */
   #if !defined (OS_VXWORKS) && !defined (PORTMAP)
      int		mode;		/* server mode */
   #endif 
   
   #ifdef OS_VXWORKS
      rpcTaskInit();
   #endif
   
      /* init server mutex */
      if (MUTEX_CREATE (servermux) != 0) {
         return -1;
      }
   
      gdsDebug ("open message server");
      printf ("message server: open\n");
   
      /* get server mux */
      MUTEX_GET (servermux);
      id_cb = id;
   
      /* register rpc interface for future calls */
      progver_srv = _PROGVER;
      prognum_cb = callback_prognum;
      progver_cb = _PROGVER_CB;
      addrlen = sizeof (struct sockaddr_in);
      if ((rpcRegisterCallback (&prognum, progver_srv, &transport, 
         rgdsmsg_1) < 0) || 
         (getpeername (sock, (struct sockaddr*) &peer, &addrlen) < 0)) {
         MUTEX_RELEASE (servermux);
         return -2;
      }
      printf ("message server: open 2\n");
   
      /* initializes diagnostics kernel */
   #if _DYNAMIC_LOAD
      capability = _gdsCmdInit (flag, conf);
      printf ("message server: open = %i\n", capability);
      if ((capability < 0) ||
         (_gdsCmdNotifyHandler (rpcNotifyClient) < 0)) {
         MUTEX_RELEASE (servermux);
         return -3;
      }
   #else
      capability = gdsCmdInit (flag, conf);
      if ((capability < 0) ||
         (gdsCmdNotifyHandler (rpcNotifyClient) < 0)) {
         MUTEX_RELEASE (servermux);
         return -3;
      }
   #endif
      printf ("message server: open 3\n");
   
      /* set startup results */
      addr_cb = peer.sin_addr;
      shutdownflag = 0;
      initServer = 1;
      prognum_srv = prognum;
      printf ("message server: open 4\n");
   
      /* set keep alive and release server mutex */
      MUTEX_RELEASE (servermux);
      rpcSetServerBusy (1);
   
      /* send answer through socket */
      *((int*) buf) = (int) htonl (0); /* status = ok */
      *((unsigned long*) (buf + 4)) = htonl (prognum); /* rpc server # */
      *((int*) (buf + 8)) = (int) htonl (capability); /* server capab. */
      send (sock, buf, 12, 0);
      printf ("message server: open 5\n");
   
      /* close socket */
      close (sock);
   
      /* start server */
      printf ("message server: open done\n");
   #if !defined(OS_VXWORKS) && !defined(PORTMAP)
      mode = RPC_SVC_MT_AUTO;
      rpc_control (RPC_SVC_MTMODE_SET, &mode);
   #endif
      printf ("message server: open 6\n");
      rpcStartServer (1, &shutdownflag);
   
      /* never reached */
      return -4;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: gdsmsg_server				*/
/*                                                         		*/
/* Procedure Description: start rpc or socket  service task for gds msg	*/
/*                        requires that the (tcp) socket is accepted 	*/
/*                                                         		*/
/* Procedure Arguments: socket						*/
/*                                                         		*/
/* Procedure Returns: does not return if successful, <0 otherwise	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   int gdsmsg_server (int sock)
   {
      char		buf[256];	/* input buffer */
      int		nRead;		/* received bytes */
      int		nB;		/* temp */
      char*		p = buf;	/* buffer pointer */
      unsigned long	cbPrognum;	/* callback prognum */
      int		cbId;		/* callback id */
      int		flag;		/* startup flag */
      int		len;		/* length of string argument */
      char*		conf;		/* configuration string */
      int		ret;		/* return error code */
   
      /* get start up parameters from socket */
      nRead = 0;
      do {
         nB = recv (sock, p, _CMDINIT_LEN - nRead, 0);
         if (nB <= 0) {
            return -1;
         }
         p += nB;
         nRead += nB;
      } while (nRead < _CMDINIT_LEN);
   
      /* get startup parameters */
      cbPrognum = ntohl (*((unsigned long*) buf));
      cbId = (int) ntohl (*((unsigned long*) (buf + 4)));
      flag = (int) ntohl (*((unsigned long*) (buf + 8)));
      len = (int) ntohl (*((unsigned long*) (buf + 12)));
      conf = (len > 0) ? malloc (len + 10) : NULL;
      if ((len > 0) && (conf == NULL)) {
	 gdsDebug("gdsmsg_server malloc(len + 10) failed.\n") ; /* JCB */
         return -1;
      }
   
      /* get configuration string from socket */
      if (conf != NULL) {
         p = conf;
         nRead = 0;
         do {
            nB = recv (sock, p, len - nRead, 0);
            if (nB <= 0) {
               return -1;
            }
            p += nB;
            nRead += nB;
         } while (nRead < len);
      }
      *p = 0;
   
      if (cbPrognum < 65536) {
         /* socket connection requested */
         ret = socketServerStart (sock, cbPrognum, flag, conf);
      }
      else {
         /* rpc connection requested */
         ret = rpcServerStart (sock, cbPrognum, cbId, flag, conf);
      }
   
      /* return on error only */
      if (ret < 0) {
         *((int*) buf) = (int) htonl (ret); /* status = failed */
         send (sock, buf, 4, 0);
      }
      return ret;
   }

