
#include <cstdlib>
#include <time.h>
#include "gdsnotify.hh"

namespace diag {
   using namespace std;

   const char* const cmdnotify::msgs[] = {
   "notification: invalid", 
   "notification: begin of test", 
   "notification: end of test", 
   "notification: begin of measurement", 
   "notification: end of measurement",
   "notification: new test result",
   "notification: new iterator result",
   "notification: data receiving error (measurement skipped)"
   };

   const char* const cmderror = "notification: test failure";

   bool cmdnotify::send (flag id) const 
   {
      if ((notifyFunc != 0) && (id >= 0) && 
         (id < (int)(sizeof (msgs) / sizeof (char*)))) {
         char*		reply = 0;
         int		rlen;
         int		ret;
         ret = notifyFunc (msgs[id], 0, 0, &reply, &rlen);
         free (reply);
         return (ret == 0);
      }
      else {
         return false;
      }
   }


   bool cmdnotify::sendError (const string& errmsg) const
   {
      if (notifyFunc != 0) {
         char*		reply = 0;
         int		rlen;
         int		ret;
         string		msg = cmderror;
      
         msg += string ("\n") + errmsg;
         ret = notifyFunc (msg.c_str(), 0, 0, &reply, &rlen);
         free (reply);
         return (ret == 0);
      }
      else {
         return false;
      }
   }

}


