//
// Created by erik.vonreis on 3/15/24.
//

#ifndef CDS_CRTOOLS_AWG_TIME_H
#define CDS_CRTOOLS_AWG_TIME_H

#include "tconv.h"

#ifdef __cplusplus
extern "C" {
#endif //__cplusplus

/**
   * Return the real-time system time in nano-seconds since the GPS epoch.
   * There's a default implementation, but it can be over written with awg_set_time_function();
   * Or reset to to default with awg_set_default_time_function()
   * @return
   */
tainsec_t awg_gps_time();

/**
 * Override the current time function with a new function.
 * Useful for providing custom synchronization functions as from DTT.
 * Affects calls to awg_gps_time();
 * @param f
 */
void awg_set_time_function(tainsec_t (*f)());

/**
 * Revert to the default time function.
 * Affects calls to awg_gps_time().
 */
void awg_set_default_time_function();

#ifdef __cplusplus
}
#endif //__cplusplus

#endif //CDS_CRTOOLS_AWG_TIME_H
