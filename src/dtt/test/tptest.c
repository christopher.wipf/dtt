/* version $Id: tptest.c 6306 2010-09-17 16:54:02Z james.batch@LIGO.ORG $ */
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include "gdsutil.h"
#include "testpoint.h"

#ifdef OS_VXWORKS
#include <taskLib.h>
   static void sleep (int t)
   {
      taskDelay (60 * t);
   }
#endif

   void
   #ifdef OS_VXWORKS
   tptest (void)
   #else
   main (int argc, char *argv[])
   #endif
   {
      int		ret;
      int		i;
      testpoint_t	tp[100];
      testpoint_t	tp2[100];
      taisec_t		time;
      int		epoch;
   
   
      tp[0] = 1;
      tp[1] = 2;
      tp[2] = 0;
   
      ret = tpRequest (0, tp, 2, -1, &time, &epoch);
      printf ("tpRequest: %i - active at %li(%i) \n", ret, time, epoch);
      tp[1] = 3;
      ret = tpRequest (0, tp, 3, -1, &time, &epoch);
      printf ("tpRequest: %i - active at %li(%i) \n", ret, time, epoch);
   
      ret = tpQuery (0, 0, tp2, 100, time, epoch);
      printf ("tpQuery: %i\n", ret);
      for (i = 0; i < ret; i++) {
         printf ("slot %i: test point %i\n", i, tp2[i]);
      }
      sleep (2);
   
      ret = tpQuery (0, 0, tp2, 100, time, epoch);
      printf ("tpQuery: %i\n", ret);
      for (i = 0; i < ret; i++) {
         printf ("slot %i: test point %i\n", i, tp2[i]);
      }
   
      tp[0] = -1;
      ret = tpClear (0, tp, 1);
      printf ("tpClear: %i\n", ret);
   
      ret = tpQuery (0, 0, tp2, 100, time + 3, epoch);
      printf ("tpQuery: %i\n", ret);
      for (i = 0; i < ret; i++) {
         printf ("slot %i: test point %i\n", i, tp2[i]);
      }
   
      ret = tpAddr (0, 1, time + 3, 8);
      printf ("tpAddr of 1 at epoch 8: 0x%x\n", ret);
      ret = tpAddr (0, 2, time + 3, 0);
      printf ("tpAddr of 2 at epoch 0: 0x%x\n", ret);
      ret = tpAddr (0, 2, time + 3, 2);
      printf ("tpAddr of 2 at epoch 2: 0x%x\n", ret);
      sleep (2);
   
      ret = tpClear (0, NULL, 0);
      printf ("tpClear: %i\n", ret);
      sleep (2);
   
      ret = tpQuery (0, 0, tp2, 100, time + 7, epoch);
      printf ("tpQuery: %i\n", ret);
      for (i = 0; i < ret; i++) {
         printf ("slot %i: test point %i\n", i, tp2[i]);
      }
   }
