/* version $Id: conftest.cc 6306 2010-09-17 16:54:02Z james.batch@LIGO.ORG $ */
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include <fstream>
#include <iostream>

#include "confinfo.h"


   int main ()
   {
      cout << "start" << endl;
      const char* const* info = getConfInfo (0, 1.0);
      const char* const* p = info;
      while (*p != 0) {
         cout << "info = " << *p << endl;
         p++;
      }
      cout << "stop" << endl;
      return 0;
   }
