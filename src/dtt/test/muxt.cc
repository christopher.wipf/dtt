/* version $Id: muxt.cc 6306 2010-09-17 16:54:02Z james.batch@LIGO.ORG $ */

#include <unistd.h>
#include <iostream>
#include "gmutex.hh"
#include "gdstask.h"

   using namespace std;
   using namespace diag;

   class ttt {
   public:
      recursivemutex 	mx;
      void call () {
         semlock	lockit (mx);
         cout << "call" << endl;
      }
      void call2 () {   
         semlock	lockit (mx);
         cout << "call recursive" << endl;
         call();
      }
   };

   readwritelock		rwlock (3);

   void readtask ()
   {
      taskID_t		id = pthread_self();
      cout << "read task " << id << " lock..." << endl;
      rwlock.readlock();
      cout << "read task " << id << " locked" << endl;
      sleep (5);
      cout << "read task " << id << " unlock..." << endl;
      rwlock.unlock();
      cout << "read task " << id << " unlocked" << endl;
   }


   void writetask ()
   {
      taskID_t		id = pthread_self();
      cout << "write task " << id << " lock..." << endl;
      rwlock.writelock();
      cout << "write task " << id << " locked" << endl;
      sleep (5);
      cout << "write task " << id << " unlock..." << endl;
      rwlock.unlock();
      cout << "write task " << id << " unlocked" << endl;
   }


   void tryreadtask ()
   {
      taskID_t		id = pthread_self();
      cout << "read task " << id << " try lock..." << endl;
      if (rwlock.trylock(readwritelock::rdlock)) {
         cout << "read task " << id << " locked" << endl;
         sleep (5);
         cout << "read task " << id << " unlock..." << endl;
         rwlock.unlock();
         cout << "read task " << id << " unlocked" << endl;
      }
      else {
         cout << "read task " << id << " not locked" << endl;
      }
   }


   void trywritetask ()
   {
      taskID_t		id = pthread_self();
      cout << "write task " << id << " try lock..." << endl;
      if (rwlock.trylock(readwritelock::wrlock)) {
         cout << "write task " << id << " locked" << endl;
         sleep (5);
         cout << "write task " << id << " unlock..." << endl;
         rwlock.unlock();
         cout << "write task " << id << " unlocked" << endl;
      }
      else {
         cout << "write task " << id << " not locked" << endl;
      }
   }


   int main ()
   {
      taskCreate (0, 0, 0, "1", (taskfunc_t) writetask, 0);
      sleep (1);
      taskCreate (0, 0, 0, "2", (taskfunc_t) readtask, 0);
      taskCreate (0, 0, 0, "3", (taskfunc_t) readtask, 0);
      taskCreate (0, 0, 0, "4", (taskfunc_t) readtask, 0);
      taskCreate (0, 0, 0, "5", (taskfunc_t) readtask, 0);
      sleep (6);
      taskCreate (0, 0, 0, "6", (taskfunc_t) trywritetask, 0);
   
      sleep (22);
      return 0;
   
      ttt		test;
   
      cout << "first try" << endl;
      test.call();
      cout << "2nd try" << endl;
      test.call();
   
      cout << "recursive try" << endl;
      test.call2();
   
      mutex		mux;
      for (int i = 0; i < 3; i++) {
         cout << "entering loop..." << endl;
         semlock		lockit (mux);
         cout << "loop = " << i << endl;
      }
   
      return 0;
   }
