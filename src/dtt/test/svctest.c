/* version $Id: svctest.c 6344 2010-11-10 06:19:42Z john.zweizig@LIGO.ORG $ */

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "gdsutil.h"
#include "gdsheartbeat.h"
#include "gdssched.h"
#include "gdssched_server.h"

   int 	cond = 0;

   int yell (schedulertask_t* info,
            taisec_t time, int epoch, void* arg)
   {
      if (info->xdr_arg == NULL) {
         printf ("%2li: time = %li, epoch = %i\n", (long) arg, time, epoch);
      }
      else {
         printf ("%2li: time = %li, epoch = %i\n", *((long*) arg), time, epoch);
      }
      return cond;
   }

   void yellAgain (taisec_t time, int epoch, void* arg)
   {
      printf ("this is the end of %2li (time = %li, epoch = %i)\n", 
             (long) arg, time, epoch);
   }


/* main program */
   void
   #ifdef OS_VXWORKS
   svctest (void)
   #else
   main (int argc, char *argv[])
   #endif
   {
      int			retval;
      scheduler_dt_t 		schedclass;
      scheduler_dtentry_t	scheddt[10];
   
      printf ("time now: %f\n", TAInow ()/1E9);
      printf ("install heartbeat = %i\n", installHeartbeat (NULL));
   
      schedclass.prognum = 0x31000000;
      schedclass.progver = 1;
      schedclass.numentries = 1;
      scheddt[0].id = 1;
      scheddt[0].func = yell;
      scheddt[0].freeResources = yellAgain;
      scheddt[0].xdr_arg = NULL;
      schedclass.dt = scheddt;
   
      retval = registerSchedulerClass (&schedclass);
      if (retval != 0) {
         printf ("error %i during registration\n", retval);
         exit (1);
      }
      runSchedulerService (SCHED_MT_AUTO);
      exit (EXIT_SUCCESS);
   }


