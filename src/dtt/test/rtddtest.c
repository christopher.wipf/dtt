/* version $Id: rtddtest.c 6306 2010-09-17 16:54:02Z james.batch@LIGO.ORG $ */
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include "gdsutil.h"
#include "gdstask.h"
#include "gdsrtdd.h"

#define CHN_NUM		11
#define TIME		1000

   mutexID_t		mux;
   tainsec_t 		start;
   float		buf[8 * 1024];
   int			bcount = 0;
   int			counter = 0;
   int			error = 0;
   int			terminate = 0;

   static int dumpdata (void* usr, taisec_t time, int epoch, float data[], 
                     int ndata, int err)
   {
      tainsec_t		now;
      float		t;
      /* int		i;
      static int	count = 0;*/
   
      now = TAInow();
      t = (float) (now - start) / (float) _EPOCH;
      /* printf ("%9.4f: received %i data at time=%li, epoch =%i (err = %i)\n", 
             t, ndata, time, epoch, err); */
      MUTEX_GET (mux);
      counter++;
      if (err) {
         error++;
      }
      MUTEX_RELEASE (mux);
   
   /*
      for (i = 0; i < ndata; i++, bcount++) {
         buf[bcount] = data[i];
      } 
      return (++count / NUMBER_OF_EPOCHS); */
   
      if (counter % 1000 == 0) {
         if (error > 0) {
            printf ("%i transmission errors in %i packages\n", error, counter);
         }
         else {
            printf ("no transmission error in %i packages\n", counter);
         }
      }
   
      if (terminate || 
         (counter > CHN_NUM * NUMBER_OF_EPOCHS * (TIME - 1))) {
         return 1;
      }
      else {
         return 0;
      }
   }


   /* main program */
   #ifdef OS_VXWORKS
   int rtddtest (void)
   #else
   int main (int argc, char *argv[])
   #endif
   {
      static const struct timespec	delay = {TIME, 0};
      static const struct timespec	tick = {1, 0};
      int		subs[CHN_NUM+1];	/* subscription */
      int		i;
      char		fname[256];
      FILE*		out;
   
      if (MUTEX_CREATE (mux) != 0) {
         return 1;
      }
      start = TAInow();
      for (i = 1; i <= CHN_NUM; i++) {
         sprintf (fname, "H1:GDS-TEST_%i", i);
         subs[i] = gdsSubscribeData (fname, 0, dumpdata, 0);
      }
      for (i = 1; i <= CHN_NUM; i++) {
         if (subs[i] < 0) {
            printf ("unable to subscribe %i: error = %i\n", i, subs[i]);
            return 1;
         }
      }
      gdsConsoleMessage ("waiting...");
      nanosleep (&delay, NULL);
      terminate  = 1;
      nanosleep (&tick, NULL);
   
      for (i = 1; i <= CHN_NUM; i++) {
         gdsUnsubscribeData (subs[i]);
      }
   
      if (error > 0) {
         printf ("%i transmission errors in %i packages\n", error, counter);
      }
      else {
         printf ("no transmission error in %i packages\n", counter);
      }
   
      if (bcount > 0) {
         strncpy (fname, ARCHIVE "/src/data.txt", sizeof (fname));
         fname[sizeof(fname)-1] = 0;
         out = fopen (fname, "w");
         if (out == NULL) {
            printf ("unable to open output file %s\n", fname);
            return 1;
         }
         for (i = 0; i < bcount; i++) {
            fprintf (out, "%6i %12.6f\n", i, buf[i]);
         }
         fclose (out);
      }
      return 0;
   }
