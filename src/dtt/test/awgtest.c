#define DEBUG

/* define DEBUG_INIT in awg.c !!! */

#include <stdio.h>
#include <limits.h>
#include <math.h>
#include <string.h>
#include "gdsutil.h"
#include "awgapi.h"
#include "awgfunc.h"

#define __ONESEC		1E9
#define fMod(x,y)	(((x) >= 0) ? ((x) - (y) * floor ((x) / (y))) : \
				      ((x) - (y) * ceil ((x) / (y))))

#define FMod(x,y)	((x) - (y) * floor ((x) / (y)))


   double FMOD (double x, double y)
   {
      while (x > y) {
         x -= y;
      }
      return x;
   }

   double _FMOD (double x, double y)
   {
      if ((x >= 0) && (x < y)) {
         return x;
      }
      else {
         return fMod (x, y);
      }
   }

   double __FMOD (double x, double y)  
   {
      return fMod (x, y);
   }


   int
   #ifdef OS_VXWORKS
   atest (void)
   #else
   main(int argc, char* argv[])
   #endif
   {
   
   
      tainsec_t		tstart, tstop;
      int		i;
      volatile double	x,y,z;
   
      for (x = 1.0; x < 100001; x *= sqrt(10)) {
         tstart = TAInow();
         for (i = 0; i < 100000; i++) {
            y = _FMOD (x, 3.14159687398);
         }
         tstop = TAInow();
         z = (double) (tstop - tstart);
         printf ("time for _FMOD %8.0f is %12.3f ms\n", x, z/1E6);
      }
   
      for (x = 1.0; x < 100001; x *= sqrt(10)) {
         tstart = TAInow();
         for (i = 0; i < 10000; i++) {
            y = fmod (x, 3.14159687398);
         }
         tstop = TAInow();
         z = (double) (tstop - tstart);
         printf ("time for fmod %8.0f is %12.3f ms\n", x, z/1E6);
      }
   
      for (x = 1.0; x < 100001; x *= sqrt(10)) {
         tstart = TAInow();
         for (i = 0; i < 10000; i++) {
            y = FMOD (x, 3.14159687398);
         }
         tstop = TAInow();
         z = (double) (tstop - tstart);
         printf ("time for fmod %8.0f is %12.3f ms\n", x, z/1E6);
      }
   
      for (x = 1.0; x < 100001; x *= sqrt(10)) {
         tstart = TAInow();
         for (i = 0; i < 10000; i++) {
            y = fMod (x, 3.14159687398);
         }
         tstop = TAInow();
         z = (double) (tstop - tstart);
         printf ("time for x-i*y %8.0f is %12.3f ms\n", x, z/1E6);
      }
   
      for (x = 1.0; x < 100001; x *= sqrt(10)) {
         y = FMod (-x, 3.14159687398); 
         z = fmod (-x, 3.14159687398);
         printf ("y = %20.18f  z=%20.18f  y-z=%20.18f\n", y, z, y-z);
      }
   
      return 0;
   /*
      static const struct timespec wait = {5, 0};
   
      int 		awgid;
      int		ret;
      AWG_Component	comp[10];
      AWG_Component	comp2[10];
   
      awgid = awgSetChannel ("H1:GDS-SIGNAL1");
      if (awgid < 0) {
         printf ("unable to set AWG channel: error code = %i\n", awgid);
         return 1;
      }
      printf ("returned handle = %i\n", awgid);
   
      if (awgPeriodicComponent (awgSine, 100, 1.0, 0, 0.5, comp) < 0) {
         printf ("unable to set AWG waveform\n");
         return 1;
      }
   
      ret = awgAddWaveform (awgid, comp, 1);
      if (ret < 0) {
         printf ("unable to set AWG waveform: error code = %i\n", ret);
         return 1;
      }
      printf ("waveform added\n");
   
      ret = awgQueryWaveforms (awgid, comp2, 10);
      if (ret < 0) {
         printf ("unable to query AWG waveforms: error code = %i\n", ret);
         return 1;
      }
      printf ("query returns %i waveforms\n", ret);
      printf ("Waveform %i: f = %09f, A = %09f, Ofs = %09f, phi = %09f\n",
             comp2[0].wtype, comp2[0].par[0], comp2[0].par[1], 
             comp2[0].par[3],comp2[0].par[2]);
   
      printf ("wait...\n");
      nanosleep (&wait, NULL);
   
      ret = awgClearWaveforms (awgid);
      if (ret < 0) {
         printf ("unable to clear AWG waveforms: error code = %i\n", ret);
         return 1;
      }
      printf ("waveform cleared\n");
   
      ret = awgRemoveChannel (awgid);
      if (ret < 0) {
         printf ("unable to remove AWG channel: error code = %i\n", ret);
         return 1;
      }
      printf ("channel removed\n");
   
      return 0; */
   }
