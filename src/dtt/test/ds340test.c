/* version $Id: ds340test.c 6306 2010-09-17 16:54:02Z james.batch@LIGO.ORG $ */
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>
#include "gdsutil.h"
#include "ds340.h"

#define ONE_PI		3.14159265358979323846
#define TWO_PI		(2*ONE_PI)


   int
   #ifdef OS_VXWORKS
   ds340test (void)
   #else
   main (int argc, char *argv[] )
   #endif
   {
      DS340_ConfigBlock 	cb;
      char 			cbuf[10000];
      int 			n;
      int			ID;
      int			i;
      int			len;
      float			x;
      float			y[16300];
   
   /* printf ("RS232/Cobox? [0/1] >"); 
      scanf ("%d", &n);*/
      printf ("Connecting via CoBox\n");
      n = 1;
      ID = 0;
      if (n) {
         printf ("Connecting to 10.1.0.130 Serial port 1\n");
         connectCoboxDS340 (ID, "10.1.0.130", 1);
      } 
      else {
         printf ("Term device (str) >"); 
         scanf ("%s", cbuf);
         connectSerialDS340 (ID, cbuf);
      }
      if (isDS340Alive (ID)) {
         printf ("connection established\n");
      }
      else {
         printf ("connection failed\n");
         return 1;
      }
   
      showDS340Block (ID, cbuf, sizeof (cbuf));
      printf ("%s\n", cbuf);
   
      downloadDS340Block (ID);
      getDS340 (ID, &cb);
      cb.func = ds340_square;
      cb.freq = 1345.67;
      cb.ampl = 2.5;
      cb.offs = 0;
      setDS340 (ID, &cb);
      uploadDS340Wave (ID);
   
      len = 10000;
      for (i = 0, x = 0; i < len; i++) {
         y[i] = 5.0 * sin (TWO_PI * (float) i / (float) len) * 
                sin (100 * TWO_PI * (float) i / (float) len);
      }
      sendWaveDS340 (ID, y, len);
      getDS340 (ID, &cb);
      cb.toggles |= DS340_TSRC;
      cb.func = ds340_arb;
      cb.fsmp = 10E5;
      setDS340 (ID, &cb);
      uploadDS340Wave (ID);
   
      downloadDS340Block (ID);
      showDS340Block (ID, cbuf, sizeof (cbuf));
      printf ("%s\n", cbuf);
   
   #if 0
      while (1) {
         printf ("command > ");
         scanf ("%s", cp);
         if (gds_strncasecmp (cp, "quit", 4) == 0)
            break;
         if (gds_strncasecmp (cp, "show", 4) == 0) {
            showDS340Block (ID, cbuf, sizeof (cbuf));
            printf ("%s\n", cbuf);
            continue;
         }
         if (gds_strncasecmp (cp, "upload", 4) == 0) {
            uploadDS340Block (ID);
            continue;
         }
         if (gds_strncasecmp (cp, "download", 4) == 0) {
            downloadDS340Block (ID);
            continue;
         }
      }
   #endif
   
      resetDS340 (ID);
      return 0; 
   }

