/* version $Id: gpstest.c 6306 2010-09-17 16:54:02Z james.batch@LIGO.ORG $ */
#include "gpstime.h"
#include <stdlib.h>
#include <stdio.h>

   void
   #ifdef OS_VXWORKS
   gpstest (void)
   #else
   main (int argc, char* argv[])
   #endif     
   { 
      gpstime_t		gt;
      tais_t 		t;
   
      gt.year  = 1999;
      gt.yearday  = 99;  /* Jan 1 is yearday 1 */
      gt.hour       =2;
      gt.minute     =28;
      gt.second     =11;
   
      gpstime_to_gpssec (&gt, &t);
      printf("ticks since gps epoch  %llu\n",t.s);
   }

