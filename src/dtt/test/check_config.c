/* version $Id: check_config.c 6306 2010-09-17 16:54:02Z james.batch@LIGO.ORG $ */
#include <stdio.h>
#include <stdlib.h>
#include "gdsutil.h"

#ifdef OS_VXWORKS
int check_config()
#else
   int main() 
   #endif
   {
   
   #ifdef PROCESSOR_SPARC
   printf("PROCESSOR = PROCESSOR_SPARC\n");
   #elif defined PROCESSOR_BAJA47
   printf("PROCESSOR = PROCESSOR_BAJA47\n");
   #elif defined PROCESSOR_MV162
   printf("PROCESSOR = PROCESSOR_ MV162\n");
   #else
      printf("PROCESSOR = Undefined\n");
   #endif
   
   #ifdef OS_SOLARIS
   printf("OS = OS_SOLARIS\n");
   #elif defined OS_VXWORKS
   printf("OS = OS_VXWORKS\n");
   #else
      printf("OS = Unrecognized\n");
   #endif
   
   #ifndef SITE
      printf("SITE = Undefined\n");
   #elif SITE == GDS_SITE_NONE
   printf("SITE = GDS_SITE_NONE\n");
   #elif SITE == GDS_SITE_LHO
   printf("SITE = GDS_SITE_LHO\n");
   #elif SITE == GDS_SITE_LLO
   printf("SITE = GDS_SITE_LLO\n");
   #elif SITE == GDS_SITE_CIT
   printf("SITE = GDS_SITE_CIT\n");
   #elif SITE == GDS_SITE_MIT
   printf("SITE = GDS_SITE_MIT\n");
   #else
   printf("SITE = Unrecognized\n");
   #endif
   
   #ifndef SITE_PREFIX
      printf("SITE_PREFIX = Undefined\n");
   #else
   printf("SITE_PREFIX = %s\n", SITE_PREFIX);
   #endif
   
   #ifndef IFO
      printf("IFO = Undefined\n");
   #elif IFO == GDS_IFO_NONE
   printf("IFO = GDS_IFO_NONE\n");
   #elif IFO == GDS_IFO1
   printf("IFO = GDS_IFO1\n");
   #elif IFO == GDS_IFO2
   printf("IFO = GDS_IFO2\n");
   #elif IFO == GDS_IFO_PEM
   printf("IFO = GDS_IFO_PEM\n");
   #else
   printf("IFO = Unrecognized\n");
   #endif
   
   #ifndef IFO_PREFIX
      printf("IFO_PREFIX = Undefined\n");
   #else
   printf("IFO_PREFIX = %s\n", IFO_PREFIX);
   #endif
   
   #ifndef ARCHIVE
      printf("ARCHIVE = Undefined\n");
   #else
   printf("ARCHIVE = %s\n", ARCHIVE);
   #endif
   
   #ifdef _POSIX_C_SOURCE
   printf ("_POSIX_C_SOURCE\n");
   #endif
   
   #ifdef _USE_POSIX_TIMER
   printf ("_USE_POSIX_TIMER\n");
   #endif
   
   #ifdef DEBUG
   printf ("DEBUG version\n");
   #endif
   
      return 0;
   }
