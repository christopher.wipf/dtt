/* version $Id: hbtest.c 6306 2010-09-17 16:54:02Z james.batch@LIGO.ORG $ */
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <unistd.h>
#include "gdsutil.h"
#include "gdsheartbeat.h"

#define NUMBER_OF_EPOCHES 16

/* main program */
   void
   #ifdef OS_VXWORKS
   hbtest (void)
   #else
   main (int argc, char *argv[])
   #endif
   {
      struct timespec	now;
      tainsec_t		t;
      double 	slack;
      int	slackn;
      double	slacksum;
      double	slacksqr;
   
      printf ("install heartbeat = %i\n", installHeartbeat (NULL));
      printf ("time now: %li\n", time(NULL));
   
      slackn = 0;
      slacksum = 0.0;
      slacksqr = 0.0;
      for (;;) {
         syncWithHeartbeat();
      
         /* printf ("Heartbeat count = %li\n", getHeartbeatCount());
         clock_gettime (CLOCK_REALTIME, &now);*/
         t = TAInow();
         now.tv_sec = t / _ONESEC;
         now.tv_nsec = t % _ONESEC;
         printf ("beat at time: %li.%09ld\n", now.tv_sec, now.tv_nsec);
         slack = ((double) now.tv_nsec) / 1.0E9;
         slack -= floor (16 * slack + 0.5) / 16.0;
         slackn++;
         slacksum += slack;
         slacksqr += slack * slack;
         if (slackn % (10 * NUMBER_OF_EPOCHES) == 0) {
            printf ("average slack = %f ms\n", 1E3 * slacksum / slackn);
            printf ("slack stddev  = %f ms\n", 
                   1E3 * sqrt (fabs (slacksqr - slacksum * slacksum / slackn) / 
                   slackn));
            break;
         }
         if (slackn % (100 * NUMBER_OF_EPOCHES) == 0) {
            break;
         }
      
      }
   
      return;
   }
