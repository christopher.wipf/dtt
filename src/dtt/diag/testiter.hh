/* version $Id: testiter.hh 6916 2013-11-15 18:15:10Z james.batch@LIGO.ORG $ */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: testiter.h						*/
/*                                                         		*/
/* Module Description: iteration class for repeating tests		*/
/*									*/
/*                                                         		*/
/* Module Arguments: none				   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 30Dec98  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: testiter.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-2178  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Ultra-Enterprise, Solaris 5.6			*/
/*	Compiler Used: sun workshop C++ 4.2				*/
/*	Runtime environment: sparc/solaris				*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			OK			*/
/*			  POSIX			OK			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*								 	*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_TESTITER_H
#define _GDS_TESTITER_H

/* Header File List: */
#include <string>
#include "tconv.h"
#include "gdsstring.h"
#include "gmutex.hh"
#include "gdsnotify.hh"

namespace diag {


   class diagStorage;
   class excitationManager;
   class testpointMgr;
   class diagtest;


/** @name Abstract Test Iterator
   
    @memo Abstract object representing a test iterator
    @author Written April 1999 by Daniel Sigg
    @version 0.1
 ************************************************************************/

//@{

/** Test iterator.
    This object implements the iteration behaviour of a diagnostics
    tests. All iterators must be descendents from this object. This
    object implements a mutex, a pointer to the storage object, a
    pointer to the excitation manager for iterator controlled channels, 
    a pointer to the diagnostics test, a command notification object
    and a iteration counter.
   
    @memo Object for implementing a test iterator
    @author Written December 1998 by Daniel Sigg
    @version 0.1
 ************************************************************************/
   class testiterator {   
   public:
      /** Constructs a test iterator object.
          @param Name name of test iterator
   	  @memo Default constructor
       ******************************************************************/
      explicit testiterator (const std::string& Name) : myname (Name) {
      }
   
      /** Destructs a test iterator object.
   	  @memo Default constructor
       ******************************************************************/
      virtual ~testiterator ();
   
      /** Initializes the test iterator. Sets the storage pointer,
          the command notification object, the excitation manager
          pointer, the test pointer, and calls the begin function.
          @memo Initialization method.
          @param Storage diagnostics storage object
          @param Excitations excitation manager
          @param Test diagnostics test
          @param Notify command notification object
          @param rtMode Real-time mode if true (old data otherwise)
          @return true if successful
       ******************************************************************/
      virtual bool init (diagStorage& Storage,
                        excitationManager& Excitations,
                        diagtest& Test,
                        const cmdnotify& Notify, 
                        bool rtMode = true);
   
      /** Begins the test iterator. This function should be 
          overwritten by descendents for initialization.
          @memo Startup method.
          @param errmsg error message stream
          @return true if successful
       ******************************************************************/
      virtual bool begin (std::ostringstream& errmsg);
   
      /** Ends the test iterator. This function should be 
          overwritten by descendents for clean up.
          @memo Cleanup method.
          @param errmsg error message stream
          @return true if successful
       ******************************************************************/
      virtual bool end (std::ostringstream& errmsg);
   
      /** Sets up the test iterator. This function should be 
          overwritten by descendents. This one just increases the
          iteration count.
          @memo Setup method.
          @param errmsg error message stream
          @param starttime earliest time excitation can be turned on
          @return true if successful
       ******************************************************************/
      virtual bool setup (std::ostringstream& errmsg, tainsec_t starttime);
   
      /** Evaluates a test after each iteration. This function should
          be overwritten by descendents to implement an evaluation
          step (can call the merit function of the test). If the 
          function returns with another one set to true another test 
          iteration step will be scheduled, otherwise the test stops.
          If the iterator writes result records it must increase the
          rindex upon return to the next free index.
          @memo Evaluation method.
          @param errmsg error message stream
   	  @param anotherone true if continue, false if stop
          @param testrindex first result index of test
          @param rindex result index for iterator result (in/out)
          @param notify if true upon return sends a notfication message
          @return true if successful
       ******************************************************************/
      virtual bool evaluate (std::ostringstream& errmsg, 
                        bool& anotherone, int testrindex,
                        int& rindex, bool& notify);
   
      /** Returns the name of the supervisory task.
          @memo Name of supervisory task.
          @return name of supervisory task
       ******************************************************************/
      virtual std::string name () const {
         return myname;
      }
   
      /** Returns the iteration number.
          @return iteration count
       ******************************************************************/
      virtual int step () const {
         return stepnow;
      }
   
      /** Returns the total number of test iteration. 
          This function must be overwritten by descendents and should
          return -1 if the number is unknown.
          @memo Setup method.
          @return number of iteration steps
       ******************************************************************/
      virtual int numOfSteps () const = 0;
   
      /** A function which returns a new test iterator object of the same
          type. Must be overwritten by descendents.
          @memo New test iterator object.
          @return new test iterator
       ******************************************************************/
      virtual testiterator* self () const = 0;
   
   protected:
      /// Mutex to protect supervisory object
      thread::recursivemutex		mux;
      /// Name of supervisory task
      std::string		myname;
      /// Real-time mode
      bool			RTmode;
      /// Pointer to diagnostic storage object
      diagStorage*		storage;
      /// Pointer to test point management object
      testpointMgr*		tpMgr;
      /// Pointer to excitation manager
      excitationManager*	excitations;
      /// Pointer to diagnostics test
      diagtest*			test;
      /// Command notification object
      cmdnotify 		notify;
      /// Iteration count
      int			stepnow;
   };

//@}
}
#endif // _GDS_TESTITER_H
