/* version $Id: testsync.hh 7980 2018-01-05 01:26:54Z john.zweizig@LIGO.ORG $ */
/* -*- mode: c++; c-basic-offset: 3; -*- */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: testsync.h						*/
/*                                                         		*/
/* Module Description: test synchronization class			*/
/*									*/
/*                                                         		*/
/* Module Arguments: none				   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 19Apr999 D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: testsync.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-2178  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Ultra-Enterprise, Solaris 5.6			*/
/*	Compiler Used: sun workshop C++ 4.2				*/
/*	Runtime environment: sparc/solaris				*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			OK			*/
/*			  POSIX			OK			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*								 	*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_TESTSYNC_H
#define _GDS_TESTSYNC_H

//=====================================  c++17 compatibility
#ifndef NOEXCEPT
#if __cplusplus > 201100
#define NOEXCEPT noexcept
#else
#define NOEXCEPT throw()
#endif
#endif

/* Header File List: */
#include <memory>
#include "gdsmain.h"
#include "tconv.h"
#include "gmutex.hh"

namespace diag {

/** @name Synchronization tools
    This module implements test synchronization point objects.
    Synchronization points are used by the supervisory task
    to synchronize with a test.
   
    @memo Test synchronization points
    @author Written April 1999 by Daniel Sigg
    @version 0.1
 ************************************************************************/

//@{


template <class T>
   class auto_ptr_copy {
   public:
      /// element type
      typedef T element_type;
   
      /** Constructs an auto pointer which allows copy from const.
          @memo Constructor.
          @return void
       ******************************************************************/
      explicit auto_ptr_copy (T* x = 0) NOEXCEPT 
      : p (x), own (true) {
      }
   
      /** Constructs an auto pointer which allows copy from const. 
          @memo Constructor.
          @return void
       ******************************************************************/
      explicit auto_ptr_copy (T& x) NOEXCEPT 
      : p (new (std::nothrow) T (x)), own (true) {
      }
   
      /** Constructs an auto pointer which allows copy from const.
          The ownership is transferred to this object. 
          @memo Copy constructor.
          @return void
       ******************************************************************/
      auto_ptr_copy (const auto_ptr_copy<T>& ptr) NOEXCEPT 
      : p (ptr.p), own (ptr.own) {
         ptr.own = false;
      }
   
      /** Destructs an auto pointer.
          The pointer is freed if ownership is by this object. 
          @memo Destructor.
          @return void
       ******************************************************************/
      virtual ~auto_ptr_copy () NOEXCEPT {
         if (own) {
            delete p;
            own = false;
         }
      }
   
      /** Copies an auto pointer from an other auto pointer.
          The ownership is transferred to this object. 
          @memo Copy operator.
          @return void
       ******************************************************************/
      auto_ptr_copy<T>& operator= (const auto_ptr_copy<T>& ptr) NOEXCEPT {
         if (this != &ptr) {
            p = ptr.p;
            own = ptr.own;
            ptr.own = false;
         }
         return *this;
      }
   
      /** Dereference operator.
          @memo Dereference operator.
          @return T
       ******************************************************************/
      T& operator* () const NOEXCEPT {
         return *p;
      }	
   
      /** Arrow operator.
          @memo Arrow operator.
          @return T
       ******************************************************************/
      T* operator-> () const NOEXCEPT {
         return p;
      }	
   
      /** Get method.
          @memo Get method.
          @return T
       ******************************************************************/
      T* get () const NOEXCEPT {
         return p;
      }	
   
      /** Release method.
          @memo Release method.
          @return T
   ******************************************************************/
      T* release () NOEXCEPT {
         if (own) {
            own = false;
            return p;
         }
         else {
            return 0;
         }
      }
   
      /** Reset method.
          @memo Reset method.
          @return T
       ******************************************************************/
      void reset (T* x = 0) NOEXCEPT {
         if (own && (x != p)) {
            delete p;
         }
         p = x;
         own = true;
      }
   
   private:
      /// pointer to object
      T*		p;
      /// true if owner
      mutable bool	own;
   };


   class syncpoint;

/** This type describes an auto pointer to a syncpoint object which
    allows copy from const.
 ************************************************************************/
   typedef auto_ptr_copy<syncpoint> syncpointer;


/** This class implements a synchronization point. It is used by 
    test object to tell the supervisory when to callback and what to
    do. It is an abstract object.

    @memo Class for setting synchronization points.
    @author DS, November 98
    @see Diagnostics Management
 ************************************************************************/
   class syncpoint {
   public:
      /// type of synchronization event
      enum synctype {
      /// invalid synchronization event
      invalid = 0,
      /// synchronize on absolute time
      absoluteTime = 1,
      /// synchronize on the time when the data is ready
      dataReady = 2,
      /// synchronize on the time when an excitation is needed
      excitationNeeded = 3
      };
   
      /// synchronization type
      synctype 		sync;
      /// true if an abort command can be recognized during the wait
      bool		abortable;
      /// true if an pause command can be recognized during the wait
      bool		pauseable;
      /// synchronization time
      tainsec_t		time;
   
      /** Constructs an abstract synchronization point. By default the
          synchronization point is invalid.
   	  @param Sync synchronization type
   	  @param Time synchronization time
   	  @param Abortable true if abort is allowed
   	  @param Pauseable true if pause is allowed
          @memo Default constructor.
          @return void
       ******************************************************************/
      syncpoint (synctype Sync = invalid, tainsec_t Time = 0, 
                bool Abortable = false, bool Pauseable = false) : 
      sync (Sync), abortable (Abortable), pauseable (Pauseable),
      time (Time) {
      }
   
      /** Destructs an abstract synchronization point.
          @memo Destructor.
          @return void
       ******************************************************************/
      virtual ~syncpoint ();
   
         /** Returns true if the synchronization point is invalid.
          @memo Not operator.
          @return true if invalid synchronization point
       ******************************************************************/
      bool operator! () {
         return sync == invalid;
      }
   
      /** A pure virtual function which is called when the 
          synchronization event happens; must be overwritten by 
          descendents. This function must return a pointer to the next 
          synchronization point. An invalid synchronization point 
          or a zero pointer marks the end of the test. The caller
          is responsible to deallocate the returned syncpoint object.
          @memo Synchronization action.
          @param syncpt next synchronization point (return)
          @param notify if true upon return sends a notfication message
          @return true if successful
       ******************************************************************/
      virtual bool action (syncpointer& syncpt, bool& notify) = 0;
   
      /** A pure virtual function which is called when a test is aborted
          while it waits for the synchronization event; must be 
          overwritten by descendents. Can be used to perform abort
          specific functions. (Test and test iterator cleanup functions
          will be called independently.)
          @memo abort function.
          @return true if successful
       ******************************************************************/
      virtual bool abort () = 0;
   
      /** A pure virtual function which is called when a test is paused
          while it waits for the synchronization event; must be 
          overwritten by descendents. This function should be used to 
          freeze the state of the test at the current settings and 
          excitations. If a non-zero syncpointer is returned it will
          be used at the next one; otherwise the current one is 
          continued being used.
          @memo pause function.
          @param syncpt next synchronization point (optional return)
          @return true if successful
       ******************************************************************/
      virtual bool pause (syncpointer& syncpt) = 0;
   
      /** A pure virtual function which is called when a paused test is
          continued; must be  overwritten by descendents. This function
          must provide a new valid synchronization point upon return, so
          that the test can be continued. This function must provide all
          the necessary steps to restart the test and continue the 
          measurement at the point it was paused. If a non-zero 
          syncpointer is returned it will be used at the next one; 
          otherwise the current one is continued being used.
          @memo resume function.
          @param syncpt next synchronization point (optional return)
          @return true if successful
       ******************************************************************/
      virtual bool resume (syncpointer& syncpt) = 0;
      /** Peeks ahead and returns the synchronization time of a 
          future sync object. By default returns the time of this
          sync point. This routine can be used to optimize reading
          data ahead of the current sync point.
          @memo time ahead function.
          @return sync time in the future
       ******************************************************************/
      virtual tainsec_t time_ahead() const {
         return time; }
   
   protected:
      /// mutex to protect syncronization point
      mutable thread::recursivemutex	mux;
   };


/** This template class implements synchronization points for test 
    objects. It uses callback functions into the template class
    for action, abort, pause and resume functions. The first template
    parameter is the class which is used for callbacks (typically
    a descendent of diagtest); the second template parameter is the
    type which is passed as the argument to the callback fucntions;
    the third template parameter is the default callback member function
    for synchronization events; the fourth template parameter is
    the default callback member function for aborting a test; the fifth
    template parameter is the default callback member function for
    pausing a test; and the sixth template parameter is the default 
    callback member function for resuming a test. 
    (When instantinating this template, member functions have to be
    specified without a preceeding address operator&. Also it must not
    be an inline function.)

    @param testT template class used for peforming the callback
    @param idT identification type used as callback parameter 
           (default int)
    @param actionDefault default member function for a synchronization 
           events (default 0)
    @param abortDefault default member function for aborting a test
           (default 0)
    @param pauseDefault default member function for pausing a test
           (default 0)
    @param resumeDefault default member function for resuming a test
           (default 0)
    @memo Template class for setting synchronization points in tests.
    @author DS, November 98
    @see Diagnostics Management
 ************************************************************************/
template <class testT, class idT = int>
   class testsync : public syncpoint {
   public:
   
      /** Constructs a test synchronization object. The synchronization
          point is abortable if the abort function specified in the
          template instantiation, is non zero. The synchronization
          point is pauseable if both the pause and the resume 
          function specified in the template instantiation are non
          zero.
   
      	  @param Test reference to a diagnostics test
      	  @param Id identification used by callback
          @param Sync synchronization type
          @param Time synchronization time
          @memo Constructor.
          @return void
       ******************************************************************/
      explicit testsync (testT& Test, idT Id = idT(),
                        synctype Sync = invalid, tainsec_t Time = 0);
   
      virtual ~testsync(void) {}
   
      /** Calls the action function for synchronization events.
          @memo Synchronization action.
          @param next synchronization point (return)
          @param notify if true upon return sends a notfication message
          @return true if successful
       ******************************************************************/
      virtual bool action (syncpointer& sync, bool& notify);
   
      /** Calls the abort test function.
          @memo abort function.
          @return true if successful
       ******************************************************************/
      virtual bool abort ();
   
      /** Calls the pause test function.
          @memo pause function.
          @param syncpt next synchronization point (optional return)
          @return true if successful
       ******************************************************************/
      virtual bool pause (syncpointer& syncpt);
   
      /** Calls the resume test function.
          @memo resume function.
          @param syncpt next synchronization point (optional return)
          @return true if successful
       ******************************************************************/
      virtual bool resume (syncpointer& syncpt);
      /** Calls the time_ahead test function.
          @memo time ahead function.
          @return sync time in the future
       ******************************************************************/
      virtual tainsec_t time_ahead() const;
   
      /// callback identifier
      idT			id;
   
   protected:
      /// diagnostics test used for callback
      testT*			test;
   };


template <class testT, class idT>
   testsync<testT, idT>::testsync (testT& Test, idT Id, 
                     synctype Sync, tainsec_t Time) 
   : syncpoint (Sync, Time, true, true), id (Id), test (&Test) 
   {
   }


template <class testT, class idT>
   bool testsync<testT, idT>::action (syncpointer& syncpt, bool& notify) 
   {
      thread::semlock		lockit (mux);
      if (test != 0) {
         return test->syncAction (id, syncpt, notify);
      }
      else {
         return false;
      }
   }


template <class testT, class idT>           
   bool testsync<testT, idT>::abort () 
   {
      thread::semlock		lockit (mux);
   
      if (test != 0) {
         return test->syncAbort (id);
      }
      else {
         return false;
      }
   }


template <class testT, class idT>
   bool testsync<testT, idT>::pause (syncpointer& syncpt) 
   {
      thread::semlock		lockit (mux);
   
      if (test != 0) {
         return test->syncPause (id, syncpt);
      }
      else {
         return false;
      }
   }


template <class testT, class idT>
   bool testsync<testT, idT>::resume (syncpointer& syncpt) 
   {
      thread::semlock		lockit (mux);
   
      if (test != 0) {
         return test->syncResume (id, syncpt);
      }
      else {
         return false;
      }
   }


template <class testT, class idT>
   tainsec_t testsync<testT, idT>::time_ahead() const
   {
      thread::semlock		lockit (mux);
      tainsec_t tahead = time;
      if (test != 0) {
         test->syncTimeAhead (tahead);
      }
      return tahead;
   }

//@}
}

#endif // _GDS_TESTSYNC_H
