/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: diagtest						*/
/*                                                         		*/
/* Module Description: manages diagnostics supervisories, test and	*/
/* iterators.								*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

/* Header File List: */

#include "diagnames.h"
#include "stdsuper.hh"
#include "repeat.hh"
#include "sineresponse.hh"
#include "sweptsine.hh"
#include "timeseries.hh"
#include "ffttools.hh"
#include "diagorg.hh"
#include "diagdatum.hh"

namespace diag {
   using namespace std;


   static const supervisory* const supervisoryList[] =
   {
   new (nothrow) standardsupervisory ()
   };
   const int supervisoryListLen = 
   sizeof (supervisoryList) / sizeof (supervisory*);


   static const diagtest* const testList[] = 
   {
   new (nothrow) sineresponse (), 
   new (nothrow) sweptsine (),
   new (nothrow) timeseries (),
   new (nothrow) ffttest ()
   };
   const int testListLen = sizeof (testList) / sizeof (diagtest*);


   static const testiterator* const iteratorList[] =
   {
   new (nothrow) repeatiterator ()
   };
   const int iteratorListLen = 
   sizeof (iteratorList) / sizeof (testiterator*);



   const supervisory* getSupervisory (const string& name)
   {
      for (int i = 0; i < supervisoryListLen; i++) {
         if ((supervisoryList[i] != 0) &&
            (compareTestNames (name, supervisoryList[i]->name()) == 0)) {
            return supervisoryList[i];
         }
      }
      return 0;
   }


   const supervisory* getSupervisory (const diagStorage& storage)
   {
      if ((storage.Supervisory != 0) &&
         (storage.Supervisory->datatype == gds_string) &&
         (storage.Supervisory->value != 0)) {
         return getSupervisory ((char*) storage.Supervisory->value);
      }
      else {
         return 0;
      }
   }


   const testiterator* getTestIterator (const string& name) 
   {
      for (int i = 0; i < iteratorListLen; i++) {
         if ((iteratorList[i] != 0) &&
            (compareTestNames (name, iteratorList[i]->name()) == 0)) {
            return iteratorList[i];
         }
      }
      return 0;
   }


   const testiterator* getTestIterator (const diagStorage& storage)
   {
      if ((storage.TestIterator != 0) &&
         (storage.TestIterator->datatype == gds_string) &&
         (storage.TestIterator->value != 0)) {
         return getTestIterator ((char*) storage.TestIterator->value);
      }
      else {
         return 0;
      }
   }


   const diagtest* getTest (const string& name) 
   {
      for (int i = 0; i < testListLen; i++) {
         if ((testList[i] != 0) &&
            (compareTestNames (name, testList[i]->name()) == 0)) {
            return testList[i];
         }
      }
      return 0;
   }


   const diagtest* getTest (const diagStorage& storage)
   {
      if ((storage.TestType != 0) &&
         (storage.TestType->datatype == gds_string) &&
         (storage.TestType->value != 0)) {
         return getTest ((char*) storage.TestType->value);
      }
      else {
         return 0;
      }
   }


}

