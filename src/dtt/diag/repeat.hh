/* version $Id: repeat.hh 6312 2010-09-17 17:09:04Z james.batch@LIGO.ORG $ */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: repeat.h						*/
/*                                                         		*/
/* Module Description: Diagnostics test of a swept sine 		*/
/*									*/
/*                                                         		*/
/* Module Arguments: none				   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 30Dec98  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: repeat.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-2178  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Ultra-Enterprise, Solaris 5.6			*/
/*	Compiler Used: sun workshop C++ 4.2				*/
/*	Runtime environment: sparc/solaris				*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			OK			*/
/*			  POSIX			OK			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*								 	*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_REPEAT_H
#define _GDS_REPEAT_H

/* Header File List: */
#include <string>
#include "gdsstring.h"
#include "testiter.hh"
#include "diagnames.h"

namespace diag {

/** @name Repeat Test Iterator
   
    @memo Object representing a repeat test iterator
    @author Written April 1999 by Daniel Sigg
    @version 0.1
 ************************************************************************/

//@{

/** Repeat test iterator
    This object implements the test iterator for repeating tests. The only
    parameter is the number of iterations.
   
    @memo Object for implementing a repeat test iterator
    @author Written April 1999 by Daniel Sigg
    @version 0.1
 ************************************************************************/
   class repeatiterator : public testiterator {   
   public:
   
      /** Constructs a repeat test iterator object.
   	  @memo Default constructor
       ******************************************************************/
      explicit repeatiterator () : 
      testiterator (repeatIteratorName), numsteps (0) {
      }
   
      /** Begins the test iterator. Reads the repeat value from the 
          storage object.
          @memo Startup method.
          @return true if successful
       ******************************************************************/
      virtual bool begin (std::ostringstream& errmsg);
   
      /** Evaluates a test after each iteration. Returns false if the 
          iteration count exceeds the total number of steps (repeats).
          @memo Evaluation method.
          @param errmsg error message stream
   	  @param anotherone true if continue, false if stop
          @param testrindex first result index of test
          @param rindex result index for iterator result (in/out)
          @param notify if true upon return sends a notfication message
          @return true if successful
       ******************************************************************/
      virtual bool evaluate (std::ostringstream& errmsg, 
                        bool& anotherone, int testrindex,
                        int& rindex, bool& notify);
   
      /** Returns the total number of repeats.
          @memo Setup method.
          @return number of iteration steps
       ******************************************************************/
      virtual int numOfSteps () const;
   
      /** A function which returns a new repeat test iterator object.
          @memo New repeat test iterator object.
          @return new test iterator
       ******************************************************************/
      virtual testiterator* self () const;
   
   protected:
      /// number of iterations
      int		numsteps;
   };

//@}
}

#endif // _GDS_REPEAT_H
