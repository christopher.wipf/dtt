
#include "gdsstring.h"
#include "diagnames.h"
#include "testiter.hh"
#include "diagtest.hh"
#include "excitation.hh"
#include "diagdatum.hh"
#include "testpointmgr.hh"

namespace diag {
   using namespace std;
   using namespace thread;


   testiterator::~testiterator () 
   {
   }


   bool testiterator::init (diagStorage& Storage,
                     excitationManager& Excitations,
                     diagtest& Test,
                     const cmdnotify& Notify, bool rtMode) {
      semlock	lockit (mux);
      stepnow = 0;
      storage = &Storage;
      excitations =&Excitations;
      test = &Test;
      notify = Notify;
      RTmode = rtMode;
   
      return true;
   }


   bool testiterator::begin (ostringstream& errmsg) {
   
      return true;
   }


   bool testiterator::end (ostringstream& errmsg) {
      return true;
   }


   bool testiterator::setup (ostringstream& errmsg, tainsec_t starttime) {
      stepnow++;
      return true;
   }


   bool testiterator::evaluate (ostringstream& errmsg, 
                     bool& anotherone, int testrindex,
                     int& rindex, bool& note) {
      anotherone = false;
      note = false;
      return true;
   }

}
