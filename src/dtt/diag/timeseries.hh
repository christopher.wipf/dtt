/* version $Id: timeseries.hh 7690 2016-08-17 00:07:17Z james.batch@LIGO.ORG $ */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: timeseries.h						*/
/*                                                         		*/
/* Module Description: Diagnostics test of a triggered time series	*/
/*									*/
/*                                                         		*/
/* Module Arguments: none				   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 5Jun9    D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: timeseries.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-2178  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Ultra-Enterprise, Solaris 5.6			*/
/*	Compiler Used: sun workshop C++ 4.2				*/
/*	Runtime environment: sparc/solaris				*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			OK			*/
/*			  POSIX			OK			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*								 	*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_TIMESERIES_H
#define _GDS_TIMESERIES_H

/* Header File List: */
#include <string>
#include "stdtest.hh"

   class Pipe;

namespace diag {


/** Triggered Time Series Measurement
    This object implements the triggered time series measurement.
   
    @memo Object for implementing a triggered time series test
    @author Written June 1999 by Daniel Sigg
    @see Diagnostics test manual for used algorithms
    @version 0.1
 ************************************************************************/
   class timeseries : public stdtest {  
   public:
   
      /** Constructs a test object for measuring the time series.
          @memo Default constructor.
          @return void
       ******************************************************************/
      timeseries ();
   
      // JCB
      ~timeseries() ;

      /** Returns a new diagnostics test object describing a time
          series measurement.
          @memo Diagnostics test object creation function.
          @return new diagnotsics test object
       ******************************************************************/
      virtual diagtest* self () const;
   
      /** End of test. This function cleans up temporary storage.
          @memo Cleanup method.
          @param errmsg error message stream
          @return true if successful
       ******************************************************************/
      virtual bool end (std::ostringstream& errmsg);
   
   protected:
   
      /// temporary storage for intermediate results
      class tmpresult {
      public:
         /** Constructs a temporary object to store intermediate results.
             @memo Default constructor.
             @param Name channel name
             @param Size size of storage arrays
             @param Dx X spacing (dt)
             @param Cmplx set true if time series was down-converted
             @return void
          ***************************************************************/
         explicit tmpresult (std::string Name, int Size = 0, double Dx = 1.0,
                           bool Cmplx = false, std::string Filter = "");
      
         /** Destructs the temporary object.
             @memo Destructor.
             @return void
          ***************************************************************/
         ~tmpresult();
      
         /** Constructs a temporary object from another one.
             @memo Copy constructor.
             @param tmp temporary object
             @return void
          ***************************************************************/
         tmpresult (const tmpresult& tmp);
      
         /** Copies a temporary object from another one. Moves the 
             pointers rather than copy. It will set the pointers in
             the original object to zero.
             @memo Copy operator.
             @param tmp temporary object
             @return reference to object
          ***************************************************************/
         tmpresult&  operator= (const tmpresult& tmp);
      
         /** Allocates new storage. Deletes the old arrays first.
             @memo Allocate memory.
             @param Size size of storage arrays
             @return true if successful
          ***************************************************************/
         bool allocate (int Size = 0);
      
         /** True if valid temporary object.
             @memo Valid method.
             @return true if valid
          ***************************************************************/
         bool valid () const;

	 // JCB - for debugging
	 int instance(void) { return myinstance ; } ; // JCB
      
         /// channel name
         std::string	name;
         /// true if complex time series
         bool		cmplx;
         /// size of arrays, i.e. number of time series points
         int		size;
         /// current time series
         float*		x;
         /// temporary array
         float*		xsqr;
         /// x spacing (dt)
         double		dx;
         /// filter spec
         std::string		filterspec;
         /// IIR filter
         Pipe*		filter;
      // JCB - keep track of instance of this class.
      private:
	 static int instance_count ;
	 int	myinstance ;
      };
      typedef std::vector<tmpresult> tmpresults;
   
      /// measurement time
      double		measTime;
      /// pre-trigger time
      double		preTriggerTime;
      /// settling time
      double		settlingTime;
      /// dead time
      /// Ramp down time
      double            rampDown ;
      /// Ramp up time
      double            rampUp ;
      double		deadTime;
      /// signal bandwidth
      double		sigBW;
      /// include statistics?
      bool		includeStatistics;
      /// filter specification
      std::string	filterSpec;
   
      /// highest frequency of interest
      double		fMaxMeas;
      /// lowest sampling frequency
      double		fMinSample;
      /// highest sampling frequency
      double		fMaxSample;
      /// heterodyne frequency
      double		fZoom;
      /// measurement start time
      double 		mStart;
      /// t = 0 for excitation signals
      double 		exct0;
      /// time to add to mTime to set it to the next time grid
      double 		mTimeAdd;
      /// adjusted dead time
      double 		dTime;
      /// number of skipped measurement steps
      int		skipMeas;
   
      /// temporary storage
      tmpresults	tmps;
   
      /** Read parameters. This function reads the parameters from the 
          storage object.
          @memo Read parameter method.
          @param errmsg error message stream
          @return true if successful
       ******************************************************************/
      virtual bool readParam (std::ostringstream& errmsg);
   
      /** Calcluate measurement time. This function calculates start 
   	  time and duration of each measuremenmt period.
          @memo Calculate measurement time method.
          @param errmsg error message stream
          @param t0 start time
          @return true if successful
       ******************************************************************/
      virtual bool calcTimes (std::ostringstream& errmsg,
                        tainsec_t& t0);
   
      /** Calcluate a new measurement point. This function calculates 
          a new measurement interval, synchronization point and
          measurement partitions.
          @memo Calculate measurement point method.
          @param t0 start time (t = 0)
          @param t1 earliest time measurement can start
          @param i measurement point index
          @param measPoint measurement index
          @return true if successful
       ******************************************************************/
      virtual bool newMeasPoint (int i, int measPoint = 0);
   
      /** Calculates measurement parameters. This function determines 
          excitation signals, partitions for the rtdd and synchronization 
          points.
          @memo Add measurements method.
          @param errmsg error message stream
          @param t0 start time
          @param measPoint measurement point number
          @return true if successful
       ******************************************************************/
      virtual bool calcMeasurements (std::ostringstream& errmsg,
                        tainsec_t t0 = 0, int measPoint = 0);
   
      /** Stops the measurements. This function deletes the temporary
          storage space, then calls its base class function.
          @memo Stop measurements method. 
          @param firstIndex first data index to delete
          @return true if successful
       ******************************************************************/
      virtual bool stopMeasurements (int firstIndex = -1);
   
      /** Analysis routine which performs the analysis. This function 
          must be overwritten by descendents.
          @memo Analysis method.
          @param id callback argument describing the sync event
          @param measnum measurement number
          @param notify if true upon return sends a notfication message
          @return true if successful
       ******************************************************************/
      virtual bool analyze (const callbackarg& id, int measnum, 
                        bool& notify);
   
      /** Sums up and averages time series.
          @memo Sum/Average method
          @param resultnum number of result record
          @param measnum measurement number
          @param chnname channel name (including array indices)
          @param stim true if stimulus readback channel
          @param id callback argument describing the sync event
          @return true if successful
       ******************************************************************/
      bool sum (int resultnum, int measnum, std::string chnname, bool stim,
               const callbackarg& id);   

      // JCB - for debugging return the instance of this object.
      int instance (void) { return myinstance; } ;

      // JCB - keep track of instance number for scope.
      private:
	 static int instance_count ;
	 int	myinstance ;
   };

}
#endif // _GDS_TIMESERIES_H
