/* version $Id: diagorg.hh 6312 2010-09-17 17:09:04Z james.batch@LIGO.ORG $ */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: diagorg.h						*/
/*                                                         		*/
/* Module Description: Diagnostics test organization			*/
/*									*/
/*                                                         		*/
/* Module Arguments: none				   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 28Nov98  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: diagtest.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-2178  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Ultra-Enterprise, Solaris 5.6			*/
/*	Compiler Used: sun workshop C++ 4.2				*/
/*	Runtime environment: sparc/solaris				*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			OK			*/
/*			  POSIX			OK			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*								 	*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_DIAGORG_H
#define _GDS_DIAGORG_H

/* Header File List: */
#include <string>
#include "gdsstring.h"
#include "diagtest.hh"
#include "testiter.hh"
#include "supervisory.hh"

namespace diag {


/** @name Diagnostics test object repository
    This module implements a reposiiitory of diagnostics tests,
    diagnostics test iterators and diagnostics supervisry tasks.
   
    @memo Repository of diagnostics objects
    @author Written April 1999 by Daniel Sigg
    @version 0.1
 ************************************************************************/

//@{

/** Gets a supervisory task. Gets a pointer to a supervisory task
    of the specified name. If the name is invalid, it returns 0. A new
    supervisory task can be created by using the self() function of
    the returned object.

    @param name name of supervisory task
    @return pointer to supervisory task, 0 if failed
    @author DS, July 98
************************************************************************/
   const supervisory* getSupervisory (const std::string& name);

/** Gets a supervisory task. Gets a pointer to a supervisory task
    with the name read from the specified storage object. If the name 
    is invalid, it returns 0. A new supervisory task can be created by 
    using the self() function of the returned object.
   
    @param storage diagnostics storage object
    @return pointer to supervisory task, 0 if failed
    @author DS, July 98
 ************************************************************************/
   const supervisory* getSupervisory (const diagStorage& storage);

/** Gets a test iterator. Gets a pointer to a test iterator
    of the specified name. If the name is invalid returns 0. A new
    test iterator can be created by using the self() function of
    the returned object.

    @param name name of test iterator
    @return pointer to test iterator, 0 if failed
    @author DS, July 98
************************************************************************/
   const testiterator* getTestIterator (const std::string& name);

/** Gets a test iterator. Gets a pointer to a test iterator
    with the name read from the specified storage object. If the name 
    is invalid, it returns 0. A new test iterator can be created by 
    using the self() function of the returned object.

    @param storage diagnostics storage object
    @return pointer to test iterator, 0 if failed
    @author DS, July 98
************************************************************************/
   const testiterator* getTestIterator (const diagStorage& storage);

/** Gets a diagnostics test. Gets a pointer to a diagnostics test
    of the specified name. If the name is invalid returns 0. A new
    diagnostics test can be created by using the self() function of
    the returned object.

    @param name name of diagnostics test
    @return pointer to diagnostics test, 0 if failed
    @author DS, July 98
************************************************************************/
   const diagtest* getTest (const std::string& name);

/** Gets a diagnostics test. Gets a pointer to a diagnostics test
    with the name read from the specified storage object. If the name 
    is invalid, it returns 0. A new diagnostics test can be created by 
    using the self() function of the returned object.

    @param storage diagnostics storage object
    @return pointer to diagnostics test, 0 if failed
    @author DS, July 98
************************************************************************/
   const diagtest* getTest (const diagStorage& storage);


//@}

}
#endif // _GDS_DIAGORG_H
