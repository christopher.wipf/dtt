/* version $Id: diagtest.hh 7102 2014-06-23 21:06:02Z james.batch@LIGO.ORG $ */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: diagtest.h						*/
/*                                                         		*/
/* Module Description: Abstract diagnostics test class			*/
/*									*/
/*                                                         		*/
/* Module Arguments: none				   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 28Nov98  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: diagtest.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-2178  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Ultra-Enterprise, Solaris 5.6			*/
/*	Compiler Used: sun workshop C++ 4.2				*/
/*	Runtime environment: sparc/solaris				*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			OK			*/
/*			  POSIX			OK			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*								 	*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_DIAGTEST_H
#define _GDS_DIAGTEST_H

/* Header File List: */
#include <string>
#include <iostream> // JCB
#include "tconv.h"
#include "gmutex.hh"
#include "gdsstring.h"
#include "gdsnotify.hh"
#include "testenv.hh"
#include "testsync.hh"

namespace diag {

   using namespace std ; // JCB
   class diagStorage;
   class dataBroker;
   class excitationManager;

/** @name Abstract diagnostics test object
    This module implements an abstract diagnostics test object from 
    which all diagnostics test objects must be inherited.
   
    @memo Abstract objects of a diagnostics test
    @author Written November 1998 by Daniel Sigg
    @version 0.1
 ************************************************************************/

//@{


/** This abstract class implements the basic functionality of a 
    diagnostics test. Every diagnostics test class must be derived
    from this one. It implements abstract methods to query the name,
    get the excitation channels, get the measurement time intervals 
    and the analysis algorithms.

    The public interface of this object is multi-thread safe.

    @memo Abstract class for diagnostics tests.
    @author DS, November 98
    @see Diagnostics Management
 ************************************************************************/
   class diagtest : public testenvironment {
   public:
      /** Calculates the measurement time for a periodic function.
          This function returns the maximum of duration and cycles/f.
          A negative duration or cycle count is ignored. Optionally,
          the measurement time can be rounded up to the next cycle 
          count.
          @memo Calculate measurement time.
          @param duration duration in sec
          @param cycles number of cycles
   	  @param f frequency of periodic function
   	  @param roundup if true round up to the next cycle
          @return measurement time or <0 on error
       ******************************************************************/
      static double measurementTime (double duration, double cycles, 
                        double f, bool roundup = true);
   
      /** Assuming dt specifies the sampling period of interest, this
          function returns a time interval which is rounded up to the
          next integer multiple of the sampling period. 
          @memo Adjust for sampling.
          @param t time in sec
          @param dt sampling period in sec
          @return rounded time or <0 on error
       ******************************************************************/
      static double adjustForSampling (double t, double dt);
   
      /** Assuming dt specifies the sampling period of interest, this
          function returns a GPS time value which is rounded up to the
          next integer multiple of the sampling period. 
          @memo Fine adjust for sampling.
          @param t time in GPS nsec
          @param dt sampling period in sec
          @return rounded GPS time or <0 on error
       ******************************************************************/
      static tainsec_t fineAdjustForSampling (tainsec_t t, double dt);
   
   
      /** Constructs an abstract diagnostics object by setting the
          reference pointer to the storage object.
          @memo Default constructor.
          @return void
       ******************************************************************/
      explicit diagtest (const std::string& Name) :
      myname (Name), notify (), step (0), rindex (0), rnumber (0) {
	 myinstance = instance_count ;  // JCB
	 instance_count++ ;  // JCB
      }
   
      /** Destructs the abstract diagnostics object.
          @memo Destructor.
          @return void
       ******************************************************************/	
      virtual ~diagtest ();
   
      /** A pure virtual function which returns a new diagnostics test
          object of the same type as the current one. Must be overwritten
          by descendent objects. This function together with the name
          function is used by the diagnostics test pointer object to 
          automatically create a diagnostics test object from a storage
          object.
          @memo Diagnostics test object creation function.
          @return new diagnostics test object
       ******************************************************************/
      virtual diagtest* self () const = 0;
   
      /** A function which returns a unique name describing
          the type of diagnostics test represented by this object.
          @memo Name of diagnostics test class.
          @return name of diagnostics test class
       ******************************************************************/
      virtual std::string name () const {
         return myname;
      }
   
      /** Initializes the test. Sets the storage pointer,
          the command notification object and the excitation manager
          pointers for the environment and the test signals; calls the 
          begin function.
          @memo Initialization method.
          @param Storage diagnostics storage object
          @param Notify command notification object
          @param EnvExc excitation manager for environment
          @param TestExc excitation manager for test signals
          @param KeepTraces do we keep the original time traces
          @param rtMode Real-time mode if true (old data otherwise)
          @return true if successful
       ******************************************************************/
      virtual bool init (diagStorage& Storage, 
                        const cmdnotify& Notify,
                        dataBroker& databroker,
                        excitationManager& EnvExc,
                        excitationManager& TestExc,
                        int KeepTraces, bool rtMode = true);
   
      /** Sets the test iteration step.
          @memo Set step method.
          @param Step new test iteration step
          @param resindex first index to be used for result
       ******************************************************************/
      virtual void setStep (int Step, int resindex) 
      {
         rindex = resindex;
         step = Step;
      }
   
      /** Gets the next free index into the result array. This function
          only returns a valid value after calls to setStep and setup
          have been completed.
          @memo Get next result index method.
          @return next free index
       ******************************************************************/
      virtual int getResultNumber () 
      {
         return rnumber;
      }
   
      /** Begin of test. This function should be overwritten by 
          descendents for initialization (read parameters from 
          storage object and subscribe to channels).
          @memo Startup method.
          @param errmsg error message stream
          @return true if successful
       ******************************************************************/
      virtual bool begin (std::ostringstream& errmsg) 
      {
         return true;
      }
   
      /** End of test. This function should be  overwritten by 
          descendents for clean up (unsubscribe channels).
          @memo Cleanup method.
          @param errmsg error message stream
          @return true if successful
       ******************************************************************/
      virtual bool end (std::ostringstream& errmsg) {
         return true;
      }
   
      /** Sets up the test. This function should be overwritten by 
          descendents set excitation and measurement channels.
          This function may adjust the start time if necessary.
          In general starting a test later is always supported,
          whereas starting it earlier might not be possible.
          @memo Setup method.
          @param errmsg error message stream
          @param starttime start time of test
          @param sync synchronization point (return)
          @return true if successful
       ******************************************************************/
      virtual bool setup (std::ostringstream& errmsg, 
                        tainsec_t starttime,
                        syncpointer& sync) {
         return true;
      }
   
      /** A function which reads the enviroment of a test. Implements 
          only a dummy; must be overwritten by descendents in order
          to implement a real environment.
          @memo Read environment.
          @return true if successful
       ******************************************************************/
      virtual bool readEnvironment () {
         return true;
      }
   
   
   protected:
   
      /// Mutex to protect test iterator
      mutable thread::recursivemutex	mux;
      /// Name of test
      std::string		myname;
      /// Command notification object
      cmdnotify			notify;
     /// Pointer to diagnostic storage object
      diagStorage*		storage;
      /// Pointer to test point management object
      dataBroker*		dataMgr;
      /// Pointer to excitation manager for test channels
      excitationManager*	testExc;
      /// Real-time mode
      bool			RTmode;
      /// test iteration step
      int			step;
      /// first index to be used for saving results
      int			rindex;
      /// number of result records saved
      int			rnumber;
      /// keep the original time traces?
      int			keepTraces;

   // JCB 
   // Keep track of instance number for scope tests.
   private:
      static int instance_count ;
      int	myinstance ;
   };


//@}
}

#endif // _GDS_DIAGTEST_H
