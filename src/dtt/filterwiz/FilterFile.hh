/* version $Id: FilterFile.hh 7856 2017-02-22 20:34:46Z james.batch@LIGO.ORG $
 */
#ifndef _LIGO_FILTERIO_H
#define _LIGO_FILTERIO_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: FilterFile						*/
/*                                                         		*/
/* Module Description: Filter IO to read and write online files		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 1.0	 4Aug02   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: FilterFile.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include <sys/stat.h>
#include "FilterDesign.hh"
#include "FilterSection.hh"
#include "FilterModule.hh"
#include <cstdlib>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include "IIRSos.hh"

namespace filterwiz
{

    /// Magic string for an online filter file
    const char* const kMagic = "# FILTERS FOR ONLINE SYSTEM";
    /// Magic string for a Matlab design file
    const char* const kMatlabMagic = "# MATLAB FILTER DESIGN";

    /** The %FilterFile class reads and writes the filter files of the online
      * system. The filter file contains a series of named modules, each of
      * which contains one or more filter sections. Upon reading a file, the
      * filter definitions are kept in an internal module list. This list is
      * accessible by the  modules() methods or by accessing a named
     FilterModule
      * with the find() methods. The content of the filter list can be written
     to
      * a file system with the write() method.
      *
      * The format of the external files is as follows:
      *  - Filter file identifier:
      *    The filter file identifier is a fixed string making up the first
      *    line of the onlie filter file:
      *    \verbatim
           # FILTERS FOR ONLINE SYSTEM \endverbatim
      *  - Each file contains one or more lists of modules \e i.e.
      *    \verbatim
           # MODULES <modname1> [... <modnameN> ] \endverbatim
      *  - The sample rate for all the modules is specified by
      *    \verbatim
           # SAMPLING RATE <rate> \endverbatim
      *  - The original design string for each stage is listed with:
      *    \verbatim
           # DESIGN <modname> <sect-number> <design-string> \endverbatim
      *    Sections are numebered from 0. The design strings are in the
      *    syntax used by the FilterDesign class.
      *  - Following the design string is a list of coefficients for each stage.
      *    \verbatim
           <modname> <sect-number> <sw> <sos-number> <c_0> <c_1> <c_2> <c_3>
     <c_4> <c_5> <c_6> <c_7> <c_8>
                                      ...
                                                           <c_4j+1)> ...
     <c_4j+1)> \endverbatim
      *    The number of lines corresponds to the number of second-order
     sections.
      *
      * The FilterFile class supports writing legacy format files as well as
      * the current filter file format. The new format differs in that it writes
      * 16 digits after the decimal point for the SOS coefficients, whereas
      * the legacy version supports only 14 decimal digits. The new format also
      * Allows "gain only" filter modules, \e i.e. modules with no filter
      * coefficients and a non-unit gain. Gain stages in the legacy format must
      * have at least 1 SOS stage. Gain only stages are allowed by default for
      * the new format and may be enabled/disabled using the set0SosAllowed()
      * method.
      *
      * @memo Filter File IO
      * @author Written August 2002 by Daniel Sigg
      * @version 1.0
     ************************************************************************/
    class FilterFile
    {
    public:
        /**  Construct an empty filter file.
         *  @memo Default constructor.
         */
        FilterFile( );
        ~FilterFile( );

        /**  Clear all modules from list
         *  @memo clear the file.
         */
        void clear( );

        /**  Add a module to the internal representation.
         *  \brief Add a module.
         *  \param name Name of module to be added
         *  \param fsample Sample rate of module to be added.
         */
        void add( const char* name, double fsample = 1.0 );

        /**  Remove the named module.
         *  \brief Remove a module
         *  \param name Name of module to be removed.
         */
        void remove( const char* name );

        /**  Find a named module in the FilterFile list. If the module is not
         *  found, find() returns  a NULL pointer.
         *  \brief find a module.
         *  \param name Name of the mondule to be found.
         *  \return Pointer to the found module or NULL.
         */
        FilterModule* find( const char* name );

        /**  Find a named module in the FilterFile list. If the module is not
         *  found, find() returns  a NULL pointer.
         *  \brief find a module.
         *  \param name Name of the mondule to be found.
         *  \return Pointer to the found module or NULL.
         */
        const FilterModule* find( const char* name ) const;

        /**  Return a constant reference to the internal module list.
         *  \brief Get module list reference.
         *  \return Constant reference to the internal module list.
         */
        const FilterModuleList&
        modules( ) const
        {
            return fModules;
        }

        /**  Return a reference to the internal module list.
         *  \brief Get module list reference.
         *  \return Feference to the internal module list.
         */
        FilterModuleList&
        modules( )
        {
            return fModules;
        }

        /**  Get name of the file from which the module list was read and/or
         *  to which the file will be written.
         *  \brief Filter file name.
         *  \return Pointer to the file nmae string.
         */
        const char*
        getFilename( ) const
        {
            return fFilename.c_str( );
        }

        void
        setFilename( const char* filename )
        {
            fFilename = filename;
        }

        /**  Check if filters are valid and return an error string if an
         *  invalid module definition is found.
         *  \brief Cheif the filters are valid.
         *  \param errmsg Error message string.
         *  \return True if all modules are valid.
         */
        bool valid( std::string& errmsg ) const;

        /** Update all sections of all modules . Note that the update may not
         * be complete if one or more sections fail.
         * \brief Update filtering
         * \return True if successful.
         */
        bool update( );

        /**  Open and read an online filter file.
         *  \brief Read an online filer file.
         *  \param filename Name of file to be read/parsed.
         *  \return True if successful.
         */
        bool read( const char* filename );

        /**  Read a module from a char array
         *  \brief Parse an online file string.
         *  \param p      pointer to file string.
         *  \param maxlen length of text string.
         *  \return maxlen on success, otherwise, 0.
         */
        int read( const char* p, int maxlen, bool force_no_correct_on_load );

        /**  Write the contents of the  filter module list to the specified
         *  file name.
         *  \brief Write an online file
         *  \param filename Name of file to be written.
         *  \return True on success.
         */
        bool write( const char* filename );

        /**  Write the contents of the filter module list to a character
         *  array in the online filter file format.
         *  \brief Write a module to a char array
         *  \param p character array pointer to receive module list definition.
         *  \param maxlen Allocated length of buffer pointer to be p.
         *  \return Number of bytes written to \a p or zero on failure.
         */
        int write( char* p, int maxlen );

        /**  Convert the filter file to presumably a new data rate.
         *  \brief Change sample rate of filter file.
         *  \param newRate Data rate for filters.
         *  \return none
         */
        void convertFilters( double newRate );

        /** Write the contents of the filter module list to a file specified
         * by the filename.  If errmsg is non-null, it should point to
         * sufficient storage for error messages returned during the write
         * process.
         */
        bool write( const char* filename, char* errmsg );

        /**  Check the file to see if some other process has modified it.
         *  \brief Check file status.
         *  \return True if the file has not been rewritten.
         */
        bool checkFileStat( ); // JCB

        /**  Update the stat structure after a file has been written.
         *  \brief Update file status.
         *  \param filename Full path name of file.
         */
        void updateFileStat( const char* filename ); // JCB

        /** Get the "real" path of a file given an filename.
         * The filename may be a symbolic link, in which case
         * the file to write would be what the link points to
         * instead of the link itself.  Follow links until a
         * non-link is found or the file doesn't exist, then
         * return the path found.  The method requires a dirname
         * to be the directory in which filename is found in case
         * the filename is a relative path.
         * \brief Get real filename.
         * \param filename Name of file which may be a link.
         * \param dirnam Name of directory which contains filename.
         * \return String containing actual file path.
         */
        std::string getRealFilename( std::string filename,
                                     std::string dirname ); // JCB

        /**  Enable/disable writing a legacy version online filter file. The
         *  legacy version is needed for iLigo front ends and has the following
         *  limitations:
         *   - reduced preciion in the filter coefficients (14 digits
         *     instead of 16.
         *   - All filter seconds must have at leas one sos - Gain only
         *     modules are not supported.
         *  \brief Set the legacy write flag.
         *  \param flag Enable (non-zero) or disable (zero) writing legacy
         * files.
         */
        void
        setLegacyWrite( int flag = 0 )
        {
            legacy_write = flag;
        }; // JCB

        /**  Get the legacy write flags.
         *  \brief Get the legacy write mode.
         *  \return True if legacy files are to be written.
         */
        bool
        LegacyWrite( )
        {
            return legacy_write;
        }; // JCB

        /**  Set a flag to allow any sample rate to be used in the filter file.
         *  \brief Set any sample rate flag
         */
        void
        SetAnySampleRate( bool any_sample = 0 )
        {
            any_sample_rate = any_sample;
        };

        /**  Get the value of the any_sample_rate flag.
         *  \brief Return the any_sample_rate flag value.
         *  \return True if any sample rate is allowed.
         */
        bool
        AnySampleRate( )
        {
            return any_sample_rate;
        };

        /**  Enable or disable writing of stages consisting of no
         *  SOS sections,
         *  \brief Set the 0 SOS allowed flag.
         *  \param flag Enable/Disable flag
         */
        void
        set0SosAllowed( int flag = 0 )
        {
            gain_only_allowed = flag;
        }; // JCB

        /**  Get the 0 SOS allowed flag.
         *  \brief Test zero sos allowed
         *  \return True if zero sos sections allowed
         */
        bool
        sos0Allowed( )
        {
            return gain_only_allowed;
        }; // JCB

        /** Get the sample rate for the file */
        const char*
        getFSampleStr( )
        {
            return fSample.c_str( );
        }; // JCB
        double
        getFSample( )
        {
            return atof( fSample.c_str( ) );
        }; // JCB

        /* Set the sample rate for the file */
        void setFSample( const char* sample ); // JCB
        void setFSample( double sample ); // JCB
        void setFSample( unsigned int sample ); // JCB

        // Merge functions
        int merge( const char* filename );
        int merge( const char* beg, int maxlen );

        void
        setMergeDebug( int val )
        {
            mergeDebug = val;
        };

        std::vector< message_t >
        getMergeMessages( )
        {
            return merge_message_vec;
        }

        void
        clearMergeMessages( )
        {
            merge_message_vec.clear( );
        }

        bool
        emptyMergeMessages( )
        {
            return merge_message_vec.empty( );
        }

        std::vector< message_t >
        getFileMessages( )
        {
            return file_message_vec;
        }

        void
        clearFileMessages( )
        {
            file_message_vec.clear( );
        }

        bool
        emptyFileMessages( )
        {
            return file_message_vec.empty( );
        }

        bool
        get_correct_on_load( )
        {
            return _correct_on_load;
        }

        void
        set_correct_on_load( bool new_correct_on_load )
        {
            _correct_on_load = new_correct_on_load;
        }

    protected:
        /// Filter file
        std::string fFilename;
        /// Filter module list
        FilterModuleList fModules;
        /// File stat structure when file is opened.
        struct stat fStat; // JCB
        /// Legacy write allows file formats for old systems...
        int legacy_write; // JCB

        /// Allow any sample rate to be specified in the file if true.
        bool any_sample_rate;

        // Correct coefficients on load
        // used by seismic and perhaps others to enforce filter stability
        bool _correct_on_load;

        /// Are gain-only filters with no SOS allowed?
        int gain_only_allowed; // JCB
        /// Sample rate for all filters in the file.  When a file is read,
        /// the sample rate read from the file is copied to all modules as
        /// they are read, and the rate is not changeable.  Functions that
        /// create or edit filters should use the module's sample rate to
        /// be consistent with editing filters from diaggui.
        std::string fSample; // JCB

        /// Returns a valid filter section name
        std::string validSectionName( const char* name );

        // Set to true for debugging the merge functions.
        int mergeDebug;

        //      int			linenum ;

        std::vector< message_t > merge_message_vec;

        std::vector< message_t > file_message_vec;

        /**
         * Read in a buffer into a new FilterFile file object.
         * The buffer is typically one formed by FilterFile::write(),
         * Copy any errors from the new FilterFile object to this object,
         * The delete the new filter file object.
         *
         * Use case: when writing to a file, call this function to find
         * any load-time errors that would be caused by the file.
         *
         * @param buffer A buffer containing the contents of the file.
         * @param buflen The size of the buffer, as required by
         * FilterFile::read()
         */
        void check_load( const char* buffer, int buflen );

        /**
         * Apply GDS signalprocessing library's biquad function
         * to an out-going SOS.
         *
         * Pushes an error to the error list if the SOS is invalid.
         *
         * When an SOS is tweaked, a warning is pushed to the error list.
         * @param coeffs [in/out] four values representing a single SOS in A1 A2
         * B1 B2 format.
         * @param mod module.  Used to help generate any warnings or errors.
         * @param section section index into module.  Used to help generate any
         * warnings or errors
         * @returns true if there were no warnings
         */
        bool check_poles( double*             coeffs,
                          const FilterModule& mod,
                          int                 section,
                          bool                correct_on_load );

        /**
         * Apply GDS signalprocessing library's biquad function
         * to an out-going SOS.
         *
         * Pushes an error to the error list if the SOS is invalid.
         *
         * When an SOS is tweaked, a warning is pushed to the error list.
         * @param sos [in/out] an SOS object.
         * @param mod module.  Used to help generate any warnings or errors.
         * @param section section index into module.  Used to help generate any
         * warnings or errors
         * @returns true if there were no warnings
         */
        bool check_poles( IIRSos&             sos,
                          const FilterModule& mod,
                          int                 section,
                          bool                correct_on_load );

        /**
         * Apply check_poles to one filter's worth of SOSs
         * @param coeffs a list of coefficients for a filter, including gain as
         * the first value.  There should be (4*num_sos) + 1 values.
         * @param num_sos
         */
        void check_poles_list( double*             coeffs,
                               int                 num_sos,
                               const FilterModule& mod,
                               int                 section,
                               bool                correct_on_load );
    };

    // JCB - start
    // Filter section copy class.  Holds information of Filter Sections for
    // copy/paste operation. Since copy/paste needs to work across modules and
    // files, create data structures sufficient to hold the design data for the
    // section.
    /// \cond
    class SectCopy
    {
    public:
        /// Constructor - Given a pointer to a FilterSection, create a SectCopy
        /// class instance
        SectCopy( FilterSection* section );

        /// Access to index
        int
        GetIndex( )
        {
            return fIndex;
        };

        /// Access to design string
        const char*
        GetDesign( )
        {
            return ( fDesign.c_str( ) );
        };

        /// Access to name string.
        const char*
        GetName( )
        {
            return ( fName.c_str( ) );
        };

        /// Access to ramp, tolerance, timeout, input and output switch values.
        double
        GetRamp( )
        {
            return fRamp;
        };
        double
        GetTolerance( )
        {
            return fTolerance;
        };
        double
        GetTimeout( )
        {
            return fTimeout;
        };
        input_switching
        GetInputSw( )
        {
            return fInp_sw;
        };
        output_switching
        GetOutputSw( )
        {
            return fOut_sw;
        };

        /// Set the index of the copied section explicitly, overriding it's
        /// original value.
        void SetIndex( int index );

        /// Copy this section to the filter section.
        void PasteSection( FilterSection* section );

    private:
        // 0-9 or -1.  Holds original index of filter section in multi-selection
        // mode or -1 for single selection mode.
        int fIndex;

        // design string of original filter section.
        std::string fDesign;

        // name string of original filter section.
        std::string fName;

        // Input switch setting for the section.
        input_switching fInp_sw;

        // Output switch setting for the section.
        output_switching fOut_sw;

        // Variables to hold ramp, tolerance, and timeout values for the
        // section.
        double fRamp;

        double fTolerance;

        double fTimeout;
    };
    typedef std::vector< SectCopy > SectCopyList;
    extern SectCopyList             fSectCopyList; // Defined in FilterFile.cc

    /// \endcond

    // JCB - end

} // namespace filterwiz

#endif // _LIGO_FILTERIO_H
