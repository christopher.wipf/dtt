#include <cstdarg>
#include <algorithm>
#include <iostream>
#include <functional>

#include "FilterMessage.hh"

std::string
format( const char* format, ... )
{
    char    buffer[ 1024 ];
    va_list argp;

    // Textbook case of handling variable arguments being passed to another
    // function that takes variable arguments.
    va_start( argp, format );
    vsnprintf( buffer, sizeof( buffer ), format, argp );
    va_end( argp );

    return std::string( buffer );
}

std::string
message_level_to_string( message_level_t level )
{
    switch ( level )
    {
    case WARNING:
        return std::string( "WARNING" );
    case ERROR:
        return std::string( "ERROR" );
    default:
        return std::string( );
    }
}

message_t::message_t( message_level_t level, std::string text )
{
    this->has_line_number = false;

    this->level = level;
    this->text = text;
}

message_t::message_t( int line_number, message_level_t level, std::string text )
{
    this->has_line_number = true;
    this->line_number = line_number;

    this->level = level;
    this->text = text;
}

std::string
message_t::get_fmt_str( ) const
{
    char fmt_str[ 1024 ];

    if ( this->has_line_number )
    {
        snprintf( fmt_str,
                  sizeof( fmt_str ),
                  "Line %i: %s: %s",
                  this->line_number,
                  message_level_to_string( this->level ).c_str( ),
                  this->text.c_str( ) );
    }
    else
    {
        snprintf( fmt_str,
                  sizeof( fmt_str ),
                  "%s: %s",
                  message_level_to_string( this->level ).c_str( ),
                  this->text.c_str( ) );
    }

    return std::string( fmt_str );
}

std::vector< message_t >
message_vec_filter_level( std::vector< message_t > message_vec,
                          message_level_t          level )
{
    std::vector< message_t > filtered_message_vec;

    std::copy_if(
        message_vec.begin( ),
        message_vec.end( ),
        std::back_inserter( filtered_message_vec ),
        [ level ]( message_t m ) { return m.get_level( ) == level; } );

    return filtered_message_vec;
}

void
print_message_vec( std::vector< message_t > message_vec )
{
    auto print = []( const message_t& m ) {
        std::cout << m.get_fmt_str( ) << std::endl;
    };

    std::for_each( message_vec.cbegin( ), message_vec.cend( ), print );
}

std::vector< std::string >
message_vec_to_string_vec( std::vector< message_t > message_vec )
{
    std::vector< std::string > string_vec;

    std::transform( message_vec.begin( ),
                    message_vec.end( ),
                    std::back_inserter( string_vec ),
                    std::mem_fn( &message_t::get_fmt_str ) );

    return string_vec;
}
