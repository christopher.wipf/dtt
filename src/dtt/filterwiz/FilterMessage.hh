#ifndef _LIGO_FILTERMSG_H
#define _LIGO_FILTERMSG_H

#include <string>
#include <vector>

std::string format( const char*, ... );

enum message_level_t
{
    WARNING,
    ERROR
};

std::string message_level_to_string( );

class message_t
{
public:
    message_t( message_level_t, std::string );

    message_t( int, message_level_t, std::string );

    message_level_t
    get_level( )
    {
        return this->level;
    }

    std::string
    get_text( )
    {
        return this->text;
    }

    std::string get_fmt_str( ) const;

private:
    message_level_t level;
    std::string     text;

    bool has_line_number;
    int  line_number;
};

std::vector< message_t >
message_vec_filter_level( std::vector< message_t > message_vec,
                          message_level_t          level );

void print_message_vec( std::vector< message_t > message_vec );

std::vector< std::string >
message_vec_to_string_vec( std::vector< message_t > message_vec );

#endif
