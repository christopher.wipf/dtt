/* version $Id: FilterModule.cc 8034 2018-07-12 18:51:43Z john.zweizig@LIGO.ORG
 * $ */
#include <ctype.h>
#include <string.h>
#include <sstream>
#include <set>
#include "FilterFile.hh"
#include "IIRFilter.hh"
#include "iirutil.hh"
#include <string>
#include <iostream>
#include <cstdarg>
#include <iomanip>

using namespace std;

namespace filterwiz
{

    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    // FilterModule //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////
    FilterModule::FilterModule( ) : fSample( 1 )
    {
        // This simply adds kMaxFilterSections FilterSections to the
        // vector of FilterSections fSect.
        for ( int i = 0; i < kMaxFilterSections; ++i )
        {
            fSect.push_back( FilterSection( fSample, i ) );
        }
    }

    //______________________________________________________________________________
    FilterModule::FilterModule( const char* name, double fsample )
        : fName( name ? name : "" ), fSample( fsample )
    {
        // Add kMaxFilterSections FilterSections to the vector of
        // FilterSections fSect.
        for ( int i = 0; i < kMaxFilterSections; ++i )
        {
            fSect.push_back( FilterSection( fSample, i ) );
        }
    }

    FilterModule::~FilterModule( )
    {
    }

    /**
     * Return biquad() corrected filter
     * applies the same corrections libfilterfile applies
     * when writing or reading the second order sections from a file.
     * @param rate rate to create the filter.  Should be same rate as "filter"
     * @param filter the filter to correct.
     * @return a new IIRfilter with corrected second order sections.
     */
    IIRFilter
    biquad_corrected_filter( double rate, const Pipe& filter )
    {
        auto fd = FilterDesign( rate );
        auto iir = iir2iir( filter );
        auto sos_list = iir.getSOS( );
        for ( auto sos : sos_list )
        {
            fd.biquad( 1.0, sos.B1( ), sos.B2( ), sos.A1( ), sos.A2( ) );
        }
        fd.gain( iir.getGain( ) );
        return iir2iir( fd.get( ) );
    }

    void
    print_filt( IIRFilter& filt, const char* name )
    {
        fprintf( stderr, "%8s %6.0f gain = %0.17lf\n", name, filt.getFSample( ), filt.getGain() );
        int first = 1;
        for ( auto sos : filt.getSOS( ) )
        {
            fprintf( stderr,
                     "%s(%8.17f %8.17f %8.17f  %8.17f %8.17f);\n",
                     "\t\t\t",
                     sos.B0( ),
                     sos.B1( ),
                     sos.B2( ),
                     sos.A1( ),
                     sos.A2( ) );
            first = 0;
        }
    }

    /**
     * Check if a design string numerically reproduces a given filter exactly
     * @param design A filter design string.  Sample rate is assumed to be same
     * as for filt.
     * @param filt An iir filter
     * @return 1 if filter created by design is identical to filt, 0 otherwise
     */
    int
    design_equals_iir( const char* design, IIRFilter& filt )
    {
        FilterDesign fd( filt.getFSample( ) );
        fd.filter( design );
        IIRFilter iir_design = iir2iir( fd.get( ) );

        auto filt_sos = filt.getSOS( );
        auto design_sos = iir_design.getSOS( );

        fprintf(stderr, "design string = %s\n", design);
        print_filt(iir_design, "design");
        print_filt( filt, "filter");

        if ( filt_sos.size( ) != design_sos.size( ) )
        {
            return 0;
        }

        for ( int n = 0; n < filt_sos.size( ); ++n )
        {
            if ( filt_sos[ n ].B0( ) != design_sos[ n ].B0( ) )
            {
                return 0;
            }
            if ( filt_sos[ n ].B1( ) != design_sos[ n ].B1( ) )
            {
                return 0;
            }
            if ( filt_sos[ n ].B2( ) != design_sos[ n ].B2( ) )
            {
                return 0;
            }
            if ( filt_sos[ n ].A1( ) != design_sos[ n ].A1( ) )
            {
                return 0;
            }
            if ( filt_sos[ n ].A2( ) != design_sos[ n ].A2( ) )
            {
                return 0;
            }
        }
        return 1;
    }

    /**
     * The library function iir2z applies "corrections" when creating a design string to SOSs that
     * have low significant digits.   In the case of checkDesign, we want a string that's an exact match to the IIR,
     * not one that's in someway "better".
     * \brief Return a SOS design string that exactly matches the filter.
     * \param filt An IIR filter
     * \return
     */
    string get_exact_sos_string(IIRFilter& filt) {
        stringstream ds;

        ds << setprecision(17) << "sos(" << filt.getGain() << ", [";

        auto filt_sos = filt.getSOS();
        for(int n=0; n < filt_sos.size(); ++n) {
            if(n > 0) {
                ds << "; ";
            }
            ds << filt_sos[n].B1() << "; " << filt_sos[n].B2() << "; " << filt_sos[n].A1() << "; " << filt_sos[n].A2();

        }
        ds << "])";
        return ds.str();
    }

    const double grace_epsilon = 1E-10;

    /**
     *
     *
     * \brief Check if a zero is in the right-hand plane, but do it in the Z plane,
     * So we're really checking whether it's outside the unit circle.
     *
     * We allow for an epsilon because rounding can push zeros outside the circle by a tiny bit.
     *
     * We want to ignore those.
     *
     * \param zero
     * \return return true if the zero is far enough into the right-hand plane to be
     */
    bool
    check_zero_in_rhp_zplane(dComplex zero_z) {

        if(zero_z.Mag() <= 1 + grace_epsilon) {
            return false;
        }
        else {
            //printf("zmag=%lf\n", zero_z.Mag());
            return true;
        }
    }

    /**
     * In particular, returns default if the variable is not defined.
     * or if the truth value of the variable cannot be determined
     * a variable is true if it starts with any of 'T','t','1', and false
     * if it starts with any of 'F','f','0'.  Any other first character results
     * in an undetermined truth value, and default is returned.
     * \brief Check if an flag is set in the environment
     * \param var_name
     * \return
     */
    bool check_environment_flag(const char *var_name, bool def) {
        char *val = getenv(var_name);
        if(nullptr != val) {
            if(strchr("Tt1", val[0]) != nullptr) {
                return true;
            }
            if(strchr("Ff0", val[0]) != nullptr) {
                return false;
            }
        }
        return def;
    }

    //______________________________________________________________________________
    void
    FilterModule::checkDesign( std::vector< message_t >* message_vec )
    {
        int  split = 0;
        bool noDesign =
            false; // Flag to indicate that no design string was provided.

        // keep track of which sections have right-hand plane zeros
        set<int> rhp_zero_sections;


        for ( int i = kMaxFilterSections - 1; i >= 0; --i )
        {
            noDesign = false;

            if ( fSect[ i ].refDesign( ) == "split" )
            {
                ++split;
                continue;
            }
            // get filter from design string
            IIRFilter iir_design;
            // Create a new filter with the sample rate.
            FilterDesign design( fSample );
            // Make a filter from the design string, add it to the filter.
            // getDesign() gets the design string from the filter section.
            if ( fSect[ i ].getDesign( ) != (char*)NULL &&
                 fSect[ i ].getDesign( )[ 0 ] == '\0' )
            {
                noDesign = true;
                // errorMessage("Warning: Filter module %s section %d design not
                // specified. \n  A new design will be generated from
                // coefficients.", getName(), i) ; If there's no design and the
                // filter is marked as gain-only, we need to create a design
                // string anyway.
                if ( fSect[ i ].getGainOnly( ) )
                {
                    // A design string of the form zpk([],[],<gain>,"n") will
                    // do.
                    string s = "zpk([],[]," + fSect[ i ].getGainOnlyGain( ) +
                        ",\"n\")";
                    cerr << "No design string for gain only filter "
                         << fSect[ i ].getName( ) << ", creating one: " << s
                         << endl;

                    message_t msg(
                        WARNING,
                        format( "Module %s section %d: Missing design string, "
                                "a new string will be generated.",
                                getName( ),
                                i ) );
                    cerr << msg.get_fmt_str( ) << endl;
                    message_vec->push_back( msg );

                    fSect[ i ].setDesign( s );
                }
            }
            bool ok = design.filter( fSect[ i ].getDesign( ) );
            if ( ok )
            {
                try
                {
                    // design.get() gets the filter as a Pipe &.
                    // iir2iir() at it's simplest will cast the Pipe& to an
                    // IIRFilter and return.
                    iir_design = iir2iir( design.get( ) );
                }
                catch ( ... )
                {
                    ok = false;

                    message_t msg(
                        ERROR,
                        format( "Filter module %s section %d: Failed to create "
                                "filter from design string %s",
                                getName( ),
                                i,
                                fSect[ i ].getDesign( ) ) );
                    message_vec->push_back( msg );
                }
            }
            // get filter from coefficients
            IIRFilter iir_coeff;
            if ( ok )
            {
                for ( int j = i; j <= i + split; ++j )
                {
                    try
                    {
                        int       nzeros;
                        dComplex* zero;
                        int       npoles;
                        dComplex* pole;
                        double    gain;

                        IIRFilter iir = iir2iir( fSect[ j ].filter( ).get( ) );
                        iir_coeff *= iir;
                        int order = iirorder( iir );
                        zero = new dComplex[ order ];
                        pole = new dComplex[ order ];
                        iir2z( iir_coeff, nzeros, zero, npoles, pole, gain );

                        for ( int k = 0; k < nzeros; ++k )
                        {
                            //if ( zero[ k ].Real( ) > 0 )
                            if (check_zero_in_rhp_zplane(zero[k]))
                            {
                                rhp_zero_sections.insert(i);
                            }
                        }
                        delete[] zero;
                        delete[] pole;
                    }
                    catch ( ... )
                    {
                        ok = false;

                        message_t msg(
                            ERROR,
                            format(
                                "Filter module %s section %d: Failed to create "
                                "filter from coefficients.",
                                getName( ),
                                i ) );
                        message_vec->push_back( msg );
                        break;
                    }
                }
            }
            // now compare the two
            if ( ok )
            {
                // iircmp() is found in SignalProcessing/IIRFilter/iirutil.cc
                ok = iircmp( iir_design, iir_coeff );

                if ( !ok )
                {
                    if ( noDesign )
                    {
                        message_t msg( WARNING,
                                       format( "Module %s section %d: Missing "
                                               "design string, a "
                                               "new string will be generated.",
                                               getName( ),
                                               i ) );
                        message_vec->push_back( msg );
                    }
                    else
                    {
                        message_t msg(
                            ERROR,
                            format( "Module %s section %d: Mismatch "
                                    "between design and coefficients.",
                                    getName( ),
                                    i ) );
                        message_vec->push_back( msg );
                    }
                }
            }
            // if not ok, patch in some names
            if ( !ok )
            {
                cerr << "Filter module " << getName( ) << " section " << i
                     << ": mismatch between design and coefficients" << endl;
                for ( int j = i; j <= i + split; ++j )
                {
                    string cmd;
                    int    corrected = 0;
                    if ( iir2zpk( fSect[ j ].filter( ).get( ), cmd, "n" ) )
                    {
                        // try to make a design string from zeros, poles and
                        // gain but check if it really reproduces the filter
                        if ( design_equals_iir( cmd.c_str( ), iir_coeff ) )
                        {
                            fSect[ j ].setDesign( cmd.c_str( ) );
                        }
                        else if ( iir2z(
                                      fSect[ j ].filter( ).get( ), cmd, "s" ) )
                        {
                            // if we can't produce a zpk design string, make a
                            // second order section design string.  This should
                            // always work, but we check anyway.
                            cmd = get_exact_sos_string(iir_coeff);
                            if ( design_equals_iir( cmd.c_str( ), iir_coeff ) )
                            {
                                fSect[ j ].setDesign( cmd.c_str( ) );
                            }
                            else
                            {
                                // give up.  put in an emtpy design string
                                // Should also issue a warning!
                                fSect[ j ].setDesign( "" );
                                //fSect[ j ].filter( ).reset( );

                                message_t msg(
                                    ERROR,
                                    format( "Module %s section %d: Could not "
                                            "create an "
                                            "design string that produces the "
                                            "filter "
                                            "exactly.\n"
                                            "  The filter will be deleted.\n",
                                            getName( ),
                                            i ) );
                                message_vec->push_back( msg );
                            }
                        }
                    }
                }
            }
            split = 0;
        }

        if(rhp_zero_sections.size() > 0 && check_environment_flag("FOTON_WARN_RHP_ZEROS", false)) {
            stringstream msg_stream;
            msg_stream <<"Module "<< getName()
                   << " has one or more zeros in the "
                   << "right half s-plane ";
            if(rhp_zero_sections.size() > 1) {
                msg_stream << "in these sections: ";
            }
            else {
                msg_stream << "in this section: ";
            }
            bool first = true;
            for(int s: rhp_zero_sections) {
                if (!first) {
                    msg_stream << ", ";
                }
                msg_stream << s;
                first = false;
            }
            message_t msg = message_t( WARNING, msg_stream.str());
            cerr << msg.get_fmt_str( ) << endl;
            message_vec->push_back( msg );
        }
    }

    //______________________________________________________________________________
    void
    FilterModule::changeSampleRate( double                    newSample,
                                    std::vector< message_t >* message_vec )
    {
        int split = 0;

        for ( int i = kMaxFilterSections - 1; i >= 0; --i )
        {
            if ( fSect[ i ].refDesign( ) == "split" )
            {
                ++split;
                continue;
            }
            // get filter from design string
            IIRFilter iir_design;
            // Create a new filter with the new sample rate.
            FilterDesign design( newSample );

            // Make a filter from the design string, add it to the filter.
            // getDesign() gets the design string from the filter section.
            if ( fSect[ i ].getDesign( ) != (char*)NULL &&
                 fSect[ i ].getDesign( )[ 0 ] == '\0' )
            {
                // If there's no design and the filter is marked as gain-only,
                // we need to create a design string anyway.
                if ( fSect[ i ].getGainOnly( ) )
                {
                    // A design string of the form zpk([],[],<gain>,"n") will
                    // do.
                    string s = "zpk([],[]," + fSect[ i ].getGainOnlyGain( ) +
                        ",\"n\")";
                    cerr << "No design string for gain only filter "
                         << fSect[ i ].getName( ) << ", creating one: " << s
                         << endl;

                    message_t msg(
                        WARNING,
                        format( "Module %s section %d: Missing design string, "
                                "a new string will be generated.",
                                getName( ),
                                i ) );
                    message_vec->push_back( msg );

                    fSect[ i ].setDesign( s );
                }
            }
            bool ok = design.filter( fSect[ i ].getDesign( ) );
            if ( ok )
            {
                try
                {
                    // design.get() gets the filter as a Pipe&.
                    // iir2iir() at it's simplest will cast the Pipe& to an
                    //           IIRFilter and return.
                    iir_design = iir2iir( design.get( ) );
                }
                catch ( ... )
                {
                    ok = false;
                    message_t msg(
                        ERROR,
                        format( "Filter module %s section %d: Failed to create "
                                "filter from design string %s",
                                getName( ),
                                i,
                                fSect[ i ].getDesign( ) ) );
                    message_vec->push_back( msg );
                }
            }

            // Replace the old filter with the new filter.
            fSect[ i ].filter( ) = design;

            fSect[ i ].update( );
        }
    }

    //______________________________________________________________________________
    void
    FilterModule::setName( const char* p )
    {
        fName = p;
        string::size_type pos;
        while ( ( pos = fName.find_first_of( " \n\f\r\t\v" ) ) != string::npos )
        {
            fName.erase( pos, 1 );
        }
    }

    //______________________________________________________________________________
    void
    FilterModule::setFSample( double sample )
    {
        fSample = sample;
        for ( int i = 0; i < kMaxFilterSections; ++i )
        {
            fSect[ i ].filter( ).setFSample( fSample );
        }
    }

    //______________________________________________________________________________
    // This should be removed...
    void
    FilterModule::defaultFSample( void )
    {
        setFSample( 16384 );
    }

    //______________________________________________________________________________
    bool
    FilterModule::operator<( const FilterModule& b ) const
    {
        return ( strcmp( fName.c_str( ), b.getName( ) ) < 0 );
    }

    //______________________________________________________________________________
    bool
    FilterModule::operator==( const FilterModule& b ) const
    {
        return ( !strcmp( fName.c_str( ), b.getName( ) ) );
    }

    // JCB - start
    //______________________________________________________________________________
    // Copy the design data for each filter section in the module before paste.
    void
    FilterModule::SaveSections( )
    {
        // Clear out the old data.
        fRevertSect.clear( );

        // Copy each section, even if it's empty.
        for ( int i = 0; i < kMaxFilterSections; i++ )
        {
            fRevertSect.push_back( SectCopy( &( fSect[ i ] ) ) );
        }
    }

    //______________________________________________________________________________
    // Restore the saved design data for each filter section, used in case an
    // error was made in the paste operation (user error)
    // If successful, an Update (1, 1) should be called next to finish the job.
    bool
    FilterModule::RestoreSections( )
    {
        if ( fRevertSect.empty( ) )
        {
            return 0;
        }
        else
        {
            for ( int i = 0; i < kMaxFilterSections; i++ )
            {
                // Paste the section design data into the filter section.
                fRevertSect[ i ].PasteSection( &( fSect[ i ] ) );
            }
            // Undo has put the sections back, clear the vector.
            fRevertSect.clear( );
        }
        return 1;
    }

    //______________________________________________________________________________
    // Indicate if the saved design data set is empty.  Returns kTRUE if empty.
    bool
    FilterModule::RestoreSectionsEmpty( )
    {
        return fRevertSect.empty( );
    }

    // JCB - end
    //______________________________________________________________________________
} // namespace filterwiz
