/* version $Id: lidax.cc 6319 2010-09-17 18:06:52Z james.batch@LIGO.ORG $ */
/* -*- mode: c++; c-basic-offset: 4; -*- */
//////////////////////////////////////////////////////////////////////////
//                                                                      //
// lidax							        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "TLGLidax.hh"
#include "ldxtype.hh"
#include "ldxprog.hh"
#include "ldxreport.hh"
#include "XsilLidax.hh"
#include "dataacc.hh"
#include "goption.hh"
#include "fdstream.hh"
#include "pipe_exec.hh"
#include <string.h>
#include <strings.h>
#include <pwd.h>
#include <TROOT.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TGMsgBox.h>
#include <pthread.h>
#include <signal.h>
#include <cstdlib>

   using namespace std;
   using namespace gdsbase;
   using namespace lidax;
   using namespace dfm;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Constants       						        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   const char* const cmd_help = 
   "usage: lidax [-vp] [-rc 'file'] [-o 'dir' [-f 'format]]\n"
   "       lidax -h\n"
   "       lidax -H 'env'\n"
   "       -c 'file' : configuration file\n"
   "       -r : run directly from configuration file\n"
   "       -p : don't fork (useful for pipe)\n"
   "       -v : verbose\n"
   "       -o 'dir' : output is written into specified directory\n"
   "       -f 'format' : output frame format\n"
   "       -h : this help\n"
   "       -H 'env' : help for an environment variable";

   const char* const env_help = 
   "Environment variables:\n"
   "       LIGOSMPART : shared memory partition name\n"
   "       NDSSERVER  : list of NDS servers\n"
   "       UDNFILE    : list of configuration files with file/dir UDNs";

   const char* const ndsserver_help = 
   "The NDSSERVER environment variable lists a set of NDS addresses\n"
   "separated by commas.\n"
   "The server address is of the format: 'ip address'[:'port number']\n"
   "The port number is optional and defaults to 8088.\n"
   "Example:\n"
   "setenv NDSSERVER red.ligo-wa:8088,london.ligo-la\n";

   const char* const udnfile_help = 
   "The UDNFILE environment variable lists a set of configuration\n"
   "files separated by colons. A configuration file can contain\n"
   "comments (lines starting with #); all other lines are interpreted\n"
   "as a file and directory UDNs.\n"
   "Example:\n"
   "setenv UDNFILE /home/sigg/file1.udn:/home/sigg/file2.udn\n"
   "Example /home/sigg/file1.udn:\n"
   "# E2 data on fortress\n"
   "dir:///export/raid3/E2/00-11-08_19:59:35.@\n"
   "dir:///export/raid2/E2/00-11-09_09:17:47.@\n"
   "dir:///export/raid2/E2/00-11-12_17:10:19.@\n"
   "dir:///export/raid2/E2/00-11-14_00:25:08.@\n";

   const char* const udnsm_help = 
   "The LIGOSMPART environment variable lists the name of the default\n"
   "shared memory partition; whereas the LIGOSMPARTS variable\n"
   "lists a set of colon separeted shared memory partition names\n"
   "Example:\n"
   "setenv LIGOSMPART LHO_Online\n"
   "setenv LIGOSMPARTS sigg_1:sigg_2\n";



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Globals       						        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TROOT root ("LiDaX", "LIGO Data Access");
   dataaccess dacc (dataaccess::kSuppAll);
   const double epsilon = 1E-7;



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// progress_thread						        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
extern "C" 
   void* progress_thread (void* p)
   {
      new TLGLidaxProgress (gClient->GetRoot(), 0, *(LidaxProgress*)p);
      return 0;
   }

typedef std::vector<prog_exec*> prog_vect;
typedef prog_vect::iterator prog_iter;

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// main								        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
extern "C"
   int main (int argc, char** argv)
   {
      option_string opt (argc, argv, "vprc:o:f:hH:d:");
      // help
      if (opt.help()) {
         cout << cmd_help << endl;
         cout << env_help << endl;
         exit (0);
      }
      string env;
      if (opt.getOpt ('H', env)) {
         if (strncasecmp (env.c_str(), "NDSSERVER", 3) == 0) {
            cout << endl << ndsserver_help << endl;
         }
         else if (strncasecmp (env.c_str(), "UDNFILE", 3) == 0) {
            cout << endl << udnfile_help << endl;
         }
         else if (strncasecmp (env.c_str(), "LIGOSMPART", 3) == 0) {
            cout << endl << udnsm_help << endl;
         }
         else {
            cout << env_help << endl;
         }
         exit (0);
      }
   
      // silence output if not verbose
      int fd_std = 1; // preserve std output
      int fd_err = 2; // preserve std error
      if (!opt.opt ('v')) {
         fd_std = dup (1);
         fd_err = dup (2);
         int i = ::open ("/dev/null", 2);
         (void) dup2 (i, 1);
         (void) dup2 (i, 2);   
      }
      LidaxParam param (dacc);
      // set default e-mail address
      struct passwd pwd;
      struct passwd* ppwd;
      char nbuf[1024];
      if ((getpwuid_r (getuid(), &pwd, nbuf, sizeof (nbuf), &ppwd) == 0) && 
         (ppwd != 0)) {
         param.fEmail = ppwd->pw_name;
         param.fEmail += "@ligo.caltech.edu"; // works for me!
      }
   
      // check if runnning from configuration file
      bool runfromconf = false;
      string conffile;
      if (opt.getOpt ('c', conffile) && !conffile.empty()) {
         string error;
         if (!xsilRestoreLidaxFromFile (conffile.c_str(), 
                              param, error)) {
            fd_ostream err (fd_err);
            err << "Configuration error: " << error << endl;
            exit (1);
         }
         runfromconf = opt.opt ('r');
      }   
   
      // check output if output is defined on the command line
      string outdir;
      if (opt.getOpt ('o', outdir) && !outdir.empty()) {
         // setup UDN
         outdir.insert (0, "dir://");
         UDNList ulist;
         ulist.insert (UDNList::value_type (UDN (outdir.c_str()), UDNInfo()));
         dataservername dn (st_File, "");
         selserverentry out (dn, ulist);
         // set output format
         string format;
         if (opt.getOpt ('f', format)) {
            out.selectFormat (format.c_str());
         }
         // set destination selection
         dacc.dest().setMultiple (false);
         dacc.dest().selectS (out);
         // disable output selection and set log to std. output (if pipe)
         param.fOutEnable = false;
         param.fLogOn = opt.opt ('p');
         param.fLog = "";
      }
   
      // start application
      TApplication theApp ("LiDaX", &argc, argv);
      if (!runfromconf) {
         Int_t ret;
         new TLGLidaxDialog (gClient->GetRoot(), 0, param, ret);
         if (ret == kLidaxCancel) {
            exit (1);
         }
      }
   
      // fork if 'p' (pipe) is not set
      if (!opt.opt ('p')) {
         pid_t pid = fork();
         if (pid == -1) {
            if (runfromconf) {
               fd_ostream err (fd_err);
               err << "Fork error: Unable to start background process" << endl;
            }
            else {
               int retval;
               new TGMsgBox (gClient->GetRoot(), 0, "Error", 
                            "Unable to start background process", kMBIconStop, 
                            kMBOk, &retval);
            }
            exit (1);
         }
         // quit if parent
         else if (pid != 0) {
            exit (0);
         }
         // become session leader
         setsid ();
      }
   
      // set off request
      if (!dacc.request ()) {
         if (runfromconf) {
            fd_ostream err (fd_err);
            err << "Request error: Unable to get data" << endl;
         }
         else {
            int retval;
            new TGMsgBox (gClient->GetRoot(), 0, "Error", 
                         "Unable to get data.", kMBIconStop, kMBOk, &retval);
         }
         exit (1);
      }
   
      // create progress dialog box
      LidaxProgress progress;
      dacc.setAbort (progress.Cancel());
      if (param.fDlgProgress) {
         pthread_attr_t		tattr;
         int			status;
         // set thread parameters: detached & process scope
         if (pthread_attr_init (&tattr) != 0) {
            int retval;
            new TGMsgBox (gClient->GetRoot(), 0, "Error", 
                         "Unable to create update thread", 
                         kMBIconStop, kMBOk, &retval);
            exit(1);
         }
         pthread_attr_setdetachstate (&tattr, PTHREAD_CREATE_DETACHED);
         pthread_attr_setscope (&tattr, PTHREAD_SCOPE_PROCESS);
         // create thread
         pthread_t tid;
         status = pthread_create (&tid, &tattr, progress_thread, 
                              (void*) &progress);
         pthread_attr_destroy (&tattr);
      }
      // init log & web
      lidax_report* report = new lidax_report (param, progress, fd_std);
   
      // start monitor programs
      prog_vect savemonitors;
      if (!param.fMonitors.empty()) {
         for (const_monitor_iter i = param.fMonitors.begin();
             i != param.fMonitors.end(); ++i) {
            // command
            string cmd = i->fName + " " + i->fArgs;
            // substitue $part with partition name
            if (i->fUDN.find ("dmt://") == 0) {
               string part (i->fUDN, 6, i->fUDN.size() - 6);
               if (!part.empty() && (part[0] == '/')) {
                  part.erase (0, 1);
               }
               string::size_type pos = part.find_first_of (" \f\r\n\t\v");
               if (pos != string::npos) {
                  part.erase (pos);
               }
               if (part.empty()) {
                  continue;
               }
               while ((pos = cmd.find ("$part")) != string::npos) {
                  cmd.erase (pos, 5);
                  cmd.insert (pos, part);
               }
            }
            // execute command
            prog_exec* prog = new (nothrow) prog_exec (cmd.c_str());
            if (!prog || !*prog) {
               int retval;
               string errmsg = "Unable start " + cmd + ".\nCancel?";
               new TGMsgBox (gClient->GetRoot(), 0, "Warning", errmsg.c_str(), 
                            kMBIconQuestion, kMBYes | kMBNo, &retval);
               if (retval == kMBYes) {
                  exit (1);
               }
            }
            else {
               savemonitors.push_back (prog);
            }
         }
      }
   
      // start processing frames
      Time T0 = dacc.sel().selectedTime();
      Interval dT = dacc.sel().selectedDuration();
      Time T1 = dacc.sel().selectedStop() - Interval (epsilon);
      Time t;
      while (!progress.IsCancel() && ((t = dacc.processTime()) < T1)) {
         if ((double)dT <= 0) {
            break;
         }
         // set progress report
         Interval d = (t > T0) ? t - T0 : Interval (0.0);
         progress.SetCompletion ((double)d / (double)dT);
         char tbuf[256];
         TimeStr (t, tbuf, "%Y %M %d, %H:%N:%S");
         char buf[256];
         sprintf (buf, "Working on frame %li (%s)", t.getS(), tbuf);
         progress.SetMsg (buf);
         // process next frame
         if (dacc.process() <= Interval (0.)) {
            progress.SetCancel();
            break; // incomplete!
         }
         // update log & web
         if (report) report->update ();
      }
      if (progress.IsCancel()) {
         param.fDacc->abort();
      }
      else {
         param.fDacc->flush();
      }
      // finish log & web
      timespec wait = {5, 0};
      nanosleep (&wait, 0);
      delete report;
   
      // cleanup 
      progress.SetCancel();
      timespec wait2 = {2, 0};
      nanosleep (&wait2, 0);
      if (param.fDMTKill && !savemonitors.empty()) {
         // send Ctrl-C first
         for (prog_iter i=savemonitors.begin(); i != savemonitors.end(); ++i) {
            (*i)->kill (SIGINT);
         }
         // wait a little while, then kill
         nanosleep (&wait2, 0);
         for (prog_iter i=savemonitors.begin(); i != savemonitors.end(); ++i) {
            delete *i; *i = 0;
         }
      }
   
      // exit
      //cerr << "exit" << endl;
      exit (0);
      return 0;
   }
