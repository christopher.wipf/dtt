/* version $Id: ldxtype.hh 6319 2010-09-17 18:06:52Z james.batch@LIGO.ORG $ */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: ldxtype							*/
/*                                                         		*/
/* Module Description: Types used by lidax				*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 9Jan01   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: dfmxml.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_LDXTYPE_H
#define _LIGO_LDXTYPE_H

#include <string>
#include "dataacc.hh"
#include "montype.hh"


namespace lidax {


/** @name Lidax Types 
    This header defines some common types used by lidax.
   
    @memo Lidax Types
    @author Written April 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{


/** LIGO data access parameters.
    @memo Lidax parameters.
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   struct LidaxParam {
      /// Pointer to data access object
      dfm::dataaccess* 	fDacc;
      /// Output selection enabled?
      bool		fOutEnable;
      /// DMT monitor selection enabled?
      bool		fDMTEnable;
      /// Kill DMT monitor after completion?
      bool		fDMTKill;
      /// DMT monitor list
      dfm::MonitorList	fMonitors;
      /// Log file active?
      bool		fLogOn;
      /// Log file
      std::string	fLog;
      /// Web site active?
      bool		fWebOn;
      /// Web file
      std::string	fWeb;
      /// Email active?
      bool		fEmailOn;
      /// Email address
      std::string	fEmail;
      /// Progress window active?
      bool		fDlgProgress;
   
      /// Create a lidax parameter object
      LidaxParam (dfm::dataaccess& acc) 
      : fDacc (&acc), fOutEnable (true), fDMTEnable (true), 
      fDMTKill (true), fLogOn (false), fLog ("lidax.log"), 
      fWebOn (false), fWeb ("lidax.html"), 
      fEmailOn (false), fDlgProgress (true) {
      }
   };


//@}

}

#endif // _LIGO_LDXTYPE_H
