#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;
#pragma link C++ nestedtypedefs;

#pragma link C++ class lidax::LidaxParam;
#pragma link C++ class lidax::TLGLidaxDialog;
#pragma link C++ class lidax::xsilHandlerLidax;
#pragma link C++ class lidax::xsilHandlerQueryLidax;
#pragma link C++ class lidax::LidaxProgress;
#pragma link C++ class lidax::TLGLidaxProgress;
#pragma link C++ class lidax::lidax_report;
#pragma link C++ class lidax;

#endif

