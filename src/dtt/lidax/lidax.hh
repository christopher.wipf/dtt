/* version $Id: lidax.hh 6319 2010-09-17 18:06:52Z james.batch@LIGO.ORG $ */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: lidax							*/
/*                                                         		*/
/* Module Description: LIGO Data Access					*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 9Jan01   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: lidax.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_LIDAX_H
#define _LIGO_LIDAX_H


/** @name LIGO Data Access (LiDaX) 
    LIGO Data Access program
    \begin{verbatim}
    usage: lidax
    \end{verbatim}
    Starts the LIGO data access program which can retrieve frame data 
    from disk, tape, DMT shared memory partition, NDS and the archive.
    Further selections of universal data set name, channel names,
    time selection and staging can be applied to the input frames.
    The frames can either be stored on disk or sent to a DMT monitor
    process.

    @memo Accessing LIGO frame data
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/


#endif // _LIGO_LIDAX_H



