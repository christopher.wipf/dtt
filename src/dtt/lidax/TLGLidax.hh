/* version $Id: TLGLidax.hh 7389 2015-06-11 18:51:56Z james.batch@LIGO.ORG $ */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: TLGLidax						*/
/*                                                         		*/
/* Module Description: LIGO Data Access					*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 9Jan01   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: lidax.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_TLGLIDAX_H
#define _LIGO_TLGLIDAX_H


#include <TGFrame.h>
#include <TGLabel.h>
#include <TGTextEntry.h>
#include <TGButton.h>
#include <TTimer.h>
#include <TGFrame.h>


namespace dfm {
   class TLGDfmSelection;
}

namespace lidax {

   struct LidaxParam;


/** @name LIGO Data Access GUI 

    @memo LIGO Data Access GUI
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{

   const Int_t kLidaxCancel = 0;
   const Int_t kLidaxOk = 1;
   const Int_t kLidaxMonitors = 5;
   const Int_t kLidaxClear = 10;
   const Int_t kLidaxStore = 11;
   const Int_t kLidaxRestore = 12;
   const Int_t kLidaxAbout = 9;
   const Int_t kLidaxSource = 100;
   const Int_t kLidaxDestination = 110;
   const Int_t kLidaxLogSel = 150;
   const Int_t kLidaxWebSel = 151;
   const Int_t kLidaxDlgSel = 152;
   const Int_t kLidaxLogfile = 153;
   const Int_t kLidaxLogfileChoose = 154;
   const Int_t kLidaxWebfile = 155;
   const Int_t kLidaxWebfileChoose = 156;
   const Int_t kLidaxEmailSel = 157;
   const Int_t kLidaxEmail = 158;



/** LIGO data access main window.
    @memo Lidax main window.
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGLidaxDialog : public TGTransientFrame {
   protected:
      /// Return argument (1 = run, 0 = cancel)
      Int_t*		fOk;
      /// Return parameters
      LidaxParam*	fParam;
      /// X11 watchdog
      TTimer*		fX11Watchdog;
   
      /// Numereous layout hints
      TGLayoutHints*	fL[9];
      /// Frames
      TGCompositeFrame*	fF[3];
      /// Source group
      dfm::TLGDfmSelection*	fSource;
      /// Destination group
      dfm::TLGDfmSelection*	fDestination;
      /// Progress group
      TGGroupFrame*	fProg;
      /// Log file selection
      TGButton*		fLogFileSel;
      /// Log file name
      TGTextEntry*	fLogFile;
      /// Log file choose
      TGButton*		fLogFileChoose;
      /// Web page selection
      TGButton*		fWebPageSel;
      /// Web page name
      TGTextEntry*	fWebPage;
      /// Web page choose
      TGButton*		fWebPageChoose;
      /// e-mail selection
      TGButton*		fEmailSel;
      /// e-mail address
      TGTextEntry*	fEmail;
      /// dialog selection
      TGButton*		fDialogSel;
      /// Labels
      TGLabel*		fLabel[3];
      /// DMT configure
      TGButton*		fDMTConfig;
      /// Clear button
      TGButton*		fClearButton;
      /// Store button
      TGButton*		fStoreButton;
      /// Restore button
      TGButton*		fRestoreButton;
      /// About button
      TGButton*		fAboutButton;
      /// Monitors button
      TGButton*		fMonitorButton;
      /// Ok button
      TGButton*		fOkButton;
      /// Cancel button
      TGButton*		fCancelButton;
   
      /// Set values
      void SetValues ();
      /// Get values
      Bool_t GetValues ();
   
   public:
      /// Constructor
      TLGLidaxDialog (const TGWindow* p, const TGWindow* main,
                     LidaxParam& prm, Int_t& ret);
      /// Destructor
      virtual ~TLGLidaxDialog ();
      /// Close window
      virtual void CloseWindow();
      /// Handle Timer events
      virtual Bool_t HandleTimer (TTimer* timer);
      /// Process messages
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   };

//@}

}

#endif // _LIGO_TLGLIDAX_H



