/* Version $Id$ */
#ifndef ROOT_TLGComboTree
#define ROOT_TLGComboTree

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGComboTree, TLGComboTreePopup                                      //
//                                                                      //
// A combobox (also known as a drop down listbox) allows the selection  //
// of one item out of a list of items. The selected item is visible in  //
// a little window. To view the list of possible items one has to click //
// on a button on the right of the little window. This will drop down   //
// a listbox. After selecting an item from the listbox the box will     //
// disappear and the newly selected item will be shown in the little    //
// window.                                                              //
//                                                                      //
// The TLGComboTree is user callable. The TLGComboTreePopup is a service//
// class of the combobox.                                               //
//                                                                      //
// Selecting an item in the combobox will generate the event:           //
// kC_COMMAND, kCM_COMBOBOX, combobox id, item id.                      //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include <TGListBox.h>
#include <TGClient.h>
#include <TVirtualX.h>
#include "TLGLBTree.hh"

#define TEST_LOCALRATE 1
#include <iostream> // For Debugging JCB

   class TGScrollBarElement;

namespace dttgui {


   class TLGComboTree;


/** @name TLGComboTree
    This header exports a combobox which organizes its entries in a 
    tree.

    Selecting an item in the combobox will generate the event:
    \begin{verbatim}
    kC_COMMAND, kCM_COMBOBOX, combobox id, item id.
    \end{verbatim}
    Changing text in the text entry widget will generate the event:      
    \begin{verbatim}
    kC_TEXTENTRY, kTE_TEXTCHANGED, combobox id, 0.                         
    \end{verbatim}
    Hitting the enter key in the text entry widget will generate:                                 
    \begin{verbatim}
    kC_TEXTENTRY, kTE_ENTER, combobox id, 0.                    
    \end{verbatim}

    @memo Tree combobox
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{


/** A helper class which manages the popup window of a combobox.
    For internal use only.
    @memo Tree combobox popup.
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGComboTreePopup : public TGCompositeFrame {
   
      //friend class TGClient;
   
   protected:
      /// Default cursor style
      static Cursor_t  fgDefaultCursor;
      /// Parent combobox
      TLGComboTree     *fTreebox;
   
   public:
      /// Create the popup
#if ROOT_VERSION_CODE > 197893
      TLGComboTreePopup (const TGWindow *p, TLGComboTree* combo,
                        UInt_t w, UInt_t h,
                        UInt_t options = kVerticalFrame,
                        ULong_t back = GetWhitePixel());
#else
      TLGComboTreePopup (const TGWindow *p, TLGComboTree* combo,
                        UInt_t w, UInt_t h,
                        UInt_t options = kVerticalFrame,
                        ULong_t back = fgWhitePixel);
#endif
   
      /// Handle button events
      virtual Bool_t HandleButton(Event_t *);
      /// Place the popup
      void PlacePopup(Int_t x, Int_t y, UInt_t w, UInt_t h);
      /// Dismiss the popup
      void EndPopup();
   
   //ClassDef(TLGComboTreePopup,0)  // Combobox popup window
   };


/** A tree combobox. This combobox variant presents a tree selection
    choice in its popup listbox.
    @memo Tree combobox.
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGComboTree : public TGCompositeFrame {
   
      //friend class TGClient;
      friend class TLGComboTreePopup;
   
   protected:
      /// Selcteded entry is editable?
      Bool_t		   fEditable;
      /// active state
      Bool_t		   fActive;
      /// combobox widget id
      Int_t                fComboBoxId;
      /// height of popup window
      UInt_t               fPopupHeight;
      /// selected item frame
      TGFrame   	  *fSelEntry;
      /// button controlling drop down of popup 
      TGScrollBarElement  *fDDButton;
      /// button controlling drop down of popup
      TLGComboTreePopup   *fComboFrame; 
      /// the tree listbox with text items
      TLGLBTree           *fListBox;    
      /// down arrow picture used in fDDButton
      const TGPicture     *fBpic;          
      /// window handling combobox messages
      const TGWindow      *fMsgWindow;     
      /// layout hints for selected item frame
      TGLayoutHints       *fLhs;           
      /// layout hints for fDDButton
      TGLayoutHints       *fLhb;           
      /// layout hints for fListBox
      TGLayoutHints       *fLhdd;
   
      /// Get text of selected entry
      virtual const char* SelGetText() const;
      /// Set text of selected entry
      virtual void SelSetText (const char* txt);
   
   public:
      /// Ceeate a tree combobox
#if ROOT_VERSION_CODE > 197893
      TLGComboTree(const TGWindow *p, Int_t id, Bool_t editable,
                  UInt_t options = kHorizontalFrame | kSunkenFrame | kDoubleBorder,
                  ULong_t back = GetWhitePixel());
#else
      TLGComboTree(const TGWindow *p, Int_t id, Bool_t editable,
                  UInt_t options = kHorizontalFrame | kSunkenFrame | kDoubleBorder,
                  ULong_t back = fgWhitePixel);
#endif
      /// Destroy the tree combobox
      virtual ~TLGComboTree();
   
      /// Draw border
      virtual void DrawBorder();
      /// Get default size
      virtual TGDimension GetDefaultSize() const { 
         return TGDimension (fWidth, fHeight); }
      /// Set the height of the popup window
      virtual void SetPopupHeight (UInt_t h) {
         fPopupHeight = h;
      }
      /// Assiciate
      virtual void Associate(const TGWindow *w) { 
         fMsgWindow = w; }
      /// Handle button events
      virtual Bool_t HandleButton(Event_t *event);
      /// Process messages
      virtual Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2);
   
      /// Add item
      TLGLBTreeEntry *AddItem(TLGLBTreeEntry *parent, const char *string,
                        const TGPicture *open = 0,
                        const TGPicture *closed = 0) {
         return fListBox->AddItem (parent, string, open, closed); }
      /// Add item
      TLGLBTreeEntry *AddItem(TLGLBTreeEntry *parent, const char *string,
                        void *userData, const TGPicture *open = 0,
                        const TGPicture *closed = 0) {
         return fListBox->AddItem (parent, string, userData, open, closed); }
      /// Add item
      TLGLBTreeEntry *AddItem(TLGLBTreeEntry *parent, const char *string,
                        const char* fullname, const TGPicture *open = 0,
                        const TGPicture *closed = 0) {
         return fListBox->AddItem (parent, string, fullname, open, closed); }
      /// Add item
      TLGLBTreeEntry *AddItem(TLGLBTreeEntry *parent, const char *string,
                        const char* fullname, void *userData, 
                        const TGPicture *open = 0, 
                        const TGPicture *closed = 0) {
         return fListBox->AddItem (parent, string, fullname, 
                              userData, open, closed); }
      /// Rename item
      void  RenameItem(TLGLBTreeEntry *item, const char *string) {
         fListBox->RenameItem (item, string); }
      ///  Delete item
      Int_t DeleteItem(TLGLBTreeEntry *item) {
         return fListBox->DeleteItem (item); }
      /// Delete items recursivly
      Int_t RecursiveDeleteItem(TLGLBTreeEntry *item, void *userData) {
         return fListBox->RecursiveDeleteItem(item, userData); }
      /// Delete all children
      Int_t DeleteChildren(TLGLBTreeEntry *item) {
         return fListBox->DeleteChildren (item); }
      /// Reparanet
      Int_t Reparent(TLGLBTreeEntry *item, TLGLBTreeEntry *newparent) {
         return fListBox->Reparent (item, newparent); }
      /// Reparent children
      Int_t ReparentChildren(TLGLBTreeEntry *item, TLGLBTreeEntry *newparent) {
         return fListBox->ReparentChildren (item, newparent); }
   
      /// Sort items
      Int_t Sort(TLGLBTreeEntry *item) {
         return fListBox->Sort (item); }
      /// Sort siblings
      Int_t SortSiblings(TLGLBTreeEntry *item) {
         return fListBox->SortSiblings (item); }
      /// Sort children
      Int_t SortChildren(TLGLBTreeEntry *item) {
         return fListBox->SortChildren (item); }
   
      /// Get the popup listbox
      virtual const TLGLBTree *GetLBTree() const { 
         return fListBox; }
      /// Select an item
      virtual Bool_t Select (void* userData);
      /// Select an item
      virtual Bool_t SelectByName (const char* fullname, const int rate = -1);
      /** Set by name. This will force the name to be displayed even 
          if not in list. Returns true if name in list */
      virtual Bool_t SetByName (const char* fullname, const int rate = -1);
      /** Get by name. This will read the displayed name even 
          if not in list. Returns true if name in list */
      virtual const char* GetDisplayedName () const;
      /// Get the selected item
      virtual void* GetSelected() const { 
	 //std::cerr << "TLGComboTree::GetSelected() returns " << (long) (fListBox->GetSelected()) << std::endl ;
         return fListBox->GetSelected(); }

      virtual void *GetSelectedUserData() {
	 return fListBox->GetSelectedUserData(); }

      virtual void SetSelectedUserData(void *userdata) {
	 //std::cerr << " SetSelectedUserData(" << (long) userdata << ")" << std::endl ;
	 fListBox->SetSelectedUserData(userdata) ; // calls TLGLBTree::SetSelectedUserData()
      }

      /// Get the selected item
      virtual const char* GetSelectedName() const { 
         return fListBox->GetSelectedName(); }
      /// Get the selected item
      virtual TLGLBTreeEntry *GetSelectedEntry() const { 
         return fListBox->GetSelectedEntry(); }
      /// Check if selected
      virtual Bool_t GetSelection (void *userData) const {
         return fListBox->GetSelection (userData); }
      /// Check if selected
      virtual Bool_t GetSelectionByName (const char* fullname) const {
         return fListBox->GetSelectionByName (fullname); }
   
      // Set state
      virtual void SetState (Bool_t active) {
         fActive = active; fComboFrame->EndPopup();
      }
      // Get state
      virtual Bool_t GetState () const {
         return fActive; }
   
#ifdef TEST_LOCALRATE
      void SetRate(long rate) {
	 fRate = rate ;
      }
      long GetRate() {
	 return fRate; 
      }
   protected:
      long fRate ;
#endif

   //ClassDef(TLGComboTree,0)  // Combo box widget
   };


//@}
}

#endif
