/* Version $Id: TLGMainMenu.cc 6422 2011-04-20 01:05:01Z james.batch@LIGO.ORG $ */
/*---------------------------------------------------------------------------*/
/*                                                                           */
/* Module Name:  TLGMainMenu						     */
/*                                                                           */
/* Module Description:  main menu of diagnostics viewer			     */
/*                                                                           */
/*---------------------------------------------------------------------------*/
#include "PConfig.h"
#include "TLGMainMenu.hh"
#include "TLGPad.hh"
#include "TLGPrint.hh"
#include "TGMsgBox.h"


namespace dttgui {
   using namespace std;

   const char* const aboutmsg = 
   "LIGO))) Laser Interferometer Gravitational-wave Observatory\n"
   "Global Diagnostics System\n\n"
   "by Daniel Sigg et al., 1998 - 2009\n"
   "version " CDS_VERSION "\n\n"
   "http://www.ligo.caltech.edu\n"
   "http://git.ligo.org/cds/dtt";


//______________________________________________________________________________
   TLGMainMenu::TLGMainMenu ()
   {
      fParent = 0;
      fMultiPad = 0;
      fFileSaveFlag = kSaveRestoreStandard;
      fFileRestoreFlag = kSaveRestoreAll;
      fSettingsSaveFlag = kTRUE; 
      fSettingsRestoreFlag = kTRUE;
      fCalibrationSaveFlag = kTRUE; 
      fCalibrationRestoreFlag = kTRUE;
      fMenuFileFlag[0] = 0;
      fMenuFileFlag[1] = 0;
      fMenuFilePrintGraph = 0;
      fMenuFile = 0;
      fMenuEdit = 0;
      fMenuPlot = 0;
      fMenuWindow = 0;
      fMenuWindowZoom = 0;
      fMenuWindowActive = 0;
      fMenuHelp = 0;
      fMenuBarLayout = 0;
      fMenuBarItemLayout = 0;
      fMenuBarHelpLayout = 0;
      fMenuBar = 0;
   }

//______________________________________________________________________________
   TLGMainMenu::~TLGMainMenu ()
   {
      delete fMenuFileFlag[0];
      delete fMenuFileFlag[1];
      delete fMenuFilePrintGraph;
      delete fMenuFile;
      delete fMenuEdit;
      delete fMenuPlot;
      delete fMenuWindow;
      delete fMenuWindowZoom;
      delete fMenuWindowActive;
      delete fMenuHelp;
      delete fMenuBarLayout;
      delete fMenuBarItemLayout;
      delete fMenuBarHelpLayout;
      delete fMenuBar;
   }

//______________________________________________________________________________
   void TLGMainMenu::MenuSetup (TGCompositeFrame* frame, 
                     TLGMultiPad* mpad)
   {
      fParent = frame;
      fMultiPad = mpad;
      if (frame != 0) {
         fMenuBarLayout = 
            new TGLayoutHints (kLHintsTop | kLHintsLeft | kLHintsExpandX,
                              0, 0, 1, 1);
         fMenuBarItemLayout = 
            new TGLayoutHints (kLHintsTop | kLHintsLeft, 0, 4, 0, 0);
         fMenuBarHelpLayout = 
            new TGLayoutHints (kLHintsTop | kLHintsRight);
         fMenuBar = new TGMenuBar (fParent, 1, 1, kHorizontalFrame | kRaisedFrame);
         fParent->AddFrame (fMenuBar, fMenuBarLayout);
         AddMenuFile();
         AddMenuEdit();
         AddMenuPlot();
         AddMenuWindow();
         AddMenuHelp();
      }
   }

//______________________________________________________________________________
   void TLGMainMenu::AddMenuFile()
   {
      if (!fMenuFile) {
         fMenuFile = new TGPopupMenu (gClient->GetRoot());
         fMenuFile->Associate (fParent);
      }
      fMenuFile->AddEntry ("&New...", kM_FILE_NEW);
      fMenuFile->AddEntry ("&Open...", kM_FILE_OPEN);
      fMenuFile->AddSeparator();
      fMenuFile->AddEntry ("&Save", kM_FILE_SAVE);
      fMenuFile->AddEntry ("Save &As...", kM_FILE_SAVEAS);
      fMenuFile->AddEntry ("&Import...", kM_FILE_IMPORT);
      fMenuFile->AddEntry ("&Export...", kM_FILE_EXPORT);
      fMenuFile->AddSeparator();
      fMenuFileFlag[0] = new TGPopupMenu (gClient->GetRoot());
      fMenuFileFlag[0]->AddEntry ("Everything (include raw time series)", 
                           kM_FILE_RFLAG_ALL);
      fMenuFileFlag[0]->AddEntry ("Results and Parameters", kM_FILE_RFLAG_STD);
      fMenuFileFlag[0]->AddEntry ("Parameters only", kM_FILE_RFLAG_PRM);
      fMenuFileFlag[0]->AddSeparator ();
      fMenuFileFlag[0]->AddEntry ("Plot settings", kM_FILE_RFLAG_SET);
      fMenuFileFlag[0]->AddSeparator ();
      fMenuFileFlag[0]->AddEntry ("Calibration data", kM_FILE_RFLAG_CAL);
      fMenuFile->AddPopup ("Restore Flag", fMenuFileFlag[0]);
      fMenuFileFlag[0]->CheckEntry (kM_FILE_RFLAG_ALL);
      fMenuFileFlag[0]->CheckEntry (kM_FILE_RFLAG_SET);
      fMenuFileFlag[0]->CheckEntry (kM_FILE_RFLAG_CAL);
      fMenuFileFlag[1] = new TGPopupMenu (gClient->GetRoot());
      fMenuFileFlag[1]->AddEntry ("Everything (include raw time series)", 
                           kM_FILE_SFLAG_ALL);
      fMenuFileFlag[1]->AddEntry ("Results and Parameters", kM_FILE_SFLAG_STD);
      fMenuFileFlag[1]->AddEntry ("Parameters only", kM_FILE_SFLAG_PRM);
      fMenuFileFlag[1]->AddSeparator ();
      fMenuFileFlag[1]->AddEntry ("Plot settings", kM_FILE_SFLAG_SET);
      fMenuFileFlag[1]->AddSeparator ();
      fMenuFileFlag[1]->AddEntry ("Calibration data", kM_FILE_SFLAG_CAL);
      fMenuFile->AddPopup ("Save Flag", fMenuFileFlag[1]);
      fMenuFileFlag[1]->CheckEntry (kM_FILE_SFLAG_STD);
      fMenuFileFlag[1]->CheckEntry (kM_FILE_SFLAG_SET);
      fMenuFileFlag[1]->CheckEntry (kM_FILE_SFLAG_CAL);
      fMenuFile->AddSeparator();
      fMenuFile->AddEntry ("&Print...", kM_FILE_PRINT);
      fMenuFile->AddEntry ("P&rint Setup...", kM_FILE_PRINTSETUP);
      fMenuFilePrintGraph = new TGPopupMenu (gClient->GetRoot());
      fMenuFilePrintGraph->AddEntry ("A", kM_FILE_PRINT_GRAPHA);
      fMenuFilePrintGraph->AddEntry ("B", kM_FILE_PRINT_GRAPHB);
      fMenuFile->AddPopup ("Print &Graph", fMenuFilePrintGraph);
      fMenuFile->AddSeparator();
      fMenuFile->AddEntry ("E&xit", kM_FILE_EXIT);
      fMenuBar->AddPopup ("&File", fMenuFile, fMenuBarItemLayout);
   }

//______________________________________________________________________________
   void TLGMainMenu::AddMenuEdit()
   {
   }

//______________________________________________________________________________
   void TLGMainMenu::AddMenuPlot()
   {
      if (!fMenuPlot) {
         fMenuPlot = new TGPopupMenu (gClient->GetRoot());
         fMenuPlot->Associate (fParent);
      }
      fMenuPlot->AddEntry ("Re&ference...", kM_PLOT_REFERENCE);
//      fMenuPlot->AddEntry ("&Math...", kM_PLOT_MATH);
      fMenuPlot->AddSeparator();
      fMenuPlot->AddEntry ("Calibration &Edit...", kM_PLOT_CALIBRATION_EDIT);
      fMenuPlot->AddEntry ("Calibration &Read...", kM_PLOT_CALIBRATION_READ);
      fMenuPlot->AddEntry ("Calibration &Write...", kM_PLOT_CALIBRATION_WRITE);
      fMenuBar->AddPopup ("&Plot", fMenuPlot, fMenuBarItemLayout);
   }

//______________________________________________________________________________
   void TLGMainMenu::AddMenuWindow()
   {
      if (!fMenuWindow) {
         fMenuWindow = new TGPopupMenu (gClient->GetRoot());
         fMenuWindow->Associate (fParent);
      }
      fMenuWindow->AddEntry ("&New", kM_WINDOW_NEW);
      fMenuWindow->AddSeparator();
      fMenuWindowZoom = new TGPopupMenu (gClient->GetRoot());
      fMenuWindowZoom->AddEntry ("Out", kM_WINDOW_ZOOkM_OUT);
      fMenuWindowZoom->AddEntry ("Current", kM_WINDOW_ZOOkM_CUR);
      fMenuWindowZoom->AddEntry ("A", kM_WINDOW_ZOOkM_A);
      fMenuWindowZoom->AddEntry ("B", kM_WINDOW_ZOOkM_B);
      fMenuWindow->AddPopup ("&Zoom", fMenuWindowZoom);
      fMenuWindowActive = new TGPopupMenu (gClient->GetRoot());
      fMenuWindowActive->AddEntry ("Next", kM_WINDOW_ACTIVE_NEXT);
      fMenuWindowActive->AddEntry ("A", kM_WINDOW_ACTIVE_A);
      fMenuWindowActive->AddEntry ("B", kM_WINDOW_ACTIVE_B);
      fMenuWindow->AddPopup ("&Active", fMenuWindowActive);
      fMenuWindow->AddSeparator();
      fMenuWindow->AddEntry ("&Layout...", kM_WINDOW_LAYOUT);
      fMenuBar->AddPopup ("&Window", fMenuWindow, fMenuBarItemLayout);
   }

//______________________________________________________________________________
   void TLGMainMenu::AddMenuHelp()
   {
      if (!fMenuHelp) {
         fMenuHelp = new TGPopupMenu(gClient->GetRoot());
         fMenuHelp->Associate (fParent);
      }
      //fMenuHelp->AddEntry("&Contents", kM_HELP_CONTENTS);
      //fMenuHelp->AddEntry("&Search...", kM_HELP_SEARCH);
      fMenuHelp->AddSeparator();
      fMenuHelp->AddEntry("&About", kM_HELP_ABOUT);
      fMenuBar->AddPopup ("&Help", fMenuHelp, fMenuBarHelpLayout);
   }

//______________________________________________________________________________
   Bool_t TLGMainMenu::FileExport()
   {
      if (fMultiPad) {
         return fMultiPad->ExportDlg();
      }
      else {
         return kFALSE;
      }
   }

//______________________________________________________________________________
   Bool_t TLGMainMenu::FileImport()
   {
      if (fMultiPad) {
         return fMultiPad->ImportDlg();
      }
      else {
         return kFALSE;
      }
   }

//______________________________________________________________________________
   Bool_t TLGMainMenu::ProcessMenu (Long_t parm1, Long_t parm2)
   {
      switch (parm1) {
         case kM_FILE_NEW:
            {
               return FileNew();
            }
         case kM_FILE_OPEN:
            {
               return FileOpen();
            }
         case kM_FILE_SAVE:
            {
               return FileSave();
            }
         case kM_FILE_SAVEAS:
            {
               return FileSaveAs();
            }
         case kM_FILE_IMPORT:
            {
               fMenuBar->UnmapWindow();
               Bool_t ret = FileImport();
               fMenuBar->MapWindow();
               return ret;
            }
         case kM_FILE_EXPORT:
            {
               fMenuBar->UnmapWindow();
               Bool_t ret = FileExport();
               fMenuBar->MapWindow();
               return ret;
            }
         case kM_FILE_RFLAG_ALL:
            {
               fFileRestoreFlag = kSaveRestoreAll;
               fMenuFileFlag[0]->CheckEntry (kM_FILE_RFLAG_ALL);
               fMenuFileFlag[0]->UnCheckEntry (kM_FILE_RFLAG_STD);
               fMenuFileFlag[0]->UnCheckEntry (kM_FILE_RFLAG_PRM);
               break;
            }
         case kM_FILE_RFLAG_STD:
            {
               fFileRestoreFlag = kSaveRestoreStandard;
               fMenuFileFlag[0]->UnCheckEntry (kM_FILE_RFLAG_ALL);
               fMenuFileFlag[0]->CheckEntry (kM_FILE_RFLAG_STD);
               fMenuFileFlag[0]->UnCheckEntry (kM_FILE_RFLAG_PRM);
               break;
            }
         case kM_FILE_RFLAG_PRM:
            {
               fFileRestoreFlag = kSaveRestoreParameterOnly;
               fMenuFileFlag[0]->UnCheckEntry (kM_FILE_RFLAG_ALL);
               fMenuFileFlag[0]->UnCheckEntry (kM_FILE_RFLAG_STD);
               fMenuFileFlag[0]->CheckEntry (kM_FILE_RFLAG_PRM);
               break;
            }
         case kM_FILE_RFLAG_SET:
            {
               fSettingsRestoreFlag = !fSettingsRestoreFlag;
               if (fSettingsRestoreFlag) {
                  fMenuFileFlag[0]->CheckEntry (kM_FILE_RFLAG_SET);
               }
               else {
                  fMenuFileFlag[0]->UnCheckEntry (kM_FILE_RFLAG_SET);
               }
               break;
            }
         case kM_FILE_RFLAG_CAL:
            {
               fCalibrationRestoreFlag = !fCalibrationRestoreFlag;
               if (fCalibrationRestoreFlag) {
                  fMenuFileFlag[0]->CheckEntry (kM_FILE_RFLAG_CAL);
               }
               else {
                  fMenuFileFlag[0]->UnCheckEntry (kM_FILE_RFLAG_CAL);
               }
               break;
            }
         case kM_FILE_SFLAG_ALL:
            {
               fFileSaveFlag = kSaveRestoreAll;
               fMenuFileFlag[1]->CheckEntry (kM_FILE_SFLAG_ALL);
               fMenuFileFlag[1]->UnCheckEntry (kM_FILE_SFLAG_STD);
               fMenuFileFlag[1]->UnCheckEntry (kM_FILE_SFLAG_PRM);
               break;
            }
         case kM_FILE_SFLAG_STD:
            {
               fFileSaveFlag = kSaveRestoreStandard;
               fMenuFileFlag[1]->UnCheckEntry (kM_FILE_SFLAG_ALL);
               fMenuFileFlag[1]->CheckEntry (kM_FILE_SFLAG_STD);
               fMenuFileFlag[1]->UnCheckEntry (kM_FILE_SFLAG_PRM);
               break;
            }
         case kM_FILE_SFLAG_PRM:
            {
               fFileSaveFlag = kSaveRestoreParameterOnly;
               fMenuFileFlag[1]->UnCheckEntry (kM_FILE_SFLAG_ALL);
               fMenuFileFlag[1]->UnCheckEntry (kM_FILE_SFLAG_STD);
               fMenuFileFlag[1]->CheckEntry (kM_FILE_SFLAG_PRM);
               break;
            }
         case kM_FILE_SFLAG_SET:
            {
               fSettingsSaveFlag = !fSettingsSaveFlag;
               if (fSettingsSaveFlag) {
                  fMenuFileFlag[1]->CheckEntry (kM_FILE_SFLAG_SET);
               }
               else {
                  fMenuFileFlag[1]->UnCheckEntry (kM_FILE_SFLAG_SET);
               }
               break;
            }
         case kM_FILE_SFLAG_CAL:
            {
               fCalibrationSaveFlag = !fCalibrationSaveFlag;
               if (fCalibrationSaveFlag) {
                  fMenuFileFlag[1]->CheckEntry (kM_FILE_SFLAG_CAL);
               }
               else {
                  fMenuFileFlag[1]->UnCheckEntry (kM_FILE_SFLAG_CAL);
               }
               break;
            }
         case kM_FILE_PRINT:
            {
               if (fMultiPad != 0) {
                  fMultiPad->PrintPSDlg();
               }
               break;
            }
         case kM_FILE_PRINTSETUP:
            {
               if (fMultiPad != 0) {
                  TLGPrintParam* pparam = fMultiPad->GetDefPrintSetup();
                  if (pparam) {
                     pparam->ShowDialog (gClient->GetRoot(), fParent,
                                        TLGPrintParam::kPrintSetup);
                  }
               }
               break;
            }
         case kM_FILE_PRINT_GRAPHA:
         case kM_FILE_PRINT_GRAPHB:
            {
               if (fMultiPad != 0) {
                  TLGPrintParam* pparam = fMultiPad->GetDefPrintSetup();
                  TLGPrintParam pdlg;
                  if (pparam) pdlg = *pparam;
                  pdlg.fPlotSelection = parm1 - kM_FILE_PRINT_GRAPHA;
                  if (!fMultiPad->PrintPS (pdlg)) {
                     TString msg = TString ("Unable to complete print job.");
                     new TGMsgBox(gClient->GetRoot(), fParent, "Error", 
                                 msg, kMBIconStop, kMBOk);
                  }
               }
               break;
            }
         case kM_FILE_EXIT:
            {
               TGMainFrame* win = dynamic_cast<TGMainFrame*> (fParent);
               if (win) win->CloseWindow();
               break;
            }
         
         case kM_PLOT_REFERENCE:
            {
               if (fMultiPad != 0) {
                  fMultiPad->ReferenceTracesDlg();
               }
               break;
            }
         case kM_PLOT_MATH:
            {
               if (fMultiPad != 0) {
                  fMultiPad->MathDlg();
               }
               break;
            }
         case kM_PLOT_CALIBRATION_EDIT:
            {
               if (fMultiPad != 0) {
                  fMenuBar->UnmapWindow();
                  fMultiPad->CalibrationEditDlg();
                  fMenuBar->MapWindow();
               }
               break;
            }   
         case kM_PLOT_CALIBRATION_READ:
            {
               if (fMultiPad != 0) {
                  fMultiPad->CalibrationImportDlg();
               }
               break;
            }   
         case kM_PLOT_CALIBRATION_WRITE:
            {
               if (fMultiPad != 0) {
                  fMultiPad->CalibrationExportDlg();
               }
               break;
            }   
         case kM_WINDOW_NEW:
            {
               if (fMultiPad != 0) {
                  fMultiPad->NewWindow();
               }
               break;
            }   
         case kM_WINDOW_ZOOkM_OUT:
            {
               if (fMultiPad != 0) {
                  fMultiPad->Zoom (-1);
               }
               break;
            }
         case kM_WINDOW_ZOOkM_CUR:
            {
               if (fMultiPad != 0) {
                  fMultiPad->Zoom (fMultiPad->GetActivePad());
               }
               break;
            }   
         case kM_WINDOW_ZOOkM_A:
            {
               if (fMultiPad != 0) {
                  fMultiPad->Zoom (0);
               }
               break;
            }
         case kM_WINDOW_ZOOkM_B:
            {
               if (fMultiPad != 0) {
                  fMultiPad->Zoom (1);
               }
               break;
            }
         case kM_WINDOW_ACTIVE_NEXT:
            {
               if (fMultiPad != 0) {
                  Int_t id = fMultiPad->GetActivePad() + 1;
                  if (id >= fMultiPad->GetPadNumber()) id = 0;
                  fMultiPad->SetActivePad (id);
               }
               break;
            }
         case kM_WINDOW_ACTIVE_A:
            {
               if (fMultiPad != 0) {
                  fMultiPad->SetActivePad (0);
               }
               break;
            }
         case kM_WINDOW_ACTIVE_B:
            {
               if (fMultiPad != 0) {
                  fMultiPad->SetActivePad (1);
               }
               break;
            }
         case kM_WINDOW_LAYOUT:
            {
               if (fMultiPad != 0) {
                  fMultiPad->OptionDlg ();
               }
               break;
            }
         case kM_HELP_ABOUT:
            {
               new TGMsgBox (gClient->GetRoot(), fParent, "About", 
                            aboutmsg, 0, kMBOk);
               break;
            }
         default:
            {
               return kFALSE;
            }
      }
      return kTRUE;
   }

}
