/* Version $Id$ */
#ifndef _LIGO_TLGFRAME_H
#define _LIGO_TLGFRAME_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: TLGFrame						*/
/*                                                         		*/
/* Module Description: frames						*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 8Apr03   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: TLGFrame.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include <TGFrame.h>

namespace dttgui {


/** Transient frame.
   
    @memo Transient frame
    @author Written April 2003 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGTransientFrame : public TGTransientFrame {
   public:
      /// Constructor
      TLGTransientFrame (const TGWindow* p, const TGWindow* main,
                        UInt_t w, UInt_t h, 
                        UInt_t options = kVerticalFrame);
      /// Delete window
      virtual void DeleteWindow();
   };

/** Main frame.
   
    @memo Main frame
    @author Written April 2003 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGMainFrame : public TGMainFrame {
   public:
      /// Constructor
      TLGMainFrame (const TGWindow* p, UInt_t w, UInt_t h, 
                   UInt_t options = kVerticalFrame);
      /// Delete window
      virtual void DeleteWindow();
   };

}

#endif
