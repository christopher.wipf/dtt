/* Version $Id$ */
#ifndef _LIGO_TLGCOMBOEDITBOX_H
#define _LIGO_TLGCOMBOEDITBOX_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: TLGEntry						*/
/*                                                         		*/
/* Module Description: Entry fields	 				*/
/*		       modified test entry and numeric field entry	*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 29Oct99  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: TLGEntry.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include <TGFrame.h>
#include <TGComboBox.h>
#include <TGTextEntry.h>

namespace dttgui {

/** @name TLGComboEditBox
    This header exports a combobox with an editable top field. 

    @memo Comboboc with editable top field
    @author Written September 2002 by Daniel Sigg
    @version 1.0
 ************************************************************************/


   class TLGTextLBEntry : public TGTextLBEntry {
   public:
#if ROOT_VERSION_CODE > 197893
      TLGTextLBEntry (const TGWindow *p, TGString *s, Int_t id,
                     GContext_t norm = GetDefaultGC()(), 
                     FontStruct_t font = GetDefaultFontStruct(),
                     UInt_t options = kHorizontalFrame, 
                     ULong_t back = GetWhitePixel());
#else
      TLGTextLBEntry (const TGWindow *p, TGString *s, Int_t id,
                     GContext_t norm = fgDefaultGC(), 
                     FontStruct_t font = fgDefaultFontStruct,
                     UInt_t options = kHorizontalFrame, 
                     ULong_t back = fgWhitePixel);
#endif
      virtual void SetText (TGString *new_text);
   };



/** Combobox that supports an inactive state.
   
    @memo Combobox
    @author Written September 2002 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGComboBox : public TGComboBox {
   protected:
      /// State
      Bool_t 		fState;
   public:
      /// Create combobox
#if ROOT_VERSION_CODE > 197893
      TLGComboBox (const TGWindow* p, Int_t id, UInt_t options = 
                  kHorizontalFrame|kSunkenFrame|kDoubleBorder,
                  ULong_t back = GetWhitePixel());
#else
      TLGComboBox (const TGWindow* p, Int_t id, UInt_t options = 
                  kHorizontalFrame|kSunkenFrame|kDoubleBorder,
                  ULong_t back = fgWhitePixel);
#endif
      /// Intercept button message
      virtual Bool_t HandleButton(Event_t* event);
      /// Set state
      void SetState (Bool_t state);
      /// Get state
      Bool_t GetState() const {
         return fState; }
   };



/** Combobox with an editable top field.
   
    @memo Combobox with editable top field
    @author Written September 2002 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGComboEditBox : public TGCompositeFrame, public TGWidget {
   protected:
      /// State
      Bool_t 		fState;
   
   protected:
      TGTextEntry         *fSelEntry;      // selected item frame
      TGScrollBarElement  *fDDButton;      // button controlling drop down of popup
      TGComboBoxPopup     *fComboFrame;    // popup containing a listbox
      TGListBox           *fListBox;       // the listbox with text items
      const TGPicture     *fBpic;          // down arrow picture used in fDDButton
      TGLayoutHints       *fLhs;           // layout hints for selected item frame
      TGLayoutHints       *fLhb;           // layout hints for fDDButton
      TGLayoutHints       *fLhdd;          // layout hints for fListBox
   
   public:
#if ROOT_VERSION_CODE > 197893
      TLGComboEditBox (const TGWindow *p, Int_t id, UInt_t options = 
                      kHorizontalFrame | kSunkenFrame | kDoubleBorder,
                      ULong_t back = GetWhitePixel());
#else
      TLGComboEditBox (const TGWindow *p, Int_t id, UInt_t options = 
                      kHorizontalFrame | kSunkenFrame | kDoubleBorder,
                      ULong_t back = fgWhitePixel);
#endif
      virtual ~TLGComboEditBox();
   
      virtual void DrawBorder();
      virtual TGDimension GetDefaultSize() const { 
         return TGDimension(fWidth, fHeight); }
   
      virtual Bool_t HandleButton(Event_t *event);
      virtual Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2);
   
      virtual void AddEntry(TGString *s, Int_t id)
      { fListBox->AddEntry(s, id); }
      virtual void AddEntry(const char *s, Int_t id)
      { fListBox->AddEntry(s, id); }
      virtual void AddEntry(TGLBEntry *lbe, TGLayoutHints *lhints)
      { fListBox->AddEntry(lbe, lhints); }
      virtual void InsertEntry(TGString *s, Int_t id, Int_t afterID)
      { fListBox->InsertEntry(s, id, afterID); }
      virtual void InsertEntry(const char *s, Int_t id, Int_t afterID)
      { fListBox->InsertEntry(s, id, afterID); }
      virtual void InsertEntry(TGLBEntry *lbe, TGLayoutHints *lhints, Int_t afterID)
      { fListBox->InsertEntry(lbe, lhints, afterID); }
      virtual void RemoveEntry(Int_t id)
      { fListBox->RemoveEntry(id); }
      virtual void RemoveEntries(Int_t from_ID, Int_t to_ID)
      { fListBox->RemoveEntries(from_ID, to_ID); }
   
      virtual const TGListBox *GetListBox() const { 
         return fListBox; }
      virtual void  Select(Int_t id);
      virtual Int_t GetSelected() const { 
         return fListBox->GetSelected(); }
      virtual TGLBEntry *GetSelectedEntry() const
      { 
         return fListBox->GetSelectedEntry(); }
   
      virtual void Selected(Int_t widgetId, Int_t id); //*SIGNAL*
      virtual void Selected(Int_t id) { 
         Emit("Selected(Int_t)", id); } //*SIGNAL*
      virtual void Selected(const char *txt) { 
         Emit("Selected(char*)", txt); } //*SIGNAL*
   
   // public:
   //    /// Create editable combobox
      // TLGComboEditBox (const TGWindow* p, Int_t id, 
                      // UInt_t options = kHorizontalFrame | kSunkenFrame |
                      // kDoubleBorder, ULong_t back = fgWhitePixel);
   //    /// Desctructor
      // virtual ~TLGComboEditBox();
   //    /// Set top entry (if non editable)
      // virtual void SetTopEntry (TGLBEntry *e, TGLayoutHints *lh);
   //    /// Select an item
      // virtual void Select(Int_t id);
      /** Set by name. This will force the name to be displayed even 
          if not in list. Returns true if successful */
      virtual void SetText (const char* name);
      /** Get by name. This will read the displayed name even 
          if not in list. Returns true if name in list */
      virtual const char* GetText () const;
   //    /// Process message
      // virtual Bool_t ProcessMessage(Long_t msg, Long_t, Long_t parm2);
      /// Set state
      void SetState (Bool_t state);
      /// Get state
      Bool_t GetState() const {
         return fState; }
   };


}

#endif // _LIGO_TLGCOMBOEDITBOX_H
