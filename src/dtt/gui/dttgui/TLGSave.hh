/* Version $Id: TLGSave.hh 7582 2016-03-01 22:25:48Z john.zweizig@LIGO.ORG $ */
#ifndef _LIGO_TLGSAVE_H
#define _LIGO_TLGSAVE_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: TLGSave							*/
/*                                                         		*/
/* Module Description: Save and restore functions.			*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 13Jun00  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: TLGSave.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
#include "PConfig.h"
#include <iosfwd>
#include <sstream>
#include <string>
#include <vector>
#include <complex>
#include <TString.h>
#ifndef __CINT__
#include "Xsil.hh"
#endif

   class PlotSet;
   class ParameterDescriptor;
   class PlotDescriptor;

namespace calibration {
   class Calibration;
   class Table;
}

namespace dttgui {

   struct OptionAll_t;
   class OptionArray;
   struct ReferenceTraceList_t;
   struct ReferenceTrace_t;
   struct MathTable_t;

/** @name TLGSave
   
    @memo Save and restore functions
    @author Written June 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{

/** @name XML handlers for diagnostics viewer data.
   
    @memo XML handlers
    @author Written June 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{

#ifndef __CINT__

   
/** Handler for plot data objects.
   
    @memo Plot data record handler
    @author Written June 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class xsilHandlerData : public xml::xsilHandlerUnknown {
      xsilHandlerData (const xsilHandlerData&);
      xsilHandlerData& operator= (const xsilHandlerData&);
   
   public:
      /// data type
      enum DataType {
      /// Time series
      kTimeSeries = 0,
      /// Spectrum 
      kSpectrum = 1,
      /// Transfer function
      kTransferFunction = 2,
      /// Coefficients
      kCoefficients = 3,
      /// Histogram
      kHistogram = 4,
      /// LDAS special
      kLdasTimeSeries = 5
      };
      /// Channel list
      typedef std::vector<std::string> channels;
   
   protected:
      /// Name of data object
      std::string	fName;
      /// Name of A channel
      std::string	fAChn;
      /// Names of B channels
      channels		fBChn;
      /// Data type
      DataType		fType;
      /// Data subtype
      int		fSubtype;
      /// Measurement step
      int		fStep;
      /// time
      unsigned long 	fSec;
      /// time
      unsigned long 	fNsec;
      /// x0
      double		fX0;
      /// x spacing
      double		fDx;
      /// averages
      int		fAvrg;
      /// bandwidth
      double		fBW;
   
      //==== Histogram Specific Parameters (mito)
      /// bin edges
      double*		fBins;
      /// bin errors
      double*		fErrors;
      /// title
      std::string       fTitle;
      /// x-axis label
      std::string       fXLabel;
      /// count axis label for histogram
      std::string       fNLabel;
      /// number of bins
      int               fNBinx;
      /// number of data entries
      int               fNData;
      /// sum of weight, weight^2, weight*data, weight*data^2
      double*           fStats;
      //=========================================
   
      /// Plot set
      PlotSet*		fPlot;
      /// Calibration table
      calibration::Table* fCal;
      /// Reference trace record
      ReferenceTrace_t* fRef;
      /// Reference trace number
      int		fRefTrace;
      /// Unrecognized paramter stream
      std::ostringstream	fOsPrm;
   
   public:
      /// Constructor
      explicit xsilHandlerData (const std::string& name,
                        DataType dtype, PlotSet& pset,
                        calibration::Table* caltable = 0,
                        ReferenceTrace_t* ref = 0,
                        int reftrace = -1);
      /// Destructor
      virtual ~xsilHandlerData();
   
      /// bool parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const bool& p, int N = 1) {
         return xsilHandlerUnknown::HandleParameter (name, attr, p, N); }
      /// byte parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const char& p, int N = 1) {
         return xsilHandlerUnknown::HandleParameter (name, attr, p, N); }
      /// short parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const short& p, int N = 1) {
         return xsilHandlerUnknown::HandleParameter (name, attr, p, N); }
      /// int parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const int& p, int N = 1);
   #ifndef __CINT__
      /// long parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const long long& p, int N = 1) {
         return xsilHandlerUnknown::HandleParameter (name, attr, p, N); }
   #endif
      /// float parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const float& p, int N = 1) {
         return xsilHandlerUnknown::HandleParameter (name, attr, p, N); }
      /// double parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const double& p, int N = 1);
      /// complex float parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const std::complex<float>& p, int N = 1) {
         return xsilHandlerUnknown::HandleParameter (name, attr, p, N); }
      /// complex double parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const std::complex<double>& p, int N = 1) {
         return xsilHandlerUnknown::HandleParameter (name, attr, p, N); }
      /// string parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr, const std::string& p);
      /// time callback (must return true if handled)
      virtual bool HandleTime (const std::string& name,
                        const attrlist& attr,
                        unsigned long sec, unsigned long nsec);
      /// data callback (must return true if data is adopted)
      virtual bool HandleData (const std::string& name,
                        float* x, int dim1, int dim2 = 0, 
                        int dim3 = 0, int dim4 = 0);
      /// data callback (must return true if data is adopted)
      virtual bool HandleData (const std::string& name,
                        std::complex<float>* x, 
                        int dim1, int dim2 = 0, 
                        int dim3 = 0, int dim4 = 0);
      /// data callback (must return true if data is adopted)
      virtual bool HandleData (const std::string& name,
                        double* x, int dim1, int dim2 = 0, 
                        int dim3 = 0, int dim4 = 0);
      /// data callback (must return true if data is adopted)
      virtual bool HandleData (const std::string& name,
                        std::complex<double>* x, 
                        int dim1, int dim2 = 0, 
                        int dim3 = 0, int dim4 = 0);
      /// handler for nested data objects (used for ldas)
      virtual xml::xsilHandler* GetHandler (const attrlist& attr);
   protected:
      /// Convert between single and double precision
      virtual bool ConvertPrecision (bool to_double = true);
   };


/** Handler for plot settings.
   
    @memo Plot settings record handler
    @author Written June 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class xsilHandlerOptions : public xml::xsilHandler {
      xsilHandlerOptions (const xsilHandlerOptions&);
      xsilHandlerOptions& operator= (const xsilHandlerOptions&);
   
   protected:
      /// Option pointer
      OptionAll_t*	fOpt;
   
   public:
      /// Constructor
      explicit xsilHandlerOptions (OptionAll_t& opt);
      /// Destructor
      virtual ~xsilHandlerOptions();
   
      /// bool parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const bool& p, int N = 1);
      /// byte parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const char& p, int N = 1) {
         return false; }
      /// short parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const short& p, int N = 1) {
         return false; }
      /// int parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const int& p, int N = 1);
   #ifndef __CINT__
      /// long parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const long long& p, int N = 1) {
         return false; }
   #endif
      /// float parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const float& p, int N = 1) {
         return false; }
      /// double parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const double& p, int N = 1);
      /// complex float parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const std::complex<float>& p, int N = 1) {
         return true; }
      /// complex double parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const std::complex<double>& p, int N = 1) {
         return true; }
      /// string parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr, const std::string& p);
      /// time callback (must return true if handled)
      virtual bool HandleTime (const std::string& name,
                        const attrlist& attr,
                        unsigned long sec, unsigned long nsec) {
         return true; }
   };


/** Handler for calibration records.
   
    @memo Calibration record handler
    @author Written June 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class xsilHandlerCalibration : public xml::xsilHandler {
      xsilHandlerCalibration (const xsilHandlerCalibration&);
      xsilHandlerCalibration& operator= (const xsilHandlerCalibration&);
   
   protected:
      /// Calibration table
      calibration::Table*	fCal;
      /// Overwrite old entries?
      bool			fOverwrite;
      /// Calibration record
      calibration::Calibration*	fRec;
   
   public:
      /// Constructor
      explicit xsilHandlerCalibration (calibration::Table& cals,
                        bool overwrite = true);
      /// Destructor
      virtual ~xsilHandlerCalibration();
   
      /// bool parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const bool& p, int N = 1);
      /// byte parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const char& p, int N = 1) {
         return false; }
      /// short parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const short& p, int N = 1) {
         return false; }
      /// int parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const int& p, int N = 1);
   #ifndef __CINT__
      /// long parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const long long& p, int N = 1) {
         return false; }
   #endif
      /// float parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const float& p, int N = 1);
      /// double parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const double& p, int N = 1);
      /// complex float parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const std::complex<float>& p, int N = 1) {
         return false; }
      /// complex double parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const std::complex<double>& p, int N = 1);
      /// string parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr, const std::string& p);
      /// time callback (must return true if handled)
      virtual bool HandleTime (const std::string& name,
                        const attrlist& attr,
                        unsigned long sec, unsigned long nsec);
   };



/** Xsil plot data handler query class.
    The query will return a handler if the data object type is
    Spectrum, TimeSeries, Coefficients or TransferFunction.
    with 'Plot'.

    @memo Xsil plot data handler query.
    @author Written June 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class xsilHandlerQueryData : public xml::xsilHandlerQuery {
   protected:
      /// Plot set
      PlotSet*		fPlot;
      /// read raw time series
      bool		fRawData;
      /// Calibration table
      calibration::Table*	fCal;
      /// Reference trace list
      ReferenceTraceList_t* fRef;
   
   public:
      /// Constructor
      xsilHandlerQueryData (PlotSet* pset, bool rawdata = true,
                        calibration::Table* caltable = 0,
                        ReferenceTraceList_t* reftable = 0) 
      : fPlot (pset), fRawData (rawdata), fCal (caltable),
      fRef (reftable) {
      }
      /// returns a handler for the specified object (or 0 if not)
      virtual xml::xsilHandler* GetHandler (const attrlist& attr);
   };

/** Xsil option handler query class.
    The query will return a handler if the data object name starts
    with 'Plot'.

    @memo Xsil option handler query.
    @author Written June 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class xsilHandlerQueryOptions : public xml::xsilHandlerQuery {
   protected:
      /// Option array
      OptionArray*	fOpt;
   public:
      /// Constructor
      xsilHandlerQueryOptions (OptionArray* opts) 
      : fOpt (opts) {
      }
      /// returns a handler for the specified object (or 0 if not)
      virtual xml::xsilHandler* GetHandler (const attrlist& attr);
   };

/** Xsil reference traces handler query class.
    The query will return a handler if the data object name starts
    with 'Reference'.

    @memo Xsil reference traces handler query.
    @author Written June 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class xsilHandlerQueryReferenence : public xml::xsilHandlerQuery {
   protected:
      /// Reference list
      ReferenceTraceList_t*	fRef;
   public:
      /// Constructor
      xsilHandlerQueryReferenence (ReferenceTraceList_t* ref) 
      : fRef (ref) {
      }
      /// returns a handler for the specified object (or 0 if not)
      virtual xml::xsilHandler* GetHandler (const attrlist& attr);
   };

/** Xsil calibration handler query class.
    The query will return a handler if the data object name starts
    with 'Calibration'.

    @memo Xsil calibration handler query.
    @author Written June 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class xsilHandlerQueryCalibration : public xml::xsilHandlerQuery {
   protected:
      /// Calibration table
      calibration::Table*	fCal;
      /// Overwrite old entries
      bool			fOverwrite;
   public:
      /// Constructor
      xsilHandlerQueryCalibration (calibration::Table* cals,
                        bool overwrite = true) 
      : fCal (cals), fOverwrite (overwrite) {
      }
      /// returns a handler for the specified object (or 0 if not)
      virtual xml::xsilHandler* GetHandler (const attrlist& attr);
   };

/** Xsil math table handler query class.
    The query will return a handler if the data object name starts
    with 'Math'.

    @memo Xsil math table handler query.
    @author Written June 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class xsilHandlerQueryMath : public xml::xsilHandlerQuery {
   protected:
      /// Math table
      MathTable_t*		fMath;
   public:
      /// Constructor
      xsilHandlerQueryMath (MathTable_t* math) 
      : fMath (math) {
      }
      /// returns a handler for the specified object (or 0 if not)
      virtual xml::xsilHandler* GetHandler (const attrlist& attr);
   };

/** Xsil unknown object handler query class.
    The query will always return the unknown handler.

    @memo Xsil unknown object handler query.
    @author Written June 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class xsilHandlerQueryUnknown : public xml::xsilHandlerQuery {
   protected:
      /// string stream
      std::ostream*		fOs;
   public:
      /// Constructor
      xsilHandlerQueryUnknown (std::ostream* ss) 
      : fOs (ss) {
      }
      /// returns a handler for the specified object (or 0 if not)
      virtual xml::xsilHandler* GetHandler (const attrlist& attr);
   };

//@}
#endif

/** @name Save and restore classes
   
    @memo Save and restore classes
    @author Written June 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{

/** Helper class for storing the diagnostics viewer data to file.
   
    @memo Store data
    @author Written June 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGSaver {
   public:
      /// Save flag
      enum ESaveFlag {
      /// Save everything
      kSaveAll = 0,
      /// Save common objects only
      kSaveStandard = 1,
      /// Save parameters only
      kSaveParameterOnly = 2
      };
   
   protected:
      /// Filename
      TString	fFilename;
      /// Save restore flag
      ESaveFlag fFileSaveFlag;
      /// Error message
      TString*	fError;
   
   public:
      /// Constructor
      TLGSaver (const char* filename, ESaveFlag saveflag, 
               TString& error)
      : fFilename (filename ? filename : ""), fFileSaveFlag (saveflag), 
      fError (&error) {
      }
      /// Desturctor
      virtual ~TLGSaver() {
      }
      /// Setup
      virtual Bool_t Setup () {
         return kTRUE; }
      /// Save data
      virtual Bool_t Data (PlotSet& pset) {
         return kTRUE; }
      /// Save plot settings
      virtual Bool_t PlotSettings (OptionArray& opts) {
         return kTRUE; }
      /// Save reference list
      virtual Bool_t ReferenceList (ReferenceTraceList_t& ref) {
         return kTRUE; }
      /// Save calibration table
      virtual Bool_t CalibrationData (calibration::Table& cals) {
         return kTRUE; }
      /// Save math functions
      virtual Bool_t Math (MathTable_t& math) {
         return kTRUE; }
      /// Cleanup
      virtual Bool_t Done (Bool_t success) {
         return kTRUE; }
   };


/** Helper class for restoring the diagnostics viewer data from file.
   
    @memo Restore data
    @author Written June 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGRestorer {
   public:
      /// Restore flag
      enum ERestoreFlag {
      /// Restore everything
      kRestoreAll = 0,
      /// Restore common objects only
      kRestoreStandard = 1,
      /// Restore parameters only
      kRestoreParameterOnly = 2
      };
   
   protected:
      /// Filename
      TString	fFilename;
      /// Save restore flag
      ERestoreFlag fFileRestoreFlag;
      /// Error message
      TString*	fError;
   
   public:
      /// Constructor
      TLGRestorer (const char* filename, ERestoreFlag restoreflag, 
                  TString& error)
      : fFilename (filename ? filename : ""), 
      fFileRestoreFlag (restoreflag), fError (&error) {
      }
      /// Destructor
      virtual ~TLGRestorer() {
      }
      /// Setup
      virtual Bool_t Setup () {
         return kTRUE; }
      /// Restore data
      virtual Bool_t Data (PlotSet& pset) {
         return kTRUE; }
      /// Restore plot settings
      virtual Bool_t PlotSettings (OptionArray& opts) {
         return kTRUE; }
      /// Restore reference list
      virtual Bool_t ReferenceList (ReferenceTraceList_t& ref) {
         return kTRUE; }
      /// Restore calibration table
      virtual Bool_t CalibrationData (calibration::Table& cals) {
         return kTRUE; }
      /// Restore math functions
      virtual Bool_t Math (MathTable_t& math) {
         return kTRUE; }
      /// Cleanup
      virtual Bool_t Done (Bool_t success) {
         return kTRUE; }
   };


/** Helper class for storing the diagnostics viewer data to file in 
    LIGO light weight format (XML).
   
    @memo Store XML data
    @author Written June 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGXMLSaver : public TLGSaver {
   protected:
      /// Output stream
      std::ostream*	fOut;
      /// Additional xml objects
      std::string* 	fXMLObjs;
   
   public:
      /// Constructor (xml is adopted)
      TLGXMLSaver (const char* filename, ESaveFlag saveflag, 
                  TString& error, std::string* xml = 0)
      : TLGSaver (filename, saveflag, error), fOut (0), fXMLObjs (xml) {
      }
      /// Destructor
      virtual ~TLGXMLSaver() {
         if (fOut) delete fOut; 
         if (fXMLObjs) delete fXMLObjs; }
      /// Setup
      virtual Bool_t Setup ();
      /// Save data
      virtual Bool_t Data (PlotSet& pset);
      /// Save plot settings
      virtual Bool_t PlotSettings (OptionArray& opts);
      /// Save reference list
      virtual Bool_t ReferenceList (ReferenceTraceList_t& ref);
      /// Save calibration table
      virtual Bool_t CalibrationData (calibration::Table& cals);
      /// Save math functions
      virtual Bool_t Math (MathTable_t& math);
      /// Cleanup
      virtual Bool_t Done (Bool_t success);
   
      /// Get the channels associated with the plot descriptor
      static bool GetChannelList (PlotSet& pset,
                        const PlotDescriptor& plotd, 
                        std::vector<std::string>& AChannels,
                        std::vector<std::string>& BChannels);
   };


/** Helper class for restoring the diagnostics viewer data from file in 
    LIGO light weight format (XML).
   
    @memo Restore XML data
    @author Written June 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGXMLRestorer : public TLGRestorer {
   protected:
      /// Stream for unrecognized data objects
      std::ostream*	fOs;
      /// Pointer to plot set
      PlotSet*		fPlots;
      /// Pointer to plot set
      OptionArray*	fOpts;
      /// Pointer to plot set
      ReferenceTraceList_t*	fRef;
      /// Pointer to plot set
      calibration::Table*	fCal;
      /// Pointer to plot set
      MathTable_t*	fMath;
      /// Input stream
      std::istream*	fInp;
   #ifndef __CINT__   
      /// Parser
      xml::xsilParser 	fXml;
   #endif
      /// Add computed traces at the end
      virtual Bool_t AddComputedTraces ();
   
   public:
      /// Constructor
      TLGXMLRestorer (const char* filename, ERestoreFlag restoreflag, 
                     TString& error, std::ostream* os = 0)
      : TLGRestorer (filename, restoreflag, error), fOs (os), fPlots (0),
      fOpts (0), fRef (0), fCal (0), fMath (0), fInp (0) {
      }
      /// Destructor
      virtual ~TLGXMLRestorer() {
         if (fInp) delete fInp; }
      /// Setup
      virtual Bool_t Setup ();
      /// Restore data
      virtual Bool_t Data (PlotSet& pset) {
         fPlots = &pset; 
         return kTRUE; }
      /// Restore plot settings
      virtual Bool_t PlotSettings (OptionArray& opts) {
         fOpts = &opts; 
         return kTRUE; }
      /// Restore reference list
      virtual Bool_t ReferenceList (ReferenceTraceList_t& ref) {
         fRef = &ref; 
         return kTRUE; }
      /// Restore calibration table
      virtual Bool_t CalibrationData (calibration::Table& cals) {
         fCal = &cals; 
         return kTRUE; }
      /// Restore math functions
      virtual Bool_t Math (MathTable_t& math) {
         fMath = &math; 
         return kTRUE; }
      /// Cleanup
      virtual Bool_t Done (Bool_t success);
   #ifndef __CINT__   
      /** add compupted traces for a plot descriptor */
      static void AddComputedTrace (PlotSet& pset,
                        std::vector<PlotDescriptor*>& pds,
                        const PlotDescriptor& pd,
                        calibration::Table* caltable = 0);
   #endif
   };

//@}


/** Output operators for data containers of this libarary.
   
    @memo XML output
    @name Output operators for data containers
    @author Written June 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
//@{

/** Write parameter descriptor to output stream.
    @memo XML output of parameter descriptor 
    @param os output stream
    @param pd parameter descriptor
    @return output stream
   ************************************************************************/
   std::ostream& operator<< (std::ostream& os, 
                     const ParameterDescriptor& pd);

/** Write plot descriptor to output stream.
    @memo XML output of plot descriptor 
    @param os output stream
    @param pd plot descriptor
    @param resnum current result number
    @param refnum current reference trace number
    @param compress if true  (multiple) referenced objects are written 
           as one
    @return true if successful
   ************************************************************************/
   bool writePlotDescriptor (std::ostream& os, const PlotDescriptor& pd, 
                     int& resnum, int& refnum, bool compress = false);

/** Write plot descriptor to output stream.
    @memo XML output of plot descriptor 
    @param os output stream
    @param pd plot descriptor
    @return output stream
   ************************************************************************/
   std::ostream& operator<< (std::ostream& os, const PlotDescriptor& pd);

/** Write plot data to output stream.
    @memo XML output of plot data
    @param os output stream
    @param pset plot set
    @return output stream
 ************************************************************************/
   std::ostream& operator<< (std::ostream& os, const PlotSet& pset);

/** Write plot settings to output stream.
    @memo XML output of plot settings
    @param os output stream
    @param ps plot settings
    @return output stream
 ************************************************************************/
   std::ostream& operator<< (std::ostream& os, const OptionArray& opts);

/** Write reference list to output stream.
    @memo XML output of reference list
    @param os output stream
    @param rl reference list
    @return output stream
 ************************************************************************/
   std::ostream& operator<< (std::ostream& os, 
                     const ReferenceTraceList_t& rl);

/** Write calibration table to output stream.
    @memo XML output of calibration table
    @param os output stream
    @param ct calibration table
    @return output stream
 ************************************************************************/
   std::ostream& operator<< (std::ostream& os, 
                     const calibration::Table& ct);

/** Write math functions to output stream.
    @memo XML output of math funcitons
    @param os output stream
    @param mt math functions
    @return output stream
 ************************************************************************/
   std::ostream& operator<< (std::ostream& os, const MathTable_t& mt);

//@}

//@}

#ifndef __NO_NAMESPACE
}
#endif

#endif // _LIGO_TLGSAVE_H
