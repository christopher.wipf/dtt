/* Version $Id: TLGExport.hh 7625 2016-05-13 22:27:25Z john.zweizig@LIGO.ORG $ */
#ifndef _LIGO_TLGEXPORT_H
#define _LIGO_TLGEXPORT_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: TLGExport						*/
/*                                                         		*/
/* Module Description: Export option for plotsets/plot descriptors.	*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 29Oct99  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: TLGOptions.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include <TGFrame.h>
#include <TGComboBox.h>
#include <TGLabel.h>
#include <TGTextEntry.h>
#include <TGButton.h>
#include "TLGFrame.hh"

   class PlotSet;
   class PlotMap;

namespace calibration {
   class Table;
}
namespace dttgui {

   class TLGNumericControlBox;

/** @name TLGExport
    This header exports options which are used for exporting and
    importing data from and to a plot set, respectively.
   
    @memo Export options
    @author Written January 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{

/** @name Constants
    Constants for export option parameters, messages and widget IDs.
   
    @memo Export option constants
    @author Written January 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
//@{
   /// Maximum number of columns in export option
   const Int_t kMaxExportColumn = 50;
   /// Number of columns in export dialog
   const Int_t kShowExportColumn = 5;

   /// Widget ID of export button in export option dialog
   const int kExportExport = 1;
   /// Widget ID of cancel button in export option dialog
   const int kExportCancel = 2;
   /// Widget ID of ASCII format radio button in export option dialog
   const int kExportASCII = 3;
   /// Widget ID of binary format radio button in export option dialog
   const int kExportBinary = 4;
   /// Widget ID of XML format radio button in export option dialog
   const int kExportXML = 5;
   /// Widget ID of XY format radio button in export option dialog
   const int kExportXY = 6;
   /// Widget ID of X complex format radio button in export option dialog
   const int kExportXComplex = 7;
   /// Widget ID of zero time format radio button in export option dialog
   const int kExportZeroTime = 8;
   /// Widget ID of column major format radio button in export option dialog
   const int kExportColumnMajor = 9;
   /// Widget ID of save in separete files radio button in export option dialog
   const int kExportSeparateFiles = 10;
   /// Widget ID of double prec. radio button in export option dialog
   const int kExportBinDouble = 11;
   /// Widget ID of little endian radio button in export option dialog
   const int kExportBinLE = 12;
   /// Widget ID of XML store/restore all one radio button in export option dialog
   const int kExportXMLAll = 13;
   /// Widget ID of XML replace existing radio button in export option dialog
   const int kExportXMLReplace = 14;
   /// Widget ID of XML include cal radio button in export option dialog
   const int kExportXMLIncludeCal = 15;
   /// Widget ID of data type selection in export option dialog
   const int kExportDataType = 16;
   /// Widget ID of start index in export option dialog
   const int kExportStart = 17;
   /// Widget ID of maximum length in export option dialog
   const int kExportMax = 18;
   /// Widget ID of binning in export option dialog
   const int kExportBin = 19;
   /// Widget ID of column active in export option dialog
   const int kExportColActive = 20;
   /// Widget ID of Write Option Header option in export option dialog
   const int kWriteOptions = 21 ; // JCB
   /// Widget ID of column A channel in export option dialog
   //const int kExportColA = kExportColActive + kShowExportColumn; JCB
   const int kExportColA = kWriteOptions + kShowExportColumn;
   /// Widget ID of column active in export option dialog
   const int kExportColB = kExportColA + kShowExportColumn;
   /// Widget ID of column complex type in export option dialog
   const int kExportColType = kExportColB + kShowExportColumn;
   /// Widget ID of column selection in export option dialog
   const int kExportColSel = kExportColType + kShowExportColumn;

//@}

/** @name Enumerated types
    Enumerated types used by export options.
   
    @memo Export option enumerated types
    @author Written January 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
//@{
   /// Export type for complex (order must be the same as EDataCopy)
   enum EExportTypeConversion {
   /// Magnitude
   kETypeMagnitude = 0,
   /// dB Magnitude
   kETypedBMagnitude = 1,
   /// Real
   kETypeReal = 2,
   /// Imaginary
   kETypeImaginary = 3,
   /// As is
   kETypeAsIs = 4,
   /// Phase (degree)
   kETypePhaseDeg = 5,
   /// Phase (rad)
   kETypePhaseRad = 6,
   /// Continuous phase (degree)
   kETypePhaseDegCont = 7,
   /// Continuous phase (rad)
   kETypePhaseRadCont = 8,
   /// Complex (re/im)
   kETypeComplex = 9,
   /// Complex (dB/degree)
   kETypeComplexAlt1 = 10,
   /// Complex (abs/rad)
   kETypeComplexAlt2 = 11,
   /// Complex (dB/cont. degree)
   kETypeComplexAlt3 = 12,
   /// Complex (abs/cont. rad)
   kETypeComplexAlt4 = 13
   };

   /// Output type
   enum EExportOutputType {
   /// ASCII
   kEOutTypeASCII = 0,
   /// Binary
   kEOutTypeBinary = 1,
   /// XML / LIGO light weight
   kEOutTypeXML = 2
   };

//@}

/** @name Export structures
    Structures used by export dialog box.
   
    @memo Export option structures
    @author Written January 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
//@{

   /// Export column
   struct ExportColumn_t {
      /// Included?
      Bool_t		fInclude;
      /// A channel name
      TString		fAChn;
      /// B channel name
      TString		fBChn;
      /// Conversion output option
      EExportTypeConversion fTypeConversion;
   };

   /// Export options
   struct ExportOption_t {
      /// Filename
      TString		fFilename;
      /// Plot type
      TString		fPlotType;
      /// List of columns
      ExportColumn_t	fColumn[kMaxExportColumn];
      /// Output type
      EExportOutputType fOutputType;
      /// XY format?
      Bool_t		fXY;
      /// Complex X?
      Bool_t		fXComplex;
      /// Zero time?
      Bool_t		fZeroTime;
      /// Column major?
      Bool_t		fColumnMajor;
      /// Save traces separately?
      Bool_t            fSeparateFiles;
      /// Write option header? 
      Bool_t		fWriteOptionHeader; // JCB
      /// Zero padding?
      Bool_t		fZeroPadding;
      /// Binary option: Double precision?
      Bool_t		fBinDouble;
      /// Binary option: Little endian
      Bool_t		fBinLittleEndian;
      /// XML option: write/read all
      Bool_t		fXMLAll;
      /// XML option: replace existing
      Bool_t		fXMLReplace;
      /// XML option: include calibration info
      Bool_t		fXMLIncludeCal;
      /// Start index
      Int_t		fStart;
      /// Maximum data length
      Int_t		fMax;
      /// Bining
      Int_t		fBin;
   };

   /// Import options
   typedef ExportOption_t ImportOption_t;

//@}


/** Function to set an export option structure to its default values.
    @memo Set default export options.
    @param ex Export option structure
    @return void
 ************************************************************************/
   void SetDefaultExportOptions (ExportOption_t& ex);

/** Exports data to a file in XML format.
    @memo Export to file.
    @param ex Export option structure
    @param pl Plot set
    @param caltable Calibration table for XML export
    @return true if successful
 ************************************************************************/
   Bool_t ExportToFileXML (ExportOption_t& ex, PlotSet& pl,
                     const calibration::Table* caltable = 0);
/** Exports data to a file in ASCII and Binary format.
    @memo Export to file.
    @param ex Export option structure
    @param pl Plot set
    @return true if successful
 ************************************************************************/
   Bool_t ExportToFileASCII (ExportOption_t& ex, PlotSet& pl);

 /** Writes export options to the output file for ASCII files.
     @memo Write header to file.
     @param out ofstream for output.
     @param ex Pointer to export option structure.
     @return None.
 ************************************************************************/
   void WriteFileHeader(std::ostream &out, ExportOption_t *ex) ;  // JCB

/** Exports data to file using dialog box to determine the export 
    options. This function will first display the export option dialog
    box, then ask for a filename, and finally write the data to disk.
    @memo Export to file with dialog.
    @param p Parent window
    @param main Main window
    @param pl Plot set
    @param ex Default export options
    @param caltable Calibration table for XML export
    @return true if successful
 ************************************************************************/
   Bool_t ExportToFileDlg (const TGWindow* p, const TGWindow* main,
                     PlotSet& pl, ExportOption_t* ex = 0,
                     const calibration::Table* caltable = 0);


/** Function to set an import option structure to its default values.
    @memo Set default import options.
    @param im Import option structure
    @return void
 ************************************************************************/
   void SetDefaultImportOptions (ImportOption_t& im);

/** Import data from a file.
    @memo Import to file.
    @param im Import option structure
    @param pl Plot set
    @param caltable Calibration table for adding units
    @return true if successful
 ************************************************************************/
   Bool_t ImportFromFile (ImportOption_t& im, PlotSet& pl,
                     calibration::Table* caltable = 0);

/** Import data to file using dialog box to determine the import 
    options. This function will first display the import option dialog
    box, then ask for a filename, and finally read the data from disk.
    @memo Import from file with dialog.
    @param p Parent window
    @param main Main window
    @param pl Plot set
    @param im Default import options
    @param caltable Calibration table for adding units
    @return true if successful
 ************************************************************************/
   Bool_t ImportFromFileDlg (const TGWindow* p, const TGWindow* main,
                     PlotSet& pl, ExportOption_t* im = 0,
                     calibration::Table* caltable = 0);

/** Export/Import option dialog box.
    @memo Dialog box for setting export or import options.
    @author Written January 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGExportDialog : public TLGTransientFrame {
   protected:
      /// Plot types
      const PlotMap* 	fPlotList;
      /// export/import
      Bool_t		fExport;
      /// Export option value pointer
      ExportOption_t*	fExportValues;
      /// Temporary export option storage
      ExportOption_t	fExportTemp;
      /// Return parameter
      Bool_t*		fOk;
      /// Current column number 
      Int_t		fColCur;
   
      /// Data/bin group
      TGCompositeFrame* fGroupDataBin;
      /// Data type group
      TGGroupFrame* 	fGroupDataType;
      /// Bin group
      TGGroupFrame* 	fGroupBin;
      /// Data/bin frame
      TGCompositeFrame* fBinFrame;
      /// Column selection group
      TGGroupFrame*	fGroupColumnSel;
      /// Output group
      TGCompositeFrame* fGroupOut;
      /// Output type group
      TGGroupFrame*	fGroupType;
      /// Output format group
      TGGroupFrame*	fGroupFormat;
      /// Binary options group
      TGGroupFrame*	fGroupBinOpt;
      /// XML options group
      TGGroupFrame*	fGroupXMLOpt;
      /// Button frame
      TGCompositeFrame*	fButtonFrame;
      /// Numereous layout hints
      TGLayoutHints*	fL[10];
   
      /// Data type selection
      TGComboBox*	fDataType;
      /// Data range labels
      TGLabel*		fRangeLabel[3];
      /// Start index 
      TLGNumericControlBox* fStart;
      /// Maximum length 
      TLGNumericControlBox* fMax;
      /// Bining 
      TLGNumericControlBox* fBin;
      /// Display columns selection
      TGCompositeFrame*	fGroupColumnShow
      [kMaxExportColumn / kShowExportColumn / 2 + 1];
      /// Column selection radio buttons
      TGButton*		fColSel[kMaxExportColumn / kShowExportColumn];
      /// Column groups
      TGGroupFrame*	fGroupColumn[kShowExportColumn];
      /// Column group names
      TGString*		fGroupColumnName[kShowExportColumn];
      /// Line frames
      TGCompositeFrame*	fColFrame[kShowExportColumn][2];
      /// Column active checkbox
      TGButton*		fColActive[kShowExportColumn];
      /// Column A channel selection
      TGComboBox*	fColAe[kShowExportColumn];
      /// Column A channel selction (import)
      TGTextEntry*	fColAi[kShowExportColumn];
      /// Column B channel selection label
      TGLabel*		fColBLabel[kShowExportColumn];
      /// Column B channel selection (export)
      TGComboBox*	fColBe[kShowExportColumn];
      /// Column B channel selction (import)
      TGTextEntry*	fColBi[kShowExportColumn];
      /// Column complex type selection label
      TGLabel*		fColTypeLabel[kShowExportColumn];
      /// Column complex type selection
      TGComboBox*	fColType[kShowExportColumn];
   
      /// Output type radio buttons
      TGButton*		fOutType[3];
      /// XY check button
      TGButton*		fXY;
      /// X complex check button
      TGButton*		fXComplex;
      /// Zero time check button
      TGButton*		fZeroTime;
      /// X complex check button
      TGButton*		fColumnMajor;
      /// save trace in separate files check botton
      TGButton*         fSeparateFiles;
      /// Write option header? 
      TGButton*		fWriteOptionHeader; // JCB
      /// Double precision binary check button
      TGButton*		fBinDouble;
      /// Little endian binary check button
      TGButton*		fBinLE;
      /// XML save/restore all check button
      TGButton*		fXMLAll;
      /// XML replace check button
      TGButton*		fXMLReplace;
      /// XML include cal check button
      TGButton*		fXMLIncludeCal;
      /// Export button
      TGButton*		fExportButton;
      /// Cancel button
      TGButton*		fCancelButton;
   
      void SetColumns (Int_t range);
      void BuildPlotType (Int_t level, Int_t id = -1);
      Bool_t IsDataValid();
   
   public:
      TLGExportDialog (const TGWindow *p, const TGWindow *main,
                      ExportOption_t& ex, const PlotMap& plotlist,
                      Bool_t& ret, Bool_t import = kFALSE);
      virtual ~TLGExportDialog ();
      virtual void CloseWindow();
   
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   };



//@}
}

#endif // _LIGO_TLGEXPORT_H

