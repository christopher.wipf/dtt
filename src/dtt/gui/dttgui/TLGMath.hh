/* Version $Id: TLGMath.hh 7008 2014-02-14 23:50:45Z james.batch@LIGO.ORG $ */
#ifndef _LIGO_TLGMATH_H
#define _LIGO_TLGMATH_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: TLGMath							*/
/*                                                         		*/
/* Module Description: Math functions/options for 			*/
/*                     plotsets/plot descriptors.	      		*/
/* 							   		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 17May00  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: TLGMath.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include "TLGFrame.hh"
#include <TGFrame.h>
#include <TGListBox.h>
#include <TGComboBox.h>
#include <TGLabel.h>
#include <TGTab.h>
#include <TGButton.h>

   class PlotSet;

namespace calibration {
   class Table;
}
namespace dttgui {

   class TLGTextEntry;
   class TLGNumericControlBox;

/** @name TLGMath
    This header exports functions and options to do math with the
    data arrays described by the plot descriptors of a plot set.
   
    @memo Math functions and options
    @author Written May 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{

/** @name Constants
    Constants for math option parameters, messages and widget IDs.
   
    @memo Constants
    @author Written May 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
//@{
   /// Maximum number of reference traces
   const Int_t kMaxReferenceTraces = 1000;

   /// Widget ID of ok button in reference trace dialog
   const int kRefTraceOk = 1;
   /// Widget ID of cancel button in reference trace dialog
   const int kRefTraceCancel = 2;
   /// Widget ID of new button in reference trace dialog
   const int kRefTraceNew = 3;
   /// Widget ID of update button in reference trace dialog
   const int kRefTraceUpdate = 4;
   /// Widget ID of update all button in reference trace dialog
   const int kRefTraceUpdateAll = 5;
   /// Widget ID of clear button in reference trace dialog
   const int kRefTraceClear = 6;
   /// Widget ID of reference listbox in reference trace dialog
   const int kRefTraceRef = 10;
   /// Widget ID of graph in reference trace dialog
   const int kRefTraceGraph = 11;
   /// Widget ID of A channel in reference trace dialog
   const int kRefTraceAchn = 12;
   /// Widget ID of B channel in reference trace dialog
   const int kRefTraceBchn = 13;

//@}

/** @name Enumerated types
    Enumerated types used by math options.
   
    @memo Enumerated types
    @author Written May 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
//@{

   /// Reference trace modification flag
   enum ERefTraceMod {
   /// no modification
   kRefTraceModNo = 0,
   /// Added
   kRefTraceModAdd = 1,
   /// Deleted
   kRefTraceModDelete = 2,
   /// Updated
   kRefTraceModUpdate = 3
   };

//@}

/** @name Structures
    Structures used by the math library.
   
    @memo Structures
    @author Written May 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
//@{

   /// Reference trace
   struct ReferenceTrace_t {
      /// Valid?
      Bool_t		fValid;
      /// Modified flag
      ERefTraceMod	fModified;
      /// Graph type
      TString		fGraph;
      /// A channel name
      TString		fAChn;
      /// B channel name
      TString		fBChn;
   };

   /// List of reference traces
   struct ReferenceTraceList_t {
      /// List of traces
      ReferenceTrace_t	fTraces[kMaxReferenceTraces];
   };

   /// Math table
   struct MathTable_t {
   };


//@}

/** Function to set a reference trace list structure to its default 
    values.
    @memo Set default reference trace list options.
    @param ref list of reference traces
    @return void
 ************************************************************************/
   void SetDefaultReferenceTraces (ReferenceTraceList_t& ref);

/** Function to set a math table structure to its default values.
    @memo Set default math table options.
    @param math math table
    @return void
 ************************************************************************/
   void SetDefaultMathTable (MathTable_t& math);


/** Function to bring up the reference trace dialog box. Lets the user
    define, update and delete reference traces. A new reference trace
    is automatically added to the plot set when leaving the dialog box.
    A reference trace has the same name as the original with an 
    added "(ref #)" extension. The function returns true if a
    reference plot descriptor was added or updated. 
    @memo Reference trace dialog.
    @param pset Plot set
    @param ref List of reference traces
    @return true if changes were made
 ************************************************************************/
   Bool_t ReferenceTraceDlg (const TGWindow* p, const TGWindow* main,
                     PlotSet& pset, ReferenceTraceList_t& ref,
                     calibration::Table* caltable = 0);

/** Function to bring up the math editor.  This was never completed, and
    no requirements, specification, or design documents exist for it so
    it has been abandoned.  The menu item that called it has been deleted.
    @memo Math function editor.
    @param pset Plot set
    @param math Math function table
    @return true if changes were made
 ************************************************************************/
   Bool_t MathEditor (const TGWindow* p, const TGWindow* main,
                     PlotSet& pset, MathTable_t& math);



/** Reference traces dialog box.
    @memo Dialog box for setting the reference traces.
    @author Written May 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGReferenceDialog : public TLGTransientFrame {
   protected:
      /// Plot types
      PlotSet* 		fPlotList;
      /// Calibration table
      calibration::Table*	fCal;
      /// Original reference traces list
      ReferenceTraceList_t*	fRef;
      /// Temporary reference list
      ReferenceTraceList_t	fTemp;
      /// Currently selected reference trace
      Int_t 		fCurRef; 
      /// Currently selected graph
      TString		fCurGraph;
      /// Currently selected A channel
      TString		fCurAchn;
      /// Currently selected B channel
      TString		fCurBchn;
      /// Return parameter
      Bool_t*		fOk;
   
      /// Reference list box frame
      TGCompositeFrame* fFRef;
      /// Trace selection frame
      TGCompositeFrame* fFTrace;
      /// Button frame
      TGCompositeFrame* fFButton;
      /// 1st line
      TGCompositeFrame* fF1;
      /// 2nd line
      TGCompositeFrame* fF2;
      /// 3rd line
      TGCompositeFrame* fF3;
      /// Numereous layout hints
      TGLayoutHints*	fL[6];
      /// Reference selection list box
      TGListBox*	fRefSel;
      /// Graph label
      TGLabel*		fGraphLabel;
      /// A channel label
      TGLabel*		fALabel;
      /// B channel label
      TGLabel*		fBLabel;
      /// Graph selection
      TGComboBox*	fGraph;
      /// A channel selection
      TGComboBox*	fAchn;
      /// B channel selection
      TGComboBox*	fBchn;
      /// New button
      TGButton*		fNewButton;
      /// Update button
      TGButton*		fUpdateButton;
      /// Update all button
      TGButton*		fUpdateAllButton;
      /// Clear button
      TGButton*		fClearButton;
      /// Ok button
      TGButton*		fOkButton;
      /// Cancel button
      TGButton*		fCancelButton;
   
   public:
      TLGReferenceDialog (const TGWindow *p, const TGWindow *main,
                        PlotSet& pset, ReferenceTraceList_t& ref,
                        Bool_t& ret, calibration::Table* caltable = 0);
      virtual ~TLGReferenceDialog ();
      virtual void CloseWindow();
   
      virtual void BuildRefList ();
      virtual void BuildPlotType (Int_t level);
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   };



//@}
}

#endif // _LIGO_TLGMATH_H

