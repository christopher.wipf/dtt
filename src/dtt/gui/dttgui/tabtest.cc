VCSID("$(#)$Id$");
#include <TROOT.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TEnv.h>
#include <TGButton.h>
#include <TGFrame.h>
#include <TTimer.h>
#include <TVirtualX.h>
#include "TLGMultiTab.hh"
#include <iostream>

   using namespace dttgui;
   using namespace std;


   class MainMainFrame : public TGMainFrame {
   private:
      TGCompositeFrame*	 fF[3];
      TGLayoutHints*     fL1;
      TLGMultiTab* 	 fTabs[4];
      TGButton*		 fExitButton;
   
   public:
      MainMainFrame (const TGWindow *p, UInt_t w, UInt_t h);
      virtual ~MainMainFrame();
   
      virtual void CloseWindow();
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, Long_t);
   };


   MainMainFrame::MainMainFrame (const TGWindow *p, UInt_t w, UInt_t h)
   : TGMainFrame (p, w, h, kMainFrame | kVerticalFrame)
   {
   /*------------------------------------------------------------------------*/
   /* Set font for label text.                                               */
   /*------------------------------------------------------------------------*/
   
      FontStruct_t labelfont;
      labelfont = gClient->GetFontByName 
         (gEnv->GetValue
         ("Gui.NormalFont",
         "-adobe-helvetica-medium-r-*-*-12-*-*-*-*-*-iso8859-1"));
   
   /* Define new graphics context. Only the fields specified in
      fMask will be used. */
   
      GCValues_t   gval;
      gval.fMask = kGCForeground | kGCFont;
      gval.fFont = gVirtualX->GetFontHandle(labelfont);
   
      fF[0] = new TGHorizontalFrame (this, 200, 300);
      fF[1] = new TGVerticalFrame (fF[0], 200, 300);
      fF[2] = new TGVerticalFrame (fF[0], 200, 300);
      fL1 = new TGLayoutHints (kLHintsTop | kLHintsLeft, 6, 6, 6, 6);
      AddFrame (fF[0], fL1);
      fF[0]->AddFrame (fF[1], fL1);
      fF[0]->AddFrame (fF[2], fL1);
      for (int i = 0; i < 4; i++) {
         fTabs[i] = new TLGMultiTab (fF[i/2+1], 200, 200, i+1);
         fF[i/2+1]->AddFrame (fTabs[i], fL1);
      }
   
      // add tabs
      TGCompositeFrame* cf;
      TGButton*	b;
      cf = fTabs[0]->AddTab ("First");
      b = new TGTextButton (cf, "  Test  ", 10);
      cf->AddFrame (b, new TGLayoutHints (kLHintsTop | kLHintsCenterX, 20, 20, 20, 20));
      cf = fTabs[0]->AddTab ("Second");
      cf = fTabs[0]->AddTab ("Third");
   
      cf = fTabs[1]->AddTab ("First");
      b = new TGTextButton (cf, "  Test  ", 10);
      cf->AddFrame (b, new TGLayoutHints (kLHintsTop | kLHintsCenterX, 20, 20, 20, 20));
      cf = fTabs[1]->AddTab ("Second");
      cf = fTabs[1]->AddTab ("Third");
      cf = fTabs[1]->AddTab ("Fourth");
      cf = fTabs[1]->AddTab ("Fivth");
   
      cf = fTabs[2]->AddTab ("First");
      b = new TGTextButton (cf, "  Test  ", 10);
      cf->AddFrame (b, new TGLayoutHints (kLHintsTop | kLHintsCenterX, 20, 20, 20, 20));
      cf = fTabs[2]->AddTab ("Second");
      cf = fTabs[2]->AddTab ("Third");
      cf = fTabs[2]->AddTab ("Fourth");
      cf = fTabs[2]->AddTab ("Sixth");
      cf = fTabs[2]->AddTab ("Seventh");
      cf = fTabs[2]->AddTab ("Eight");
      cf = fTabs[2]->AddTab ("Ninth");
      cf = fTabs[2]->AddTab ("Tenth");
   
      cf = fTabs[3]->AddTab ("First");
      b = new TGTextButton (cf, "  Test  ", 10);
      cf->AddFrame (b, new TGLayoutHints (kLHintsTop | kLHintsCenterX, 20, 20, 20, 20));
      cf = fTabs[3]->AddTab ("Second");
      cf = fTabs[3]->AddTab ("Third");
      cf = fTabs[3]->AddTab ("Fourth");
      cf = fTabs[3]->AddTab ("Sixth");
      cf = fTabs[3]->AddTab ("Seventh");
      cf = fTabs[3]->AddTab ("Eight");
      cf = fTabs[3]->AddTab ("Ninth");
      cf = fTabs[3]->AddTab ("Tenth");
      cf = fTabs[3]->AddTab ("Eleventh");
      cf = fTabs[3]->AddTab ("Twelvth");
   
      // add buttons
      fExitButton = new TGTextButton (this, " Exit ", 1);
      fExitButton->Associate (this);
      AddFrame (fExitButton, fL1);
   
      SetWindowName ("Tab Test");
      SetIconName ("Tab Test");
      SetClassHints ("Tab Test", "Tab Test");
      SetWMPosition (0,0);
      MapSubwindows ();
   
      // we need to use GetDefault...() to initialize the layout algorithm...
      Resize (GetDefaultSize());
      MapWindow ();
   }


   MainMainFrame::~MainMainFrame()
   {
      delete fF[0];
      delete fF[1];
      delete fF[2];
      delete fTabs[0];
      delete fTabs[1];
      delete fTabs[2];
      delete fTabs[3];
      delete fExitButton;
      delete fL1;
   }



   Bool_t MainMainFrame::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      switch (GET_MSG (msg)) {
         case kC_COMMAND:
            {
               switch (GET_SUBMSG (msg)) {
                  case kCM_BUTTON:
                     {
                        switch (parm1) {
                           // exit button
                           case 1:
                              {
                                 CloseWindow();
                                 break;
                              }
                        }
                        break;
                     }
               }
            }
            break;
      }
      return kTRUE;
   }


   void MainMainFrame::CloseWindow()
   {
      TGMainFrame::CloseWindow();
      gApplication->Terminate(0);
   }


   TROOT root("GUI", "Multi Tab Test");



   int main(int argc, char **argv)
   {
      TApplication theApp ("Multi tab test", &argc, argv);
      MainMainFrame mainWindow (gClient->GetRoot(), 600, 240);
      theApp.Run();
   
      return 0;
   }
