#include <TVirtualX.h>

#include "TLGProgressBar.hh"

namespace dttgui {
   using namespace std;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGProgressBar (progress bar)				        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   GContext_t TLGProgressBar::fgBarGC = (GContext_t)-1;

//______________________________________________________________________________
   TLGProgressBar::TLGProgressBar (const TGWindow* p, Int_t w, Int_t h,
				   float& progress)
   : TGFrame (p, w, h, kRaisedFrame|kDoubleBorder), fC (&progress)
   {
      if (fgBarGC == (GContext_t)-1) {
         ULong_t col;
         gClient->GetColorByName ("green", col);
         GCValues_t gval;
         gval.fMask = kGCForeground | kGCBackground | kGCFillStyle  | 
            kGCGraphicsExposures;
         gval.fBackground = col;
         gval.fForeground = col;
         gval.fFillStyle  = kFillSolid;
         gval.fGraphicsExposures = kFALSE;
         //fgBarGC = gVirtualX->CreateGC (gClient->GetRoot()->GetId(), &gval);
         fgBarGC = gClient->GetGC(&gval)->GetGC();
      }
   }

//______________________________________________________________________________
   TGDimension TLGProgressBar::GetDefaultSize() const
   {
      return TGDimension (fWidth, fHeight);
   }

//______________________________________________________________________________
   void TLGProgressBar::Update()
   {
      gClient->NeedRedraw (this);
   }

//______________________________________________________________________________
   void TLGProgressBar::DoRedraw()
   {
      float c = *fC;
      if (c < 0.) c = 0.;
      if (c > 1.) c = 1.;
      TGFrame::DoRedraw();
      if (c > 0) 
         gVirtualX->FillRectangle (fId, fgBarGC, 2, 2, 
				   UInt_t(c * (fWidth - 4)), fHeight - 4);
   }


}


