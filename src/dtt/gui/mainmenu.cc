/*---------------------------------------------------------------------------*/
/*                                                                           */
/* Module Name:  mainmenu						     */
/*                                                                           */
/* Module Description:  ROOT gui for main diagnostics test menu		     */
/*                                                                           */
/*---------------------------------------------------------------------------*/

/** @name mainmenu
    This is a helper program to start the diagnostics tools. It displayes
    a button array for selecting tools.
   
    @memo Diagnostics tool menu
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/

#ifndef __EXTENSIONS__
#define __EXTENSIONS__
#endif

/* Header File List */

#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <math.h>
#include <ctype.h>
#include <unistd.h>
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/time.h>

#include <TROOT.h>
#include <TApplication.h>
#include <TControlBar.h>
#include <TVirtualX.h>
#include <TTimer.h>
#include <TGListBox.h>
#include <TGClient.h>
#include <TGString.h>
#include <TGFrame.h>
#include <TGLabel.h>
#include <TGButton.h>
#include <TGTextEntry.h>
#include <TGMsgBox.h>
#include <TGMenu.h>
#include <TGComboBox.h>
#include <TGFileDialog.h>
#include <TSystem.h>
#include <TString.h>
#include <TEnv.h>
#include "TLGEntry.hh"
#include "gdsmain.h"
#include "confinfo.h"
#include "launch_client.hh"

   using namespace dttgui;
   using namespace std;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Constants	                                                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   // port for configuration information
   const int _MAGICPORT = 5355;

   const TString aboutmsg = 
   "LIGO))) Laser Interferometer Gravitational-wave Observatory\n"
   "Global Diagnostics System\n\n"
   "by Daniel Sigg, 1998 - 2003, copyright\n"
   "version " VERSION "\n\n"
   "http://www.ligo.caltech.edu\n"
   "http://www.ligo-wa.caltech.edu/gds";

   // verbose mode
   Bool_t verbose = kFALSE;

   const Int_t kMaxMenuLaunchSub = 100;
   const Int_t kMaxButtonLaunch = 10;

   const Long_t kX11WatchdogInterval = 120*1000; // 2 minutes

   enum EMenuCommandIdentifiers {
   M_FILE_CONF,
   M_FILE_INFO,
   M_FILE_EXIT,
   
   M_DISPLAY_0,
   M_DISPLAY_1,
   M_DISPLAY_2,
   M_DISPLAY_REMOTE,
   
   M_HELP_MANUAL,
   M_HELP_ABOUT,
   
   M_LAUNCH_DV,
   M_LAUNCH_FFT,
   M_LAUNCH_SS,
   M_LAUNCH_SR,
   M_LAUNCH_TS,
   M_LAUNCH_DMTView,
   M_LAUNCH_FOTON,
   M_LAUNCH_AWG,
   M_LAUNCH_LIDAX,
   M_LAUNCH_CMD,
   M_LAUNCH_OTHERS
   };

   const int BTNdv = 10;
   const int BTNawg = 1;
   const int BTNfft = 2;
   const int BTNss = 3;
   const int BTNsr = 4;
   const int BTNts = 5;
   const int BTNtp = 6;
   const int BTNcmdline = 7;
   const int BTNmanual = 8;
   const int BTNexit = 9;
   const int NDSType = 11;
   const int BTNdmtview = 12;
   const int BTNfoton = 13;
   const int BTNlidax = 14;
   const int BTNothers = 20;

   //const int BTNfile = 2;
   const int BTNok = 1;
   const int BTNcancel = 0;

   char		archive[1024];
   const char*	_startDV = "dataviewer";
   const char*	_startFFT = "fft";
   const char*	_startSweptsine = "sweptsine";
   const char*	_startSineresp = "sineresp";
   const char*	_startTimeseries = "timeseries";
   const char*	_startDMTView = "dmtviewer";
   const char*	_startFoton = "foton";
   const char*	_startAWG = "awg";
   const char*	_startTP = "testpoint";
   const char*	_startCMDLINE = "diagtest";
   const char*	_startMANUAL = "diagman";
   const char*	_startMain = "main";
   const char*	_startNDS = "ndslaunch";
   const char*	_startLidax = "lidax";


   enum ENdsHost {
   kNdsDefault = 0,
   //kNdsFile = 1,
   kNdsUser = 1
   };



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// ConfigDiag_t	                                                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class ConfigDiag_t {
   public:
      ENdsHost		fNds;
      TString		fNdsFilename;
      TString		fNdsName;
      Int_t		fNdsPort;
   
      ConfigDiag_t () 
      : fNds (kNdsDefault), fNdsFilename (""), fNdsName ("fb0"), 
      fNdsPort (8088) {
      }
   };


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// MainMainFrame                                                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class MainMainFrame : public TGMainFrame {
   private:
      // menu
      TGMenuBar		*fMenuBar;
      TGPopupMenu	*fMenuFile;
      TGPopupMenu       *fMenuLaunch;
      TGPopupMenu	*fMenuLaunchSub[kMaxMenuLaunchSub];
      TGPopupMenu	*fMenuDisplay;
      TGPopupMenu	*fMenuHelp;
      TGLayoutHints	*fMenuBarLayout;
      TGLayoutHints	*fMenuBarItemLayout; 
      TGLayoutHints	*fMenuBarHelpLayout;
   
      // buttons
      TGCompositeFrame	*fButtonFrame;
      TGButton		*fDVButton;
      TGButton		*fAWGButton;
      TGButton		*fTPButton;
      TGButton		*fFFTButton;
      TGButton		*fSSButton;
      TGButton		*fSRButton;
      TGButton		*fTSButton;
      TGButton		*fDMTViewButton;
      TGButton		*fFotonButton;
      TGButton		*fLidaxButton;
      TGButton		*fCMDLINEButton;
      TGButton		*fMANUALButton;
      TGButton		*fExitButton;
      TGLayoutHints	*fButtonLayout;
      TGLayoutHints	*fButtonFrameLayout;
      TString		 fOtherButtonTitle[kMaxButtonLaunch];
      TGButton		*fOtherButton[kMaxButtonLaunch];
   
      // display settings
      TString		fRemote;
      int		fScreen;
      // Remote programs
      launch_client	fLaunch;
      // configuration
      ConfigDiag_t	fConf;
      // X11 watchdog timer
      TTimer*		fX11Watchdog;
   
   public:
      MainMainFrame (const TGWindow *p, UInt_t w, UInt_t h);
      virtual ~MainMainFrame();
   
      TString GetDisplay() const;
   
      virtual void CloseWindow();
      virtual Bool_t HandleTimer (TTimer* timer);
      virtual Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t);
      virtual Bool_t ProcessMenu (Long_t parm1, Long_t);
      virtual Bool_t ProcessButton (Long_t parm1, Long_t);
   };


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Secondary button list                                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class ButtonListDiag : public TGTransientFrame {
   private:
      // buttons
      TGCompositeFrame	*fButtonFrame;
      TGLayoutHints	*fButtonLayout;
      TGLayoutHints	*fButtonFrameLayout;
      TGButton		*fButton[3*kMaxButtonLaunch];
      launch_client&	 fLaunch;
      int		*fRet;
   
   public:
      ButtonListDiag (const TGWindow *p, const TGWindow *main,
                     const char* t, launch_client& launch, int& ret);
      virtual ~ButtonListDiag ();
      virtual void CloseWindow();
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, Long_t);
   };



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// RemoteHostDiag	                                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class RemoteHostDiag : public TGTransientFrame {
   private:
      TGCompositeFrame	*fTextFrame;
      TGLabel		*fRemoteTitle;
      TGTextBuffer	*fRemoteBuffer;
      TGTextEntry	*fRemoteText;
   
      TGCompositeFrame	*fButtonFrame;
      TGButton		*fOkButton;
      TGButton		*fCancelButton;
   
      TGLayoutHints	*fL1;
      TGLayoutHints	*fL2;
      TGLayoutHints	*fL3;
      TGLayoutHints	*fL4;
      TGLayoutHints	*fL5;
      TGLayoutHints	*fL6;
   
      TString*		fRemoteHost;
   
   public:
      RemoteHostDiag (const TGWindow *p, const TGWindow *main,
                     TString& remoteHost);
      virtual ~RemoteHostDiag ();
      virtual void CloseWindow();
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, Long_t);
   };


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// ConfigDiag	                                                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class ConfigDiag : public TGTransientFrame {
   private:
      TGGroupFrame*	fNdsGroup;
      TGCompositeFrame*	fNdsLine[3];
      TGButton*		fNdsType[2];
      //TGTextEntry*	fNdsFile;
      TGTextEntry*	fNdsServer;
      TGLabel*		fNdsPortLabel;
      TGTextEntry*	fNdsPort;
      TGCompositeFrame*	fBtnFrame;
      TGButton*		fOkButton;
      TGButton*		fCancelButton;
      //TGButton*	fFileButton;
      TGLayoutHints*	fL1;
      TGLayoutHints*	fL2;
      TGLayoutHints*	fL3;
      TGLayoutHints*	fL4;
      TGLayoutHints*	fL5;
      TGLayoutHints*	fL6;
      TGLayoutHints*	fL7;
   
      ConfigDiag_t*	fConf;
      ConfigDiag_t	fConfTemp;
   
   public:
      ConfigDiag (const TGWindow *p, const TGWindow *main,
                 ConfigDiag_t& cfg);
      virtual ~ConfigDiag ();
      virtual void CloseWindow();
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, Long_t);
   };


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Forwards	                                                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   static Bool_t isFrame (const char* filename);
   static Bool_t isValidPath (const char* filename);
   static string trim (const char* p);



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// MainMainFrame                                                        //
//                                                                      //
// Main window           	      		                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   MainMainFrame::MainMainFrame (const TGWindow *p, UInt_t w, UInt_t h)
   : TGMainFrame (p, w, h), fRemote ("$DISPLAY"), fScreen (0)
   {
   
      // check DISPLAY/REMOTEHOST environment
      if ((gSystem->Getenv ("DISPLAY") == 0) &&
         (gSystem->Getenv ("REMOTEHOST") != 0)) {
         fRemote = "$REMOTEHOST";
      }
   
      // check DMTLAUNCHSERVER environment
      const char* launchenv = gSystem->Getenv ("DMTLAUNCHSERVER");
      if (launchenv && *launchenv) {
         // scan environment variable for server names
         char* s = new (nothrow) char [strlen (launchenv) + 10];
         strcpy (s, launchenv);
         char* last;
         char* p = strtok_r (s, ",", &last);
         while (p) {
            string addr = trim (p);
            p = strtok_r (0, ",", &last);
            // add launch server
            fLaunch.AddServer (addr.c_str());
         }
         delete [] s;
      }
   
      // Main menu
      fMenuBarLayout = new TGLayoutHints(kLHintsTop | kLHintsLeft | 
                           kLHintsExpandX, 0, 0, 1, 1);
      fMenuBarItemLayout = new TGLayoutHints(kLHintsTop | kLHintsLeft, 0, 4, 0, 0);
      fMenuBarHelpLayout = new TGLayoutHints(kLHintsTop | kLHintsRight);
   
      // File menu
      fMenuFile = new TGPopupMenu (p);
      fMenuFile->AddEntry ("&Setup...", M_FILE_CONF);
      fMenuFile->AddEntry ("&Configuration info...", M_FILE_INFO);
      fMenuFile->AddSeparator ();
      fMenuFile->AddEntry ("E&xit", M_FILE_EXIT);
      fMenuFile->Associate (this);
   
      // Launch menu
      fMenuLaunch = new TGPopupMenu (p);
      fMenuLaunch->Associate (this);
      for (int i = 0; i < kMaxMenuLaunchSub; ++i) {
         fMenuLaunchSub[i] = 0;
      }
      // Standard launch sub menu
      fMenuLaunchSub[0] = new TGPopupMenu (p);
      fMenuLaunchSub[0]->Associate (this);
      fMenuLaunchSub[0]->AddEntry ("&Data Viewer", M_LAUNCH_DV);
      fMenuLaunchSub[0]->AddSeparator ();
      fMenuLaunchSub[0]->AddEntry ("F&ourier Tools", M_LAUNCH_FFT);
      fMenuLaunchSub[0]->AddEntry ("&Swept Sine Measurement", M_LAUNCH_SS);
      fMenuLaunchSub[0]->AddEntry ("Sine &Response Measurement", 
                           M_LAUNCH_SR);
      fMenuLaunchSub[0]->AddEntry ("&Triggered Time Series Measurement", 
                           M_LAUNCH_TS);
      fMenuLaunchSub[0]->AddSeparator ();
   #ifndef GDS_NODMT
      fMenuLaunchSub[0]->AddEntry ("&DMT Viewer", M_LAUNCH_DMTView);
   #endif
      fMenuLaunchSub[0]->AddEntry ("&Foton", M_LAUNCH_FOTON);
      fMenuLaunchSub[0]->AddEntry ("&Lidax", M_LAUNCH_LIDAX);
      fMenuLaunchSub[0]->AddSeparator ();
   #ifdef GDS_ONLINE
      fMenuLaunchSub[0]->AddEntry ("&Arbitrary Waveform Generator", 
                           M_LAUNCH_AWG);
      fMenuLaunchSub[0]->AddSeparator ();
   #endif
      fMenuLaunchSub[0]->AddEntry ("&Command Line Interpreter", 
                           M_LAUNCH_CMD);
      fMenuLaunch->AddPopup ("&Online diagnostics", fMenuLaunchSub[0], 0);
      // Add all other launch submenus
      string oldtitle = "";
      TGPopupMenu* pop = 0;
      int submenu = 1;
      for (int i = 0; i < fLaunch.size(); ++i) {
         string title = fLaunch[i].fTitle + " (" + 
            fLaunch[i].fAddr + ")";
         if (!pop || 
            (strcasecmp (oldtitle.c_str(), title.c_str()) != 0)) {
            if (submenu >= kMaxMenuLaunchSub) {
               break;
            }
            pop = new TGPopupMenu (p);
            pop->Associate (this);
            fMenuLaunchSub[submenu] = pop;
            fMenuLaunch->AddPopup (title.c_str(), pop, 0);
            oldtitle = title;
            ++submenu;
         }
         pop->AddEntry (fLaunch[i].fProgram.c_str(), 
                       M_LAUNCH_OTHERS + i);
      }
   
      // Display menu
      fMenuDisplay = new TGPopupMenu (p);
      fMenuDisplay->AddEntry ("Display &0", M_DISPLAY_0);
      fMenuDisplay->AddEntry ("Display &1", M_DISPLAY_1);
      fMenuDisplay->AddEntry ("Display &2", M_DISPLAY_2);
      fMenuDisplay->AddSeparator ();
      fMenuDisplay->AddEntry ("&Remote...", M_DISPLAY_REMOTE);
      fMenuDisplay->CheckEntry (M_DISPLAY_0);
      fMenuDisplay->Associate (this);
   
      // Help menu
      fMenuHelp = new TGPopupMenu (p);
      fMenuHelp->AddEntry ("&Manual", M_HELP_MANUAL);
      fMenuHelp->Associate (this);
      fMenuHelp->AddEntry ("&About", M_HELP_ABOUT);
      fMenuHelp->Associate (this);
   
      // Put menu items onto menu bar
      fMenuBar = new TGMenuBar (this, 1, 1, kHorizontalFrame | kRaisedFrame);
      fMenuBar->AddPopup ("&File", fMenuFile, fMenuBarItemLayout);
      fMenuBar->AddPopup ("&Launch", fMenuLaunch, fMenuBarItemLayout);
      fMenuBar->AddPopup ("&Display", fMenuDisplay, fMenuBarItemLayout);
      fMenuBar->AddPopup ("&Help", fMenuHelp, fMenuBarHelpLayout);
      AddFrame (fMenuBar, fMenuBarLayout);
   
      // Buttons
      fButtonFrame = new TGCompositeFrame (this, 600, 20, 
                           kVerticalFrame | kSunkenFrame);
      fButtonFrameLayout =  new TGLayoutHints (kLHintsBottom | kLHintsExpandX, 
                           0, 0, 1, 0);
      AddFrame (fButtonFrame, fButtonFrameLayout);
      fButtonLayout = new TGLayoutHints (kLHintsTop | kLHintsLeft | kLHintsExpandX, 
                           2, 2, 2, 2);
   
      fDVButton = new TGTextButton (fButtonFrame, 
                           "&Data Viewer", BTNdv);
      fDVButton->Associate (this);
      fButtonFrame->AddFrame (fDVButton, fButtonLayout);
      fFFTButton = new TGTextButton (fButtonFrame, 
                           "F&ourier Tools", BTNfft);
      fFFTButton->Associate (this);
      fButtonFrame->AddFrame (fFFTButton, fButtonLayout);
      fSSButton = new TGTextButton (fButtonFrame, 
                           "&Swept Sine Measurement", BTNss);
      fSSButton->Associate (this);
      fButtonFrame->AddFrame (fSSButton, fButtonLayout);
      fSRButton = new TGTextButton (fButtonFrame, 
                           "Sine &Response Measurement", BTNsr);
      fSRButton->Associate (this);
      fButtonFrame->AddFrame (fSRButton, fButtonLayout);
      fTSButton = new TGTextButton (fButtonFrame, 
                           "&Triggered Time Series Measurement", BTNts);
      fTSButton->Associate (this);
      fButtonFrame->AddFrame (fTSButton, fButtonLayout);
   #ifndef GDS_NODMT
      fDMTViewButton = new TGTextButton (fButtonFrame, 
                           "&DMT Viewer", BTNdmtview);
      fDMTViewButton->Associate (this);
      fButtonFrame->AddFrame (fDMTViewButton, fButtonLayout);
   #else
      fDMTViewButton = 0;
   #endif
      fFotonButton = new TGTextButton (fButtonFrame, 
                           "&Foton", BTNfoton);
      fFotonButton->Associate (this);
      fButtonFrame->AddFrame (fFotonButton, fButtonLayout);
      fLidaxButton = new TGTextButton (fButtonFrame, 
                           "&Lidax", BTNlidax);
      fLidaxButton->Associate (this);
      fButtonFrame->AddFrame (fLidaxButton, fButtonLayout);
   #ifdef GDS_ONLINE
      fAWGButton = new TGTextButton (fButtonFrame, 
                           "&Arbitrary Waveform Generator", BTNawg);
      fAWGButton->Associate (this);
      fButtonFrame->AddFrame (fAWGButton, fButtonLayout);
      fTPButton = new TGTextButton (fButtonFrame, 
                           "Test &Point Selection", BTNtp);
      fTPButton->Associate (this);
      fButtonFrame->AddFrame (fTPButton, fButtonLayout);
      fTPButton->SetState (kButtonDisabled);
   #else
      fAWGButton = 0;
      fTPButton = 0;
   #endif
      fCMDLINEButton = new TGTextButton (fButtonFrame, 
                           "&Command Line Interpreter", BTNcmdline);
      fCMDLINEButton->Associate (this);
      fButtonFrame->AddFrame (fCMDLINEButton, fButtonLayout);
   
      // Add all other launch buttons
      for (int i = 0; i < kMaxButtonLaunch; ++i) {
         fOtherButton[i] = 0;
      }
      oldtitle = "";
      int subbutton = 0;
      for (int i = 0; i < fLaunch.size(); ++i) {
         string title = fLaunch[i].fTitle;
         if (strcasecmp (oldtitle.c_str(), title.c_str()) != 0) {
            if (subbutton >= kMaxButtonLaunch) {
               break;
            }
            TString text = title.c_str();
            text += "...";
            fOtherButton[subbutton] = new TGTextButton (fButtonFrame, 
                                 text, BTNothers + subbutton);
            fOtherButton[subbutton]->Associate (this);
            fButtonFrame->AddFrame (fOtherButton[subbutton], fButtonLayout);
            fOtherButtonTitle[subbutton] = title.c_str();
            oldtitle = title;
            ++subbutton;
         }
      }
   
      // Help and exit at end
      fMANUALButton = new TGTextButton (fButtonFrame, 
                           "&Manual / Help / Getting Started", BTNmanual);
      fMANUALButton->Associate (this);
      fButtonFrame->AddFrame (fMANUALButton, fButtonLayout);
      fExitButton = new TGTextButton (fButtonFrame, "E&xit", BTNexit);
      fExitButton->Associate (this);
      fButtonFrame->AddFrame (fExitButton, fButtonLayout);
   
      SetWindowName ("Diagnostics");
      SetWMPosition (0,0);
      MapSubwindows ();
   
      // we need to use GetDefault...() to initialize the layout algorithm...
      Resize (GetDefaultSize());
      MapWindow ();
   
      // initialze X11 watchdog
      fX11Watchdog = new TTimer (this, kX11WatchdogInterval, kTRUE);
      fX11Watchdog->TurnOn();
   }

//______________________________________________________________________________
   MainMainFrame::~MainMainFrame()
   {
      delete fX11Watchdog;
   
      delete fDVButton;
      delete fAWGButton;
      delete fTPButton;
      delete fFFTButton;
      delete fSSButton;
      delete fSRButton;
      delete fTSButton;
      delete fDMTViewButton;
      delete fFotonButton;
      delete fLidaxButton;
      delete fExitButton;
      for (int i = 0; i < kMaxButtonLaunch; ++i) {
         delete fOtherButton[i];
      }
      delete fButtonFrame;
      delete fButtonLayout;
      delete fButtonFrameLayout;
      delete fMenuFile;
      for (int i = 0; i < kMaxMenuLaunchSub; ++i) {
         delete fMenuLaunchSub[i];
      }
      delete fMenuLaunch;
      delete fMenuDisplay;
      delete fMenuHelp;
      delete fMenuBar;
      delete fMenuBarLayout;
      delete fMenuBarItemLayout; 
      delete fMenuBarHelpLayout;
   }

//______________________________________________________________________________
   TString MainMainFrame::GetDisplay() const
   {
      // determine display settings
      int pos;
      TString remote = fRemote;
      // recognize environment variables
      while ((pos = remote.First ('$')) >= 0) {
         const char* p = (const char*)remote + pos + 1;
         int size = 0;
         while (isalnum (p[size]) || (p[size] == '_')) size++;
         TString envvar (p, size);
         TString envval = gSystem->Getenv (envvar);
         int pos2;
         int pos3;
         if (((pos2 = envval.First (':')) >= 0) &&
            ((pos3 = envval.Last ('.')) > pos2)) {
            envval.Remove (pos3);
         }
         remote.Remove (pos, size + 1);
         remote.Insert (pos, envval);
      }
      TString	screen = TString (". ");
      screen[1] = '0' + fScreen;
      if ((pos = remote.First (':')) < 0) {
         remote += TString (":0") + screen;
      }
      else if (remote.Last ('.') < pos) {
         remote += screen;
      }
      return remote;
   }

//______________________________________________________________________________
   Bool_t MainMainFrame::HandleTimer (TTimer* timer)
   {
      // quit, if X11 died
      if (timer == fX11Watchdog) {
         if (!gXDisplay) gApplication->Terminate(0);
         timer->Reset();
      }
   
      // return
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t MainMainFrame::ProcessMenu (Long_t parm1, Long_t parm2)
   {
      switch (parm1) {
         case M_FILE_CONF:
            {
               new ConfigDiag (fClient->GetRoot(), this, fConf);
               break;
            }
         case M_FILE_INFO:
            {
               TString msg;
               const char* const* conf = getConfInfo(0, 0.5);
               if ((conf == 0) || (*conf == 0)) {
                  msg = TString ("No configuration information available");
               }
               else {
                  msg = TString ("type ifo num address port/prognum progver sender");
                  while (*conf != 0) {
                     msg += TString ("\n") + TString (*conf);
                     conf++;
                  }
               }
               int retval;
               new TGMsgBox(fClient->GetRoot(), this, "Configuration", 
                           msg, 0, kMBOk, &retval);
               break;
            }
         case M_FILE_EXIT:
            {
               CloseWindow();
               break;
            }
         case M_DISPLAY_0:
            {
               fMenuDisplay->CheckEntry (M_DISPLAY_0);
               fMenuDisplay->UnCheckEntry (M_DISPLAY_1);
               fMenuDisplay->UnCheckEntry (M_DISPLAY_2);
               fScreen = 0;
               break;
            }
         case M_DISPLAY_1:
            {
               fMenuDisplay->UnCheckEntry (M_DISPLAY_0);
               fMenuDisplay->CheckEntry (M_DISPLAY_1);
               fMenuDisplay->UnCheckEntry (M_DISPLAY_2);
               fScreen = 1;
               break;
            }
         case M_DISPLAY_2:
            {
               fMenuDisplay->UnCheckEntry (M_DISPLAY_0);
               fMenuDisplay->UnCheckEntry (M_DISPLAY_1);
               fMenuDisplay->CheckEntry (M_DISPLAY_2);
               fScreen = 2;
               break;
            }
         case M_DISPLAY_REMOTE:
            {
               new RemoteHostDiag (fClient->GetRoot(), this, fRemote);
               break;
            }
         case M_HELP_MANUAL:
            {
               return ProcessButton (BTNmanual, 0);
               break;
            }
         case M_HELP_ABOUT:
            {
               int retval;
               new TGMsgBox (fClient->GetRoot(), this, "About", 
                            aboutmsg, 0, kMBOk, &retval);
               break;
            }
         case M_LAUNCH_DV:
            {
               return ProcessButton (BTNdv, 0);
            }
         case M_LAUNCH_FFT:
            {
               return ProcessButton (BTNfft, 0);
            }
         case M_LAUNCH_SS:
            {
               return ProcessButton (BTNss, 0);
            }
         case M_LAUNCH_SR:
            {
               return ProcessButton (BTNsr, 0);
            }
         case M_LAUNCH_TS:
            {
               return ProcessButton (BTNts, 0);
            }
         case M_LAUNCH_DMTView:
            {
               return ProcessButton (BTNdmtview, 0);
            }
         case M_LAUNCH_FOTON:
            {
               return ProcessButton (BTNfoton, 0);
            }
         case M_LAUNCH_LIDAX:
            {
               return ProcessButton (BTNlidax, 0);
            }
         case M_LAUNCH_AWG:
            {
               return ProcessButton (BTNawg, 0);
            }
         case M_LAUNCH_CMD:
            {
               return ProcessButton (BTNcmdline, 0);
            }
         default:
            {
               // start remote application
               int index = (int)parm1 - (int)M_LAUNCH_OTHERS;
               if ((index >= 0) && (index < fLaunch.size())) {
                  if (!fLaunch.Launch (index, GetDisplay())) {
                     int retval;
                     new TGMsgBox (fClient->GetRoot(), this, "Error", 
                                  "Unable to start remote application", 
                                  kMBIconStop, kMBOk, &retval);
                  }
               }
               break;
            }
      }
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t MainMainFrame::ProcessButton (Long_t parm1, Long_t parm2)
   {
      // determine display settings
      TString remote = GetDisplay();
      TString	display;
      if (parm1 == BTNdv) {
         display = TString (" -x ") + remote;
      }
      else if (parm1 == BTNfoton) {
         display = TString (" -d ") + remote;
      }
      else {
         display = TString (" -display ") + remote;
      }
   
      // determine remote program name
      TString	progname;
      Bool_t	usesnds = kFALSE;
      switch (parm1) {
         case BTNdv: 
            {
               progname = _startDV;
               usesnds = kTRUE;
               break;
            }
         case BTNawg: 
            {
               progname = _startAWG;
               break;
            }
         case BTNfft:
            {
               progname = _startFFT;
               usesnds = kTRUE;
               break;
            }
         case BTNss:
            {
               progname = _startSweptsine;
               usesnds = kTRUE;
               break;
            }
         case BTNsr:
            {
               progname = _startSineresp;
               usesnds = kTRUE;
               break;
            }
         case BTNts:
            {
               progname = _startTimeseries;
               usesnds = kTRUE;
               break;
            }
         case BTNdmtview:
            {
               progname = _startDMTView;
               break;
            }
         case BTNfoton:
            {
               progname = _startFoton;
               break;
            }
         case BTNlidax:
            {
               progname = _startLidax;
               break;
            }
         case BTNtp:
            {
               progname = _startTP;
               break;
            }
         case BTNcmdline:
            {
               progname = _startCMDLINE;
               usesnds = kTRUE;
               break;
            }
         case BTNmanual:
            {
               progname = _startMANUAL;
               break;
            }
         case BTNexit:
            {
               CloseWindow();
               return kTRUE;
            }
         default:
            {
               // start remote application
               int index = (int)parm1 - (int)BTNothers;
               if ((index >= 0) && (index < kMaxButtonLaunch)) {
                  string title = (const char*)fOtherButtonTitle[index];
                  // build a new button list
                  int ret;
                  new ButtonListDiag (fClient->GetRoot(), this, 
                                     title.c_str(), fLaunch, ret);
                  if (ret) {
                     return ProcessMenu (ret, 0);
                  }
               }
               return kTRUE;
            }
      }
      // NDS parameters
      TString 	ndsprm;
      if (usesnds && (fConf.fNds == kNdsUser)) {
         char buf[256];
         if (parm1 == BTNdv) {
            sprintf (buf, " -s %s -p %i", (const char*)fConf.fNdsName, 
                    fConf.fNdsPort);
         }
         else {
            sprintf (buf, " -n %s -m %i", (const char*)fConf.fNdsName, 
                    fConf.fNdsPort);
         }
         ndsprm = buf;
      }
   
      // now start external program
      char buf[16*1024];
      // if (usesnds && (fConf.fNds == kNdsFile)) {
         // ndsprm = (parm1 == BTNdv) ? " -s 0 -p #" : " -n 0 -m #";
         // TString verbprm = verbose ? " -v": "";
         // sprintf (buf, "%s%s %s \"%s%s%s%s%s\"%s &", archive, _startNDS, 
                 // (const char*)fConf.fNdsFilename, 
                 // archive, (const char*)progname, 
                 // (const char*)ndsprm, (const char*) verbprm,
                 // (const char*)display, (const char*) verbprm);
      // }
      // else {
      TString verbprm = (verbose && (parm1 != BTNdv)) ? " -v": "";
      sprintf (buf, "%s%s%s%s%s &", archive, (const char*)progname, 
              (const char*)ndsprm, (const char*) verbprm,
              (const char*)display);
      // }
      if (verbose) cout << "start : " << buf << endl;
      if (gSystem->Exec (buf) < 0) {
         TString msg = TString ("Unable to start ") + progname;
         int retval;
         new TGMsgBox(fClient->GetRoot(), this, "Error", 
                     msg, kMBIconStop, kMBOk, &retval);
      }
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t MainMainFrame::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      switch (GET_MSG (msg)) {
         case kC_COMMAND:
            {
               switch (GET_SUBMSG (msg)) {
                  case kCM_BUTTON:
                     {
                        return ProcessButton (parm1, parm2);
                     }
                  case kCM_MENU: 
                     {
                        return ProcessMenu (parm1, parm2);
                     }
               }
               break;
            }
      }
      return kTRUE;
   }

//______________________________________________________________________________
   void MainMainFrame::CloseWindow()
   {
      TGMainFrame::CloseWindow();
      gApplication->Terminate(0);
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// ButtonListDiag                                                       //
//                                                                      //
// Button list dialog box						//
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   ButtonListDiag::ButtonListDiag (const TGWindow *p, 
                     const TGWindow *main, const char* t,
                     launch_client& launch, int& ret)
   : TGTransientFrame (p, main, 10, 10), fLaunch (launch), fRet (&ret)
   {
      // Buttons
      fButtonFrame = new TGCompositeFrame (this, 600, 20, 
                           kVerticalFrame | kSunkenFrame);
      fButtonFrameLayout =  new TGLayoutHints (kLHintsBottom | 
                           kLHintsExpandX, 0, 0, 1, 0);
      AddFrame (fButtonFrame, fButtonFrameLayout);
      fButtonLayout = new TGLayoutHints (kLHintsTop | kLHintsLeft | 
                           kLHintsExpandX, 2, 2, 2, 2);
      // Add launch buttons
      for (int i = 0; i < 3*kMaxButtonLaunch; ++i) fButton[i] = 0;
      int subbutton = 0;
      for (int i = 0; i < fLaunch.size(); ++i) {
         if (strcasecmp (t, fLaunch[i].fTitle.c_str()) == 0) {
            if (subbutton + 1 >= 3*kMaxButtonLaunch) {
               break;
            }
            string text = fLaunch[i].fProgram + " (" +
               fLaunch[i].fAddr + ")";
            fButton[subbutton] = new TGTextButton (fButtonFrame, 
                                 text.c_str(), M_LAUNCH_OTHERS + i);
            fButton[subbutton]->Associate (this);
            fButtonFrame->AddFrame (fButton[subbutton], fButtonLayout);	
            ++subbutton;
         }
      }
      // Add cancel button
      fButton[subbutton] = new TGTextButton (fButtonFrame, 
                           "&Cancel", 0);
      fButton[subbutton]->Associate (this);
      fButtonFrame->AddFrame (fButton[subbutton], fButtonLayout);	
   
      // set dialog box title
      SetWindowName (t);
      SetIconName (t);
      SetClassHints (t, t);
   
      // resize & move to center
      MapSubwindows ();
      Resize (GetDefaultSize());
      Window_t wdum;
      int ax;
      int ay;
      gVirtualX->TranslateCoordinates (main->GetId(), GetParent()->GetId(),
                           ((TGFrame*)main)->GetWidth() - (fWidth >> 1), 
                           (int)(((TGFrame*)main)->GetHeight() * 0.7),
                           ax, ay, wdum);
      Move (ax, ay);
      SetWMPosition (ax, ay);
   
      SetMWMHints (kMWMDecorAll, kMWMFuncAll, kMWMInputModeless);
   
      MapWindow();
      fClient->WaitFor (this);
   }

//______________________________________________________________________________
   ButtonListDiag::~ButtonListDiag ()
   {
      for (int i = 0; i < 3*kMaxButtonLaunch; ++i) {
         delete fButton[i]; 
      }
      delete fButtonFrame;
      delete fButtonFrameLayout;
      delete fButtonLayout;
   }

//______________________________________________________________________________
   void ButtonListDiag::CloseWindow()
   {
      *fRet = 0;
      DeleteWindow();
   }

//______________________________________________________________________________
   Bool_t ButtonListDiag::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      if ((GET_MSG (msg) == kC_COMMAND) &&
         (GET_SUBMSG (msg) == kCM_BUTTON)) {
         // start remote application
         *fRet = (parm1 >= (int)M_LAUNCH_OTHERS) ? parm1 : 0;
         DeleteWindow();
      }
      return kTRUE;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// RemoteHostDiag                                                       //
//                                                                      //
// Remote host dialog box						//
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   RemoteHostDiag::RemoteHostDiag (const TGWindow *p, const TGWindow *main,
                     TString& remoteHost)
   : TGTransientFrame (p, main, 10, 10), fRemoteHost (&remoteHost)
   {
      // setup text entry
      fTextFrame = new TGCompositeFrame (this, 100, 20, kHorizontalFrame |
                           kSunkenFrame);
      fRemoteTitle = new TGLabel (fTextFrame, new TGString ("Remote host:"));
      fL1 = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 10, 5, 2, 2);
      fTextFrame->AddFrame (fRemoteTitle, fL1);
      fRemoteBuffer = new TGTextBuffer (80);
      fRemoteBuffer->AddText (0, *fRemoteHost);
      fRemoteText = new TGTextEntry (fTextFrame, fRemoteBuffer, 100);
      fRemoteText->Resize (250, fRemoteText->GetDefaultHeight());
      fRemoteText->Associate (this);
      fL2 =  new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 0, 10, 5, 5);
      fTextFrame->AddFrame (fRemoteText, fL2);
      fL6 = new TGLayoutHints (kLHintsTop | kLHintsExpandX, 2, 2, 2, 2);
      AddFrame (fTextFrame, fL6);
   
      // setup buttons
      fButtonFrame = new TGCompositeFrame 
         (this, 100, 20, kHorizontalFrame | kSunkenFrame);
   
      fOkButton = new TGTextButton (fButtonFrame, 
                           new TGHotString ("&Ok"), BTNok);
      fOkButton->Associate (this);
      fL3 = new TGLayoutHints (kLHintsTop | kLHintsLeft | kLHintsExpandX,
                           15, 2, 2, 2);
      fButtonFrame->AddFrame (fOkButton, fL3);
   
      fCancelButton = new TGTextButton (fButtonFrame, 
                           new TGHotString ("&Cancel"), BTNcancel);
      fCancelButton->Associate (this);
      fL4 = new TGLayoutHints (kLHintsTop | kLHintsLeft | kLHintsExpandX, 
                           15, 15, 2, 2);
      fButtonFrame->AddFrame (fCancelButton, fL4);
   
      fL5 = new TGLayoutHints (kLHintsBottom | kLHintsExpandX, 0, 0, 1, 0);
      AddFrame (fButtonFrame, fL5);
   
      // set dialog box title
      SetWindowName ("Display on Remote Host");
      SetIconName ("Display on Remote Host");
      SetClassHints ("DiagRemote", "DiagRemote");
   
      // resize & move to center
      MapSubwindows ();
      Resize (GetDefaultSize());
      Window_t wdum;
      int ax;
      int ay;
      // gVirtualX->TranslateCoordinates (main->GetId(), GetParent()->GetId(),
                           // main->GetWidth() - (fWidth >> 1), 
                           // (main->GetHeight() - fHeight) >> 1,
                           // ax, ay, wdum);
      gVirtualX->TranslateCoordinates (main->GetId(), GetParent()->GetId(),
                           ((TGFrame*)main)->GetWidth() - (fWidth >> 1), 
                           (((TGFrame*)main)->GetHeight() - fHeight) >> 1,
                           ax, ay, wdum);
      Move (ax, ay);
      SetWMPosition (ax, ay);
   
      SetMWMHints (kMWMDecorAll, kMWMFuncAll, kMWMInputModeless);
   
      MapWindow();
      fClient->WaitFor (this);
   }

//______________________________________________________________________________
   RemoteHostDiag::~RemoteHostDiag ()
   {
      delete fOkButton;
      delete fCancelButton;
      delete fButtonFrame;
   
      delete fRemoteTitle;
      delete fRemoteText;
      delete fTextFrame;
   
      delete fL1;
      delete fL2;
      delete fL3;
      delete fL4;
      delete fL5;
      delete fL6;
   }

//______________________________________________________________________________
   void RemoteHostDiag::CloseWindow()
   {
      DeleteWindow();
   }

//______________________________________________________________________________
   Bool_t RemoteHostDiag::ProcessMessage (Long_t msg, Long_t parm1, Long_t)
   {
      switch (GET_MSG (msg)) {
         case kC_COMMAND:
            {
               switch (GET_SUBMSG (msg)) {
                  case kCM_BUTTON:
                     {
                        switch (parm1) {
                           case BTNok:
                              {
                                 *fRemoteHost = fRemoteBuffer->GetString();
                                 // remove spaces
                                 while ((fRemoteHost->Length() > 0) &&
                                       ((*fRemoteHost)[0] == ' ')) {
                                    fRemoteHost->Remove (0, 1);
                                 }
                                 int pos;
                                 while (((pos = fRemoteHost->Length()) > 0) &&
                                       ((*fRemoteHost)[pos - 1] == ' ')) {
                                    fRemoteHost->Remove (pos - 1, 1);
                                 }
                                 DeleteWindow();
                                 break;
                              }
                           case BTNcancel:
                              {
                                 DeleteWindow();
                                 break;
                              }
                        }
                        break;
                     }
               }
               break;
            }
      }
      return kTRUE;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// ConfigDiag                                                           //
//                                                                      //
// Configuration information dialog box					//
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   ConfigDiag::ConfigDiag (const TGWindow *p, const TGWindow *main,
                     ConfigDiag_t& cfg)
   : TGTransientFrame (p, main, 10, 10), fConf (&cfg)
   {
      fConfTemp = *fConf;
   
      fL1 = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 0, 5, 2, 2);
      fL2 = new TGLayoutHints (kLHintsTop | kLHintsExpandX, 2, 2, 4, 0);
      fL3 = new TGLayoutHints (kLHintsTop | kLHintsLeft | kLHintsExpandX,
                           15, 2, 2, 2);
      fL4 = new TGLayoutHints (kLHintsTop | kLHintsLeft | kLHintsExpandX,
                           15, 15, 2, 2);
      fL5 = new TGLayoutHints (kLHintsBottom | kLHintsExpandX, 2, 2, 5, 5);
      fL6 = new TGLayoutHints (kLHintsTop | kLHintsExpandX, 5, 5, 5, 5);
      fL7 = new TGLayoutHints (kLHintsTop | kLHintsExpandX, 2, 2, 4, -6);
   
      // nds group
      fNdsGroup = new TGGroupFrame (this, "NDS selection");
      AddFrame (fNdsGroup, fL6);
      for (int i = 0; i < 2; i++) {
         static const char* text[3] = {"Default", "Server: "};
         fNdsLine[i] = new TGHorizontalFrame (this, 10, 10);
         fNdsGroup->AddFrame (fNdsLine[i], i == 2 ? fL7 : fL2);
         fNdsType[i] = new TGRadioButton (fNdsLine[i], text[i], NDSType + i);
         fNdsType[i]->Associate (this);
         fNdsLine[i]->AddFrame (fNdsType[i], fL1);
      }
   
      // fNdsFile = new TGTextEntry (fNdsLine[1], "", 100);
      // fNdsFile->Resize (350, fNdsFile->GetDefaultHeight());
      // fNdsFile->Associate (this);
      // fNdsLine[1]->AddFrame (fNdsFile, fL1);
      fNdsServer = new TGTextEntry (fNdsLine[1], "", 101);
      fNdsServer->Resize (230, 25);
      fNdsServer->Associate (this);
      fNdsLine[1]->AddFrame (fNdsServer, fL1);
      fNdsPortLabel = new TGLabel (fNdsLine[1], "   Port: ");
      fNdsLine[1]->AddFrame (fNdsPortLabel, fL1);
      fNdsPort = new TGTextEntry (fNdsLine[1], "", 102);
      fNdsPort->Resize (68, 25);
      fNdsPort->Associate (this);
      fNdsLine[1]->AddFrame (fNdsPort, fL1);
   
      // setup buttons
      fBtnFrame = new TGHorizontalFrame (this, 100, 20);
      AddFrame (fBtnFrame, fL5);
      fOkButton = new TGTextButton (fBtnFrame, 
                           new TGHotString ("&Ok"), BTNok);
      fOkButton->Associate (this);
      fBtnFrame->AddFrame (fOkButton, fL3);
      // fFileButton = new TGTextButton (fBtnFrame, 
                           // new TGHotString ("&File"), BTNfile);
      // fFileButton->Associate (this);
      // fBtnFrame->AddFrame (fFileButton, fL3);
      fCancelButton = new TGTextButton (fBtnFrame, 
                           new TGHotString ("&Cancel"), BTNcancel);
      fCancelButton->Associate (this);
      fBtnFrame->AddFrame (fCancelButton, fL4);
   
      // set initial values
      if ((fConfTemp.fNds < 0) || (fConfTemp.fNds > 2)) {
         fConfTemp.fNds = kNdsDefault;
      }
      fNdsType[fConfTemp.fNds]->SetState (kButtonDown);
      // fNdsFile->SetText (fConfTemp.fNdsFilename);
      // fNdsFile->SetState (kFALSE);
      fNdsServer->SetText (fConfTemp.fNdsName);
      char buf[64];
      sprintf (buf, "%i", fConfTemp.fNdsPort);
      fNdsPort->SetText (buf);
   
      // set dialog box title
      SetWindowName ("Configuration");
      SetIconName ("Configuration");
      SetClassHints ("DiagConf", "DiagConf");
   
      // resize & move to center
      MapSubwindows ();
      Resize (GetDefaultSize());
      Window_t wdum;
      int ax;
      int ay;
      gVirtualX->TranslateCoordinates (main->GetId(), GetParent()->GetId(),
                           ((TGFrame*)main)->GetWidth() - (fWidth >> 1), 
                           (((TGFrame*)main)->GetHeight() - fHeight) >> 1,
                           ax, ay, wdum);
      Move (ax, ay);
      SetWMPosition (ax, ay);
   
      SetMWMHints (kMWMDecorAll, kMWMFuncAll, kMWMInputModeless);
   
      MapWindow();
      fClient->WaitFor (this);
   }

//______________________________________________________________________________
   ConfigDiag::~ConfigDiag ()
   {
      delete fNdsType[0];
      delete fNdsType[1];
      //delete fNdsType[2];
      //delete fNdsFile;
      delete fNdsServer;
      delete fNdsPortLabel;
      delete fNdsPort;
      delete fNdsLine[0];
      delete fNdsLine[1];
      //delete fNdsLine[2];
      delete fNdsGroup;
      delete fOkButton;
      delete fCancelButton;
      //delete fFileButton;
      delete fBtnFrame;
      delete fL1;
      delete fL2;
      delete fL3;
      delete fL4;
      delete fL5;
      delete fL6;
      delete fL7;
   }

//______________________________________________________________________________
   void ConfigDiag::CloseWindow()
   {
      DeleteWindow();
   }

//______________________________________________________________________________
   Bool_t ConfigDiag::ProcessMessage (Long_t msg, Long_t parm1, Long_t)
   {
      if (GET_MSG (msg) == kC_COMMAND) {
         switch (GET_SUBMSG (msg)) {
            case kCM_BUTTON:
               switch (parm1) {
                  case BTNok:
                     {
                        fConfTemp.fNdsName = fNdsServer->GetText();
                        fConfTemp.fNdsPort = atoi (fNdsPort->GetText());
                                 // remove spaces
                        while ((fConfTemp.fNdsName.Length() > 0) &&
                              (fConfTemp.fNdsName[0] == ' ')) {
                           fConfTemp.fNdsName.Remove (0, 1);
                        }
                        int pos;
                        while (((pos = fConfTemp.fNdsName.Length()) > 0) &&
                              (fConfTemp.fNdsName[pos - 1] == ' ')) {
                           fConfTemp.fNdsName.Remove (pos - 1, 1);
                        }
                        *fConf = fConfTemp;
                        DeleteWindow();
                        break;
                     }
                  // case BTNfile:
                     // {
                     //    // file save as dialog
                        // TGFileInfo	info;
                        // info.fFilename = 0;
                        // info.fIniDir = 0;
                     // #if ROOT_VERSION_CODE >= ROOT_VERSION(3,1,6)
                     //    info.fFileTypes = const_cast<const char**>(gFrameTypes);
                     // #else
                        // info.fFileTypes = const_cast<char**>(gFrameTypes);
                     // #endif
                        // if (TLGFileDialog (this, info, kFDOpen, 0)) {
                        //    // check if a directory
                           // Long_t id, size, flags, modtime;
                           // id = size = flags = modtime = 0;
                           // gSystem->GetPathInfo (info.fFilename, &id, &size, 
                                                // &flags, &modtime);
                           // void* dir;
                           // TString filename = info.fFilename;
                           // if ((flags & 2) && 
                              // (dir = gSystem->OpenDirectory (filename))) {
                           //    // loop through dir and take first frame file
                              // const char* sub;
                              // while ((sub = gSystem->GetDirEntry (dir))) {
                                 // char* f = gSystem->ConcatFileName (
                                                      // filename, sub);
                              //    //cout << "sub = " << sub << endl;
                                 // if (isFrame (f)) {
                                    // filename = f;
                                    // delete [] f;
                                    // break;
                                 // }
                                 // delete [] f;
                              // }
                              // gSystem->FreeDirectory (dir);
                           // }
                        //    // check if frame file
                           // if (!isFrame (filename)) {
                              // char msg[1024];
                              // Int_t retval;
                              // sprintf (msg, "%s\nis not a valid Frame file",
                                      // (const char*)filename);
                              // new TGMsgBox(fClient->GetRoot(), this, 
                                          // "Error", msg, kMBIconStop, 
                                          // kMBOk, &retval);
                           // }
                           // // check for valid path
                           // else if (!isValidPath (filename)) {
                              // char msg[1024];
                              // Int_t retval;
                              // sprintf (msg, "Invalid path (%s)\n"
                                      // "Directory must contain a trailing zero",
                                      // (const char*)filename);
                              // new TGMsgBox(fClient->GetRoot(), this, 
                                          // "Error", msg, kMBIconStop, 
                                          // kMBOk, &retval);
                           // }
                           // // update filename
                           // else {
                              // fConfTemp.fNdsFilename = filename;
                           //    //fNdsFile->SetText (fConfTemp.fNdsFilename);
                              // fConfTemp.fNds = kNdsFile;
                           //    //fNdsType[1]->SetState (kButtonDown);
                              // fNdsType[0]->SetState (kButtonUp);
                              // fNdsType[2]->SetState (kButtonUp);
                           // }
                        // }
                     // #if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
                        // delete [] info.fFilename;
                     // #endif
                        // break;
                     // }
                  case BTNcancel:
                     {
                        DeleteWindow();
                        break;
                     }
               }
               break;
            case kCM_RADIOBUTTON:
               switch (parm1) {
                  case NDSType:
                     {
                        fConfTemp.fNds = kNdsDefault;
                        fNdsType[0]->SetState (kButtonDown);
                        fNdsType[1]->SetState (kButtonUp);
                        break;
                     }
                  case NDSType + 1:
                     {
                        fConfTemp.fNds = kNdsUser;
                        fNdsType[1]->SetState (kButtonDown);
                        fNdsType[0]->SetState (kButtonUp);
                        break;
                     }
               }
               break;
         }
      }
   
      return kTRUE;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// utility functions                                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   static Bool_t isFrame (const char* filename) 
   {
      FILE*	fd;
      char	buf[16];
      int	size;
   
      fd = fopen (filename, "r");
      if (fd == 0) {
         return kFALSE;
      }
      size = 0;
      while (!feof(fd) && (size < 6)) {
         buf[size++] = getc (fd);
      }
      fclose (fd);
      if (size != 6) {
         return kFALSE;
      }
      if (strncmp (buf, "IGWD", 4) != 0) {
         return kFALSE;
      }
      return (buf[5] > 0);
   }

//______________________________________________________________________________
   static Bool_t isValidPath (const char* filename) 
   {
      char 	framedir[1024];
      char*	p;
   
      /* construct directory name */
      strcpy (framedir, filename);
      p = strrchr (framedir, '/');
      if (p == NULL) {
         if (getcwd (framedir, sizeof (framedir)) == NULL) {
            return kFALSE;
         }
      }
      else {
         *p = 0;
      }
      /* check for trailing zero */
      if ((strlen (framedir) == 0) || 
         (framedir[strlen (framedir) - 1] != '0')) {
         return kFALSE;
      }
      else {
         return kTRUE;
      }
   }


   static string trim (const char* p)
   {
      while (isspace (*p)) p++;
      string s = p;
      while ((s.size() > 0) && isspace (s[s.size()-1])) {
         s.erase (s.size() - 1);
      }
      return s;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Main program                                                         //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TROOT root("GUI", "Diagnostics GUI");

//______________________________________________________________________________
   int main(int argc, char **argv)
   
   {
      strcpy (archive, "");
      if (strlen (gdsPath ("")) > 0) {
         strcpy (archive, gdsPath ("/startup/"));
      }
      for (int i = 1; i < argc; i++) {
         if ((strcmp (argv[i], "-a") == 0) && (i < argc) &&
            (argv[i+1][0] != '-')) {
            strcpy (archive, argv[i+1]);
            if ((strlen (archive) > 0) &&
               (archive[strlen (archive)-1] != '/')) {
               strcpy (archive + strlen (archive), "/");
            }
            argv[i+1] = (char*)""; // Trick to prevent root from changing directories!
         }
         else if (strcmp (argv[i], "-v") == 0) {
            verbose = kTRUE;
         }
      }
      TApplication theApp ("Diagnostics", &argc, argv);
      MainMainFrame mainWindow (gClient->GetRoot(), 600, 240);
      theApp.Run();
   
      return 0;
   }
