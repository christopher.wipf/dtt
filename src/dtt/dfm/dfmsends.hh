/* -*- mode: c++; c-basic-offset: 3; -*- */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: dfmsends						*/
/*                                                         		*/
/* Module Description: DFM low level interface (NDS)			*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 9Jan01   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: dfmsends.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_FDFMSENDS_H
#define _LIGO_FDFMSENDS_H


#include "dfmapi.hh"

namespace dfm {


/** @name DFM low level API (NDS)
    This header defines support methods for accessung the
    data flow manager.
   
    @memo DFM low level API (NDS)
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{

/** Basic API to NDS input
    
    @memo Basic tape input class.
 ************************************************************************/
   class dfmsends : public dfmapi {
   public:
      /// Default constructor
      dfmsends ();
   
      /// Open connection
      virtual bool open (const std::string& addr, bool read = true);
      /// Close connection
      virtual void close();
      /// Request a list of avaialble UDNs
      virtual bool requestUDNs (UDNList& udn);
      /// Request information asscociated with a UDN
      virtual bool requestUDNInfo (const UDN& udn, UDNInfo& info);
   
   protected:
      /// Server name
      std::string	fServer;
      /// Port number
      int		fPort;
   };

//@}

}

#endif // _LIGO_FDFMSENDS_H
