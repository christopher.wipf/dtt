#include "dfmfunc.hh"
#include "fname.hh"
#include <stdlib.h>

namespace dfm {
   using namespace std;
   using namespace fantom;



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// dfmfunc							        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   dfmfunc::dfmfunc () 
   {
   }

//______________________________________________________________________________
   bool dfmfunc::open (const std::string& addr, bool read)
   {
      fAddr = addr;
      return true;
   }

//______________________________________________________________________________
   void dfmfunc::close()
   {
      fAddr = "";
   }

//______________________________________________________________________________
   bool dfmfunc::requestUDNs (UDNList& udn)
   {
      //udn.clear();
      return true;
   }

//______________________________________________________________________________
   bool dfmfunc::requestUDNInfo (const UDN& udn, UDNInfo& info)
   {
      // add udn to name list
      namelist list;
      list.addName ((const char*)udn);
      if (list.empty() || 
         (list.front().getDevType() != dev_func)) {
         return false;
      }
      return true;
   }


}
