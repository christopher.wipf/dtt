#include "dfmsm.hh"
#include "dmtio.hh"
#include "framefast/framefast.hh"
#include "fname.hh"
#include <cstdlib>
#include <cstring>

namespace dfm {
   using namespace std;
   using namespace fantom;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// utility functions                                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   static string trim (const char* p)
   {
      while (isspace (*p)) p++;
      string s = p;
      while ((s.size() > 0) && isspace (s[s.size()-1])) {
         s.erase (s.size() - 1);
      }
      return s;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// dfmsm							        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   dfmsm::dfmsm () 
   {
   }

//______________________________________________________________________________
   bool dfmsm::open (const std::string& addr, bool read)
   {
      fAddr = addr;
      return true;
   }

//______________________________________________________________________________
   void dfmsm::close()
   {
      fAddr = "";
   }

//______________________________________________________________________________
   bool dfmsm::requestUDNs (UDNList& udn)
   {
      //udn.clear();
      // get environment variable
      const char* udnsm = getenv (kSmUDN);
      if (udnsm && *udnsm) {
         string pname = name_from_dev (dev_dmt);
         string sname = trim (udnsm);
         if (!sname.empty() && (sname[0] != '/')) {
            pname += "/";
         }
         pname += sname;
         udn.insert (UDNList::value_type (UDN (pname.c_str()), UDNInfo()));
      }
      udnsm = getenv (kSmUDNs);
      if (!udnsm || !*udnsm) {
         return true;
      }
      // scan environment variable for names
      char* udef = new (nothrow) char [strlen (udnsm) + 10];
      strcpy (udef, udnsm);
      char* last;
      char* p = strtok_r (udef, ":", &last);
      while (p) {
         string pname = name_from_dev (dev_dmt);
         string sname = trim (p);
         if (!sname.empty() && (sname[0] != '/')) {
            pname += "/";
         }
         pname += sname;
         if (!sname.empty() && (sname[0] != '/')) {
            sname.insert (0, "/");
         }
         udn.insert (UDNList::value_type (UDN (pname.c_str()), UDNInfo()));
         p = strtok_r (0, ":", &last);
      }
      delete [] udef;
      return true;
   }

//______________________________________________________________________________
   bool dfmsm::requestUDNInfo (const UDN& udn, UDNInfo& info)
   {
      // add udn to name list
      namelist list;
      list.addName ((const char*)udn);
      if (list.empty() || 
         (list.front().getDevType() != dev_dmt)) {
         return false;
      }
      // peek at a frame in the shared memory partition
      fantom::dmt_support* dmt = new (nothrow) dmt_support (false, 
                           list.front().getName(), list.front().getConf());
      if (dmt == 0) {
         return false;
      }
      framefast::framereader fr;
      if (!fr.loadFrame (dmt->readFrame (true))) {
         delete dmt;
         return false;
      }
      // get TOC
      const framefast::toc_t* toc = fr.getTOC();
      if (!toc) {
         fr.unload();
         delete dmt;
         return false;
      }
      // build UDN info
      frametype utype = FF;
      UDNInfo uinfo;
      uinfo.setType (utype);
      // loop over TOC to get channel names
      for (int j = 0; j < 5; ++j) {
         for (int i = 0; i < (int)toc->fNData[j]; i++) {
            // check if already in list
            fantom::chniter chn = uinfo.findChn (toc->fData[j][i].fName);
            if (chn == uinfo.endChn()) {
               // if not add
               std::pair <fantom::chniter, bool> ins = 
                  uinfo.insertChn (toc->fData[j][i].fName);
               // get rate from ADC structure
               if (ins.second) {
                  framefast::data_t adcinfo;
                  if (fr.getData (adcinfo, toc->fData[j][i].fPosition[0], 
                                 (framefast::datatype_t)j,
                                 framefast::frvect_t::fv_nocopy)) {
                     ins.first->SetRate (adcinfo.fADC.fSampleRate);
                  }
               }
            }
         }
      }
      // set time segment from start time only
      uinfo.insertDSeg (fr.starttime(), Interval (0.));
   
      info = uinfo;
      fr.unload();
      delete dmt;
      return true;
   }


}
