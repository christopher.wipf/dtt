/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: dfmsm							*/
/*                                                         		*/
/* Module Description: DFM low level interface (SM)			*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 9Jan01   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: dfmsm.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_FDFMSM_H
#define _LIGO_FDFMSM_H


#include "dfmapi.hh"


namespace dfm {


/** @name DFM low level API (SM)
    This header defines support methods for accessung the
    data flow manager.
   
    @memo DFM low level API (SM)
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{


/** Basic API to shared memory input/ouput
    
    @memo Basic shared memory input/ouput class.
 ************************************************************************/
   class dfmsm : public dfmapi {
   public:
      /// Default constructor
      dfmsm ();
   
      /// Test if output is supported
      virtual bool supportOutput() const {
         return true; }
   
      /// Open connection
      virtual bool open (const std::string& addr, bool read = true);
      /// Close connection
      virtual void close();
      /// Request a list of avaialble UDNs
      virtual bool requestUDNs (UDNList& udn);
      /// Request information asscociated with a UDN
      virtual bool requestUDNInfo (const UDN& udn, UDNInfo& info);
   
   };

//@}

}

#endif // _LIGO_FDFMSM_H
