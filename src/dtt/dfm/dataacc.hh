/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: dataacc							*/
/*                                                         		*/
/* Module Description: Data access through the DFM			*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 9Jan01   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: dataacc.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_FDATACC_H
#define _LIGO_FDATACC_H


#include "Time.hh"
#include "Interval.hh"
#include "udn.hh"
#include "framefast/frameio.hh"
#include "fname.hh"
#include <string>
#include <map>
#include <set>


namespace fantom {
   class fmsgqueue;
}

namespace dfm {

   class dfmaccess;


/** @name Data Access API 
    This header defines support methods for accessing data through the
    data flow manager, the NDS, shared memory partitions and direct 
    file.
   
    @memo Data Access API
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{


   class dfmapi;

   /// Default LARS port
   const int kLARSPORT = 8075;
   /// Default NDS port
   const int kNDSPORT = 8088;
   /// Default NDS2 port
   const int kSENDSPORT = 31200;


/** Data server name
    
    @memo Data server name.
 ************************************************************************/
   class dataservername {
   public:
      /// Constructor
      explicit dataservername (const char* name = 0) 
      : fName (name ? name : "") {
      }
      /// Constructor
      explicit dataservername (const std::string& name) 
      : fName (name) {
      }
      /// Constructor
      dataservername (dataservicetype type, const std::string& addr);
      /// Name as a const char
      operator const char* () const {
         return fName.c_str(); }
      /// Name as a string
      std::string get() const {
         return fName; }
      /// Get type
      dataservicetype getType() const;
      /// Get address
      std::string getAddr() const;
   
      /// compare smaller
      bool operator< (const dataservername& name) const;
      /// compare equal
      bool operator== (const dataservername& name) const;   
   protected:
      /// Name of UDN
      std::string	fName;
   };


/** Descriptor of a data server
    
    @memo Data server.
 ************************************************************************/
   class dataserver {
   public:
      /// Function prototype for data callback
      typedef bool (*callback) (void* priv, 
                        framefast::frame_storage_ptr& data);
   
      /// Default constructor
      dataserver ();
      /// Constructor
      dataserver (dataservicetype type, const std::string& addr);
      /// Destructor
      virtual ~dataserver ();
   
      /// get the server type
      dataservicetype getType() const {
         return fType; }
      /// get address
      std::string getAddr () const {
         return fAddr; }
      /// get full server name
      dataservername getName() const {
         return dataservername (fType, fAddr); }
   
      /// gets the info associated with a UDN
      UDNInfo* get (const UDN& udn) const;
      /// insert a new UDN (returns a pointer to the info structure)
      UDNInfo* insert (const UDN& udn);
      /// insert a new UDN with info 
      UDNInfo* insert (const UDN& udn, const UDNInfo& info);
      /// Deletes a UDN form the list
      void erase (const UDN& udn);
      /// Deletes all UDNs
      void clear();
      /// Number of UDNs
      int size() const;
      /// Empty?
      bool empty() const;
      /// begin UDN
      UDNiter begin() {
         return fUDN.begin(); }
      /// end UDN
      UDNiter end() {
         return fUDN.end(); }
      /// get UDN(s)
      UDNList& getUDN () {
         return fUDN; }
      /// get UDN(s)
      const UDNList& getUDN () const {
         return fUDN; }
   
      /// Checks username/password; uses last/default login if no arguments
      virtual bool login (const UDN& udn,
                        const char* uname = 0, const char* pword = 0);
      /// Update the list of UDNs
      virtual bool updateUDNs (bool force = false);
      /// Lookup a UDN
      virtual bool lookupUDN (const UDN& udn, bool force = false);
      /// Lookup all UDNs
      virtual bool lookupUDNs (bool force = false);
      /// Is Server supporting multi UDN selection?
      virtual bool supportMultiUDN() const;
      /// Is Server supporting staging?
      virtual bool supportStaging() const;
      /// input support?
      virtual bool supportInput() const;
      /// output support?
      virtual bool supportOutput() const;
   
   protected:
      /// Server type
      dataservicetype	fType;
      /// Server address 
      std::string	fAddr;
      /// List of UDNs and associated information
      UDNList		fUDN;
      /// UDN update flag
      bool		fUpdate;
   };


/** List of data server names (unordered).
    
    @memo List of server names.
 ************************************************************************/
   typedef std::vector <dataservername> servernamelist;
   typedef servernamelist::iterator servernameiter;
   typedef servernamelist::const_iterator const_servernameiter;

/** List of data servers (map of server name and data server).
    
    @memo List of servers.
 ************************************************************************/
   typedef std::map <dataservername, dataserver> serverlist;
   typedef serverlist::iterator serveriter;
   typedef serverlist::const_iterator const_serveriter;


/** List entry of selected data servers.
    
    @memo List entry of selected data server.
 ************************************************************************/
   class selserverentry {
   public:
      /// Channel list
      typedef fantom::channellist channellist;
      /// Channel iterator
      typedef fantom::chniter chniter;
      /// Const channel iterator
      typedef fantom::const_chniter const_chniter;
   
      /// Constructor
      explicit selserverentry(dataservername name = dataservername(), 
                        const UDNList& udns = UDNList());
      /// Equality operator
      bool operator== (const selserverentry& sel) const {
         return fName == sel.fName; }
      /// Equality operator
      bool operator== (const dataservername& name) const {
         return fName == name; }
      /// Inequality operator
      bool operator!= (const selserverentry& sel) const {
         return !(fName == sel.fName); }
      /// Set name
      void setName (const dataservername& name) {
         fName = name; }
      /// Get UDNs
      const dataservername& getName () const {
         return fName; }
      /// Set UDN(s)
      void setUDN (const UDNList& udn) {
         fUDN = udn; }
      /// get UDN(s)
      UDNList& getUDN () {
         return fUDN; }
      /// get UDN(s)
      const UDNList& getUDN () const {
         return fUDN; }
      /// Set channels (global)
      bool setChannels (const char* chns);
      /// Set channels (global)
      bool setChannels (const channellist& chns) {
         fChannels = chns; 
         return true; }
      /// Get channels (global)
      std::string getChannels() const;
      /// Get channels (global)
      const channellist& channels() const {
         return fChannels; }
      /// Update channels (sets the UDN specific channels form global)
      bool updateChannels();
      /// Set format
      void selectFormat (int len, int num, int compr, int vers) {
         fFLength = len; fFNum = num; fFCompr = compr; fFVers = vers; }
      /// Set format
      void selectFormat (const char* format);
      /// Get format
      bool selectedFormat (int& len, int& num, int& compr, int& vers) const {
         len = fFLength; num = fFNum; compr = fFCompr; vers = fFVers;
         return true; }
      /// get the format string
      std::string format() const;
      /// Set ID number
      void setID (int id) {
         fId = id; }
      /// Get ID number
      int getID() const {
         return fId; }
   
   protected:
      /// Set channels (UND specific)
      bool setChannels (const UDN& udn, const char* chns);
      /// Set channels (UND specific) (must be sorted!)
      bool setChannels (const UDN& udn, const channellist& chns);
      /// Get channels (UND specific)
      std::string getChannels (const UDN& udn) const;
      /// Get channels (UND specific)
      const channellist* channels (const UDN& udn) const;
   
      /// server name
      dataservername	fName;
      /// selected UDN(s) and channels
      UDNList 		fUDN;
      /// Selected channels
      channellist	fChannels;
      /// Frame type
      fantom::frametype	fFType;
      /// Frame length
      int		fFLength;
      /// Frame number per file
      int		fFNum;
      /// Compression type
      int		fFCompr;
      /// Frame version
      int		fFVers;
      /// ID number
      int		fId;
   };

/** List of selected data servers (unordered).
    
    @memo List of selected data servers.
 ************************************************************************/
   typedef std::vector <selserverentry> selserverlist;
   typedef selserverlist::iterator selserveriter;
   typedef selserverlist::const_iterator const_selserveriter;


/** Data server selection.
    
    @memo Data server selection.
 ************************************************************************/
   class selservers {
   public:
      /// Channel list
      typedef fantom::channellist channellist;
   
      /// Constructor
      selservers();
      /// Copy constructor
      selservers (const selservers& ss);
      /// Assignment operator
      selservers& operator= (const selservers& ss);
   
      /// add a selected server
      bool add (dataservername name = dataservername(), 
               const UDNList& udns = UDNList());
      /// add a selected server
      bool add (const selserverentry& sel);
      /// erase a selected server
      void erase (selserveriter pos);
      /// clear all selected servers
      void clear();
      /// begin selected server
      selserveriter begin() {
         return fActiveM.begin(); }
      /// end selected server
      selserveriter end() {
         return fActiveM.end(); }
      /// begin selected server
      const_selserveriter begin() const {
         return fActiveM.begin(); }
      /// end selected server
      const_selserveriter end() const {
         return fActiveM.end(); }
   
      /// multiple servers?
      bool isMultiple () const {
         return fMulServers; }
      /// set multiple servers
      void setMultiple (bool set = true) {
         fMulServers = set; }
   
      /// Select server
      bool selectServer (const std::string& sname,
                        const serverlist* servers = 0);
      /// Get selected server
      std::string selectedServer ();
      /// Selected current multi server
      void selectMServer (selserveriter sel) {
         fSelServer = sel; }
      /// Get selected multi server
      selserveriter selectedMServer() const {
         return fSelServer; }
   
      /// get selected server entry 
      selserverentry* selectedEntry();
      const selserverentry* selectedEntry() const;
      /// set selected server entry (single selection)
      void selectS (const selserverentry& entry) {
         fActiveS = entry; }
      /// get selected server entry (single selection)
      const selserverentry& selectedS() const {
         return fActiveS; }
      /// get selected server list (multiple selection
      const selserverlist& selectedM() const {
         return fActiveM; }
      /// set selected server entry (multiple selection)
      void selectM (const selserverlist& list) {
         fActiveM = list; }
   
      /// Set selected UDN(s)
      bool selectUDN (const UDNList& udn);
      /// Get selected UDN(s)
      bool selectedUDN (UDNList& udn) const;
      /// Set select channels
      bool selectChannels (const char* chns);
      /// Get selected channels
      std::string selectedChannels () const;
   
      /// Select time and duration
      bool selectTime (const Time& start, const Interval& duration,
                      const serverlist* servers = 0);
      /// Get selected start time
      Time selectedTime () const {
         return fStart; }
      /// Get selected stop time
      Time selectedStop () const {
         return fStart + fDuration; }
      /// Get selected duration
      Interval selectedDuration () const {
         return fDuration; }
      /// Select staging time
      bool selectStaging (const Interval& keep);
      /// Get selected staging time
      Interval selectedStaging () const {
         return fStagingKeep; }
      UDNInfo::UDNType selectedDataType() const {
         return fantom::FF; }
   
   protected:
      /// Multiple servers ?
      bool		fMulServers;
      /// Active server (if single)
      selserverentry	fActiveS;
      /// Active server list (if multiple)
      selserverlist	fActiveM;
      /// Selected server position (in multiple list)
      selserveriter	fSelServer;
      /// Selected start time
      Time		fStart;
      /// Selected duration
      Interval		fDuration;
      /// Staging hint for how long to keep the data
      Interval		fStagingKeep;
   };


/** Data access class
    
    @memo Data access class
 ************************************************************************/
   class dataaccess {
   public:
      /// Supported server types
      typedef std::set <dataservicetype> serversupp;
      /// Support for all servers
      static const serversupp kSuppAll;
   
      /// Default constructor
      dataaccess ();
      /// Constructor
      explicit dataaccess (serversupp supp);
      /// Destructor
      virtual ~dataaccess();
   
      /// Add/remove support for access server
      bool support (dataservicetype supp, bool add = true);
      /// Set support for access server
      bool support (const serversupp& supp);
      /// Is access server supported?
      bool isSupp (dataservicetype supp) {
         return fSupport.count (supp) > 0; }
   
      /// Lookup name of access servers
      int lookupServers (dataservicetype type);
      /// Get a access server
      dataserver* get (const std::string& sname) const;
      /// Add a access server
      bool insert (const std::string& sname, const dataserver& ds);
      /// Delete a access server
      void erase (const std::string& sname);
      /// Delete selections; and access servers if all = true
      void clear (bool all = true);
      /// begin access server
      serveriter begin() {
         return fServers.begin(); }
      /// end access server
      serveriter end() {
         return fServers.end(); }
      /// Get server list
      const serverlist& list() const {
         return fServers; }
   
      /// Get data source selection
      const selservers& sel() const {
         return fSourceSel; }
      /// Get data source selection
      selservers& sel()  {
         return fSourceSel; }
      /// Set data source selection
      void setSel (const selservers& sel) {
         fSourceSel = sel; }
      /// Get data destination selection
      const selservers& dest() const {
         return fDestinationSel; }
      /// Get data destination selection
      selservers& dest()  {
         return fDestinationSel; }
      /// Set data destination selection
      void setDest (const selservers& dest) {
         fDestinationSel = dest; }
   
      /// Add server/udn/channel
      bool addServer (const std::string& server, const std::string& udn,
                     const fantom::channellist& chns) {
         return addEntry (false, server, udn, chns); }
      /// Add client/udn/channel
      bool addClient (const std::string& client, const std::string& udn,
                     const fantom::channellist& chns, 
                     const std::string& format) {
         return addEntry (true, client, udn, chns, format); }
      /// Add client/server/udn/channel
      bool addEntry (bool isClient, const std::string& name, 
                    const std::string& udn,
                    const fantom::channellist& chns, 
                    const std::string& format = "");
      /// Get list of available input channels
      bool getInputChannelList (fantom::channellist& inchns) const;
      /// Get list of selected output channels
      bool getOutputChannelList (fantom::channelquerylist& outchns) const;
   
      /// Request data to be staged
      virtual bool staging();
      /// Request data (no processing!)
      virtual bool request ();
      /// Abort request (MT safe)
      virtual void abort();
      /// Process a frame from the input
      virtual Interval process();
      /// force a flush of the output
      virtual void flush();
      /// Returns time of processed input
      virtual Time processTime() const;
      /// Process all frames from the input (complete request)
      virtual bool processAll();
      /// Indicate that a request is done
      virtual void done();
      /// Get input log
      virtual bool inlog (fantom::fmsgqueue& mq);
      /// Get output log
      virtual bool outlog (fantom::fmsgqueue& mq);
   
      /// Return most recent error message
      std::string errormsg() const {
         return fErrMsg; }
      /// Set abort variable
      void setAbort (bool* abort) {
         fAbort = abort; }
   
      /// Clear IO and UDN caches
      static void ClearCache();
   
   protected:
      /// Error message
      mutable std::string	fErrMsg;
      /// Set of supported access servers
      serversupp	fSupport;
      /// List of acess servers
      serverlist	fServers;
      /// Selected input data source(s)
      selservers	fSourceSel;
      /// Selected output data source(s)
      selservers	fDestinationSel;
   
      /// Abort variable
      bool*		fAbort;
      /// dfm API
      dfmaccess*	fDFM;
   };


//@}

}

#endif // _LIGO_FDATACC_H
