/* version $Id: TLGDfmMonitors.cc 6319 2010-09-17 18:06:52Z james.batch@LIGO.ORG $ */
//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGDfmMonitors						        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include <time.h>
#include "TLGDfmMonitors.hh"
#include "dataacc.hh"
#include "TLGEntry.hh"
#include <TGMsgBox.h>
#include <TVirtualX.h>
#include <iostream>


namespace dfm {
   using namespace std;
   using namespace dttgui;


   static const char* const gExeTypes[] = { 
   "All files", "*",
   "All files", "*.exe",
   0, 0 };


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGDfmMonitorDlg                                                     //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGDfmMonitorDlg::TLGDfmMonitorDlg (const TGWindow *p, 
                     const TGWindow *main, const selservers& sel, 
                     MonitorList& monitors, bool& killwhendone, 
                     Bool_t& ret)
   : TLGTransientFrame (p, main, 10, 10), fSelServer (sel),
   fMonitors (&monitors), fKillAfter (&killwhendone), fOk (&ret), 
   fIndex (-1)
   {
      fCurMonitors = monitors;
      // Layout hints
      fL[0] = new TGLayoutHints (kLHintsTop | kLHintsExpandX, 5, 5, 5, 5);
      fL[1] = new TGLayoutHints (kLHintsTop | kLHintsExpandX, 0, 0, 5, 0);
      fL[2] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 0, 0, 5, 0);
      fL[3] = new TGLayoutHints (kLHintsExpandX | kLHintsCenterY, 0, 0, 0, 0);
      fL[4] = new TGLayoutHints (kLHintsTop | kLHintsLeft | kLHintsExpandX,
                           25, 25, 2, 2);
      fL[5] = new TGLayoutHints (kLHintsTop | kLHintsExpandX, 5, 5, 5, 15);
      // Group frames
      fMonGroup = new TGGroupFrame (this, "Monitor programs");
      AddFrame (fMonGroup, fL[0]);
      fSetupGroup = new TGGroupFrame (this, "Setup");
      AddFrame (fSetupGroup, fL[5]);
      // Monitor list
      fSel = new TGListBox (fMonGroup, kDfmMonList);
      fSel->Resize (500, 200);
      fSel->Associate (this);
      fMonGroup->AddFrame (fSel, fL[2]);
      // Argument selection
      fF[0] = new TGHorizontalFrame (fMonGroup, 10, 10);
      fMonGroup->AddFrame (fF[0], fL[1]);
      fLabel[0] = new TGLabel (fF[0], "Args: ");
      fF[0]->AddFrame (fLabel[0], fL[2]);
      fArg = new TLGTextEntry (fF[0], "", kDfmMonArg);
      fArg->Associate (this);
      fArg->SetMaxLength (1024);
      fF[0]->AddFrame (fArg, fL[3]);
      // UDN selection
      fF[1] = new TGHorizontalFrame (fMonGroup, 10, 10);
      fMonGroup->AddFrame (fF[1], fL[1]);
      fLabel[1] = new TGLabel (fF[1], "UDN:  ");
      fF[1]->AddFrame (fLabel[1], fL[2]);
      fUDN = new TGComboBox (fF[1], kDfmMonUDN);
      fUDN->Associate (this);
      fUDN->Resize (80, 23);
      fF[1]->AddFrame (fUDN, fL[3]);
      // Kill after done
      fKill = new TGCheckButton (fSetupGroup, "Kill after completion",
                           kDfmMonKill);
      fKill->Associate (this);
      fSetupGroup->AddFrame (fKill, fL[1]);
      // button group
      fBtnFrame = new TGHorizontalFrame (this, 100, 20);
      AddFrame (fBtnFrame, fL[0]);
      fAddButton = new TGTextButton (fBtnFrame, 
                           new TGHotString ("&Add"), kDfmMonAdd);
      fAddButton->Associate (this);
      fBtnFrame->AddFrame (fAddButton, fL[4]);
      fRemButton = new TGTextButton (fBtnFrame, 
                           new TGHotString ("&Remove"), kDfmMonRem);
      fRemButton->Associate (this);
      fBtnFrame->AddFrame (fRemButton, fL[4]);
      fOkButton = new TGTextButton (fBtnFrame, 
                           new TGHotString ("&Ok"), 1);
      fOkButton->Associate (this);
      fBtnFrame->AddFrame (fOkButton, fL[4]);
      fCancelButton = new TGTextButton (fBtnFrame, 
                           new TGHotString ("&Cancel"), 0);
      fCancelButton->Associate (this);
      fBtnFrame->AddFrame (fCancelButton, fL[4]);
      // Set values
      SetValues (0, true);
      fKill->SetState (*fKillAfter ? kButtonDown : kButtonUp);
   
      // set dialog box title
      SetWindowName ("Monitor Selection");
      SetIconName ("Monitor Selection");
      SetClassHints ("MonSelectionDlg", "MonSelectionDlg");
      // resize & move to center
      MapSubwindows ();
      UInt_t width  = GetDefaultWidth();
      UInt_t height = GetDefaultHeight();
      Resize (width, height);
      Int_t ax;
      Int_t ay;
      if (main) {
         Window_t wdum;
         gVirtualX->TranslateCoordinates (main->GetId(), GetParent()->GetId(),
                              (((TGFrame*)main)->GetWidth() - fWidth) >> 1, 
                              (((TGFrame*)main)->GetHeight() - fHeight) >> 1,
                              ax, ay, wdum);
      }
      else {
         UInt_t root_w, root_h;
         gVirtualX->GetWindowSize (fClient->GetRoot()->GetId(), ax, ay, 
                              root_w, root_h);
         ax = (root_w - fWidth) >> 1;
         ay = (root_h - fHeight) >> 1;
      }
      Move (ax, ay);
      SetWMPosition (ax, ay);
      // make the message box non-resizable
      SetWMSize(width, height);
      SetWMSizeHints(width, height, width, height, 0, 0);
      SetMWMHints(kMWMDecorAll | kMWMDecorResizeH  | kMWMDecorMaximize |
                 kMWMDecorMinimize | kMWMDecorMenu,
                 kMWMFuncAll  | kMWMFuncResize    | kMWMFuncMaximize |
                 kMWMFuncMinimize,
                 kMWMInputModeless);
   
      MapWindow();
      fClient->WaitFor (this);
   }

//______________________________________________________________________________
   TLGDfmMonitorDlg::~TLGDfmMonitorDlg ()
   {
      delete fKill;
      delete fCancelButton;
      delete fOkButton;
      delete fRemButton;
      delete fAddButton;
      delete fBtnFrame;
      delete fLabel[0];
      delete fLabel[1];
      delete fArg;
      delete fUDN;
      delete fSel;
      delete fF[0];
      delete fF[1];
      delete fMonGroup;
      delete fSetupGroup;
      for (int i = 0; i < 6; i++) {
         delete fL[i];
      }
   }

//______________________________________________________________________________
   void TLGDfmMonitorDlg::CloseWindow()
   {
      if (fOk) *fOk = kFALSE;
      DeleteWindow();
   }

//______________________________________________________________________________
   void TLGDfmMonitorDlg::SetValues (int idx, bool rebuild)
   {
      // Check if we need to read old values
      if ((fIndex != idx) && (fIndex >= 0) && 
         (fIndex < (int)fCurMonitors.size())) {
         GetValues ();
      }
      // rebuild monitor list box
      if (rebuild) {
         // first time: build UDN selection
         if (fIndex < 0) {
            // build selection string list
            int n = 0;
            if (fSelServer.isMultiple()) {
               for (const_selserveriter i = fSelServer.begin(); 
                   i != fSelServer.end(); ++i) {
                  if ((i->getName().getType() == st_SM) && 
                     !i->getUDN().empty()) {
                     string udn = (const char*)i->getUDN().begin()->first;
                     //udn.erase (0, 6);
                     fSMs[udn] = n++;
                  }
               }
            }
            else {
               const selserverentry* i = &fSelServer.selectedS();
               if ((i->getName().getType() == st_SM) && 
                  !i->getUDN().empty()) {
                  string udn = (const char*)i->getUDN().begin()->first;
                  //udn.erase (0, 6);
                  fSMs[udn] = n++;
               }
            }
            for (monitor_iter i = fCurMonitors.begin(); 
                i != fCurMonitors.end(); ++i) {
               if (!i->fUDN.empty()) {
                  fSMs[i->fUDN] = n++;
               }
            }
            // build combo list
            fUDN->RemoveEntries (0, 10000);
            for (sm_iter i = fSMs.begin(); i != fSMs.end(); ++i) {
               fUDN->AddEntry (i->first.c_str(), i->second);
            }
            fUDN->MapSubwindows();
         }
         fSel->RemoveEntries (0, 10000);
         int id = 0;
         for (monitor_iter i = fCurMonitors.begin(); 
             i != fCurMonitors.end(); ++i) {
            fSel->AddEntry (i->fName.c_str(), id++);
         }
         fSel->MapSubwindows();
         fSel->Layout();
      }
      // Set new index
      if ((idx >= 0) && (idx < (int)fCurMonitors.size())) {
         fIndex = idx;
         // Select UDN
         sm_iter n = fSMs.end();
         for (sm_iter i = fSMs.begin(); i != fSMs.end(); ++i) {
            if (i->first == fCurMonitors[idx].fUDN) {
               n = i;
               break;
            }
         }
         if (n != fSMs.end()) {
            fUDN->Select (n->second);
         }
         else {
            // clean top entry
            fUDN->SetTopEntry 
               (new TGTextLBEntry (fUDN, new TGString (""), 0), 
               new TGLayoutHints (kLHintsLeft | kLHintsExpandY | 
                                 kLHintsExpandX));
            fUDN->MapSubwindows();
         }
         // Select arguments
         fArg->SetText (fCurMonitors[idx].fArgs.c_str());
      }
   }

//______________________________________________________________________________
   void TLGDfmMonitorDlg::GetValues ()
   {
      if ((fIndex >= 0) && (fIndex < (int)fCurMonitors.size())) {
         TGTextLBEntry* e = 
            (TGTextLBEntry*) fUDN->GetSelectedEntry();
         if (e) {
            fCurMonitors[fIndex].fUDN = e->GetText()->GetString();
         }
         fCurMonitors[fIndex].fArgs = fArg->GetText();
      }
   }

//______________________________________________________________________________
   Bool_t TLGDfmMonitorDlg::AddMon ()
   {
      // file open dialog
      TGFileInfo	info;
      info.fFilename = 0;
      info.fIniDir = 0;
   #if ROOT_VERSION_CODE >= ROOT_VERSION(3,1,6)
      info.fFileTypes = const_cast<const char**>(gExeTypes);
   #else
      info.fFileTypes = const_cast<char**>(gExeTypes);
   #endif
   #if 1
      info.fFileTypeIdx = 0 ; // Point to all files
      {
	 // On return, info.fFilename will be filled in
	 // unless the user cancels the operation.
	 new TLGFileDialog(GetMain(), &info, kFDOpen) ;
      }
      if (!info.fFilename)
   #else
      if (!TLGFileDialog (GetMain(), info, kFDOpen, 0))
   #endif
      {
      #if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
         delete [] info.fFilename;
      #endif
         return kFALSE;
      }
      // new monitor program
      MonitorProgram mon;
      mon.fName = info.fFilename;
      if (!fSMs.empty()) {
         mon.fUDN = fSMs.begin()->first;
         // string part = fSMs.begin()->first;
         // if (!part.empty() && (part[0] == '/')) {
            // part.erase (0, 1);
         // }
         // string::size_type pos = part.find_first_of (" \f\r\n\t\v");
         // if (pos != string::npos) {
            // part.erase (pos);
         // }
         // if (!part.empty()) {
            // mon.fArgs = "-partition " + part;
         // }
      }
      mon.fArgs = "-partition $part";
      fCurMonitors.push_back (mon);
   #if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
      delete [] info.fFilename;
   #endif
      SetValues (fCurMonitors.size() -1, true);
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGDfmMonitorDlg::RemMon (int idx)
   {
      if ((idx < 0) && (idx >= (int)fCurMonitors.size())) {
         return kFALSE;
      }
      // delete from list
      monitor_iter i = fCurMonitors.begin();
      advance (i, idx);
      fCurMonitors.erase (i);
      if (idx >= (int)fCurMonitors.size()) idx = fCurMonitors.size() - 1;
      fIndex = -1;
      SetValues (idx, true);
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGDfmMonitorDlg::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      // Buttons
      if ((GET_MSG (msg) == kC_COMMAND) &&
         (GET_SUBMSG (msg) == kCM_BUTTON)) {
         switch (parm1) {
            // Cancel
            case 0:
               {
                  if (fOk) *fOk = kFALSE;
                  DeleteWindow();
                  break;
               }
            // Ok
            case 1:
               {
                  // get values
                  GetValues();
                  *fKillAfter = (fKill->GetState() == kButtonDown);
                  *fMonitors = fCurMonitors;
                  // set ret value
                  if (fOk) *fOk = kTRUE;
                  DeleteWindow();
                  break;
               }
            // Add
            case kDfmMonAdd:
               {
                  // add a monitor program
                  AddMon();
                  break;
               }
            // Remove
            case kDfmMonRem:
               {
                  // remove a monitor program
                  RemMon (fIndex);
                  break;
               }
         }
      }
      // Listbox
      if ((GET_MSG (msg) == kC_COMMAND) &&
         (GET_SUBMSG (msg) == kCM_LISTBOX)) {
         switch (parm1) {
            // Monitor selection box
            case kDfmMonList:
               {
                  SetValues (parm2);
                  break;
               }
         }
      }
   
      return kTRUE;
   }



}
