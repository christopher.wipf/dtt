/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: TLGDfmSel						*/
/*                                                         		*/
/* Module Description: ROOT Dialogbox for DFM selection			*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 9Jan01   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: dfmdlgroot.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_TLGDFMSEL_H
#define _LIGO_TLGDFMSEL_H


#include "TLGFrame.hh"
#include "dataacc.hh"
#include "fchannel.hh"
#include <TGFrame.h>
#include <TGComboBox.h>
#include <TGLabel.h>
#include <TGTextEntry.h>
#include <TGButton.h>


namespace dttgui {
   class TLGNumericControlBox;
}


namespace dfm {


/** @name Dfm source/destination selection 
    This header defines a ROOT dialogbox for the DFM source and
    destination selection.
   
    @memo Dfm source/destination selection
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{

   const Int_t kDfmMultiServer = 199;
   const Int_t kDfmServer = 200;
   const Int_t kDfmServerSearch = 201;
   const Int_t kDfmUDN = 202;
   const Int_t kDfmUDNMore = 203;
   const Int_t kDfmChannels = 204;
   const Int_t kDfmChnSel = 205;
   const Int_t kDfmKeep = 214;
   const Int_t kDfmStaging = 215;
   const Int_t kDfmFormatLen = 230;
   const Int_t kDfmFormatNum = 231;
   const Int_t kDfmFormatCompr = 232;
   const Int_t kDfmFormatType = 233;


/** Data selection frame.
    Supports both input (source) and output (destination). Supports
    single and multiple server selection, single and multiple UDN 
    selection, channel selection, time selection, format selection
    and staging selection.
    @memo Frame for setting data selection parameters.
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGDfmSelection : public TGGroupFrame, public TGWidget {
   protected:
      /// Source or destination selection
      Bool_t		fSourceSel;
      /// Other window if both source and destination are used
      TLGDfmSelection*	fOther;
      /// Data access object
      dataaccess* 	fDacc;
      /// Currently selected servers/UDNs/channels/times
      selservers	fSel;
      /// Channel selection?
      Bool_t 		fChannelsel;
      /// Time/format selection?
      Bool_t 		fTimeFormatsel;
      /// Staging selection?
      Bool_t 		fStagingsel;
      /// UTC dirty flag
      Bool_t 		fUTCDirty;
   
      /// Numereous layout hints
      TGLayoutHints*	fL[4];
      /// Frames
      TGCompositeFrame*	fF[5];
      /// Labels
      TGLabel*		fLabel[15];
      /// Multi-server selection
      TGComboBox*	fMultiServer;
      /// Server selection
      TGComboBox*	fServer;
      /// Search server button
      TGButton*		fSearchServer;
      /// UDN selection
      TGComboBox*	fUDN;
      /// UDN more button
      TGButton*		fUDNMore;
      /// Channel selection
      TGTextEntry*	fChannels;
      /// Channel selection button
      TGButton*		fChnSel;
      /// Start time selection (GPS sec)
      dttgui::TLGNumericControlBox*	fTimeGPS;
      /// Start time selection (GPS nsec)
      dttgui::TLGNumericControlBox*	fTimeGPSN;
      /// Start time selection (UTC date)
      dttgui::TLGNumericControlBox*	fTimeDate;
      /// Start time selection (UTC time)
      dttgui::TLGNumericControlBox*	fTimeTime;
      /// Time lookup button button
      TGButton*		fTimeLookup;
      /// Time now button
      TGButton*		fTimeNow;
      /// Duration selection
      dttgui::TLGNumericControlBox*	fDuration;
      /// Duration selection
      dttgui::TLGNumericControlBox*	fDurationN;
      /// Frame length
      dttgui::TLGNumericControlBox*	fFLen;
      /// Frame number
      dttgui::TLGNumericControlBox*	fFNum;
      /// Compression type
      TGComboBox*	fCType;
      /// frame type
      TGComboBox*	fFVers;
      /// Keep selection
      dttgui::TLGNumericControlBox*	fKeep;
      /// Staging button
      TGButton*		fStaging;
      /// Wait cursor
      static Cursor_t fWaitCursor;
   
      /// Set start time
      void SetStartTime (const Time& T0);
      /// Update GPS time from UTC
      void UpdateGPS ();
      /// Set duration
      void SetDuration (const Interval& Dt);
      /// Update channel information
      Bool_t UpdateChannels (bool read = true);
      /// Update format information
      Bool_t UpdateFormat (bool read = true);
      /// Set the wait cursor
      void SetWait (bool set = true);
   
   public:
      /** Constructs a new data selection dialog box.
          @memo Constructor.
          @param p Parent window
   	  @param dacc Data access server list
   	  @param sourcesel If true, source selection, else destination
   	  @param group Group frame name (default is source/destination)
   	  @param id Widget ID
   	  @param channelsel Channel selection enabled
   	  @param timeformatsel Time/Format selection enabled
   	  @param stagingsel Staging selection enabled
       ******************************************************************/
      TLGDfmSelection (const TGWindow* p, dataaccess& dacc, 
                      Bool_t sourcesel = kTRUE,
                      const char* group = 0, Int_t id = -1, 
                      Bool_t channelsel = kTRUE,
                      Bool_t timeformatsel = kTRUE, 
                      Bool_t stagingsel = kTRUE);
      /// Destructor
      virtual ~TLGDfmSelection ();
      /// Set selection
      void SetSel (const selservers& sel);
      /// Set selection
      const selservers& GetSel () const {
         return fSel; }
      /// Get data from GUI into dacc
      virtual Bool_t ReadData (bool quite = false);
      /// Couple source and destination windows 
      virtual void Couple (TLGDfmSelection* other = 0) {
         fOther = other; }
      /// Build the selection boxes
      virtual void Build (Int_t level);
   
      /// Select server (single)
      virtual Bool_t SelectServer (const TString& newserver);
      /// Select server (multiple)
      virtual Bool_t SelectServer (Int_t newserver);
      /// Select UDN
      virtual Bool_t SelectUDN (const UDNList& udn);
      /// Select UDN
      virtual Bool_t SelectUDN (const UDN& udn);
   
      /// Server add
      virtual Bool_t AddServer ();
      /// Multiple UDN definition
      virtual Bool_t MultipleUDN ();
      /// Channel select; true if new selection
      virtual Bool_t SelectChannels ();
      /// Time select; true if new selection
      virtual Bool_t SelectTimes ();
      /// Set staging parameters
      virtual Bool_t SetStaging ();
   
      /// Get channel list of all selected UDNs
      virtual Bool_t GetChannelList (fantom::channellist& chnavail);
   
      /// Process messages
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   };


/** DFM parameter dialog box.
    @memo Dialog box for setting DFM parameters.
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGDfmDialog : public dttgui::TLGTransientFrame {
   protected:
      /// Return parameter
      Bool_t*		fOk;
      /// Parameter group
      TLGDfmSelection*	fDfm;
      /// Button frame
      TGCompositeFrame*	fButtonFrame;
      /// Numereous layout hints
      TGLayoutHints*	fL[3];
      /// Ok button
      TGButton*		fOkButton;
      /// Cancel button
      TGButton*		fCancelButton;
   
   public:
      /// Constructor
      TLGDfmDialog (const TGWindow* p, const TGWindow* main,
                   dataaccess& dacc, Bool_t& ret,
                   Bool_t channelsel = kTRUE,
                   Bool_t timesel = kTRUE, Bool_t stagingsel = kTRUE);
      /// Destructor
      virtual ~TLGDfmDialog ();
      /// Close window
      virtual void CloseWindow();
      /// Process messages
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   };

//@}

}


#endif // _LIGO_TLGDFMSEL_H
