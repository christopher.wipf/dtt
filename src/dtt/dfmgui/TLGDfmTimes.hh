/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: TLGDfmTimes						*/
/*                                                         		*/
/* Module Description: ROOT Dialogbox for DFM				*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 9Jan01   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: dfmdlgroot.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_TLGDFMTIMES_H
#define _LIGO_TLGDFMTIMES_H


#include "TLGFrame.hh"
#include "dataacc.hh"
#include <TGFrame.h>
#include <TGLabel.h>
#include <TGTextEntry.h>
#include <TGButton.h>
#include <TGListView.h>


namespace dttgui {
   class TLGNumericControlBox;
}

namespace dfm {


/** @name Dfm times selection 
    This header defines a ROOT dialogbox for the DFM times selection.
   
    @memo Dfm times selection
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{


   const Int_t kDfmTimeGPS = 206;
   const Int_t kDfmTimeGPSN = 207;
   const Int_t kDfmTimeDate = 208;
   const Int_t kDfmTimeTime = 209;
   const Int_t kDfmTimeLookup = 210;
   const Int_t kDfmTimeNow = 211;
   const Int_t kDfmDuration = 212;
   const Int_t kDfmDurationN = 213;
   const Int_t kDfmStopGPS = 216;
   const Int_t kDfmStopGPSN = 217;
   const Int_t kDfmStopDate = 218;
   const Int_t kDfmStopTime = 219;
   const Int_t kDfmStopNow = 220;
   const Int_t kDfmTimeSelType = 221;
   const Int_t kDfmTimeSet = 225;


/** Time selection dialog box.
    @memo Time selection dialog box.
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGDfmTimeSelDlg : public dttgui::TLGTransientFrame {
   protected:
      /// Data server with list of available channels
      dataserver*	fDS;
      /// Currently selected UDNs
      const UDNList*	fUDNret;
      /// Available data (time) segments
      UDNInfo		fDSeg;
      /// Selected start time (return)
      Time*		fT0;
      /// Selected duration (return)
      Interval*		fDt;
      /// Return value
      Bool_t*		fOk;
      /// Start UTC dirty
      Bool_t		fStartUTCDirty;
      /// Stop UTC dirty
      Bool_t		fStopUTCDirty;
   
      /// Layout hinst
      TGLayoutHints*	fL[10];
      /// Group frames: Time list; Time selection
      TGGroupFrame*	fG[2];
      /// frames
      TGCompositeFrame*	fF[8];
      /// Labels
      TGLabel*		fLabel[20];
      /// Time segment list box
      TGListView*	fTimeSeg;
      /// Time segment container
      TGLVContainer*	fTSCon;
      /// Set start time button
      TGButton*		fSetStart;
      /// Set start/stop time button
      TGButton*		fSetBoth;
      /// Set stop time button
      TGButton*		fSetStop;
      /// Time selction type
      TGButton*		fType[3];
      /// Start time selection (GPS sec)
      dttgui::TLGNumericControlBox*	fTimeGPS;
      /// Start time selection (GPS nsec)
      dttgui::TLGNumericControlBox*	fTimeGPSN;
      /// Start time selection (UTC date)
      dttgui::TLGNumericControlBox*	fTimeDate;
      /// Start time selection (UTC time)
      dttgui::TLGNumericControlBox*	fTimeTime;
      /// Time now button
      TGButton*		fTimeNow;
      /// Start time selection (GPS sec)
      dttgui::TLGNumericControlBox*	fStopGPS;
      /// Start time selection (GPS nsec)
      dttgui::TLGNumericControlBox*	fStopGPSN;
      /// Start time selection (UTC date)
      dttgui::TLGNumericControlBox*	fStopDate;
      /// Start time selection (UTC time)
      dttgui::TLGNumericControlBox*	fStopTime;
      /// Time now button
      TGButton*		fStopNow;
      /// Duration selection (sec)
      dttgui::TLGNumericControlBox*	fDuration;
      /// Duration selection (nsec)
      dttgui::TLGNumericControlBox*	fDurationN;
      /// Ok button
      TGButton*		fOkButton;
      /// Cancel button
      TGButton*		fCancelButton;
   
      /// Set available data segments
      void SetDSegments ();
      /// Set start time
      void SetStartTime (const Time& T0);
      /// Set stop time
      void SetStopTime (const Time& T1);
      /// Set duration
      void SetDuration (const Interval& Dt);
      /// Set selection type
      void SetType (int sel);
   
   public:
      /** Constructs a new time selection dialog box.
          @memo Constructor.
          @param p Parent window
   	  @param main Main window
   	  @param ds Data server
   	  @param udnsel Selected list of UDNs
   	  @param start Selected start time (return)
   	  @param duration Selected duration (return)
   	  @param ret True if successful, False if cancel
       ******************************************************************/
      TLGDfmTimeSelDlg (const TGWindow* p, const TGWindow* main,
                       dataserver& ds, const UDNList& udnsel,
                       Time& start, Interval& duration, Bool_t& ret);
      /// Destructor
      virtual ~TLGDfmTimeSelDlg ();
      /// Close window
      virtual void CloseWindow();
      /// Process messages
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, Long_t);
   };


//@}

}


#endif // _LIGO_TLGDFMTIMES_H
