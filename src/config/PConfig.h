#ifndef GDS_PConfig
#define GDS_PConfig

/*************************************************************************
 *                                                                       *
 * PConfig                                                               *
 *                                                                       *
 * Platform/Compiler dependent configuration for GDS                     *
 *                                                                       *
 *************************************************************************/
/*---- archidecture ----------------------------------------------------------*/

#define __GDS__
#define __GDS_ARCH__ "linux"

/*---- machines --------------------------------------------------------------*/

#ifdef __hpux
#   define P__HPUX
#endif

#ifdef _AIX
#   define P__AIX
#endif

#if defined(__alpha) && !defined(__linux)
#   define P__ALPHA
#   ifdef __VMS
#      define P__VMS
#   endif
#endif

#if defined(__sun) && !defined(__linux)
#   ifdef __SVR4
#      define P__SOLARIS
#      define HAVE_CLOCK_GETTIME
#      define HAVE_INT_TYPES
/*#      define __USE_FN_SERVICE*/
#   else
#      define P__SUN
#   endif
#endif

#if defined(__sgi) && !defined(__linux)
#   define P__SGI
#   ifdef IRIX64
#      define P__SGI64
#   endif
#endif

#if defined(__linux)
#endif

#if defined(__linux) && defined(__i386__)
#   define P__LINUX
#   define HAVE_INT_TYPES
#   define USE_SINCOS
#   ifndef __i486__
#      define __i486__       /* turn off if you really want to run on an i386 */
#   endif
#endif

#if defined(__linux) && defined(__ia64__)
#   define P__LINUX
#endif

#if defined(__linux) && defined(__x86_64)
#   define P__LINUX
#   define USE_SINCOS
#endif

#if defined(__linux) && defined(__alpha__)
#   define P__LINUX
#endif

#if defined(__linux) && defined(__sun)
#   define P__LINUX
#endif

#if defined(__linux) && defined(__sgi)
#   define P__LINUX
#endif

#if defined(__linux) && defined(__powerpc__)
#   define P__LINUX
#endif

#if defined(__linux) && defined(__aarch64__)
#   define P__LINUX
#endif

#if defined(__APPLE__)
#   define P__DARWIN
#endif

/*---- compilers -------------------------------------------------------------*/

#ifdef __GNUG__
#   define P__GNU
#if __GNUC__ < 3
#   define __GNU_STDC_OLD   /* not ANSI compatible STL */
#endif
#endif

#ifdef __KCC
#   define P__KCC
#endif

#if defined(__CYGWIN__) && defined(__GNUC__)
#   define P__WIN32
#   define HAVE_INT_TYPES
#endif


#endif /* GDS_PConfig */

